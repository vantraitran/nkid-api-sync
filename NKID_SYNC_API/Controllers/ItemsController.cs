﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NKID_API.Models;
using System.Web.Http.Description;
using NKID_API.Filters;
using Swashbuckle.Swagger.Annotations;

using Newtonsoft.Json.Converters;

namespace NKID_API.Controllers
{
    [JwtAuthentication]
    //[Authorize(Roles = "User")]
   
    public class ItemsController : ApiController
    {
        /// <summary>
        /// Get List of Items
        /// </summary>
        /// <param name="fromDate"></param>
        /// <returns></returns>
        // GET: api/Items
        [Route("api/Items/{FromDate:DateTime}")]
        [ResponseType(typeof(List<Item>))]
        public IHttpActionResult GetList(DateTime fromDate)
        {
            List<Item> cus = new List<Item>();
            try

            {
                string message = "";
                cus = new ItemModels().GetList(fromDate, out message);



                if (string.IsNullOrEmpty(message) == false)
                    return BadRequest(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(cus);
        }
        
    }
}
