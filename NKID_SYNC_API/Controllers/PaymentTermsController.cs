﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NKID_API.Models;
using System.Web.Http.Description;
using NKID_API.Filters;

namespace NKID_API.Controllers
{
    [JwtAuthentication]
    //[Authorize(Roles = "User")]
    public class PaymentTermsController : ApiController
    {
        /// <summary>
        /// Get List of Payment Terms
        /// </summary>
        /// <param name="fromDate"></param>
        /// <returns></returns>
        // GET: api/PaymentTerms
        [ResponseType(typeof(List<PaymentTerms>))]
        public IHttpActionResult GetList()
        {
            List<PaymentTerms> cus = new List<PaymentTerms>();
            try

            {
                string message = "";
                cus = new PaymentTermsModels().GetList( out message);



                if (string.IsNullOrEmpty(message) == false)
                    return BadRequest(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(cus);
        }
    }
}
