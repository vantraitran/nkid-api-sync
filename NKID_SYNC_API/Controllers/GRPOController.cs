﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NKID_API.Models;
using System.Web.Http.Description;
using NKID_API.Filters;
using System.IO;
using Swashbuckle.Swagger.Annotations;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;
using System.Xml;

namespace NKID_API.Controllers
{
    [JwtAuthentication]
    public class GRPOController : ApiController
    {
        /// <summary>
        /// Add GRPO Base On PO to Database.
        /// </summary>
        /// <returns></returns>
        [Route("api/yusen2nkid/inbound/CreateGRPO")]
        [ResponseType(typeof(List<DocOutput>))]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully found the GRPO", typeof(OPDN))]

        public IHttpActionResult Post(OPDN _document)
        {
            List<DocOutput> lsDoc = new List<DocOutput>();

            List<OPDN> listGRPO = ReturnListGRPO(_document);
            var cl = new Dictionary<string, DocOutput>();
            foreach (OPDN document in listGRPO)
            {
                var cus = new DocOutput();

                try
                {


                    DataTools.SAPAccess.CreateObject sap = new DataTools.SAPAccess.CreateObject();
                    var document_log = sap.ListToDataTable<OPDN>(document);
                    var sw = new StringWriter();
                    document_log.TableName = "OPDN";
                    document_log.WriteXml(sw);
                    DataContractSerializer dcs = new DataContractSerializer(typeof(OPDN));

                    var ms = new MemoryStream();
                    var xw = XmlWriter.Create(ms);
                    dcs.WriteObject(xw, document);
                    xw.Flush();
                    xw.Close();
                    ms.Position = 0;
                    System.Xml.Linq.XElement xe = System.Xml.Linq.XElement.Load(ms);


                    string message = "";
                    int oError = 0;
                    GRPOModels so;
                    if (document.DocNumber != null && document.DocNumber != "")
                        document.DocEntry = int.Parse(document.DocNumber.Split('#')[0].ToString());
                    else
                        document.DocEntry = 0;
                    foreach (Detail ol in document.Details)
                    {
                        so = new GRPOModels();
                        Account acc = so.GET_GLACCOUNT_BY_BP(document.CardCode, out message);
                        if (acc != null)
                        {
                            ol.GLAccount = Int32.Parse(acc.GLAccount);
                            ol.CogsAccount = Int32.Parse(acc.CogsAccount);
                        }
                        DIMENSION dms = so.GET_DIMENSION_BY_WAREHOUSE(ol.WhsCode, document.CardCode, ol.ItemCode, out message);

                        if (dms != null)
                        {
                            if (dms.Division != null)
                            {
                                ol.OcrCode = dms.Division;
                                ol.OcrCode2 = dms.Department;
                                ol.OcrCode3 = dms.Team;
                                ol.OcrCode4 = dms.SubTeam;
                                ol.OcrCode5 = dms.Level;
                            }
                           
                        }
                        ol.DocEntry = document.DocEntry;
                        ol.BaseEntry = document.DocEntry;
                        ol.BaseType = "22";

                    }

                    so = new GRPOModels();
                    cus = so.CreateGRPO(document, sw, out oError, out message);
                    if (!cl.ContainsKey(cus.ErrorCode + "-" + cus.ErrorMessage))
                    {
                        lsDoc.Add(cus);
                        cl.Add(cus.ErrorCode + "-" + cus.ErrorMessage, cus);
                    }
                    //if (document.DocEntry != 0)
                    //    cus = so.UpdateSO(document, out oError, out message);
                    //else
                    //    cus = so.CreateSO(document,out oError, out message);

                }
                catch (Exception ex)
                {
                    cus = new DocOutput();
                    cus.ErrorCode = -111;
                    cus.ErrorMessage = ex.Message;
                    lsDoc.Add(cus);
                    //return BadRequest(ex.Message);
                }

            }


            return Ok(lsDoc);
        }
        //Xử lý item trung tren GRPO => khong the base nhieu item GRPO tren 1 line PO
        private List<OPDN> ReturnListGRPO(OPDN document)
        {
            try
            {
                List<OPDN> deli = new List<OPDN>();
                List<Detail> details = new List<Detail>();
                var cl = new Dictionary<string, Detail>();

                OPDN dl;


                while (document.Details.Count > 0)
                {
                    dl = new OPDN();
                    dl.CardCode = document.CardCode;
                    dl.DocDate = document.DocDate;
                    dl.DocDueDate = document.DocDueDate;
                    dl.DocEntry = document.DocEntry;
                    dl.DocNumber = document.DocNumber;
                    dl.DocumentType = document.DocumentType;
                    dl.U_DocNum = document.U_DocNum;
                    dl.Comments = document.Comments;

                    details = new List<Detail>();

                    cl = new Dictionary<string, Detail>();

                    for (int i = document.Details.Count - 1; i >= 0; i--)
                    {
                        if (!cl.ContainsKey(document.Details[i].ItemCode + "-" + document.Details[i].BaseLine.ToString()))
                        {
                            details.Add(document.Details[i]);
                            cl.Add(document.Details[i].ItemCode + "-" + document.Details[i].BaseLine.ToString(), document.Details[i]);
                            document.Details.RemoveAt(i);

                        }
                    }


                    dl.Details = details;
                    deli.Add(dl);
                }

                return deli;
            }
            catch (Exception ex)
            {
                return new List<OPDN>();
            }

        }
    }
}