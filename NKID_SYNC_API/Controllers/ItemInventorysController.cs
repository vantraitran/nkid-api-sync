﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NKID_API.Models;
using System.Web.Http.Description;
using NKID_API.Filters;

namespace NKID_API.Controllers
{
    [JwtAuthentication]
    //[Authorize(Roles = "User")]
    public class ItemInventorysController : ApiController
    {
        /// <summary>
        /// Get List Inventory Items
        /// </summary>
        /// <param name="fromDate"></param>
        /// <returns></returns>
        // GET: api/ItemInventory
        [ResponseType(typeof(List<ItemInventory>))]
        public IHttpActionResult GetList()
        {
            List<ItemInventory> cus = new List<ItemInventory>();
            try

            {
                string message = "";
                cus = new ItemInventoryModels().GetList( out message);



                if (string.IsNullOrEmpty(message) == false)
                    return BadRequest(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(cus);
        }
        
    }
}
