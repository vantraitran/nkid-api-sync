﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NKID_API.Models;
using System.Web.Http.Description;
using NKID_API.Filters;
using System.IO;
using Swashbuckle.Swagger.Annotations;
using Newtonsoft.Json.Converters;
using System.Xml;
using System.Runtime.Serialization;

namespace NKID_API.Controllers
{
    [JwtAuthentication]
    public class InventoryTransferController : ApiController
    {

        /// <summary>
        /// Add Inventory Transfer to Database.
        /// </summary>
        /// <returns></returns>
        [Route("api/yusen2nkid/inventory/CreateInventoryTransfer")]
        [ResponseType(typeof(List<DocOutput>))]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully found the Inventory Transfer", typeof(OWTR))]
       
        public IHttpActionResult Post(OWTR document)
        {
            var cus = new DocOutput();
           
            try
            {
                
                DataTools.SAPAccess.CreateObject sap = new DataTools.SAPAccess.CreateObject();
                var document_log = sap.ListToDataTable<OWTR>(document);
                var sw = new StringWriter();
                document_log.TableName = "OWTR";
                document_log.WriteXml(sw);
                
                DataContractSerializer dcs = new DataContractSerializer(typeof(OWTR));

                var ms = new MemoryStream();
                var xw = XmlWriter.Create(ms);
                dcs.WriteObject(xw, document);
                xw.Flush();
                xw.Close();
                ms.Position = 0;
                System.Xml.Linq.XElement xe = System.Xml.Linq.XElement.Load(ms);

                

                string message = "";
                int oError = 0;
                InventoryTransferModels so;
                if(document.DocNumber!= null && document.DocNumber != "")
                    document.DocEntry = int.Parse( document.DocNumber.Split('#')[0].ToString());
                else
                    document.DocEntry = 0;

                foreach ( TransferDetail ol in document.TransferDetails)
                {
                    so = new InventoryTransferModels();
                    DIMENSION dms = so.GET_DIMENSION_BY_WAREHOUSE(ol.WhsCode, "", ol.ItemCode,out message);
                    if (dms != null)
                    {
                        if (dms.Division != null)
                        {
                            ol.OcrCode = dms.Division;
                            ol.OcrCode2 = dms.Department;
                            ol.OcrCode3 = dms.Team;
                            ol.OcrCode4 = dms.SubTeam;
                            ol.OcrCode5 = dms.Level;
                        }
                        ol.GLAccount = Int32.Parse(dms.GLAccount);
                        ol.CogsAccount = Int32.Parse(dms.CogsAccount);
                        
                    }
                    ol.DocEntry = document.DocEntry;
                }

                so = new InventoryTransferModels();
                cus = so.CreateInventoryTransfer(document, sw, out oError, out message);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(cus);
        }
       
       
    }
}