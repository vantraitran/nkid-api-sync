﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NKID_API.Models;
using System.Web.Http.Description;
using DataTools.Models.Entity;
using NKID_API.Filters;
using Swashbuckle.Swagger.Annotations;
using Newtonsoft.Json.Converters;

namespace NKID_API.Controllers
{
    [JwtAuthentication]
    public class SegmentsController : ApiController
    {
        /// <summary>
        /// Get List Segment
        /// </summary>
        /// <returns></returns>
        // GET: api/Segment
        [ResponseType(typeof(List<Segment>))]
        public IHttpActionResult GetList()
        {
            List<Segment> cus = new List<Segment>();
            try

            {
                string message = "";
                cus = new SegmentModels().GetList(out message);



                if (string.IsNullOrEmpty(message) == false)
                    return BadRequest(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(cus);
        }

    }
}