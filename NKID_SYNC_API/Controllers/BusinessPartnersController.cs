﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NKID_API.Models;
using System.Web.Http.Description;
using DataTools.Models.Entity;
using NKID_API.Filters;
using Swashbuckle.Swagger.Annotations;

using Newtonsoft.Json.Converters;

namespace NKID_API.Controllers
{
    [JwtAuthentication]
    public class BusinessPartnersController : ApiController
    {


        /// <summary>
        /// Add BP to Database.The type of Customer to be retrieved
        /// </summary>
        /// <returns></returns>
        [Route("api/AddBP")]
        [ResponseType(typeof(List<RsOutput>))]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully found the BP", typeof(Customer))]
      
        public IHttpActionResult Post(Customer BP)
        {
         
          
            var cus = new RsOutput();
            try
            {
                DataTools.SAPAccess.CreateObject sap = new DataTools.SAPAccess.CreateObject();
                var BP_Log = sap.ListToDataTable<Customer>(BP);

                string message = "";
                Int32 groupcode = -1;
                CustomerGroupModels cg = new CustomerGroupModels();
                BPModels bpm = new Models.BPModels();
                if (string.IsNullOrEmpty(BP.CardCode))
                {
                    if (string.IsNullOrEmpty(BP.GroupCode))
                    {
                        return BadRequest("(-111) GroupCode is empty or not valid!");
                    }
                    groupcode = cg.GetGroupCodeByKyHieu(BP.GroupCode);
                    bpm = new Models.BPModels();
                    if (string.IsNullOrEmpty(BP.LocationCode))
                    {
                        return BadRequest("(-115) Location is empty!");
                    }
                    else
                    {
                        BP.CardCode = bpm.GetNextCardCode(groupcode, BP.LocationCode.ToUpper()).ToUpper();
                    }
                }
                else
                {
                    bpm = new Models.BPModels();
                    if (!bpm.CheckValidBPCode(BP.CardCode))
                        return BadRequest("(-112) CardCode is empty or not valid!");
                }
                if (string.IsNullOrEmpty(BP.GroupCode))
                {
                    return BadRequest("(-111) GroupCode is empty or not valid!");
                }
                if (string.IsNullOrEmpty(BP.SalesforceID))
                {
                    return BadRequest("(-113) SalesforceID is empty or not valid!");
                }
                if (groupcode==-1)
                    groupcode = cg.GetGroupCodeByKyHieu(BP.GroupCode);

                BP.GroupCode = groupcode.ToString();
                if (string.IsNullOrEmpty(BP.EmpCode))
                {
                    return BadRequest("(-114) EmpCode is empty or not valid!");
                }
               
                SaleEmployeeModels slp = new SaleEmployeeModels();
                Int32 slpcode = slp.GetSlpCodeByCostCenter(BP.EmpCode);
                BP.EmpCode = slpcode.ToString();
                if (string.IsNullOrEmpty(BP.LocationCode))
                {
                    return BadRequest("(-115) Location is empty!");
                }
                else
                {
                    if (BP.LocationCode.Trim().Length>1)
                        return BadRequest("(-115) Location is not valid!");
                    if (!BP.LocationCode.Substring(0,1).ToUpper().Equals("S") && !BP.LocationCode.Substring(0, 1).ToUpper().Equals("H"))
                        return BadRequest("(-115) Location is not valid!");
                }
                if (BP.LocationCode.ToUpper() == "S")
                    BP.Territory = -2;
                else
                    BP.Territory = 1;

                OwnerModels ow = new Models.OwnerModels();
                var owner = ow.GetDefaultOwner(BP.LocationCode.ToUpper());
                BP.OwnerCode  = ow.GetDefaultOwner(BP.LocationCode.ToUpper());

                BPModels so = new BPModels();
                cus = so.CreateBP(BP,BP_Log, out message);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(cus);
        }
    }
}