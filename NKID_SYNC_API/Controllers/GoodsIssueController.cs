﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NKID_API.Models;
using System.Web.Http.Description;
using NKID_API.Filters;
using System.IO;
using Swashbuckle.Swagger.Annotations;
using Newtonsoft.Json.Converters;
using System.Xml;
using System.Runtime.Serialization;

namespace NKID_API.Controllers
{
    [JwtAuthentication]
    public class GoodsIssueController : ApiController
    {

        /// <summary>
        /// Add Goods Issue to Database.
        /// </summary>
        /// <returns></returns>
        [Route("api/yusen2nkid/inventory/CreateGoodsIssue")]
        [ResponseType(typeof(List<DocOutput>))]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully found the Goods Issue", typeof(OIGE))]
       
        public IHttpActionResult Post(OIGE document)
        {
            var cus = new DocOutput();
           
            try
            {
                
                DataTools.SAPAccess.CreateObject sap = new DataTools.SAPAccess.CreateObject();
                var document_log = sap.ListToDataTable<OIGE>(document);
                var sw = new StringWriter();
                document_log.TableName = "OIGE";
                document_log.WriteXml(sw);
                
                DataContractSerializer dcs = new DataContractSerializer(typeof(OIGE));

                var ms = new MemoryStream();
                var xw = XmlWriter.Create(ms);
                dcs.WriteObject(xw, document);
                xw.Flush();
                xw.Close();
                ms.Position = 0;
                System.Xml.Linq.XElement xe = System.Xml.Linq.XElement.Load(ms);

                

                string message = "";
                int oError = 0;
                GoodIssueModels so;
                if(document.DocNumber!= null && document.DocNumber != "")
                    document.DocEntry = int.Parse( document.DocNumber.Split('#')[0].ToString());
                else
                    document.DocEntry = 0;
                
                foreach ( Detail ol in document.Details)
                {
                    so = new GoodIssueModels();
                    Account acc = so.GET_GLACCOUNT_BY_BP(document.Ref2, out message);
                    if (acc != null)
                    {
                        ol.GLAccount = Int32.Parse(acc.GLAccount);
                        //ol.CogsAccount = Int32.Parse(acc.CogsAccount);
                    }
                    DIMENSION dms = so.GET_DIMENSION_BY_WAREHOUSE(ol.WhsCode, document.Ref2, ol.ItemCode,out message);
                    if (dms != null)
                    {
                        if (dms.Division != null)
                        {
                            ol.OcrCode = dms.Division;
                            ol.OcrCode2 = dms.Department;
                            ol.OcrCode3 = dms.Team;
                            ol.OcrCode4 = dms.SubTeam;
                            ol.OcrCode5 = dms.Level;
                        }
                        
                    }
                    ol.DocEntry = document.DocEntry;
                }

                so = new GoodIssueModels();
                cus = so.CreateGoodIssue(document, sw, out oError, out message);

                //if (document.DocEntry != 0)
                //    cus = so.UpdateSO(document, out oError, out message);
                //else
                //    cus = so.CreateSO(document,out oError, out message);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(cus);
        }
       
        ///// <summary>
        ///// Returns list Delivery with Status is Open.
        ///// </summary>
        ///// <returns></returns>
        //// GET: Delivery
        //[Route("api/ListDelivery")]
        //[ResponseType(typeof(List<Delivery>))]
        //public IHttpActionResult GetList()
        //{
        //    List<Delivery> so = new List<Delivery>();
        //    try

        //    {
        //        string message = "";
        //        so = new DeliveryModels().GetList(out message);

        //        if (string.IsNullOrEmpty(message) == false)
        //            return BadRequest(message);
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }

        //    return Ok(so);
        //}

    }
}