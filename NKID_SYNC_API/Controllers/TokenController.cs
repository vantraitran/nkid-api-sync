﻿using System.Net;
using System.Web.Http;
using NKID_API.Models;
namespace NKID_API.Controllers
{
    public class TokenController : ApiController
    {
        // THis is naive endpoint for demo, it should use Basic authentication to provide token or POST request
        [AllowAnonymous]
        public string Get(string username, string password)
        {
            if (CheckUser(username, password))
            {
                return JwtManager.GenerateToken(username,1);
            }
            else
                return "Thông tin tài khoản không đúng!";
        }

        private bool CheckUser(string username, string password)
        {
            UserModels us = new UserModels();
            return us.CheckLogin(username, password);
          
        }
    }
}
