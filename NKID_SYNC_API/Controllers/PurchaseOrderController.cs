﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NKID_API.Models;
using System.Web.Http.Description;
using NKID_API.Filters;
using System.IO;
using Swashbuckle.Swagger.Annotations;
using Newtonsoft.Json.Converters;

namespace NKID_API.Controllers
{
    [JwtAuthentication]
    public class PurchaseOrderController : ApiController
    {

        /// <summary>
        /// Returns list Purchase Order with Status is Open.
        /// </summary>
        /// <returns></returns>
        // GET: Purchase Order
        [Route("api/ListPurchaseOrder")]
        [ResponseType(typeof(List<Document>))]
        public IHttpActionResult GetList()
        {
            List<Document> po = new List<Document>();
            try

            {
                string message = "";
                po = new PurchaseOrderModels().GetList( out message);

                if (string.IsNullOrEmpty(message) == false)
                    return BadRequest(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(po);
        }
        ///// <summary>
        ///// Returns a list of orders have paid enough in the time period.
        ///// </summary>
        ///// <param name="fromDate"></param>
        ///// <param name="toDate"></param>
        ///// <returns></returns>
        //// GET: SaleOrder
        //[Route("api/ListOrderPaid")]
        //[ResponseType(typeof(List<SODone>))]
        //public IHttpActionResult GetList(DateTime fromDate, DateTime toDate)
        //{
        //    List<SODone> cus = new List<SODone>();
        //    try

        //    {
        //        string message = "";
        //        cus = new SaleOrderModels().GetListSODone(fromDate,toDate, out message);

        //        if (string.IsNullOrEmpty(message) == false)
        //            return BadRequest(message);
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }

        //    return Ok(cus);
        //}


        ///// <summary>
        ///// Add Sale Order to Database.The type of Sale Order to be retrieved
        ///// </summary>
        ///// <returns></returns>
        //[Route("api/CreateOrder")]
        //[ResponseType(typeof(List<DocOutput>))]
        //[SwaggerResponse(HttpStatusCode.OK, "Successfully found the Order", typeof(Order))]
       
        //public IHttpActionResult Post(Order document)
        //{
        //    var cus = new DocOutput();
           
        //    try
        //    {


        //        DataTools.SAPAccess.CreateObject sap = new DataTools.SAPAccess.CreateObject();
        //        var document_log = sap.ListToDataTable<Order>(document);
        //        var sw = new StringWriter();
        //        document_log.TableName = "SO";
        //        document_log.WriteXml(sw);

        //        string message = "";
        //        int oError = 0;
                
        //        SaleEmployeeModels slp = new SaleEmployeeModels();
        //        Int32 slpcode = slp.GetSlpCodeByCostCenter(document.EmpCode);
        //        if(slpcode==0)
        //            return BadRequest("(-117) EmpCode is not valid!");
        //        else
        //            document.EmpCode = slpcode.ToString();

        //        BPModels bp = new Models.BPModels();
        //        if (document.DocEntry !=0)
        //            return BadRequest("(-113) DocEntry is not valid!");
        //        if (string.IsNullOrEmpty(document.SalesforceOrderId))
        //            return BadRequest("(-115) SalesforceOrderId is empty!");
        //        if (string.IsNullOrEmpty(document.CardCode))
        //            return BadRequest("(-115) CardCode is empty!");
        //        if (string.IsNullOrEmpty(document.PaymentType))
        //            return BadRequest("(-115) PaymentType is empty!");
        //        if (string.IsNullOrEmpty(document.IssueInvoice))
        //            return BadRequest("(-115) IssueInvoice is empty!");
        //        else
        //        {
        //            if (document.IssueInvoice != "Y" && document.IssueInvoice != "N" )
        //                return BadRequest("(-116) IssueInvoice is not valid!");
        //        }
        //        if (string.IsNullOrEmpty(document.OnlineOrder))
        //            return BadRequest("(-115) OnlineOrder is empty!");
        //        if (string.IsNullOrEmpty(document.EmpCode))
        //            return BadRequest("(-115) EmpCode is empty!");
               

        //        if (document.OnlineOrder != "S" && document.OnlineOrder != "O" && document.OnlineOrder != "T")
        //            return BadRequest("(-116) OnlineOrder is not valid!");

        //        if (string.IsNullOrEmpty(document.CardCode) && ( document.OnlineOrder == "S" || document.OnlineOrder =="O" || document.OnlineOrder =="T"))
        //        {
        //            var cardcode = bp.GetDefault_CardCode(document.Location, document.OnlineOrder);
        //            document.CardCode = cardcode;
        //        }

        //        var vatgroup = "";
        //        var owner = -1;
        //        var whscode = "";
        //        SaleOrderModels so = new SaleOrderModels();
        //        if (!string.IsNullOrEmpty( document.Location))
        //        {
        //            if (document.Location.Trim().Length > 1)
        //                return BadRequest("(-115) Location is not valid!");
        //            if (!document.Location.Substring(0, 1).ToUpper().Equals("S") && !document.Location.Substring(0, 1).ToUpper().Equals("H"))
        //                return BadRequest("(-115) Location is not valid!");


                    
        //            ItemInventoryModels iq = new ItemInventoryModels();
        //            whscode = iq.GetDefault_Warehouse_CRM(document.Location.ToUpper().ToString());


        //            OwnerModels ow = new Models.OwnerModels();
        //            owner = ow.GetDefaultOwner(document.Location.ToUpper().ToString());
        //            document.OwnerCode = owner.ToString();

        //            if (document.Location.ToUpper().ToString() == "S")
        //            {
        //                document.Location = "HCM";
        //                vatgroup = "SVN10";
        //                document.Series = "8";
        //            }

        //            else
        //            {
        //                document.Location = "HN";
        //                vatgroup = "SHN10";
        //                document.Series = "72";
        //            }

        //        }
        //        else
        //            return BadRequest("(-115) Location is empty!");

        //        foreach( OrderLine ol in document.OrderLines)
        //        {
        //            if (document.DocEntry.ToString().ToUpper().Trim().ToString() != ol.DocEntry.ToString().ToUpper().Trim())
        //                return BadRequest("(-117) DocEntry header and DocEntry Line is not match!");
        //            if (string.IsNullOrEmpty(ol.SalesforceOrderLineId))
        //                return BadRequest("(-115) SalesforceOrderLineId is empty!");
        //            //else
        //            //{
        //            //    //if(document.SalesforceOrderId.ToUpper().Trim().ToString() != ol.SalesforceOrderLineId.ToUpper().Trim())
        //            //    //    return BadRequest("(-117) SalesforceOrderId and SalesforceOrderLineId is not match!");
        //            //}

        //            if (string.IsNullOrEmpty(ol.ItemCode))
        //                return BadRequest("(-115) ItemCode is empty!");
                   
        //            if (string.IsNullOrEmpty(ol.Segment))
        //                return BadRequest("(-115) Segment is empty!");
        //        }
        //        document.OrderLines.ToList().ForEach(xx => xx.WhsCode = whscode);

               
        //        document.OrderLines.ToList().ForEach(xx => xx.VatGroup = vatgroup);

        //        cus = so.CreateSO(document, sw, out oError, out message);

        //        //if (document.DocEntry != 0)
        //        //    cus = so.UpdateSO(document, out oError, out message);
        //        //else
        //        //    cus = so.CreateSO(document,out oError, out message);

        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }

        //    return Ok(cus);
        //}
    }
}