﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NKID_API.Models;
using System.Web.Http.Description;
using NKID_API.Filters;

namespace NKID_API.Controllers
{
    [JwtAuthentication]
    //[Authorize(Roles = "User")]
    public class CustomerGroupsController : ApiController
    {
        /// <summary>
        /// Return the list Customer Group
        /// </summary>
        /// <returns></returns>
        
        // GET: api/CustomerGroup
        [ResponseType(typeof(List<CustomerGroup>))]
        public IHttpActionResult GetList()
        {
            List<CustomerGroup> cus = new List<CustomerGroup>();
            try

            {
                string message = "";
                cus = new CustomerGroupModels().GetList( out message);



                if (string.IsNullOrEmpty(message) == false)
                    return BadRequest(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(cus);
        }
    }
}
