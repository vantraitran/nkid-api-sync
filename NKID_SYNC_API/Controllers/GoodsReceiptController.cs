﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NKID_API.Models;
using System.Web.Http.Description;
using NKID_API.Filters;
using System.IO;
using Swashbuckle.Swagger.Annotations;
using Newtonsoft.Json.Converters;
using System.Xml;
using System.Runtime.Serialization;

namespace NKID_API.Controllers
{
    [JwtAuthentication]
    public class GoodsReceiptController : ApiController
    {

        /// <summary>
        /// Add Goods Receipt to Database.
        /// </summary>
        /// <returns></returns>
        [Route("api/yusen2nkid/inventory/CreateGoodsReceipt")]
        [ResponseType(typeof(List<DocOutput>))]
        [SwaggerResponse(HttpStatusCode.OK, "Successfully found the Goods Receipt", typeof(OIGE))]
       
        public IHttpActionResult Post(OIGN document)
        {
            var cus = new DocOutput();
           
            try
            {
                
                DataTools.SAPAccess.CreateObject sap = new DataTools.SAPAccess.CreateObject();
                var document_log = sap.ListToDataTable<OIGN>(document);
                var sw = new StringWriter();
                document_log.TableName = "OIGN";
                document_log.WriteXml(sw);
                
                DataContractSerializer dcs = new DataContractSerializer(typeof(OIGN));

                var ms = new MemoryStream();
                var xw = XmlWriter.Create(ms);
                dcs.WriteObject(xw, document);
                xw.Flush();
                xw.Close();
                ms.Position = 0;
                System.Xml.Linq.XElement xe = System.Xml.Linq.XElement.Load(ms);

                

                string message = "";
                int oError = 0;
                GoodReceiptModels so;
                if(document.DocNumber!= null && document.DocNumber != "")
                    document.DocEntry = int.Parse( document.DocNumber.Split('#')[0].ToString());
                else
                    document.DocEntry = 0;

                foreach ( Detail ol in document.Details)
                {
                    so = new GoodReceiptModels();
                    Account acc = so.GET_GLACCOUNT_BY_BP(document.Ref2, out message);
                    if (acc != null)
                    {
                        ol.GLAccount = Int32.Parse(acc.GLAccount);
                        //ol.CogsAccount = Int32.Parse(acc.CogsAccount);
                    }
                    DIMENSION dms = so.GET_DIMENSION_BY_WAREHOUSE(ol.WhsCode, document.Ref2, ol.ItemCode,out message);
                    if(dms != null )
                    {
                        if (dms.Division != null)
                        {
                            ol.OcrCode = dms.Division;
                            ol.OcrCode2 = dms.Department;
                            ol.OcrCode3 = dms.Team;
                            ol.OcrCode4 = dms.SubTeam;
                            ol.OcrCode5 = dms.Level;
                        }
                       
                       
                    }
                    ol.DocEntry = document.DocEntry;

                }

                so = new GoodReceiptModels();
                cus = so.CreateGoodReceipt(document, sw, out oError, out message);

                
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(cus);
        }
       
        

    }
}