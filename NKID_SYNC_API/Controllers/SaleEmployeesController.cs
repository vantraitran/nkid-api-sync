﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NKID_API.Models;
using System.Web.Http.Description;
using NKID_API.Filters;

namespace NKID_API.Controllers
{
    [JwtAuthentication]
    //[Authorize(Roles = "User")]
    public class SaleEmployeesController : ApiController
    {
        /// <summary>
        /// Get List of Sale Employee
        /// </summary>
        /// <param name="fromDate"></param>
        /// <returns></returns>
        // GET: api/CustomerGroup
        [ResponseType(typeof(List<SaleEmployee>))]
        public IHttpActionResult GetList()
        {
            List<SaleEmployee> cus = new List<SaleEmployee>();
            try

            {
                string message = "";
                cus = new SaleEmployeeModels().GetList( out message);



                if (string.IsNullOrEmpty(message) == false)
                    return BadRequest(message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(cus);
        }
    }
}
