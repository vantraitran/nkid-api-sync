CREATE   FUNCTION uf_FSM_AutoCreateBP -- SELECT dbo.uf_FSM_AutoCreateBP(109,'H')
(
		@GroupCode INT,
		@Location varchar(1)
)
RETURNS nvarchar(11)
AS
BEGIN
	Declare @NewCode nvarchar(11)
	IF @GroupCode=115 --SAGIANG
	BEGIN
		if exists(select top 1 Cardcode from ocrd b with(nolock) WHERE CardType='C' AND  b.U_Mavung = @Location AND GroupCode=115	AND LEN(CardCode)>=7)
		begin
			SET @NewCode = (select 'WSG'+isnull(@Location,'')+ RIGHT('000' + CONVERT(varchar(3), CONVERT(int, ISNULL(MAX(right(Cardcode,3)),0)) + 1),3) 
											from OCRD T1 with(nolock)
											where   CardType='C' AND T1.U_Mavung = @Location AND GroupCode=115	AND LEN(CardCode)>=7
											group by isnull(T1.U_Mavung,'') 						
			)
		END
		ELSE
			set @NewCode = 'WSG' + isnull(@Location,'') + '001'
			
	END
	ELSE
	BEGIN
			if exists(select top 1 Cardcode from ocrd b with(nolock) WHERE  CardType='C' AND  b.U_Mavung = @Location AND GroupCode <> 115 AND LEN(CardCode)=6 AND GroupCode IN(109,110,111,112,113,114,116,117))
			begin
				SET @NewCode = (select 'W'+isnull(@Location,'')+ RIGHT('0000' + CONVERT(varchar(4), CONVERT(int, ISNULL(MAX(right(Cardcode,4)),0)) + 1),4) 
											from OCRD T1 with(nolock)
											where  T1.U_Mavung = @Location 
											AND GroupCode <> 115	AND CardType='C'
											AND GroupCode IN(109,110,111,112,113,114,116,117)
											AND LEN(CardCode)=6
												AND t1.CardCode NOT IN('WPOH17','WPOS17','WPHHAILI','WPSHAILI','WPH000','WPS000')
											group by isnull(T1.U_Mavung,'') 						
				)
			END
			ELSE
				set @NewCode = 'W' + isnull(@Location,'') + '0001'
		END
		Return @NewCode
END