CREATE PROC ICC_US_API_GET_SLPCODE
(
	@MACP VARCHAR(18)
)
AS
SELECT TOP 1 SlpCode FROM OSLP T WHERE ISNULL(T.Fax,'')=@MACP