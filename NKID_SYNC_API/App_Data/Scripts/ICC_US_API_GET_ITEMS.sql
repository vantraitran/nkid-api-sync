CREATE PROC ICC_US_API_GET_ITEMS
	@FromDate date
AS
	SELECT ItemCode, ItemName, SalUnitMsr, InvntryUom, BuyUnitMsr
	FROM OITM
	WHERE UpdateDate>=@FromDate
