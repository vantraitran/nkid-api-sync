CREATE PROC ICC_US_API_GETBPBYKEY(@CardCode NVARCHAR(20))--ICC_US_API_GETBPBYKEY 'WSGS061'
AS
SELECT 
	T1.U_SalesforceID AS SalesforceID, 
	T1.CardCode,
	T1.CardName,
	T1.LicTradNum TaxCode,
	T1.CardFName CardTaxName,
	T1.U_HDaddress AS TaxAdress,
	cast(T1.GroupCode AS NVARCHAR(20)) AS GroupCode,
	T1.Cellular AS Phone,
	T1.U_Mavung AS LocationCode,
	cast(T2.Fax  AS NVARCHAR(20)) AS EmpCode
FROM OCRD T1 LEFT JOIN OSLP T2 ON T1.SlpCode=T2.SlpCode
WHERE T1.CardCode=@CardCode
