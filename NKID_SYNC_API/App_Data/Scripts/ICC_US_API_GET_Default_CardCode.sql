CREATE PROC ICC_US_API_GET_Default_CardCode --ICC_US_API_GET_Default_CardCode 'S','S'
(
	@Location VARCHAR(10),
	@OnlineOrder VARCHAR(10)
)
AS
BEGIN
	
	IF @OnlineOrder='O' --ONLINE
	BEGIN
		IF @Location='S' --TPHCM
			SELECT 'WPOS17'
		ELSE
			SELECT 'WPOH17'
	END
	IF @OnlineOrder='S' --Directsale
	BEGIN
		IF @Location='S'--TPHCM
			SELECT 'WPSHAILI'
		ELSE
			SELECT 'WPHHAILI'
	END
	IF @OnlineOrder='T' --Other
	BEGIN
		IF @Location='S'--TPHCM
			SELECT 'WPS000'
		ELSE
			SELECT 'WPH000'
	END
END