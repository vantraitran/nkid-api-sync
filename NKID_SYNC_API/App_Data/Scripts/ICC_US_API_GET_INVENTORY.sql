CREATE PROC ICC_US_API_GETINVENTORY
AS
select t1.ItemCode,t1.WhsCode,T0.WhsName,t1.OnHand  
from owhs t0 with(nolock) 
	join oitw t1 with(nolock) ON t0.WhsCode=t1.WhsCode
WHERE isnull(t0.U_WPCRM,'N')='Y' 
