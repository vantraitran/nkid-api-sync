CREATE PROC ICC_US_API_GET_SOLines
	@FromDate date
AS
	SELECT X2.DocEntry,x2.LineNum, ItemCode, Dscription 'ItemName',X2.WhsCode,X2.UomCode, Quantity, PriceBefDi, x2.DiscPrcnt, x2.VatPrcnt, x2.VatSum ,X2.OcrCode,X2.OcrCode2,X2.OcrCode3,X2.OcrCode4,X2.OcrCode5, x2.LineTotal
	FROM ORDR x1 with(nolock)
	JOIN RDR1 x2 with(nolock) on x1.DocEntry=x2.DocEntry
	WHERE x1.DocDate>=@FromDate