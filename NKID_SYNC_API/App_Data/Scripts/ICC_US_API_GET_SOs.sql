create   PROC ICC_US_API_GET_SOs
	@FromDate date
AS
	SELECT DocEntry, DocDate, DocTotal, DocCur, Comments, Canceled, x1.SlpCode, x2.SlpName, x1.CardCode,x1.CardName,x1.DocDueDate,X1.U_Nhomhang
	FROM ORDR x1 with (nolock)
	JOIN OSLP x2 with (nolock) on x1.SlpCode=x2.SlpCode
	WHERE DocDate>=@FromDate
	ORDER BY DocEntry