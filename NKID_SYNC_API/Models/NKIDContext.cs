﻿using NKID_API.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NKID_API.Models
{
    public class NKIDContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public NKIDContext() : base("name=NKIDContext")
        {

            string[] cnif = new CryptoEngine().Decrypt(System.Configuration.ConfigurationManager.AppSettings["info"]).Split('|');

            var server = cnif[0];
            var licenseServer = cnif[1];
            var serverType = cnif[2];
            var dbName = cnif[3];
            var dbUser = cnif[4];
            var dbPass = cnif[5];
            
            string conStr = "Data Source=" + server + ";Initial Catalog=" + dbName + ";User Id=" + dbUser + "; Password=" + dbPass;
            this.Database.Connection.ConnectionString = conStr;
            if (this.Database.Connection.State == System.Data.ConnectionState.Closed)
                this.Database.Connection.Open();
        }

        //public virtual DbSet<ODLN> DELIVERY { get; set; }
        //public virtual DbSet<Detail> DELIVERY_LINES { get; set; }
    }
}