﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataTools;
using DataTools.Models.Entity;

namespace NKID_API.Models
{
    public class BPModels
    {
        private HLCRMContext db = new HLCRMContext();
        public RsOutput CreateBP(DataTools.Models.Entity.Customer BP, System.Data.DataTable  BP_Log, out string message)
        {
            message = "";
            RsOutput entity = new RsOutput();
            try
            {
                if (BP != null)
                {
                    
                    

                    DataTools.SAPAccess.CreateObject sap = new DataTools.SAPAccess.CreateObject();

                    if (sap.CreateBussinessPartners(BP,BP_Log, out message))
                    {
                        entity.ErrorCode = 0;
                        entity.ErrorMessage = "";
                        entity.SAP_ID = BP.CardCode;
                        entity.SalesForce_ID = BP.SalesforceID;
                    }
                    else
                    {
                        entity.ErrorCode = 1003;
                        entity.ErrorMessage = message;
                    }
                   


                }
                else
                {

                    entity.ErrorCode = 10002;
                    entity.ErrorMessage = "Không tìm thấy dữ liệu.";
                }
            }
            catch (Exception ex)
            {
                entity.ErrorCode = 10001;
                entity.ErrorMessage = ex.Message;
            }

            return entity;

        }

        public List<Document> GetList(DateTime fromDate, out string message)
        {
            message = "";
            List<Document> listEntity = new List<Document>();
            try
            {
                string sql = string.Format("[ICC_US_API_GET_SOs] @FromDate='{0}'", fromDate.ToString("yyyy-MM-dd"));
                listEntity = db.Database.SqlQuery<Document>(sql).ToList();

                sql = string.Format("[ICC_US_API_GET_SOLines] @FromDate='{0}'", fromDate.ToString("yyyy-MM-dd"));
                var listEntityLines = db.Database.SqlQuery<DocumentLine>(sql).ToList();

                if (listEntity != null && listEntityLines != null)
                {
                    foreach (var item in listEntity)
                    {
                        item.DocumentLines = listEntityLines.Where(o => o.DocEntry == item.DocEntry).ToList();
                    }
                }

                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }
        public string GetNextCardCode(Int32 GroupCode,string Location)
        {
            
            try
            {
                string sql = string.Format("[ICC_US_API_GET_NEW_BPCODE] @GroupCode={0},@Location='{1}'", GroupCode,Location);
                return db.Database.SqlQuery<string>(sql).FirstOrDefault();

                
            }
            catch (Exception ex)
            {
               
            }
            return "";

        }

        public string GetDefault_CardCode(string Location, string OnlineOrder)
        {

            try
            {
                string sql = string.Format("[ICC_US_API_GET_Default_CardCode] @Location={0},@OnlineOrder='{1}'", Location, OnlineOrder);
                return db.Database.SqlQuery<string>(sql).FirstOrDefault();


            }
            catch (Exception ex)
            {

            }
            return "";

        }

        public bool CheckValidBPCode(string BPCode)
        {

            try
            {
                string sql = string.Format("[ICC_US_API_CHECK_BPCODE] @CardCode={0}", BPCode);
                if (db.Database.SqlQuery<string>(sql).FirstOrDefault() == "1")
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
           

        }

        public Customer GetByKey(string CardCode)
        {
          
            try
            {
                string sql = string.Format("[ICC_US_API_GETBPBYKEY] @CardCode='{0}'", CardCode);
                return db.Database.SqlQuery<Customer>(sql).FirstOrDefault();
                
            }
            catch (Exception ex)
            {
                return new Customer();
            }
            

        }
    }
}