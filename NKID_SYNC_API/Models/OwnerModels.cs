﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NKID_API.Models
{
    public class OwnerModels
    {
        private HLCRMContext db = new HLCRMContext();
        
        public Int32 GetDefaultOwner(string Location)
        {

            try
            {

                string sql = string.Format("[ICC_US_API_GETDEFAULT_OWNER] @Location='{0}'", Location);
                return db.Database.SqlQuery<Int32>(sql).ToList().FirstOrDefault();

            }
            catch (Exception ex)
            {

            }
            return -1;

        }

    }
}