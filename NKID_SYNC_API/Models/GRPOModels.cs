﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataTools;
using System.IO;

namespace NKID_API.Models
{
    public class GRPOModels
    {
        private NKIDContext db = new NKIDContext();
        public DocOutput CreateGRPO(OPDN document, StringWriter document_log, out int oError, out string message)
        {
            message = "";
            oError = 0;
            DocOutput entity = new DocOutput();
            try
            {
                if (document != null)
                {
                    DataTools.SAPAccess.CreateObject sap = new DataTools.SAPAccess.CreateObject();
                    var dtHeader = sap.ListToDataTable<OPDN>(document);
                    var dtLines = sap.ListToDataTable<Detail>(document.Details);

                  

                    string refCode = "";
                    string docnum = "";
                    if (sap.CreateDocument_Multiple("GRPO",dtHeader, document_log, new List<System.Data.DataTable>() { dtLines }, SAPbobsCOM.BoObjectTypes.oPurchaseDeliveryNotes, false, out refCode, out docnum, out oError, out message,22))
                    {
                        entity.ErrorCode = 0;
                        entity.ErrorMessage = "";
                        entity.RefCode =Convert.ToInt32( refCode);
                        entity.DocNum = docnum;
                       
                    }
                    else
                    {
                        entity.ErrorCode = oError;
                        entity.ErrorMessage = message;
                    }


                }
                else
                {

                    entity.ErrorCode = 10002;
                    entity.ErrorMessage = "Không tìm thấy dữ liệu.";
                }
            }
            catch (Exception ex)
            {
                entity.ErrorCode = 10001;
                entity.ErrorMessage = ex.Message;
            }

            return entity;

        }
        public Account GET_GLACCOUNT_BY_BP(string CardCode, out string message)
        {
            message = "";
            Account listEntity = new Account();
            try
            {
                string sql = string.Format("F_SP_GET_GL_COGS_ACCOUNT_BY_BUSINESSPARTNER @CardCode='{0}'", CardCode);
                listEntity = db.Database.SqlQuery<Account>(sql).FirstOrDefault();

                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }
        public DIMENSION GET_DIMENSION_BY_WAREHOUSE(string WhsCode, string CardCode, string ItemCode, out string message)
        {
            message = "";
            DIMENSION listEntity = new DIMENSION();
            try
            {
                string sql = string.Format("F_SP_GET_DIMENSION_BY_WAREHOUSE @WhsCode='{0}',@CardCode='{1}',@ItemCode='{2}'", WhsCode,CardCode ,ItemCode);
                listEntity = db.Database.SqlQuery<DIMENSION>(sql).FirstOrDefault();

                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }
       
    }
}