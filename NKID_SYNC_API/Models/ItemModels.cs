﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NKID_API.Models
{
    public class ItemModels
    {
        private HLCRMContext db = new HLCRMContext();
        public List<Item> GetList(DateTime fromDate, out string message)
        {
            message = "";
            List<Item> listEntity = new List<Item>();
            try
            {
                string sql = string.Format("[ICC_US_API_GET_ITEMS] @FromDate='{0}'", fromDate.ToString("yyyy-MM-dd HH:mm:ss"));
                listEntity = db.Database.SqlQuery<Item>(sql).ToList();

                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }
      
    }
}