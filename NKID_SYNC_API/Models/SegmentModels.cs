﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataTools;
using DataTools.Models.Entity;

namespace NKID_API.Models
{
    public class SegmentModels
    {
        private HLCRMContext db = new HLCRMContext();
     
        public List<Segment> GetList(out string message)
        {
            message = "";
            List<Segment> listEntity = new List<Segment>();
            try
            {
                string sql = string.Format("[ICC_US_API_GETLIST_SEGMENT]");
                listEntity = db.Database.SqlQuery<Segment>(sql).ToList();


                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }
       
    }
}