﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataTools;
using System.IO;

namespace NKID_API.Models
{
    public class SaleOrderModels
    {
        private NKIDContext db = new NKIDContext();
        public DocOutput CreateSO(Order document, StringWriter document_log, out int oError, out string message)
        {
            message = "";
            oError = 0;
            DocOutput entity = new DocOutput();
            try
            {
                if (document != null)
                {
                    DataTools.SAPAccess.CreateObject sap = new DataTools.SAPAccess.CreateObject();
                    var dtHeader = sap.ListToDataTable<Order>(document);
                    var dtLines = sap.ListToDataTable<OrderLine>(document.OrderLines);

                  

                    string refCode = "";
                    string docnum = "";
                    if (sap.CreateDocument_Multiple("SO",dtHeader, document_log, new List<System.Data.DataTable>() { dtLines }, SAPbobsCOM.BoObjectTypes.oOrders, false, out refCode, out docnum, out oError, out message))
                    {
                        entity.ErrorCode = 0;
                        entity.ErrorMessage = "";
                        entity.RefCode =Convert.ToInt32( refCode);
                        entity.DocNum = docnum;
                       
                    }
                    else
                    {
                        entity.ErrorCode = oError;
                        entity.ErrorMessage = message;
                    }


                }
                else
                {

                    entity.ErrorCode = 10002;
                    entity.ErrorMessage = "Không tìm thấy dữ liệu.";
                }
            }
            catch (Exception ex)
            {
                entity.ErrorCode = 10001;
                entity.ErrorMessage = ex.Message;
            }

            return entity;

        }
        public DocOutput UpdateSO(Order document, out int oError, out string message)
        {
            message = "";
            oError = 0;
            DocOutput entity = new DocOutput();
            try
            {
                if (document != null)
                {
                    DataTools.SAPAccess.CreateObject sap = new DataTools.SAPAccess.CreateObject();
                    var dtHeader = sap.ListToDataTable<Order>(document);
                    var dtLines = sap.ListToDataTable<OrderLine>(document.OrderLines);

                    string refCode = "";
                    string docnum = "";
                    if (sap.UpdateDocument_Multiple("SO",dtHeader, new List<System.Data.DataTable>() { dtLines }, SAPbobsCOM.BoObjectTypes.oOrders, false, out refCode, out docnum, out oError, out message))
                    {
                        entity.ErrorCode = 0;
                        entity.ErrorMessage = "";
                        entity.RefCode = Convert.ToInt32(refCode);
                        entity.DocNum = docnum;
                        
                    }
                    else
                    {

                        entity.ErrorCode = oError;
                        entity.ErrorMessage = message;
                        entity.RefCode = Convert.ToInt32(refCode);
                        entity.DocNum = docnum;
                    }


                }
                else
                {
                    
                    entity.ErrorCode = 10002;
                    entity.ErrorMessage = "Không tìm thấy dữ liệu.";
                }
            }
            catch (Exception ex)
            {
                entity.ErrorCode = 10001;
                entity.ErrorMessage = ex.Message;
            }

            return entity;

        }

        public List<Document> GetList(out string message)
        {
            message = "";
            List<Document> listEntity = new List<Document>();
            try
            {
                string sql = string.Format("SP_GET_SO_LIST_HEADER");
                listEntity = db.Database.SqlQuery<Document>(sql).ToList();

                sql = string.Format("SP_GET_SO_LIST_DETAIL");
                var listEntityLines = db.Database.SqlQuery<DocumentLine>(sql).ToList();

                if (listEntity != null && listEntityLines != null)
                {
                    foreach (var item in listEntity)
                    {
                        item.DocumentLines = listEntityLines.Where(o => o.DocEntry == item.DocEntry).ToList();
                    }
                }

                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }
        public List<Document> GetList(DateTime fromDate, out string message)
        {
            message = "";
            List<Document> listEntity = new List<Document>();
            try
            {
                string sql = string.Format("[ICC_US_API_GET_SOs] @FromDate='{0}'", fromDate.ToString("yyyy-MM-dd"));
                listEntity = db.Database.SqlQuery<Document>(sql).ToList();

                sql = string.Format("[ICC_US_API_GET_SOLines] @FromDate='{0}'", fromDate.ToString("yyyy-MM-dd"));
                var listEntityLines = db.Database.SqlQuery<DocumentLine>(sql).ToList();

                if (listEntity != null && listEntityLines != null)
                {
                    foreach (var item in listEntity)
                    {
                        item.DocumentLines = listEntityLines.Where(o => o.DocEntry == item.DocEntry).ToList();
                    }
                }

                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }
        public List<SODone> GetListSODone(DateTime fromDate, DateTime toDate, out string message)
        {
            message = "";
            List<SODone> listEntity = new List<SODone>();
            try
            {
                string sql = string.Format("[ICC_US_API_GET_LIST_SO_FINNISH] @FROMDATE='{0}',@TODATE='{1}'", fromDate.ToString("yyyy-MM-dd"), toDate.ToString("yyyy-MM-dd"));
                listEntity = db.Database.SqlQuery<SODone>(sql).ToList();
                
                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }
    }
}