﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NKID_API.Models
{
    public class PaymentTermsModels
    {
        private HLCRMContext db = new HLCRMContext();
        public List<PaymentTerms> GetList(out string message)
        {
            message = "";
            List<PaymentTerms> listEntity = new List<PaymentTerms>();
            try
            {
                string sql = string.Format("[ICC_US_API_GETLISTHTTT]");
                listEntity = db.Database.SqlQuery<PaymentTerms>(sql).ToList();

                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }
       

    }
}