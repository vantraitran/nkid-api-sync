﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataTools;
using System.IO;

namespace NKID_API.Models
{
    public class GoodReceiptModels
    {
        private NKIDContext db = new NKIDContext();
        public DocOutput CreateGoodReceipt(OIGN document, StringWriter document_log, out int oError, out string message)
        {
            message = "";
            oError = 0;
            DocOutput entity = new DocOutput();
            try
            {
                if (document != null)
                {
                    DataTools.SAPAccess.CreateObject sap = new DataTools.SAPAccess.CreateObject();
                    var dtHeader = sap.ListToDataTable<OIGN>(document);
                    var dtLines = sap.ListToDataTable<Detail>(document.Details);

                  

                    string refCode = "";
                    string docnum = "";
                    if (sap.CreateDocument_Multiple("OIGN",dtHeader, document_log, new List<System.Data.DataTable>() { dtLines }, SAPbobsCOM.BoObjectTypes.oInventoryGenEntry, false, out refCode, out docnum, out oError, out message))
                    {
                        entity.ErrorCode = 0;
                        entity.ErrorMessage = "";
                        entity.RefCode =Convert.ToInt32( refCode);
                        entity.DocNum = docnum;
                       
                    }
                    else
                    {
                        entity.ErrorCode = oError;
                        entity.ErrorMessage = message;
                    }


                }
                else
                {

                    entity.ErrorCode = 10002;
                    entity.ErrorMessage = "Không tìm thấy dữ liệu.";
                }
            }
            catch (Exception ex)
            {
                entity.ErrorCode = 10001;
                entity.ErrorMessage = ex.Message;
            }

            return entity;

        }
        public Account GET_GLACCOUNT_BY_BP(string CardCode, out string message)
        {
            message = "";
            Account listEntity = new Account();
            try
            {
                string sql = string.Format("F_SP_GET_GL_COGS_ACCOUNT_BY_BUSINESSPARTNER @CardCode='{0}'", CardCode);
                listEntity = db.Database.SqlQuery<Account>(sql).FirstOrDefault();

                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }
      
        public DIMENSION GET_DIMENSION_BY_WAREHOUSE(string WhsCode, string CardCode, string ItemCode, out string message)
        {
            message = "";
            DIMENSION listEntity = new DIMENSION();
            try
            {
                string sql = string.Format("F_SP_GET_DIMENSION_BY_WAREHOUSE @WhsCode='{0}',@CardCode='{1}',@ItemCode='{2}'", WhsCode,CardCode ,ItemCode);
                listEntity = db.Database.SqlQuery<DIMENSION>(sql).FirstOrDefault();

                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }
        public List<Delivery> GetList(out string message)
        {
            message = "";
            List<Delivery> listEntity = new List<Delivery>();
            try
            {
                string sql = string.Format("SP_GET_DELI_LIST_HEADER");
                listEntity = db.Database.SqlQuery<Delivery>(sql).ToList();

                sql = string.Format("SP_GET_DELI_LIST_DETAIL");
                var listEntityLines = db.Database.SqlQuery<DeliveryLine>(sql).ToList();

                if (listEntity != null && listEntityLines != null)
                {
                    foreach (var item in listEntity)
                    {
                        item.DeliveryLines = listEntityLines.Where(o => o.DocEntry == item.DocEntry).ToList();
                    }
                }

                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }

    }
}