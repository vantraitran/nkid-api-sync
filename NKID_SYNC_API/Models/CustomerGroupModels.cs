﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NKID_API.Models
{
    public class CustomerGroupModels
    {
        private HLCRMContext db = new HLCRMContext();
        public List<CustomerGroup> GetList(out string message)
        {
            message = "";
            List<CustomerGroup> listEntity = new List<CustomerGroup>();
            try
            {
                string sql = string.Format("[ICC_US_API_GETCUSTOMERGROUP]");
                listEntity = db.Database.SqlQuery<CustomerGroup>(sql).ToList();

                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }
        public Int32 GetGroupCodeByKyHieu(string KyHieu)
        {
            
            try
            {
               
                string sql = string.Format("[ICC_US_API_GET_GROUPCODE_BYKYHIEU] @KyHieu='{0}'", KyHieu);
                return db.Database.SqlQuery<Int32>(sql).ToList().FirstOrDefault();

            }
            catch (Exception ex)
            {
                
            }
            return -1;

        }

    }
}