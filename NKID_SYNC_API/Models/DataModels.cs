﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Description;
using System.Runtime.Serialization;

namespace NKID_API.Models
{
    using DataTools.Models.Entity;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;


    [DataContract(Namespace = "")]
    public class OWTR
    {

        [DataMember(Name = "DocDate")]
        /// <summary>
        /// Posting Date
        /// </summary>
        public DateTime DocDate { get; set; }

        [DataMember(Name = "DocNumber")]
        /// <summary>
        /// SOKEY#SONO 
        /// </summary>
        [Key]
        public string DocNumber { get; set; }

        /// <summary>
        /// Due Date/Delivery Date
        /// </summary>
        public DateTime DocDueDate { get; set; }
        [DataMember(Name = "Remarks")]
        /// <summary>
        /// Comment
        /// </summary>
        [StringLength(254)]
        public string Comments { get; set; }

        [DataMember(Name = "DocumentType")]
        /// <summary>
        /// Document Type default value = 5
        /// </summary>
        public int? DocumentType { get; set; }

        /// <summary>
        /// Internal Number
        /// </summary>
        [Key]
        public int? DocEntry { get; set; }
        [DataMember(Name = "WMSNumber")]
        /// <summary>
        /// WMS Number
        /// </summary>
        public string U_DocNum { get; set; }
        [DataMember(Name = "TransferDetails")]
        public List<TransferDetail> TransferDetails { get; set; }
    }

    [DataContract(Namespace = "")]
    public class OIGN
    {

        [DataMember(Name = "DocDate")]
        /// <summary>
        /// Posting Date
        /// </summary>
        public DateTime DocDate { get; set; }
        [DataMember(Name = "CustomerCode")]
        /// <summary>
        /// Vendor/Customer Code
        /// </summary>
        public string Ref2 { get; set; }
        [DataMember(Name = "DocNumber")]
        /// <summary>
        /// SOKEY#SONO 
        /// </summary>
        [Key]
        public string DocNumber { get; set; }

        /// <summary>
        /// Due Date/Delivery Date
        /// </summary>
        public DateTime DocDueDate { get; set; }
        [DataMember(Name = "Remarks")]
        /// <summary>
        /// Comment
        /// </summary>
        [StringLength(254)]
        public string Comments { get; set; }

        [DataMember(Name = "DocumentType")]
        /// <summary>
        /// Document Type default value = 5
        /// </summary>
        public int? DocumentType { get; set; }

        /// <summary>
        /// Internal Number
        /// </summary>
        [Key]
        public int? DocEntry { get; set; }
        [DataMember(Name = "WMSNumber")]
        /// <summary>
        /// WMS Number
        /// </summary>
        public string U_DocNum { get; set; }

        
        [DataMember(Name = "Details")]
        public List<Detail> Details { get; set; }
    }
    [DataContract(Namespace = "")]
    public class OIGE
    {

        [DataMember(Name = "DocDate")]
        /// <summary>
        /// Posting Date
        /// </summary>
        public DateTime DocDate { get; set; }
        [DataMember(Name = "CustomerCode")]
        /// <summary>
        /// Vendor/Customer Code
        /// </summary>
        public string Ref2 { get; set; }
        [DataMember(Name = "DocNumber")]
        /// <summary>
        /// SOKEY#SONO 
        /// </summary>
        [Key]
        public string DocNumber { get; set; }

        /// <summary>
        /// Due Date/Delivery Date
        /// </summary>
        public DateTime DocDueDate { get; set; }
        [DataMember(Name = "Remarks")]
        /// <summary>
        /// Comment
        /// </summary>
        [StringLength(254)]
        public string Comments { get; set; }

        [DataMember(Name = "DocumentType")]
        /// <summary>
        /// Document Type default value = 5
        /// </summary>
        public int? DocumentType { get; set; }

        /// <summary>
        /// Internal Number
        /// </summary>
        [Key]
        public int? DocEntry { get; set; }
        [DataMember(Name = "WMSNumber")]
        /// <summary>
        /// WMS Number
        /// </summary>
        public string U_DocNum { get; set; }

       
        [DataMember(Name = "Details")]
        public List<Detail> Details { get; set; }
    }

    [DataContract(Namespace = "")]
    public class OPDN
    {

        [DataMember(Name = "DocDate")]
        /// <summary>
        /// Posting Date
        /// </summary>
        public DateTime DocDate { get; set; }

        [DataMember(Name = "VendorCode")]
        /// <summary>
        /// Vendor Code
        /// </summary>
        public string CardCode { get; set; }
        [DataMember(Name = "DocNumber")]
        /// <summary>
        /// SOKEY#SONO 
        /// </summary>
        [Key]
        public string DocNumber { get; set; }

        /// <summary>
        /// Due Date/Delivery Date
        /// </summary>
        public DateTime DocDueDate { get; set; }
        [DataMember(Name = "Remarks")]
        /// <summary>
        /// Comment
        /// </summary>
        [StringLength(254)]
        public string Comments { get; set; }

        [DataMember(Name = "DocumentType")]
        /// <summary>
        /// Document Type default value = 5
        /// </summary>
        public int? DocumentType { get; set; }

        /// <summary>
        /// Internal Number
        /// </summary>
        [Key]
        public int? DocEntry { get; set; }
        [DataMember(Name = "WMSNumber")]
        /// <summary>
        /// WMS Number
        /// </summary>
        public string U_DocNum { get; set; }
        [DataMember(Name = "Details")]
        public List<Detail> Details { get; set; }
    }
    [DataContract(Namespace = "")]
    public class ORIN
    {

        [DataMember(Name = "DocDate")]
        /// <summary>
        /// Posting Date
        /// </summary>
        public DateTime DocDate { get; set; }

        [DataMember(Name = "CustomerCode")]
        /// <summary>
        /// Vendor/Customer Code
        /// </summary>
        public string CardCode { get; set; }
        [DataMember(Name = "DocNumber")]
        /// <summary>
        /// SOKEY#SONO 
        /// </summary>
        [Key]
        public string DocNumber { get; set; }

        /// <summary>
        /// Due Date/Delivery Date
        /// </summary>
        public DateTime DocDueDate { get; set; }
        [DataMember(Name = "Remarks")]
        /// <summary>
        /// Comment
        /// </summary>
        [StringLength(254)]
        public string Comments { get; set; }

        [DataMember(Name = "DocumentType")]
        /// <summary>
        /// Document Type default value = 5
        /// </summary>
        public int? DocumentType { get; set; }

        /// <summary>
        /// Internal Number
        /// </summary>
        [Key]
        public int? DocEntry { get; set; }
        [DataMember(Name = "WMSNumber")]
        /// <summary>
        /// WMS Number
        /// </summary>
        public string U_DocNum { get; set; }
        [DataMember(Name = "Details")]
        public List<Detail> Details { get; set; }
    }
    [DataContract(Namespace = "")]
    public class ORDN
    {

        [DataMember(Name = "DocDate")]
        /// <summary>
        /// Posting Date
        /// </summary>
        public DateTime DocDate { get; set; }

        [DataMember(Name = "CustomerCode")]
        /// <summary>
        /// Vendor/Customer Code
        /// </summary>
        public string CardCode { get; set; }
        [DataMember(Name = "DocNumber")]
        /// <summary>
        /// SOKEY#SONO 
        /// </summary>
        [Key]
        public string DocNumber { get; set; }

        /// <summary>
        /// Due Date/Delivery Date
        /// </summary>
        public DateTime DocDueDate { get; set; }
        [DataMember(Name = "Remarks")]
        /// <summary>
        /// Comment
        /// </summary>
        [StringLength(254)]
        public string Comments { get; set; }

        [DataMember(Name = "DocumentType")]
        /// <summary>
        /// Document Type default value = 5
        /// </summary>
        public int? DocumentType { get; set; }

        /// <summary>
        /// Internal Number
        /// </summary>
        [Key]
        public int? DocEntry { get; set; }
        [DataMember(Name = "WMSNumber")]
        /// <summary>
        /// WMS Number
        /// </summary>
        public string U_DocNum { get; set; }
        [DataMember(Name = "Details")]
        public List<Detail> Details { get; set; }
    }
    [DataContract(Namespace = "")]
    public class ODLN
    {

        [DataMember(Name = "DocDate")]
        /// <summary>
        /// Posting Date
        /// </summary>
        public DateTime DocDate { get; set; }

        [DataMember(Name = "CustomerCode")]
        /// <summary>
        /// Vendor/Customer Code
        /// </summary>
        public string CardCode { get; set; }
        [DataMember(Name = "DocNumber")]
        /// <summary>
        /// SOKEY#SONO 
        /// </summary>
        [Key]
        public string DocNumber { get; set; }
       
        /// <summary>
        /// Due Date/Delivery Date
        /// </summary>
        public DateTime DocDueDate { get; set; }
        [DataMember(Name = "Remarks")]
        /// <summary>
        /// Comment
        /// </summary>
        [StringLength(254)]
        public string Comments { get; set; }
       
        [DataMember(Name = "DocumentType")]
        /// <summary>
        /// Document Type default value = 5
        /// </summary>
        public int? DocumentType { get; set; } 
        
        /// <summary>
        /// Internal Number
        /// </summary>
        [Key]
        public int? DocEntry { get; set; }
        [DataMember(Name = "WMSNumber")]
        /// <summary>
        /// WMS Number
        /// </summary>
        public string U_DocNum { get; set; }
        [DataMember(Name = "Details")]
        public List<Detail> Details { get; set; }
    }
    [DataContract(Namespace = "")]
    public class Detail
    {

        /// <summary>
        /// Internal Number
        /// </summary>
        public int? DocEntry { get; set; }
       
        /// <summary>
        /// Base Type
        /// </summary>
        public string BaseType { get; set; }
       
        /// <summary>
        /// Base Entry
        /// </summary>
        public int? BaseEntry { get; set; }
        
        [DataMember]
        /// <summary>
        /// Item Code
        /// </summary>
        public string ItemCode { get; set; }
        [DataMember(Name ="LineId")]
        /// <summary>
        /// Line ID
        /// </summary>
        public int? LineNum { get; set; }
        [DataMember(Name ="BaseLine")]
        /// <summary>
        /// Base Line
        /// </summary>
        public int? BaseLine { get; set; }
        [DataMember(Name = "LOTNumber")]
        /// <summary>
        /// LOTNumber
        /// </summary>
        public string U_LOT_Number { get; set; }
        [DataMember(Name = "Quantity")]
        /// <summary>
        /// Quantity
        /// </summary>
        public decimal? Quantity { get; set; }
        [DataMember(Name = "UnitPrice")]
        /// <summary>
        /// UnitPrice
        /// </summary>
        public decimal? Price { get; set; }
        [DataMember(Name = "WarehouseCode")]
        /// <summary>
        /// Warehouse Code
        /// </summary>
        public string WhsCode { get; set; }
        [DataMember(Name = "ProductStatus")]

        /// <summary>
        /// Product Status Good/Bad
        /// </summary>
        public string U_ProductStatus { get; set; }
       
        public string OcrCode { get; set; }

        public string OcrCode2 { get; set; }

        public string OcrCode3 { get; set; }

        public string OcrCode4 { get; set; }

        public string OcrCode5 { get; set; }

        /// <summary>
        /// G/L Account
        /// </summary>
        public int? GLAccount { get; set; }
        /// <summary>
        /// Cogs Account
        /// </summary>
        public int? CogsAccount { get; set; }

       
    }


    [DataContract(Namespace = "")]
    public class TransferDetail
    {

        /// <summary>
        /// Internal Number
        /// </summary>
        public int? DocEntry { get; set; }
        [DataMember]
        /// <summary>
        /// Item Code
        /// </summary>
        public string ItemCode { get; set; }
        [DataMember(Name = "LineId")]
        /// <summary>
        /// Line ID
        /// </summary>
        public int? LineNum { get; set; }
      
        [DataMember(Name = "LOTNumber")]
        /// <summary>
        /// LOTNumber
        /// </summary>
        public string U_LOT_Number { get; set; }
        [DataMember(Name = "Quantity")]
        /// <summary>
        /// Quantity
        /// </summary>
        public decimal? Quantity { get; set; }
        [DataMember(Name = "FromWarehouseCode")]
        /// <summary>
        /// From Warehouse Code
        /// </summary>
        public string FromWhsCod { get; set; }
        [DataMember(Name = "ToWarehouseCode")]
        /// <summary>
        /// To Warehouse Code
        /// </summary>
        public string WhsCode { get; set; }
        [DataMember(Name = "ProductStatus")]

        /// <summary>
        /// Product Status Good/Bad
        /// </summary>
        public string U_ProductStatus { get; set; }

        public string OcrCode { get; set; }

        public string OcrCode2 { get; set; }

        public string OcrCode3 { get; set; }

        public string OcrCode4 { get; set; }

        public string OcrCode5 { get; set; }

        /// <summary>
        /// G/L Account
        /// </summary>
        public int? GLAccount { get; set; }
        /// <summary>
        /// Cogs Account
        /// </summary>
        public int? CogsAccount { get; set; }


    }



    [DataContract(Namespace = "")]
    public class GRPO
    {

        /// <summary>
        /// Internal Number
        /// </summary>
        [Key]
        public int? DocEntry { get; set; }
        /// <summary>
        /// Document Number
        /// </summary>
        [Key]
        public int? DocNum { get; set; }
        [DataMember]
        /// <summary>
        /// Vendor/Customer Code
        /// </summary>
        public string CardCode { get; set; }

        /// <summary>
        /// Vendor/Customer Name
        /// </summary>
        public string CardName { get; set; }

        /// <summary>
        /// NumAtCard
        /// </summary>
        public string NumAtCard { get; set; }

        /// <summary>
        /// Contact Person
        /// </summary>
        public int? CntctCode { get; set; }
        [DataMember]
        /// <summary>
        /// Posting Date
        /// </summary>
        public DateTime DocDate { get; set; }
        [DataMember]
        /// <summary>
        /// Due Date/Delivery Date
        /// </summary>
        public DateTime DocDueDate { get; set; }

        /// <summary>
        /// SlpCode
        /// </summary>
        public int? SlpCode { get; set; }
        [DataMember]
        /// <summary>
        /// Comment
        /// </summary>
        [StringLength(254)]
        public string Comments { get; set; }
        [DataMember]
        /// <summary>
        /// Số chứng từ
        /// </summary>
        [StringLength(20)]
        public string U_DocNum { get; set; }
        [DataMember]
        /// <summary>
        /// Số đơn hàng
        /// </summary>
        [StringLength(20)]
        public string U_N_POEntry { get; set; }
        /// <summary>
        /// Total Amount
        /// </summary>
        public decimal? DocTotal { get; set; }
        [DataMember]
        /// <summary>
        /// Details Information
        /// </summary>
        public List<GRPOLine> GRPOLines { get; set; }
    }
    [DataContract(Namespace = "")]
    public class GRPOLine
    {

        /// <summary>
        /// Internal Number
        /// </summary>
        public int? DocEntry { get; set; }
        [DataMember]
        /// <summary>
        /// Base Type
        /// </summary>
        public string BaseType { get; set; }
        [DataMember]
        /// <summary>
        /// Base Entry
        /// </summary>
        public int? BaseEntry { get; set; }
        [DataMember]
        /// <summary>
        /// Base Line
        /// </summary>
        public int? BaseLine { get; set; }
        [DataMember]
        /// <summary>
        /// Item Code
        /// </summary>
        public string ItemCode { get; set; }
        /// <summary>
        /// Item Name
        /// </summary>
        public string ItemName { get; set; }
        [DataMember]
        /// <summary>
        /// Quantity
        /// </summary>
        public decimal? Quantity { get; set; }
        [DataMember]
        /// <summary>
        /// Warehouse Code
        /// </summary>
        public string WhsCode { get; set; }
        [DataMember]
        /// <summary>
        /// Price
        /// </summary>
        public decimal? Price { get; set; }
        /// <summary>
        /// Price before Discount
        /// </summary>
        public decimal? PriceBefDi { get; set; }
        [DataMember]
        /// <summary>
        /// Discount Percent
        /// </summary>
        public decimal DiscPrcnt { get; set; }
        [DataMember]
        /// <summary>
        /// VAT Percent
        /// </summary>
        public decimal? VatPrcnt { get; set; }
        /// <summary>
        /// VAT Amount
        /// </summary>
        public decimal? VatSum { get; set; }
        [DataMember]

        /// <summary>
        /// Trạng thái hàng hóa Good/Bad
        /// </summary>
        public string U_ProductStatus { get; set; }
        /// <summary>
        /// Gross Price
        /// </summary>
        public decimal? U_GrossPrice { get; set; }
        [DataMember]
        /// <summary>
        /// Số Lot
        /// </summary>
        public string U_LOT_Number { get; set; }
        ///// <summary>
        ///// Bin Location
        ///// </summary>
        //public string BinLocation { get; set; }
        public string OcrCode { get; set; }

        public string OcrCode2 { get; set; }

        public string OcrCode3 { get; set; }

        public string OcrCode4 { get; set; }

        public string OcrCode5 { get; set; }

        /// <summary>
        /// G/L Account
        /// </summary>
        public int? GLAccount { get; set; }
        /// <summary>
        /// Cogs Account
        /// </summary>
        public int? CogsAccount { get; set; }

        /// <summary>
        /// Total before VAT
        /// </summary>
        public decimal? LineTotal { get; set; }

        /// <summary>
        /// Line Number
        /// </summary>
        public int? LineNum { get; set; }

    }
    public class Item
    {
        /// <summary>
        /// Item Code
        /// </summary>
        [StringLength(50)]
        public string ItemCode { get; set; }
        /// <summary>
        /// Item Name
        /// </summary>
        [StringLength(100)]
        public string ItemName { get; set; }
        /// <summary>
        /// BarCode
        /// </summary>
        [StringLength(254)]
        public string CodeBars { get; set; }
        /// <summary>
        /// Inventory Unit
        /// </summary>
        [StringLength(100)]
        public string InvntryUom { get; set; }
        /// <summary>
        /// Sales Unit
        /// </summary>
        [StringLength(100)]
        public string SalUnitMsr { get; set; }
        /// <summary>
        /// Purchase Unit
        /// </summary>
        [StringLength(100)]
        public string BuyUnitMsr { get; set; }
    }
    [DataContract(Namespace = "")]
    public class ItemInventory
    {
        [DataMember]
        /// <summary>
        /// Item Code
        /// </summary>
        [StringLength(50)]
        public string ItemCode { get; set; }
        [DataMember(Name = "Warehouse Code")]
        /// <summary>
        /// Item Name
        /// </summary>
        [StringLength(50)]
        public string WhsCode { get; set; }
        [DataMember(Name = "Warehouse Name")]
        [StringLength(200)]
        public string WhsName { get; set; }
        [DataMember(Name ="InStock")]
        /// <summary>
        /// OnHand
        /// </summary>
        public decimal OnHand { get; set; }
    }
    public class PaymentTerms
    {
        /// <summary>
        /// Payment Terms Code
        /// </summary>
        [StringLength(2)]
        public string Code { get; set; }
        /// <summary>
        /// Payment Terms Name
        /// </summary>
        [StringLength(200)]
        public string Name { get; set; }

    }
    public class SaleEmployee
    {
        /// <summary>
        /// Sale Employee Code --> Tax
        /// </summary>
        [StringLength(8)]
        public string EmpCode { get; set; }
        /// <summary>
        /// Sale Employee Name
        /// </summary>
        [StringLength(200)]
        public string EmpName { get; set; }

    }
    public class CustomerGroup
    {
        /// <summary>
        /// GroupCode --> U_KyHieu
        /// </summary>
        [StringLength(2)]
        [MinLength(2)]
        public string GroupCode { get; set; }
        /// <summary>
        /// Item Name
        /// </summary>
        [StringLength(200)]
        public string GroupName { get; set; }
        /// <summary>
        /// InternalKey -->GroupCode
        /// </summary>
        //public Int16 InternalKey { get; set; }
    }
    public class BusinessPartners
    {
        /// <summary>
        /// SalesforceID --> U_SalesforceID
        /// </summary>
        [StringLength(18)]
        public string SalesforceID { get; set; }

        /// <summary>
        /// CardCode 
        /// </summary>
        [StringLength(15)]
        public string CardCode { get; set; }

        /// <summary>
        /// CardName 
        /// </summary>
        [StringLength(100)]
        public string CardName { get; set; }

        /// <summary>
        /// TaxCode --> Đối tượng xuất HĐ:U_HDCus
        /// </summary>
        [StringLength(15)]
        public string TaxCode { get; set; }

        /// <summary>
        /// CardTaxName --> Tên đối tượng xuất HĐ:U_HDName
        /// </summary>
        [StringLength(100)]
        public string CardTaxName { get; set; }

        /// <summary>
        /// TaxAdress --> Địa chỉ xuất HĐ:U_HDaddress
        /// </summary>
        [StringLength(254)]
        public string TaxAdress { get; set; }

        /// <summary>
        /// GroupCode --> --> OCRG:U_KyHieu
        /// </summary>
        [StringLength(2)]
        public string GroupCode { get; set; }
        /// <summary>
        /// Phone --> Mobile Phone
        /// </summary>
        [StringLength(50)]
        public string Phone { get; set; }


        /// <summary>
        /// LocationCode --> --> Mã vùng:U_MaVung
        /// </summary>
        [StringLength(1)]
        public string LocationCode { get; set; }

        /// <summary>
        /// EmpCode --> --> OSLP:Tax
        /// </summary>
        [StringLength(8)]
        public string EmpCode { get; set; }

    }
    public class Warehouse
    {
        /// <summary>
        /// Warehouse Code
        /// </summary>
        [StringLength(8)]
        public string WhsCode { get; set; }
        /// <summary>
        /// Warehouse Name
        /// </summary>
        [StringLength(100)]
        public string WhsName { get; set; }
    }

    public class CancelDocument
    {
        /// <summary>
        /// BaseRef of GRPO, Delivery
        /// </summary>
        [Key]
        public int DocEntry { get; set; }
        /// <summary>
        /// Vendor/Customer Code
        /// </summary>
        public string CardCode { get; set; }
        /// <summary>
        /// Vendor/Customer Name
        /// </summary>
        public string CardName { get; set; }
        /// <summary>
        /// Posting Date
        /// </summary>
        public DateTime DocDate { get; set; }
        /// <summary>
        /// Comment
        /// </summary>
        [StringLength(254)]
        public string Comments { get; set; }
    }
    [DataContract(Namespace = "")]
    public class Order
    {

        /// <summary>
        /// SAP Order Id
        /// </summary>
        //[DataMember]
        //[Key]
        public int DocEntry { get; set; }

        /// <summary>
        /// Salesforce Order Id
        /// </summary>
        [DataMember]
        [MaxLength(18)]
        public string SalesforceOrderId { get; set; }

        /// <summary>
        /// SAP Series
        /// </summary>
        public string Series { get; set; }

        /// <summary>
        /// Vendor/Customer Code
        /// </summary>
        [DataMember]
        [Required]
        public string CardCode { get; set; }
        [DataMember]
        /// <summary>
        /// Posting Date
        /// </summary>
        [Required]
        public DateTime DocDate { get; set; }

        /// <summary>
        /// Due Date/Delivery Date
        /// </summary>
        [DataMember]
        [Required]
        public DateTime DocDueDate { get; set; }

        /// <summary>
        /// Location
        /// </summary>
        [DataMember]
        [Required]
        [MaxLength(1)]
        [Description("<b>Giá trị truyền vào là 1 trong 2 giá trị sau: <b>'S'</b> hoặc <b>'H'</b></b>")]
        public string Location { get; set; }

        /// <summary>
        /// Tên đối tượng xuất hóa đơn
        /// </summary>
        [DataMember]
        public string BPInvoiceName { get; set; }

        /// <summary>
        /// Mã số thuế
        /// </summary>
        [DataMember]
        public string BPInvoiceTaxCode { get; set; }
        [DataMember]
        /// <summary>
        /// Địa chỉ xuất hóa đơn
        /// </summary>
        public string BPInvoiceAddress { get; set; }
        [DataMember]
        /// <summary>
        /// Đối tượng xuất HĐ
        /// </summary>
        public string BPInvoiceCode { get; set; }
        [DataMember]
        [Description("<b>Giá trị truyền vào là Code lấy danh sách PaymentType theo địa chỉ : <a href='http://203.205.27.70:31121/api/PaymentTerms'>http://203.205.27.70:31121/api/PaymentTerms</a> </b>")]
        /// <summary>
        /// Hình thức thanh toán
        /// </summary>
        [Required]
        public string PaymentType { get; set; }
        [DataMember]
        /// <summary>
        /// Xuất hóa đơn gấp?
        /// </summary>
        [Required]
        [MaxLength(1)]
        [Description("<b>Giá trị truyền vào là 1 trong 2 giá trị sau: 'Y' hoặc 'N'</b>")]
        public string IssueInvoice { get; set; }
        [DataMember]
        /// <summary>
        /// Diễn giải (VN)
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// Nhom hang
        /// </summary>
        public string NhomHang { get; set; }
        [DataMember]
        /// <summary>
        /// Muc Dich Don Hang
        /// </summary>
        
        public string MucDichDonHang { get; set; }
        [DataMember]
        /// <summary>
        /// OnlineOrder
        /// </summary>
        [Required]
        [MaxLength(1)]
        [Description("<b>Giá trị truyền vào là 1 trong 3 giá trị sau: 'T','S' hoặc 'O'</b>")]
        public string OnlineOrder { get; set; }
        [DataMember]
        [Description("<b>Giá trị truyền vào là EmpCode lấy danh sách EmpCode theo địa chỉ : <a href='http://203.205.27.70:31121/api/SaleEmployees'>http://203.205.27.70:31121/api/SaleEmployees</a> </b>")]
        /// <summary>
        /// Sale Employees
        /// </summary>
        [Required]
        public string EmpCode { get; set; }
        /// <summary>
        /// Owner Code
        /// </summary>
        public string OwnerCode { get; set; }
        [DataMember]
        /// <summary>
        /// Details Information
        /// </summary>
        [Required]
        public List<OrderLine> OrderLines { get; set; }
    }
   
    [DataContract(Namespace = "")]
    public class OrderLine
    {
        //[DataMember]
        /// <summary>
        /// SAP Order ID
        /// </summary>
        public int DocEntry { get; set; }
        [DataMember]
        /// <summary>
        /// Salesforce Order Line Id
        /// </summary>
        [MaxLength(18)]
        [Required]
        public string SalesforceOrderLineId { get; set; }
        [DataMember]
        [Required]
        [Description("<b>Giá trị truyền vào là ItemCode lấy danh sách ItemCode theo địa chỉ : <a href='http://203.205.27.70:31121/api/Items'>http://203.205.27.70:31121/api/Items</a> </b>")]
        /// <summary>
        /// Item Code
        /// </summary>
        public string ItemCode { get; set; }
        [DataMember]
        [Required]
        /// <summary>
        /// Quantity
        /// </summary>
        public decimal Quantity { get; set; }
        [DataMember]
        [Required]
        /// <summary>
        /// Price After VAT
        /// </summary>
        public decimal PriceAfVAT { get; set; }
        [DataMember]
        [Required]
        /// <summary>
        /// Public Price
        /// </summary>
        public decimal PriceAfDisc { get; set; }
        

        /// <summary>
        /// VatGroup
        /// </summary>
        public string VatGroup { get; set; }
        [DataMember]
        [Required]
        [Description("<b>Giá trị truyền vào là SegmentCode lấy danh sách Segments theo địa chỉ : <a href='http://203.205.27.70:31121/api/Segments'>http://203.205.27.70:31121/api/Segments</a> </b>")]
        /// <summary>
        /// Segment 
        /// </summary>
        public string Segment { get; set; }
        /// <summary>
        /// Warehouse Code
        /// </summary>
        public string WhsCode { get; set; }
    }
    [DataContract(Namespace = "")]
    public class Delivery
    {
       
        /// <summary>
        /// Internal Number
        /// </summary>
        [Key]
        public int? DocEntry { get; set; }
        /// <summary>
        /// Document Number
        /// </summary>
        [Key]
        public int? DocNum { get; set; }
        [DataMember]
        /// <summary>
        /// Vendor/Customer Code
        /// </summary>
        public string CardCode { get; set; }
      
        /// <summary>
        /// Vendor/Customer Name
        /// </summary>
        public string CardName { get; set; }
       
        /// <summary>
        /// NumAtCard
        /// </summary>
        public string NumAtCard { get; set; }
       
        /// <summary>
        /// Contact Person
        /// </summary>
        public int? CntctCode { get; set; }
        [DataMember]
        /// <summary>
        /// Posting Date
        /// </summary>
        public DateTime DocDate { get; set; }
        [DataMember]
        /// <summary>
        /// Due Date/Delivery Date
        /// </summary>
        public DateTime DocDueDate { get; set; }
      
        /// <summary>
        /// SlpCode
        /// </summary>
        public int? SlpCode { get; set; }
        [DataMember]
        /// <summary>
        /// Comment
        /// </summary>
        [StringLength(254)]
        public string Comments { get; set; }
        [DataMember]
        /// <summary>
        /// Số chứng từ
        /// </summary>
        [StringLength(20)]
        public string U_DocNum { get; set; }
        [DataMember]
        /// <summary>
        /// Số đơn hàng
        /// </summary>
        [StringLength(20)]
        public string U_SONo { get; set; }
        [DataMember]
        /// <summary>
        /// Ngày đơn hàng
        /// </summary>
        public DateTime? U_SODate { get; set; }
        
        /// <summary>
        /// Total Amount
        /// </summary>
        public decimal? DocTotal { get; set; }
        [DataMember]
        /// <summary>
        /// Details Information
        /// </summary>
        public List<DeliveryLine> DeliveryLines { get; set; }
    }
    [DataContract(Namespace = "")]
    public class DeliveryLine
    {
     
        /// <summary>
        /// Internal Number
        /// </summary>
        public int? DocEntry { get; set; }
        [DataMember]
        /// <summary>
        /// Base Type
        /// </summary>
        public int? BaseType { get; set; }
        [DataMember]
        /// <summary>
        /// Base Entry
        /// </summary>
        public int? BaseEntry { get; set; }
        [DataMember]
        /// <summary>
        /// Base Line
        /// </summary>
        public int? BaseLine { get; set; }
        [DataMember]
        /// <summary>
        /// Item Code
        /// </summary>
        public string ItemCode { get; set; }
       
        /// <summary>
        /// Item Name
        /// </summary>
        public string ItemName { get; set; }
        [DataMember]
        /// <summary>
        /// Quantity
        /// </summary>
        public decimal? Quantity { get; set; }
        [DataMember]
        /// <summary>
        /// Warehouse Code
        /// </summary>
        public string WhsCode { get; set; }
        [DataMember]
        /// <summary>
        /// Trạng thái hàng hóa Good/Bad
        /// </summary>
        public string U_ProductStatus { get; set; }
        [DataMember]
        /// <summary>
        /// Số Lot
        /// </summary>
        public string U_LOT_Number { get; set; }
        public string OcrCode { get; set; }

        public string OcrCode2 { get; set; }

        public string OcrCode3 { get; set; }

        public string OcrCode4 { get; set; }

        public string OcrCode5 { get; set; }

        /// <summary>
        /// G/L Account
        /// </summary>
        public int? GLAccount { get; set; }
        /// <summary>
        /// Cogs Account
        /// </summary>
        public int? CogsAccount { get; set; }

    }
    public class Document
    {
        /// <summary>
        /// Internal Number
        /// </summary>
        [Key]

        public int? DocEntry { get; set; }
        

        /// <summary>
        /// Vendor/Customer Code
        /// </summary>
        public string CardCode { get; set; }

        /// <summary>
        /// Vendor/Customer Name
        /// </summary>
        public string CardName { get; set; }
        /// <summary>
        /// NumAtCard
        /// </summary>
        public string NumAtCard { get; set; }

        /// <summary>
        /// Contact Person
        /// </summary>
        public int? CntctCode { get; set; }
        /// <summary>
        /// Posting Date
        /// </summary>
        public DateTime? DocDate { get; set; }
        /// <summary>
        /// Due Date/Delivery Date
        /// </summary>
        public DateTime? DocDueDate { get; set; }
        /// <summary>
        /// SlpCode
        /// </summary>
        public int? SlpCode { get; set; }
        /// <summary>
        /// Comment
        /// </summary>
        [StringLength(254)]
        public string Comments { get; set; }
        /// <summary>
        /// Số chứng từ
        /// </summary>
        [StringLength(20)]
        public string U_DocNum { get; set; }
        /// <summary>
        /// Số đơn hàng
        /// </summary>
        [StringLength(20)]
        public string U_SONo { get; set; }
        /// <summary>
        /// Ngày đơn hàng
        /// </summary>
        public DateTime? U_SODate { get; set; }
        /// <summary>
        /// Total Amount
        /// </summary>
        public decimal? DocTotal { get; set; }
        /// <summary>
        /// Details Information
        /// </summary>
        public List<DocumentLine> DocumentLines { get; set; }
    }

    public class DocumentLine
    {
        /// <summary>
        /// Internal Number
        /// </summary>
        public int? DocEntry { get; set; }
        /// <summary>
        /// Base Type
        /// </summary>
        public string BaseType { get; set; }
        /// <summary>
        /// Base Entry
        /// </summary>
        public int? BaseEntry { get; set; }
        /// <summary>
        /// Base Line
        /// </summary>
        public int? BaseLine { get; set; }

        /// <summary>
        /// Item Code
        /// </summary>
        public string ItemCode { get; set; }
        /// <summary>
        /// Item Name
        /// </summary>
        public string ItemName { get; set; }
        /// <summary>
        /// Quantity
        /// </summary>
        public decimal? Quantity { get; set; }
        /// <summary>
        /// Warehouse Code
        /// </summary>
        public string WhsCode { get; set; }
        /// <summary>
        /// Price
        /// </summary>
        public decimal? Price { get; set; }
        /// <summary>
        /// Price before Discount
        /// </summary>
        public decimal? PriceBefDi { get; set; }
        /// <summary>
        /// Discount Percent
        /// </summary>
        public decimal DiscPrcnt { get; set; }
        /// <summary>
        /// VAT Percent
        /// </summary>
        public decimal? VatPrcnt { get; set; }
        /// <summary>
        /// VAT Amount
        /// </summary>
        public decimal? VatSum { get; set; }


        /// <summary>
        /// Trạng thái hàng hóa Good/Bad
        /// </summary>
        public string U_ProductStatus { get; set; }
        /// <summary>
        /// Gross Price
        /// </summary>
        public decimal? U_GrossPrice { get; set; }
        /// <summary>
        /// Số Lot
        /// </summary>
        public string U_LOT_Number { get; set; }
        ///// <summary>
        ///// Bin Location
        ///// </summary>
        //public string BinLocation { get; set; }
        public string OcrCode { get; set; }

        public string OcrCode2 { get; set; }

        public string OcrCode3 { get; set; }

        public string OcrCode4 { get; set; }

        public string OcrCode5 { get; set; }
        /// <summary>
        /// Total before VAT
        /// </summary>
        public decimal? LineTotal { get; set; }

        /// <summary>
        /// Line Number
        /// </summary>
        public int? LineNum { get; set; }
    }
    public class RsOutput
    {
        /// <summary>
        /// Error Code
        /// Value 0 is mean success.
        /// </summary>
        public int ErrorCode { get; set; }
        /// <summary>
        /// Error Message
        /// </summary>
        public string ErrorMessage { get; set; }
        /// <summary>
        /// Reference Code
        /// Return Reference Code when Post success.
        /// </summary>
        public string SAP_ID { get; set; }
        public string SalesForce_ID { get; set; }
    }
    public class RsOutputBP
    {
        /// <summary>
        /// Error Code
        /// Value 0 is mean success.
        /// </summary>
        public int ErrorCode { get; set; }
        /// <summary>
        /// Error Message
        /// </summary>
        public string ErrorMessage { get; set; }
        /// <summary>
        /// Reference Code
        /// Return Reference Code when Post success.
        /// </summary>
        public string RefCode { get; set; }

        public Customer BP { get; set; }
    }
    public class Account
    {
        /// <summary>
        /// G/L Account
        /// </summary>
        public string GLAccount { get; set; }
        /// <summary>
        /// Cogs Account
        /// </summary>
        public string CogsAccount { get; set; }

    }
    public class DIMENSION
    {
        /// <summary>
        /// G/L Account
        /// </summary>
        public string GLAccount { get; set; }
        /// <summary>
        /// Cogs Account
        /// </summary>
        public string CogsAccount { get; set; }

        /// <summary>
        /// Division
        /// </summary>
        public string Division { get; set; }
        /// <summary>
        /// Department
        /// </summary>
        public string Department { get; set; }
        /// <summary>
        /// Team
        /// </summary>
        public string Team { get; set; }
        /// <summary>
        /// SubTeam
        /// </summary>
        public string SubTeam { get; set; }
        /// <summary>
        /// Level
        /// </summary>
        public string Level { get; set; }
    }
    [DataContract(Namespace = "")]
    public class SODone
    {
        
        
        //[DataMember(Name = "SalesOrderID")]
        /// <summary>
        /// Sales Order ID
        /// </summary>
        public int DocEntry { get; set; }
        [DataMember(Name = "SalesOrderNumber")]
        /// <summary>
        /// Sales Order Number
        /// </summary>
        public int DocNum { get; set; }

        [DataMember]
        /// <summary>
        /// Salesforce Order Id
        /// </summary>
        public string SalesforceOrderId { get; set; }
    }
    [DataContract(Namespace = "")]
    public class DocOutput
    {
        [DataMember]
        /// <summary>
        /// Error Code
        /// Value 0 is mean success.
        /// </summary>
        public int ErrorCode { get; set; }
        [DataMember]
        /// <summary>
        /// Error Message
        /// </summary>
        public string ErrorMessage { get; set; }
        //[DataMember(Name = "SAP_ID")]
        /// <summary>
        /// SAP Document Key
        /// Return Reference Code when Post success.
        /// </summary>
        public int RefCode { get; set; }
        [DataMember(Name ="SAP_Number")]
        /// <summary>
        /// SAP Document Number
        /// </summary>
        public string DocNum { get; set; }


    }
    public class StockTransfer
    {
        /// <summary>
        /// Unique Key
        /// </summary>
        [Key]
        public int DocEntry { get; set; }
        /// <summary>
        /// Posting Date
        /// </summary>
        public DateTime DocDate { get; set; }
        /// <summary>
        /// Comment
        /// </summary>
        [StringLength(254)]
        public string Comments { get; set; }
        /// <summary>
        /// Sales Employee
        /// </summary>
        public List<StockTransferLine> StockTransferLines { get; set; }
    }

    public class StockTransferLine
    {
        public int DocEntry { get; set; }
        /// <summary>
        /// Line Number
        /// </summary>
        public int LineNum { get; set; }
        /// <summary>
        /// Item Code
        /// </summary>
        public string ItemCode { get; set; }
        /// <summary>
        /// Item Name
        /// </summary>
        public string ItemName { get; set; }
        /// <summary>
        /// Quantity
        /// </summary>
        public decimal Quantity { get; set; }
        /// <summary>
        /// From Warehouse Code
        /// </summary>
        public string FromWhsCode { get; set; }
        /// <summary>
        /// To Warehouse Code
        /// </summary>
        public string ToWhsCode { get; set; }
    }
    public class Segment
    {
        /// <summary>
        /// Segment Code
        /// </summary>
        public string SegmentCode { get; set; }
        /// <summary>
        /// Segment Name
        /// </summary>
        public string SegmentName { get; set; }
    }
    #region Entity
    [Table("CustomerList")]
    public partial class CustomerEntity
    {
        [Key]
        [StringLength(20)]
        public string CustomerID { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(10)]
        public string Gender { get; set; }

        [StringLength(200)]
        public string HomeAddress { get; set; }

        [StringLength(200)]
        public string CompanyAddress { get; set; }

        public DateTime? Birthday { get; set; }

        [StringLength(200)]
        public string Occupation { get; set; }

        [StringLength(20)]
        public string Phone1 { get; set; }

        [StringLength(20)]
        public string Phone2 { get; set; }

        [StringLength(200)]
        public string Email { get; set; }

        public bool? VerifiedContact { get; set; }

        public bool? InActive { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? AverageSeg { get; set; }

        [StringLength(200)]
        public string FavoriteSeg { get; set; }

        public int? EWallet { get; set; }

        public int? LoyaltyPoints { get; set; }

        public bool? FisrtTimeUsage { get; set; }

        [StringLength(200)]
        public string FavouriteFlavour { get; set; }

        [StringLength(200)]
        public string Interest { get; set; }

        [StringLength(8)]
        public string FavouriteStoreCode { get; set; }

        [StringLength(200)]
        public string ImportantNotes { get; set; }

        [StringLength(50)]
        public string Kid1_FirstName { get; set; }

        [StringLength(50)]
        public string Kid1_LastName { get; set; }

        public DateTime? Kid1_Birthday { get; set; }

        [StringLength(50)]
        public string Kid2_FirstName { get; set; }

        [StringLength(50)]
        public string Kid2_LastName { get; set; }

        public DateTime? Kid2_Birthday { get; set; }

        [StringLength(50)]
        public string CardNumber { get; set; }

        [StringLength(254)]
        public string CardLevel { get; set; }

        [StringLength(10)]
        public string LevelCode { get; set; }
    }

    [Table("ICC_RECONCILES")]
    public partial class ICC_RECONCILE
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int DocEntry { get; set; }

        public DateTime CountDate { get; set; }

        public int CountTime { get; set; }

        [StringLength(254)]
        public string Remarks { get; set; }

        public List<ICC_RECONCILE_LINE> RECONCILE_LINES { get; set; }
    }

    [Table("ICC_RECONCILE_LINES")]
    public partial class ICC_RECONCILE_LINE
    {
        [Key]
        [Column(Order = 1)]
        public int DocEntry { get; set; }

        public int LineNum { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string ItemCode { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string WhsCode { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CountQty { get; set; }
    }
    #endregion
}