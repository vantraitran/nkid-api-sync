﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NKID_API.Models
{
    public class SaleEmployeeModels
    {
        private HLCRMContext db = new HLCRMContext();
        public List<SaleEmployee> GetList(out string message)
        {
            message = "";
            List<SaleEmployee> listEntity = new List<SaleEmployee>();
            try
            {
                string sql = string.Format("[ICC_US_API_GET_SALE_EMPLOYEE]");
                listEntity = db.Database.SqlQuery<SaleEmployee>(sql).ToList();

                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }
        public Int32 GetSlpCodeByCostCenter(string MACP)
        {

            try
            {

                string sql = string.Format("[ICC_US_API_GET_SLPCODE] @MACP='{0}'", MACP);
                return db.Database.SqlQuery<Int32>(sql).ToList().FirstOrDefault();

            }
            catch (Exception ex)
            {

            }
            return -1;

        }

    }
}