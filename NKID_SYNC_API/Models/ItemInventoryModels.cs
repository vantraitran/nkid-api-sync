﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NKID_API.Models
{
    public class ItemInventoryModels
    {
        private HLCRMContext db = new HLCRMContext();
        public List<ItemInventory> GetList(out string message)
        {
            message = "";
            List<ItemInventory> listEntity = new List<ItemInventory>();
            try
            {
                string sql = string.Format("[ICC_US_API_GETINVENTORY]");
                listEntity = db.Database.SqlQuery<ItemInventory>(sql).ToList();

                return listEntity;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;

        }
        public string GetDefault_Warehouse_CRM(string Location)
        {

            try
            {

                string sql = string.Format("[ICC_US_API_GET_Default_Warehouse_CRM] @Location='{0}'", Location);
                return db.Database.SqlQuery<string>(sql).ToList().FirstOrDefault();

            }
            catch (Exception ex)
            {

            }
            return null;

        }
    }
}