﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;
using Microsoft.VisualBasic;
using System.Reflection;
using System.Xml;

using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Windows.Forms;

using DataTools.Models;

namespace DataTools.Utility
{
    public partial class BaseUtil
    {
        #region "Encrypt/Decrypt string"
        public string comEncryptByDES(string s)
        {
            byte[] ArrEnc = null;
            string sResult = null;
            sResult = "";

            TripleDES DES = new TripleDES();
            ArrEnc = DES.Encrypt(s);

            for (int i = 0; i <= ArrEnc.Length - 1; i++)
            {
                if (i == 0)
                {
                    sResult = sResult + Convert.ToString(ArrEnc[i]);
                }
                else
                {
                    sResult = sResult + "|" + Convert.ToString(ArrEnc[i]);
                }
            }

            DES = null;

            return sResult;
        }

        public string comDecryptByDES(string s)
        {
            try
            {
                string[] lst = null;
                lst = s.Split('|');
                byte[] ArrEnc = new byte[lst.Length];

                for (int i = 0; i <= lst.Length - 1; i++)
                {
                    ArrEnc[i] = Convert.ToByte(lst[i]);
                }

                TripleDES DES = new TripleDES();
                return DES.Decrypt(ArrEnc);
                DES = null;
            }
            catch (Exception ex)
            {
                return "error";
            }
        }
        #endregion

        public void RegConn_Create(DataConnection dataconn, ConnectType conntype, string servername, string licensename, string dbname, string userdb, string passdb, string usersap, string passsap, string serverype)
        {

            var k = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software", true);
            if (k == null)
                k = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("Software");
            k = k.CreateSubKey(ROOT_REG, Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree);
            k = k.CreateSubKey(PROJ_REG, Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree);
            k = k.CreateSubKey(CONN_REG, Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree);

            if (dataconn == DataConnection.CLIENTDB)
                k = k.CreateSubKey(CONNNAME_CLIENTDB_REG, Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree);
            else if (dataconn == DataConnection.SERVERDB)
                k = k.CreateSubKey(CONNNAME_SERVERDB_REG, Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree);


            k.SetValue(CONNECTYPE_REG, conntype);
            k.SetValue(SERVER_REG, servername);
            k.SetValue(DATABASE_REG, dbname);
            k.SetValue(USERDB_REG, userdb);
            k.SetValue(PASSDB_REG, comEncryptByDES(passdb));
            k.SetValue(LICENSE_REG, licensename);
            k.SetValue(USERSAP_REG, usersap);
            k.SetValue(PASSSAP_REG, comEncryptByDES(passsap));
            k.SetValue(SERVERTYPE_REG, serverype);
            k.Close();
        }

        public Configuration_View RegConn_Get(DataConnection dataconn)
        {
            Configuration_View obj = null;

            Microsoft.Win32.RegistryKey k;
            k = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE", Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree, System.Security.AccessControl.RegistryRights.FullControl);
            if (k != null)
                k = k.OpenSubKey(BaseUtil.ROOT_REG, true);
            if (k != null)
                k = k.OpenSubKey(BaseUtil.PROJ_REG, true);
            if (k != null)
                k = k.OpenSubKey(BaseUtil.CONN_REG, true);

            if (k != null)
                if (dataconn == DataConnection.CLIENTDB)
                    k = k.OpenSubKey(BaseUtil.CONNNAME_CLIENTDB_REG, true);
                else if (dataconn == DataConnection.SERVERDB)
                    k = k.OpenSubKey(BaseUtil.CONNNAME_SERVERDB_REG, true);


            if (k != null)
            {
                var conntype = k.GetValue(BaseUtil.CONNECTYPE_REG);
                var servername = k.GetValue(BaseUtil.SERVER_REG);
                var dbname = k.GetValue(BaseUtil.DATABASE_REG);
                var userdb = k.GetValue(BaseUtil.USERDB_REG);
                var passdb = k.GetValue(BaseUtil.PASSDB_REG);
                passdb = comDecryptByDES(passdb.ToString());
                var sappass = k.GetValue(BaseUtil.PASSSAP_REG);
                sappass = comDecryptByDES(sappass.ToString());
                var licename = k.GetValue(BaseUtil.LICENSE_REG);
                var sertype = k.GetValue(BaseUtil.SERVERTYPE_REG);
                var sapuser = k.GetValue(BaseUtil.USERSAP_REG);


                k.Close();

                obj = new Configuration_View();

                obj.CONNECTTYPE = conntype.ToString();
                obj.SERVER = servername.ToString();
                obj.LICENSENAME = licename.ToString();
                obj.DATABASE = dbname.ToString();
                obj.DBUSER = userdb.ToString();
                obj.DBPASS = passdb.ToString();
                obj.SERVERTYPE = sertype.ToString();
                obj.SAPUSER = sapuser.ToString();
                obj.SAPPASS = sappass.ToString();
            }

            return obj;
        }

        public void REG_SERVINTRVAL_CREATE(object servintrval)
        {
            var k = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software", true);
            if (k == null)
                k = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("Software");
            k = k.CreateSubKey(ROOT_REG, Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree);
            k = k.CreateSubKey(PROJ_REG, Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree);

            if (!isNumeric(servintrval) || Convert.ToInt32(servintrval) < 0)
            {
                servintrval = 1;
            }

            k.SetValue(SERVINTRVAL_REG, servintrval);


            k.Close();
        }

        public Configuration_View REG_SERVINTRVAL_GET()
        {
            Configuration_View obj = null;

            Microsoft.Win32.RegistryKey k;
            k = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("Software", true);
            if (k != null)
                k = k.OpenSubKey(BaseUtil.ROOT_REG, true);
            if (k != null)
                k = k.OpenSubKey(BaseUtil.PROJ_REG, true);

            if (k != null)
            {
                var servintrval = k.GetValue(BaseUtil.SERVINTRVAL_REG);
                k.Close();

                obj = new Configuration_View();
                obj.INTERVAL = Convert.ToInt32(servintrval);
            }

            return obj;
        }

        public void WriteLogFile(string funcname, String s)
        {
            if (funcname != null && funcname.Trim() != "")
            {
                WriteLogXml(new Log_View() { Description = string.Format("[{0}]{1}", funcname, s) });
            }
            else
            {
                WriteLogXml(new Log_View() { Description = s });
            }
        }

        public bool CheckMessageError(string message)
        {
            if (message != null && message.Trim() != "")
            {
                return true;
            }
            return false;
        }

        public bool CheckMessageError(string funcname, string message)
        {
            if (message != null && message.Trim() != "")
            {
                WriteLogFile(funcname, message);
                return true;
            }
            return false;
        }

        public static bool isNumeric(object s)
        {
            try
            {
                decimal d = Convert.ToDecimal(s);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public decimal SetTruncateNumber(object value)
        {
            try
            {
                var y = Convert.ToDecimal(value);
                var x = Math.Truncate(y);
                return x;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        #region Common
        public string FunctionGennerate(string funcname, string message)
        {
            return string.Format("[{0}]{1}", funcname, message);
        }
        #endregion

        #region XML
        public void WriteLogXml(Log_View obj)
        {
            //<?xml version="1.0" encoding="utf-8" ?>
            //<root>
            //  <log date="2013-07-15">
            //    <row>
            //      <database>DB001</database>
            //      <time>17:00</time>
            //      <description></description>
            //      <read>Y/N</read>
            //      <manual>Y/N</manual>
            //    </row>
            //    <row></row>
            //  </log>
            //</root>

            if (obj == null) return;

            if (!Directory.Exists(_serviceLogpath))
                Directory.CreateDirectory(_serviceLogpath);

            if (!File.Exists(_serviceXmlLogFile))
            {
                XElement root = new XElement("root",
                    new XElement("log",
                        new XElement("row",
                            new XElement("time", DateTime.Now.ToString(_formattime)),
                            new XElement("description", obj.Description),
                            new XElement("userid", obj.UserId),
                            new XElement("read")
                            )));
                root.Element("log").Add(new XAttribute("date", DateTime.Now.ToString(_formatdate_ymd)));

                root.Save(_serviceXmlLogFile);
            }
            else
            {
                XDocument xDoc = XDocument.Load(_serviceXmlLogFile);

                var xxx = (from x in xDoc.Descendants("log")
                           where x.Attribute("date").Value == DateTime.Today.ToString(_formatdate_ymd)
                           select x).FirstOrDefault();

                if (xxx != null)
                {

                    XElement root = new XElement("row",
                            new XElement("time", DateTime.Now.ToString(_formattime)),
                            new XElement("description", obj.Description),
                            new XElement("userid", obj.UserId),
                            new XElement("read")
                              );

                    xxx.Add(root);

                }
                else
                {
                    XElement root = new XElement("log",
                          new XElement("row",
                            new XElement("time", DateTime.Now.ToString(_formattime)),
                            new XElement("description", obj.Description),
                            new XElement("userid", obj.UserId),
                            new XElement("read")
                              ));
                    root.Add(new XAttribute("date", DateTime.Now.ToString(_formatdate_ymd)));
                    xDoc.Element("root").Add(root);
                }

                xDoc.Save(_serviceXmlLogFile);
            }

        }

        public List<Log_View> ReadLogXml(DateTime? date)
        {
            try
            {
                string d = "";
                if (date.HasValue)
                    d = date.Value.ToString(_formatdate_ymd);

                XDocument xDoc = XDocument.Load(_serviceXmlLogFile);
                if (xDoc != null)
                {
                    var xxx = (from x in xDoc.Descendants("log").Descendants("row")
                               where x.Parent.Attribute("date").Value == d || d == ""
                               select new Log_View
                               {
                                   Date = string.IsNullOrEmpty(x.Parent.Attribute("date").Value) ? DateTime.MinValue : Convert.ToDateTime(x.Parent.Attribute("date").Value),
                                   Time = string.IsNullOrEmpty(x.Element("time").Value) ? TimeSpan.MinValue : TimeSpan.Parse(x.Element("time").Value),
                                   Description = x.Element("description").Value,
                                   Read = string.IsNullOrEmpty(x.Element("read").Value) ? false : Convert.ToBoolean(x.Element("read").Value),
                                   UserId = x.Element("userid").Value,
                               }).ToList();

                    return xxx;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void ClearLog()
        {
            if (File.Exists(_serviceXmlLogFile))
            {
                //File.Delete(_serviceXmlLogFile);

                XDocument xDoc = XDocument.Load(_serviceXmlLogFile);

                xDoc.Element("root").RemoveNodes();
                xDoc.Save(_serviceXmlLogFile);
            }
        }

        public void DeleteLog(DateTime? date)
        {
            try
            {
                if (File.Exists(_serviceXmlLogFile))
                {
                    string d = "";
                    if (date.HasValue)
                        d = date.Value.ToString(_formatdate_ymd);

                    XDocument xDoc = XDocument.Load(_serviceXmlLogFile);
                    if (xDoc != null)
                    {
                        var xxx = (from x in xDoc.Descendants("log")
                                   where x.Attribute("date").Value == d || d == ""
                                   select x).ToList();


                        if (xxx != null && xxx.Count > 0)
                        {
                            foreach (var x in xxx)
                            {
                                x.Remove();
                            }
                        }

                        xDoc.Save(_serviceXmlLogFile);
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }
        #endregion

        public DataTable ListToDataTable<T>(List<T> DataObject)
        {
            DataTable result = new DataTable();

            PropertyInfo[] oProps = null;
            if (DataObject == null) return null;


            foreach (T line in DataObject)
            {
                if (oProps == null)
                {
                    oProps = ((Type)line.GetType()).GetProperties();

                    foreach (PropertyInfo pi in oProps)
                    {
                        Type colType = pi.PropertyType;
                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                            colType = colType.GetGenericArguments()[0];
                        result.Columns.Add(new DataColumn(pi.Name, colType));
                    }
                }

                DataRow dr = result.NewRow();
                foreach (PropertyInfo pi in oProps)
                {
                    object value = pi.GetValue(line, null) == null ? DBNull.Value : pi.GetValue
                    (line, null);
                    dr[pi.Name] = value;
                }
                result.Rows.Add(dr);
            }

            return result;
        }

        public DataTable ListToDataTable<T>(T LineObject)
        {
            DataTable result = new DataTable();

            PropertyInfo[] oProps = null;
            if (LineObject == null) return null;

            if (oProps == null)
            {
                oProps = ((Type)LineObject.GetType()).GetProperties();

                foreach (PropertyInfo pi in oProps)
                {
                    Type colType = pi.PropertyType;
                    if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                        colType = colType.GetGenericArguments()[0];
                    result.Columns.Add(new DataColumn(pi.Name, colType));
                }
            }

            DataRow dr = result.NewRow();
            foreach (PropertyInfo pi in oProps)
            {
                object value = pi.GetValue(LineObject, null) == null ? DBNull.Value : pi.GetValue
                (LineObject, null);
                dr[pi.Name] = value;
            }
            result.Rows.Add(dr);


            return result;
        }

        public DataTable comGetDatafromExcel(string filePath, out string sMessage)
        {
            sMessage = "";
            try
            {
                string ConnectionStringExcel = null;


                //Huan 17-5-2013 
                RegistryKey localMachine = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Office");
                string version = string.Empty;

                if (localMachine == null)
                {
                    localMachine = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Office");
                    if (localMachine == null)
                    {
                        sMessage = "Machine no office.";
                        return null;
                    }
                }
                string key = "11.0";

                foreach (string k in localMachine.GetSubKeyNames())
                {
                    if (isNumeric(k) && Convert.ToDecimal(k) > Convert.ToDecimal(key))
                    {
                        key = k;
                    }

                }

                if (key == "7.0")
                {
                    version = "1995";
                }
                else if (key == "8.0")
                {
                    version = "1997";
                }
                else if (key == "9.0")
                {
                    version = "2000";
                }
                else if (key == "10.0")
                {
                    version = "XP";
                }
                else if (key == "11.0")
                {
                    version = "2003";
                }
                else if (key == "12.0")
                {
                    version = "2007";
                }
                else if (key == "14.0")
                {
                    version = "2010";
                }
                else if (key == "15.0")
                {
                    version = "2013";
                }

                if (version == "2010" || version == "2007" || version == "2013")
                {
                    ConnectionStringExcel = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 12.0;", filePath);
                }
                else
                {
                    if (version == "2003")
                    {
                        ConnectionStringExcel = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=Excel 8.0;", filePath);
                    }
                }


                //Connect
                System.Data.OleDb.OleDbConnection connExcel = null;

                connExcel = new System.Data.OleDb.OleDbConnection(ConnectionStringExcel);


                if (connExcel.State == ConnectionState.Closed)
                {
                    connExcel.Open();
                }

                DataSet ds = new DataSet();

                DataTable table = connExcel.GetSchema("tables");
                if (table != null && table.Rows.Count > 0)
                {
                    dynamic sheetname = table.Rows[0]["table_name"];
                    System.Data.OleDb.OleDbDataAdapter da = new System.Data.OleDb.OleDbDataAdapter(string.Format("SELECT * FROM [{0}]", sheetname), connExcel);
                    da.Fill(ds);
                }

                connExcel.Close();
                if (ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
                return null;

            }
            catch (Exception ex)
            {
                sMessage = ex.Message;
                throw ex;
            }

        }

        public T ParseEnum<T>(string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        public DateTime? ParseShortDate(object value)
        {
            try
            {
                var d = Convert.ToDateTime(value);
                return d.Date;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public decimal RoundNum(decimal value)
        {
            try
            {
                var d = Math.Round(value, MidpointRounding.AwayFromZero); //.1 > .4 => .0; .5 > .9 => 1
                return d;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        #region Amount in Word
        private string[] ChuSo = new string[10] { " không", " một", " hai", " ba", " bốn", " năm", " sáu", " bẩy", " tám", " chín" };
        private string[] Tien = new string[6] { "", " nghìn", " triệu", " tỷ", " nghìn tỷ", " triệu tỷ" };
        // Hàm đọc số thành chữ
        public string DocTienBangChu(long SoTien, string strTail)
        {
            int lan, i;
            long so;
            string KetQua = "", tmp = "";
            int[] ViTri = new int[6];
            if (SoTien < 0) return "Số tiền âm !";
            if (SoTien == 0) return "Không đồng !";
            if (SoTien > 0)
            {
                so = SoTien;
            }
            else
            {
                so = -SoTien;
            }
            //Kiểm tra số quá lớn
            if (SoTien > 8999999999999999)
            {
                SoTien = 0;
                return "";
            }
            ViTri[5] = (int)(so / 1000000000000000);
            so = so - long.Parse(ViTri[5].ToString()) * 1000000000000000;
            ViTri[4] = (int)(so / 1000000000000);
            so = so - long.Parse(ViTri[4].ToString()) * +1000000000000;
            ViTri[3] = (int)(so / 1000000000);
            so = so - long.Parse(ViTri[3].ToString()) * 1000000000;
            ViTri[2] = (int)(so / 1000000);
            ViTri[1] = (int)((so % 1000000) / 1000);
            ViTri[0] = (int)(so % 1000);
            if (ViTri[5] > 0)
            {
                lan = 5;
            }
            else if (ViTri[4] > 0)
            {
                lan = 4;
            }
            else if (ViTri[3] > 0)
            {
                lan = 3;
            }
            else if (ViTri[2] > 0)
            {
                lan = 2;
            }
            else if (ViTri[1] > 0)
            {
                lan = 1;
            }
            else
            {
                lan = 0;
            }
            for (i = lan; i >= 0; i--)
            {
                tmp = DocSo3ChuSo(ViTri[i]);
                KetQua += tmp;
                if (ViTri[i] != 0) KetQua += Tien[i];
                if ((i > 0) && (!string.IsNullOrEmpty(tmp))) KetQua += ",";//&& (!string.IsNullOrEmpty(tmp))
            }
            if (KetQua.Substring(KetQua.Length - 1, 1) == ",") KetQua = KetQua.Substring(0, KetQua.Length - 1);
            KetQua = KetQua.Trim() + strTail;
            return KetQua.Substring(0, 1).ToUpper() + KetQua.Substring(1);
        }
        // Hàm đọc số có 3 chữ số
        private string DocSo3ChuSo(int baso)
        {
            int tram, chuc, donvi;
            string KetQua = "";
            tram = (int)(baso / 100);
            chuc = (int)((baso % 100) / 10);
            donvi = baso % 10;
            if ((tram == 0) && (chuc == 0) && (donvi == 0)) return "";
            if (tram != 0)
            {
                KetQua += ChuSo[tram] + " trăm";
                if ((chuc == 0) && (donvi != 0)) KetQua += " linh";
            }
            if ((chuc != 0) && (chuc != 1))
            {
                KetQua += ChuSo[chuc] + " mươi";
                if ((chuc == 0) && (donvi != 0)) KetQua = KetQua + " linh";
            }
            if (chuc == 1) KetQua += " mười";
            switch (donvi)
            {
                case 1:
                    if ((chuc != 0) && (chuc != 1))
                    {
                        KetQua += " mốt";
                    }
                    else
                    {
                        KetQua += ChuSo[donvi];
                    }
                    break;
                case 5:
                    if (chuc == 0)
                    {
                        KetQua += ChuSo[donvi];
                    }
                    else
                    {
                        KetQua += " lăm";
                    }
                    break;
                default:
                    if (donvi != 0)
                    {
                        KetQua += ChuSo[donvi];
                    }
                    break;
            }
            return KetQua;
        }
        #endregion

        public string Connection_String(BaseUtil.DataConnection dataconn)
        {
            try
            {
                var obj = RegConn_Get(dataconn);
                if (obj != null)
                {
                    BaseUtil._localconfig = obj;
                    var sConn = "";

                    if (obj.CONNECTTYPE == ConnectType.Microsoft_SQL_Server.ToString())
                        sConn = string.Format("data source={0}; Initial Catalog={1};user id={2};password={3};", obj.SERVER, obj.DATABASE, obj.DBUSER, obj.DBPASS);
                    else if (obj.CONNECTTYPE == ConnectType.Oracle_Database.ToString())
                        sConn = string.Format("data source={0}; user id={1};password={2};", obj.SERVER, obj.DBUSER, obj.DBPASS);

                    return sConn;
                }

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "";
        }

        #region Language
        public List<Language_View> GetLanguage()
        {
            try
            {
                List<Language_View> result = null;

                string source = System.AppDomain.CurrentDomain.BaseDirectory + _language_list_path;
                if (File.Exists(source))
                {
                    result = (from x in XElement.Load(source).Elements("Language")
                              select new Language_View
                              {
                                  Code = x.Element("Code").Value,
                                  Code2 = x.Element("Code2").Value,
                                  Name = x.Element("Name").Value,
                                  ForeignName = x.Element("ForeignName").Value,
                                  FlagImg = x.Element("FlagImg").Value,
                                  IsDefault = bool.Parse(x.Element("IsDefault").Value)

                              }).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Language_View GetLanguage(String LanguageCode)
        {
            try
            {
                Language_View result = null;

                string source = System.AppDomain.CurrentDomain.BaseDirectory + _language_list_path;

                if (File.Exists(source))
                {
                    result = (from x in XElement.Load(source).Elements("Language")
                              where x.Element("Code").Value == LanguageCode
                              select new Language_View
                              {
                                  Code = x.Element("Code").Value,
                                  Code2 = x.Element("Code2").Value,
                                  Name = x.Element("Name").Value,
                                  FlagImg = x.Element("FlagImg").Value

                              }).FirstOrDefault();
                }

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Caption_view GetCaption(string languagecode)
        {
            try
            {
                Caption_view result = null;
                Language_View language = null;

                language = GetLanguage(languagecode);

                if (language != null)
                {
                    var source = System.AppDomain.CurrentDomain.BaseDirectory + String.Format(_language_path, language.Code);
                    if (File.Exists(source))
                    {
                        result = (from x in XElement.Load(source).Elements("caption")
                                  select new Caption_view
                                  {
                                      PosForItmGroup = x.Element("PosForItmGroup").Value,
                                      PosCode = x.Element("PosCode").Value,
                                      PosName = x.Element("PosName").Value,
                                      GroupCode = x.Element("GroupCode").Value,
                                      GroupName = x.Element("GroupName").Value,

                                      PosForUsers = x.Element("PosForUsers").Value,

                                      EventBirthday = x.Element("EventBirthday").Value,
                                      ExtsAmount = x.Element("ExtsAmount").Value,
                                      DepositAmount = x.Element("DepositAmount").Value,
                                      BalanceAmount = x.Element("BalanceAmount").Value,
                                      Deposit = x.Element("Deposit").Value,
                                      AddChild = x.Element("AddChild").Value,
                                      AddItem = x.Element("AddItem").Value,
                                      FinishEvent = x.Element("FinishEvent").Value,
                                      ImportFile = x.Element("ImportFile").Value,

                                      GoodsIssList = x.Element("GoodsIssList").Value,
                                      CancelIss = x.Element("CancelIss").Value,

                                      GoodsRcptList = x.Element("GoodsRcptList").Value,
                                      CancelRcpt = x.Element("CancelRcpt").Value,

                                      Reconcile = x.Element("Reconcile").Value,
                                      Couting = x.Element("Couting").Value,

                                      InvCountingList = x.Element("InvCountingList").Value,
                                      RequestDate = x.Element("RequestDate").Value,

                                      RequestQty = x.Element("RequestQty").Value,
                                      CloseRequest = x.Element("CloseRequest").Value,
                                      InvTrsfRqstList = x.Element("InvTrsfRqstList").Value,

                                      GoodsIssEventList = x.Element("GoodsIssEventList").Value,
                                      BarCode = x.Element("BarCode").Value,
                                      DefaultWhs = x.Element("DefaultWhs").Value,
                                      WhsName = x.Element("WhsName").Value,
                                      OnHand = x.Element("OnHand").Value,

                                      PosForNumberList = x.Element("PosForNumberList").Value,
                                      IsPoint = x.Element("IsPoint").Value,

                                      ReportsParameter = x.Element("ReportsParameter").Value,
                                      StoreName = x.Element("StoreName").Value,

                                      PosList = x.Element("PosList").Value,
                                      PrinterName = x.Element("PrinterName").Value,
                                      HardwareId = x.Element("HardwareId").Value,

                                      RcptForEventList = x.Element("RcptForEventList").Value,

                                      RcptFrmTrnsitList = x.Element("RcptFrmTrnsitList").Value,

                                      DepositList = x.Element("DepositList").Value,

                                      ShipmentList = x.Element("ShipmentList").Value,

                                      TrsftiNiTourList = x.Element("TrsftiNiTourList").Value,

                                      TrsfToTrnsit = x.Element("TrsfToTrnsit").Value,
                                      TrsfToTrnsitList = x.Element("TrsfToTrnsitList").Value,

                                      CashInOut = x.Element("CashInOut").Value,
                                      CurrentCashBalance = x.Element("CurrentCashBalance").Value,
                                      SalesAmt = x.Element("SalesAmt").Value,
                                      BeginAtTimeIn = x.Element("BeginAtTimeIn").Value,
                                      CashInAmt = x.Element("CashInAmt").Value,
                                      CashOutAmt = x.Element("CashOutAmt").Value,
                                      SalesHistory = x.Element("SalesHistory").Value,
                                      ReceiptNo = x.Element("ReceiptNo").Value,
                                      BaseOn = x.Element("BaseOn").Value,
                                      Point = x.Element("Point").Value,
                                      ShiftReport = x.Element("ShiftReport").Value,
                                      BeginDate = x.Element("BeginDate").Value,
                                      ShiftId = x.Element("ShiftId").Value,
                                      DocTotal = x.Element("DocTotal").Value,
                                      CashAmt = x.Element("CashAmt").Value,
                                      VoucherAmt = x.Element("VoucherAmt").Value,
                                      CrCardAmt = x.Element("CrCardAmt").Value,
                                      MCardAmt = x.Element("MCardAmt").Value,
                                      CouponAmt = x.Element("CouponAmt").Value,
                                      Topup = x.Element("Topup").Value,
                                      ReceiptAmt = x.Element("ReceiptAmt").Value,
                                      ChangeAmt = x.Element("ChangeAmt").Value,
                                      Total = x.Element("Total").Value,
                                      Refund = x.Element("Refund").Value,
                                      RefundCard = x.Element("RefundCard").Value,
                                      PosScreen = x.Element("PosScreen").Value,
                                      TopupHistory = x.Element("TopupHistory").Value,
                                      ClearScreen = x.Element("ClearScreen").Value,
                                      EndShift = x.Element("EndShift").Value,
                                      isSync = x.Element("isSync").Value,

                                      /// <summary>=x.Element("        /// <summary>").Value,
                                      /// =x.Element("        /// ").Value,
                                      /// </summary>=x.Element("        /// </summary>").Value,
                                      OpenItem = x.Element("OpenItem").Value,
                                      History = x.Element("History").Value,
                                      RePrint = x.Element("RePrint").Value,
                                      RePrintCanBill = x.Element("RePrintCanBill").Value,
                                      CancelBill = x.Element("CancelBill").Value,
                                      EnterBarcode = x.Element("EnterBarcode").Value,
                                      CategoryName = x.Element("CategoryName").Value,
                                      SubTotal = x.Element("SubTotal").Value,
                                      Remain = x.Element("Remain").Value,
                                      CreditCard = x.Element("CreditCard").Value,
                                      Voucher = x.Element("Voucher").Value,
                                      Coupon = x.Element("Coupon").Value,
                                      Cash = x.Element("Cash").Value,
                                      MCard = x.Element("MCard").Value,
                                      Complete = x.Element("Complete").Value,
                                      DeleteItem = x.Element("DeleteItem").Value,
                                      DeleteCard = x.Element("DeleteCard").Value,
                                      SalesData = x.Element("SalesData").Value,
                                      CrCardNo = x.Element("CrCardNo").Value,
                                      Bank = x.Element("Bank").Value,
                                      BankName = x.Element("BankName").Value,
                                      tiNiStore = x.Element("tiNiStore").Value,
                                      tiNiPoint = x.Element("tiNiPoint").Value,

                                      Configuration = x.Element("Configuration").Value,
                                      SerivceInterval = x.Element("SerivceInterval").Value,
                                      Second = x.Element("Second").Value,
                                      Server = x.Element("Server").Value,
                                      LicenseName = x.Element("LicenseName").Value,
                                      Database = x.Element("Database").Value,
                                      DBUser = x.Element("DBUser").Value,
                                      DBPass = x.Element("DBPass").Value,
                                      ServerType = x.Element("ServerType").Value,
                                      CompnyUser = x.Element("CompnyUser").Value,
                                      CompnyPass = x.Element("CompnyPass").Value,
                                      TestConnect = x.Element("TestConnect").Value,
                                      CusFather = x.Element("CusFather").Value,
                                      PhoneNo = x.Element("PhoneNo").Value,
                                      IDNumber = x.Element("IDNumber").Value,

                                      ImportCard = x.Element("ImportCard").Value,

                                      Import = x.Element("Import").Value,
                                      NewCustomer = x.Element("NewCustomer").Value,
                                      CustomerInfo = x.Element("CustomerInfo").Value,

                                      InitialSetting = x.Element("InitialSetting").Value,
                                      Initial = x.Element("Initial").Value,
                                      Install = x.Element("Install").Value,
                                      InitialFuncList = x.Element("InitialFuncList").Value,
                                      SyncSetting = x.Element("SyncSetting").Value,
                                      Value = x.Element("Value").Value,
                                      Description = x.Element("Description").Value,
                                      AddtionMileage = x.Element("AddtionMileage").Value,
                                      VIPMileageSetting = x.Element("VIPMileageSetting").Value,
                                      VIKMileageSetting = x.Element("VIKMileageSetting").Value,

                                      Keyboard = x.Element("Keyboard").Value,
                                      Date = x.Element("Date").Value,
                                      Time = x.Element("Time").Value,
                                      User = x.Element("User").Value,
                                      Error = x.Element("Error").Value,
                                      Warning = x.Element("Warning").Value,
                                      Information = x.Element("Information").Value,
                                      SystemMessageLog = x.Element("SystemMessageLog").Value,
                                      LastMessageDisplay = x.Element("LastMessageDisplay").Value,

                                      ShowLog = x.Element("ShowLog").Value,
                                      Manual = x.Element("Manual").Value,
                                      Automatic = x.Element("Automatic").Value,
                                      Source = x.Element("Source").Value,
                                      WorkingFile = x.Element("WorkingFile").Value,
                                      WorkingPath = x.Element("WorkingPath").Value,

                                      OnServer = x.Element("OnServer").Value,
                                      MachineCode = x.Element("MachineCode").Value,
                                      MachineName = x.Element("MachineName").Value,
                                      HostName = x.Element("HostName").Value,
                                      TempPath = x.Element("TempPath").Value,
                                      BackupPath = x.Element("BackupPath").Value,
                                      FptServer = x.Element("FptServer").Value,

                                      RunManual = x.Element("RunManual").Value,
                                      SourceGroup = x.Element("SourceGroup").Value,
                                      SourceTable = x.Element("SourceTable").Value,
                                      Run = x.Element("Run").Value,

                                      ScheduleType = x.Element("ScheduleType").Value,
                                      OneTimeOccurence = x.Element("OneTimeOccurence").Value,
                                      Frequence = x.Element("Frequence").Value,
                                      Occurs = x.Element("Occurs").Value,
                                      RecursEvery = x.Element("RecursEvery").Value,
                                      DaysOrWeeksOn = x.Element("DaysOrWeeksOn").Value,

                                      Monday = x.Element("Monday").Value,
                                      Tuesday = x.Element("Tuesday").Value,
                                      Wednesday = x.Element("Wednesday").Value,
                                      Thursday = x.Element("Thursday").Value,
                                      Friday = x.Element("Friday").Value,
                                      Saturday = x.Element("Saturday").Value,
                                      Sunday = x.Element("Sunday").Value,

                                      DailyFrequency = x.Element("DailyFrequency").Value,
                                      OccurOneAt = x.Element("OccurOneAt").Value,
                                      OccurEvery = x.Element("OccurEvery").Value,
                                      StartAt = x.Element("StartAt").Value,
                                      StartEnd = x.Element("StartEnd").Value,
                                      Duration = x.Element("Duration").Value,
                                      StartDate = x.Element("StartDate").Value,
                                      EndDate = x.Element("EndDate").Value,
                                      NoEndDate = x.Element("NoEndDate").Value,
                                      Summary = x.Element("Summary").Value,

                                      ConnectonInfo = x.Element("ConnectonInfo").Value,
                                      ServiceLog = x.Element("ServiceLog").Value,
                                      Hide = x.Element("Hide").Value,
                                      DataSyncLog = x.Element("DataSyncLog").Value,
                                      Start = x.Element("Start").Value,
                                      Stop = x.Element("Stop").Value,
                                      Restart = x.Element("Restart").Value,
                                      ServiceManager = x.Element("ServiceManager").Value,







                                      ItemInValid = x.Element("ItemInValid").Value,
                                      Meassage = x.Element("Meassage").Value,
                                      FilterBy = x.Element("FilterBy").Value,


                                      RePassword = x.Element("RePassword").Value,
                                      UserGroup = x.Element("UserGroup").Value,
                                      CashierId = x.Element("CashierId").Value,


                                      AdditionValue = x.Element("AdditionValue").Value,
                                      AdditionPercent = x.Element("AdditionPercent").Value,
                                      LineId = x.Element("LineId").Value,
                                      Grade = x.Element("Grade").Value,
                                      CardType = x.Element("CardType").Value,
                                      GradeCode = x.Element("GradeCode").Value,
                                      GradeName = x.Element("GradeName").Value,
                                      Inactive = x.Element("Inactive").Value,
                                      AdditionSettingDetail = x.Element("AdditionSettingDetail").Value,
                                      Deny = x.Element("Deny").Value,
                                      ReadOnly = x.Element("ReadOnly").Value,
                                      FullControl = x.Element("FullControl").Value,
                                      Group = x.Element("Group").Value,
                                      Store = x.Element("Store").Value,
                                      StoreCode = x.Element("StoreCode").Value,
                                      CardNum = x.Element("CardNum").Value,
                                      Mileage = x.Element("Mileage").Value,
                                      CancelTopup = x.Element("CancelTopup").Value,
                                      Enter = x.Element("Enter").Value,
                                      Balance = x.Element("Balance").Value,

                                      //Common 1                             =x.Element("        //Common 1                             ").Value,
                                      Title = x.Element("Title").Value,
                                      Detail = x.Element("Detail").Value,
                                      Search = x.Element("Search").Value,
                                      ItemSearch = x.Element("ItemSearch").Value,
                                      CustomerSearch = x.Element("CustomerSearch").Value,
                                      Clear = x.Element("Clear").Value,
                                      Add = x.Element("Add").Value,
                                      AddNew = x.Element("AddNew").Value,
                                      Save = x.Element("Save").Value,
                                      SaveAsDraft = x.Element("SaveAsDraft").Value,
                                      Lock = x.Element("Lock").Value,
                                      Edit = x.Element("Edit").Value,
                                      Delete = x.Element("Delete").Value,
                                      View = x.Element("View").Value,
                                      ViewCancel = x.Element("ViewCancel").Value,
                                      Ok = x.Element("Ok").Value,
                                      Cancel = x.Element("Cancel").Value,
                                      Close = x.Element("Close").Value,
                                      Back = x.Element("Back").Value,
                                      List = x.Element("List").Value,
                                      Creation = x.Element("Creation").Value,
                                      Confirm = x.Element("Confirm").Value,
                                      Remarks = x.Element("Remarks").Value,
                                      Status = x.Element("Status").Value,
                                      DocStatus = x.Element("DocStatus").Value,
                                      ConfirmStatus = x.Element("ConfirmStatus").Value,
                                      Print = x.Element("Print").Value,
                                      Choose = x.Element("Choose").Value,
                                      Apply = x.Element("Apply").Value,
                                      Exit = x.Element("Exit").Value,
                                      FunctionRights = x.Element("FunctionRights").Value,
                                      ReportRights = x.Element("ReportRights").Value,

                                      //Common 2=x.Element("        //Common 2").Value,
                                      Code = x.Element("Code").Value,
                                      Name = x.Element("Name").Value,
                                      Period = x.Element("Period").Value,
                                      Distributor = x.Element("Distributor").Value,
                                      DistributorCode = x.Element("DistributorCode").Value,
                                      DistributorName = x.Element("DistributorName").Value,
                                      MinValue = x.Element("MinValue").Value,
                                      Bonus = x.Element("Bonus").Value,
                                      RedInvNo = x.Element("RedInvNo").Value,
                                      Serial = x.Element("Serial").Value,
                                      DocType = x.Element("DocType").Value,
                                      Ward = x.Element("Ward").Value,


                                      //Common 3=x.Element("        //Common 3").Value,
                                      Birthday = x.Element("Birthday").Value,
                                      Province = x.Element("Province").Value,
                                      CityProvince = x.Element("CityProvince").Value,
                                      District = x.Element("District").Value,
                                      Address = x.Element("Address").Value,
                                      AddressDetail = x.Element("AddressDetail").Value,
                                      Gender = x.Element("Gender").Value,
                                      Fax = x.Element("Fax").Value,
                                      Email = x.Element("Email").Value,
                                      Website = x.Element("Website").Value,
                                      Area = x.Element("Area").Value,
                                      Street = x.Element("Street").Value,
                                      Market = x.Element("Market").Value,
                                      StreetMarket = x.Element("StreetMarket").Value,

                                      //Number=x.Element("        //Number").Value,
                                      DocKey = x.Element("DocKey").Value,
                                      DocNum = x.Element("DocNum").Value,
                                      RefVoucher = x.Element("RefVoucher").Value,

                                      //Paymnet=x.Element("        //Paymnet").Value,
                                      InvoiceNo = x.Element("InvoiceNo").Value,
                                      InvoiceAmt = x.Element("InvoiceAmt").Value,
                                      PaidAmount = x.Element("PaidAmount").Value,
                                      PendingAmount = x.Element("PendingAmount").Value,
                                      ThisTimePayment = x.Element("ThisTimePayment").Value,
                                      TotalPayment = x.Element("TotalPayment").Value,

                                      //Purchasing=x.Element("        //Purchasing").Value,
                                      Promotion = x.Element("Promotion").Value,
                                      PromotionType = x.Element("PromotionType").Value,
                                      PromotionCode = x.Element("PromotionCode").Value,
                                      Vendor = x.Element("Vendor").Value,
                                      VendorCode = x.Element("VendorCode").Value,
                                      VendorName = x.Element("VendorName").Value,
                                      DocNumber = x.Element("DocNumber").Value,
                                      OrderType = x.Element("OrderType").Value,
                                      CreditLimit = x.Element("CreditLimit").Value,
                                      ChooseOpenPO = x.Element("ChooseOpenPO").Value,
                                      PODate = x.Element("PODate").Value,
                                      PONo = x.Element("PONo").Value,
                                      Reason = x.Element("Reason").Value,
                                      PaymentType = x.Element("PaymentType").Value,

                                      //Sales=x.Element("        //Sales").Value,
                                      Customer = x.Element("Customer").Value,
                                      CustomerCode = x.Element("CustomerCode").Value,
                                      CustomerName = x.Element("CustomerName").Value,
                                      ShipTo = x.Element("ShipTo").Value,
                                      BillTo = x.Element("BillTo").Value,
                                      ApplyPromotion = x.Element("ApplyPromotion").Value,
                                      SODate = x.Element("SODate").Value,
                                      SONo = x.Element("SONo").Value,

                                      //Item=x.Element("        //Item").Value,
                                      ItemCode = x.Element("ItemCode").Value,
                                      ItemName = x.Element("ItemName").Value,
                                      ItemType = x.Element("ItemType").Value,
                                      Color = x.Element("Color").Value,
                                      Size = x.Element("Size").Value,
                                      Material = x.Element("Material").Value,
                                      Quantity = x.Element("Quantity").Value,
                                      UnitPrice = x.Element("UnitPrice").Value,
                                      Discount = x.Element("Discount").Value,
                                      DiscountPer = x.Element("DiscountPer").Value,
                                      DiscountAmt = x.Element("DiscountAmt").Value,
                                      TaxCode = x.Element("TaxCode").Value,
                                      TaxRate = x.Element("TaxRate").Value,
                                      Amount = x.Element("Amount").Value,
                                      Percent = x.Element("Percent").Value,
                                      InStockQty = x.Element("InStockQty").Value,
                                      Warehouse = x.Element("Warehouse").Value,
                                      OrderQty = x.Element("OrderQty").Value,
                                      OpenQty = x.Element("OpenQty").Value,
                                      ReceiptQty = x.Element("ReceiptQty").Value,
                                      DeliveryQty = x.Element("DeliveryQty").Value,
                                      CreditQty = x.Element("CreditQty").Value,
                                      ActualQty = x.Element("ActualQty").Value,
                                      DiffQty = x.Element("DiffQty").Value,
                                      ApproveQty = x.Element("ApproveQty").Value,
                                      LineTotal = x.Element("LineTotal").Value,

                                      //Total = x.Element("Total").Value,
                                      TotalBefDiscount = x.Element("TotalBefDiscount").Value,
                                      TotalDiscount = x.Element("TotalDiscount").Value,
                                      TotalAfDiscount = x.Element("TotalAfDiscount").Value,
                                      VatAmount = x.Element("VatAmount").Value,
                                      TotalAmount = x.Element("TotalAmount").Value,

                                      //Date=x.Element("        //Date").Value,
                                      PostingDate = x.Element("PostingDate").Value,
                                      DeliveryDate = x.Element("DeliveryDate").Value,
                                      ReceiptDate = x.Element("ReceiptDate").Value,
                                      DueDate = x.Element("DueDate").Value,
                                      InvoiceDate = x.Element("InvoiceDate").Value,
                                      DocumentDate = x.Element("DocumentDate").Value,
                                      DraftDate = x.Element("DraftDate").Value,
                                      ConfirmDate = x.Element("ConfirmDate").Value,
                                      FromDate = x.Element("FromDate").Value,
                                      ToDate = x.Element("ToDate").Value,

                                      //Phone=x.Element("        //Phone").Value,
                                      Phone = x.Element("Phone").Value,
                                      Phone1 = x.Element("Phone1").Value,
                                      Phone2 = x.Element("Phone2").Value,
                                      MobilePhone = x.Element("MobilePhone").Value,
                                      Telephone = x.Element("Telephone").Value,

                                      //Group Menu                                    =x.Element("        //Group Menu                                    ").Value,
                                      System = x.Element("System").Value,
                                      MasterData = x.Element("MasterData").Value,
                                      Purchasing = x.Element("Purchasing").Value,
                                      Sales = x.Element("Sales").Value,
                                      Inventory = x.Element("Inventory").Value,
                                      Reports = x.Element("Reports").Value,
                                      UserManagement = x.Element("UserManagement").Value,
                                      AdditionSetting = x.Element("AdditionSetting").Value,
                                      Authorization = x.Element("Authorization").Value,
                                      TopupFree = x.Element("TopupFree").Value,
                                      CardManagement = x.Element("CardManagement").Value,
                                      InvTrsfRqst = x.Element("InvTrsfRqst").Value,
                                      RcptFrmTrnsit = x.Element("RcptFrmTrnsit").Value,
                                      GoodsRcpt = x.Element("GoodsRcpt").Value,
                                      GoodsIss = x.Element("GoodsIss").Value,
                                      InvCounting = x.Element("InvCounting").Value,
                                      TrsftiNiTour = x.Element("TrsftiNiTour").Value,
                                      Shipment = x.Element("Shipment").Value,
                                      Event = x.Element("Event").Value,
                                      EventBirth = x.Element("EventBirth").Value,
                                      EventList = x.Element("EventList").Value,

                                      EventTO = x.Element("EventTO").Value,
                                      GoodsIssEvent = x.Element("GoodsIssEvent").Value,
                                      GoodsRcptEvent = x.Element("GoodsRcptEvent").Value,
                                      RcptVoucher = x.Element("RcptVoucher").Value,
                                      Items = x.Element("Items").Value,
                                      BusinessPartner = x.Element("BusinessPartner").Value,
                                      Users = x.Element("Users").Value,
                                      Pos = x.Element("Pos").Value,
                                      Transaction = x.Element("Transaction").Value,
                                      EndDay = x.Element("EndDay").Value,
                                      PosUser = x.Element("PosUser").Value,
                                      PosItmGrp = x.Element("PosItmGrp").Value,
                                      PosNumberLst = x.Element("PosNumberLst").Value,
                                      GeneralSalesRpt = x.Element("GeneralSalesRpt").Value,
                                      StockRpt = x.Element("StockRpt").Value,

                                      //Logon=x.Element("        //Logon").Value,
                                      Logon = x.Element("Logon").Value,
                                      Logout = x.Element("Logout").Value,
                                      ChangePass = x.Element("ChangePass").Value,
                                      Password = x.Element("Password").Value,
                                      UserId = x.Element("UserId").Value,
                                      Language = x.Element("Language").Value,
                                      CardNo = x.Element("CardNo").Value,

                                      //Change password=x.Element("        //Change password").Value,
                                      UserName = x.Element("UserName").Value,
                                      CurrentPass = x.Element("CurrentPass").Value,
                                      NewPass = x.Element("NewPass").Value,
                                      ReNewPass = x.Element("ReNewPass").Value,

                                      //Page=x.Element("        //Page").Value,
                                      First = x.Element("First").Value,
                                      Last = x.Element("Last").Value,
                                      Previous = x.Element("Previous").Value,
                                      Next = x.Element("Next").Value,
                                      PageSize = x.Element("PageSize").Value,
                                      Suspend = x.Element("Suspend").Value,
                                      Recall = x.Element("Recall").Value,
                                      SalesOnline = x.Element("SalesOnline").Value,
                                      Notes = x.Element("Notes").Value,
                                      ClearCustomer = x.Element("ClearCustomer").Value,

                                  }).FirstOrDefault();
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        public string FormatDateTimeZone(object date)
        {
            try
            {
                var d = Convert.ToDateTime(date).ToString(_formatdate_ymd_hmsz);
                return d;
            }
            catch (Exception)
            {

            }
            return "";
        }

        #region Flash Form

        // To support flashing.
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool FlashWindowEx(ref FLASHWINFO pwfi);

        //Flash both the window caption and taskbar button.
        //This is equivalent to setting the FLASHW_CAPTION | FLASHW_TRAY flags. 
        public const UInt32 FLASHW_ALL = 3;

        // Flash continuously until the window comes to the foreground. 
        public const UInt32 FLASHW_TIMERNOFG = 12;

        [StructLayout(LayoutKind.Sequential)]
        public struct FLASHWINFO
        {
            public UInt32 cbSize;
            public IntPtr hwnd;
            public UInt32 dwFlags;
            public UInt32 uCount;
            public UInt32 dwTimeout;
        }

        // Do the flashing - this does not involve a raincoat.
        public static bool FlashWindowEx(Form form)
        {
            IntPtr hWnd = form.Handle;
            FLASHWINFO fInfo = new FLASHWINFO();

            fInfo.cbSize = Convert.ToUInt32(Marshal.SizeOf(fInfo));
            fInfo.hwnd = hWnd;
            fInfo.dwFlags = FLASHW_ALL | FLASHW_TIMERNOFG;
            fInfo.uCount = UInt32.MaxValue;
            fInfo.dwTimeout = 0;

            return FlashWindowEx(ref fInfo);
        }


        #endregion

        public static string XmlGetNodeFirst(string xmlText, string node)
        {
            try
            {
                XmlDocument xdoc = new XmlDocument();
                xdoc.LoadXml(xmlText);
                XmlNodeList xnode = xdoc.GetElementsByTagName(node);
                var text = xnode[0].InnerText;
                return text;
            }
            catch (Exception)
            {

            }
            return "";
        }

        public static DateTime ConvertSAPDate(string date)
        {
            try
            {
                var d = Convert.ToDateTime(date.Substring(0, 4) + "-" + date.Substring(4, 2) + "-" + date.Substring(6, 2));
                return d;
            }
            catch (Exception)
            {

            }
            return Convert.ToDateTime(date);
        }
        
    }
}
