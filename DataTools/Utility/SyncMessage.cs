﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using Microsoft.Win32;

namespace DataTools.Utility
{
    partial class BaseUtil
    {
        public const string BackupFileFail = "Backup file fail.";
        public const string BackupFileSuccess = "Backup file successfully.";
        public const string DownloadFileFail = "Download file from server fail.";
        public const string ExtractFileFail = "Extract file fail.";
        public const string InvalidShop = "Invalid Shop. Please check info of shop in table SysConfLocal .";
        public const string NoPropertyinSource = "No have properties in source. ";
        public const string NotFoundFile = "Not found file.";
        public const string ReadFileError = "Can't read data from file.";
        public const string SyncInDataFail = "Sync data to database fail.";
        public const string SyncInDataSuccess = "Sync data to database success.";
        public const string SyncInInvalidSource = "Invalid source object.";
        public const string SyncInNoData = "No data to wite to database.";
        public const string SyncOutDataFail = "Sync data to file fail.";
        public const string SyncOutDataLinesFail = "Get data lines fail.";
        public const string SyncOutDataLinesFail_Sap = "[SAP Addon] Get data lines fail.";
        public const string SyncOutDataSuccess = "Sync data to file success.";
        public const string SyncOutInvalidSource = "Invalid source object.";
        public const string SyncOutNoData = "No data to wite to file.";
        public const string SystemError = "System error.";
        public const string UploadFileFail = "Upload file to server fail";
        public const string WriteFileError = "Can't wite data to file.";
        public const string ZipFileFail = "Zip file fail.";
        public const string GetDataEmpty = "Data empty. Please check data or query.";
    }
}
