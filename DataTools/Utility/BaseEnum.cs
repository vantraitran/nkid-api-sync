﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTools.Utility
{
    public partial class BaseUtil
    {
        public enum ProjectOne
        {
            ACFC
        }

        public enum ConnectType
        {
            Microsoft_SQL_Server = 0,
            Oracle_Database = 1,
            SAP_Business_One = 2
        }

        public enum eRights
        {
            /// <summary>
            /// Deny
            /// </summary>
            D,
            /// <summary>
            /// Read Only
            /// </summary>
            R,
            /// <summary>
            /// Full Control
            /// </summary>
            F,
        }
        public enum enumYesNo
        {
            /// <summary>
            /// No
            /// </summary>
            N = 0,
            /// <summary>
            /// Yes
            /// </summary>
            Y = 1,
        }


        public enum SyncObject
        {
            oChartOfAccounts = 1,
            oBusinessPartners = 2,
            oBanks = 3,
            oItems = 4,
            oInvoices = 13,
            oCreditNotes = 14,
            oPurchaseInvoices = 18,
            oPurchaseCreditNotes = 19,
            oIncomingPayments = 24,
            oJournalVouchers = 28,
            oJournalEntries = 30,
            oVendorPayments = 46,
            oItemGroups = 52,
            oInventoryGenExit = 60,
            oWarehouses = 64,
            oStockTransfer = 67,
            oDrafts = 112,
            oDownPayments = 203,
            oPurchaseDownPayments = 204,
            oProjects = 63,
            oSendMail = 9801, //Function ngoài SAP.
        }

        public enum eMessageType
        {
            None = 0,
            Error = 1,
            Warning = 2,
            Information = 3,
        }

        public enum eSyncType
        {
            ByFtp = 1,
            ByDirectSql = 2,
        }

        public enum eSyncSettingType
        {
            DataSql = 1,
            DataXml = 2,
        }

        public enum eLogonMessage
        {
            Error = -1,
            Success = 0,
            WrongUser = 1,
            WrongPassword = 2,
            Inactive = 3,
        }

        public enum eFuncGroup
        {
            None = 0,
            System = 1,
            PosTopup = 2,
            PosScreen = 3,
            PosManagement = 4,
            SystemManagement = 5,
            ClientManager = 6,
            ServerManager = 7,
            Report = 8,
        }

        public enum eBPType
        {
            C = 0,//Customer
            S = 1,//Supplier
            B = 2,//Birthday
        }

        public enum IntegrationSystemType
        {
            /// <summary>
            /// None
            /// </summary>
            N,
            /// <summary>
            /// SAP Business One
            /// </summary>
            S,
            /// <summary>
            /// Acumatica
            /// </summary>
            A,
        }

        public enum DataConnection
        {
            //FOR ONE CONNECTION
            NONE = -1,
            //FOR INTEGRATION
            CLIENTDB = 0,
            SERVERDB = 1,
        }

        public enum SyncStatus
        {
            NotYetTransfer = 0,
            Transfered = 1,
            Error = 2
        }

        public enum FrmStatus
        {
            Unknow = -1,
            Search = 0,
            Edit = 1,
            New = 2,
            Lock = 3,
        }

        public enum DocStatus
        {
            /// <summary>
            /// Open
            /// </summary>
            O,
            /// <summary>
            /// Closed
            /// </summary>
            C,
            /// <summary>
            /// Cancelled
            /// </summary>
            L,
        }

        public enum ApproveStatus
        {
            /// <summary>
            /// Pending
            /// </summary>
            P,
            /// <summary>
            /// Approved
            /// </summary>
            A,
            /// <summary>
            /// Rejected
            /// </summary>
            R,
        }


        public enum PaymentType
        {
            /// <summary>
            /// Memmber Card
            /// </summary>
            M,
            /// <summary>
            /// Voucher
            /// </summary>
            V,
            /// <summary>
            /// Credit Card
            /// </summary>
            Cr,
            /// <summary>
            /// Cash
            /// </summary>
            C,
            /// <summary>
            /// Point
            /// </summary>
            P
        }
    }
}
