﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DataTools.Models;

namespace DataTools.Utility
{
    public partial class BaseUtil
    {
        #region UpdateOne
        public static string _version_filename = "version.txt";
        public static string _online_version_filename = "online_version.txt";
        public static string _this_version = "1.001";
        public static string _update_url_download = "http://localhost:8081/";
        #endregion

        #region Configuration
        public static string _project_one = "ACFC";
        public static IntegrationSystemType IntegrationSystem = IntegrationSystemType.N;
        public static string _storeCode = "";
        public static string _storeName = "";
        public static string _storeAddress = "";
        public static string _storePhone = "";
        public static string _printername = "";

        /// <summary>
        /// Use For Inventory Transfer Request-->From Warehouse
        /// </summary>
        public static string _generalStore = "01";
        /// <summary>
        /// Transit Warehouse
        /// </summary>
        public static string _transitStore = "04";
        /// <summary>
        /// Warehouse for Sell Item
        /// </summary>
        public static string _mainStore = "02";

        public static string _onetimecustomer_code = "000000000000000";
        public static string _onetimecustomer_name = "One Time Customer";
        public static string _openitem_code = "OpenItem";
        public static string _use_openitem = "1";
        #endregion

        public const string _sql_script_foder = @"SqlScript";
        public const int _serviceInterval = 1000;
        private string _serviceLogpath = @"C:/icc.log/ACFC";
        private string _serviceXmlLogFile = @"C:/icc.log/ACFC/iccService.xml";

        private const string _language_list_path = @"/Language/Language.xml";
        private const string _language_path = @"/Language/Language.{0}.xml";

        public const string _formattime = "HH:mm:ss";

        public const string _formatdate_ymd = "yyyy-MM-dd";
        public const string _formatdate_ymd_hms = "yyyy-MM-dd HH:mm:ss";
        public const string _formatdate_ymd_hmsf = "yyyy-MM-dd HH:mm:ss.ff";
        public const string _formatdate_ymd_hmsz = "yyyy-MM-ddTHH:mm:sszzz";

        public const string _formatdate_dmy = "dd-MM-yyyy";
        public const string _formatdate_dmy_hms = "dd-MM-yyyy HH:mm:ss";

        public const string _formatdate_dmony_hmst = "dd-MMM-yyyy hh:mm:ss tt";

        public const string _formatdate_dMMy = "dd-MMM-yyyy";

        public const string _formatdate_sql = "yyyy-MM-dd";
        public const string _formatdate_sql_hms = "yyyy-MM-dd HH:mm:ss";

        public const string DateDefault = "1000-10-10";

        public const string _format_currency = "#,###";
        public const string _format_number = "#";

        public static string _languagecode = "vi";
        public static Caption_view _language_data = null;

        #region REGISTRY INFO
        public const string ROOT_REG = "ICC";

        public static string PROJ_REG = "ACFC";

        public const string SERVINTRVAL_REG = "servintrval";
        public const string CONN_REG = "Connection";

        public const string CONNNAME_CLIENTDB_REG = "client_connection";
        public const string CONNNAME_SERVERDB_REG = "server_connection";

        public const string CONNECTYPE_REG = "Microsoft_SQL_Server";
        public const string SERVER_REG = "server";
        public const string DATABASE_REG = "db";
        public const string USERDB_REG = "uidb";
        public const string PASSDB_REG = "pwddb";
        public const string LICENSE_REG = "licname";
        public const string USERSAP_REG = "uidsap";
        public const string PASSSAP_REG = "pwdsap";
        public const string SERVERTYPE_REG = "sertype";
        #endregion


        //Service
        public static string _integration_sync_service_name = "Project.Integration.Sync";
        public static string _integration_sync_service_processname = "ProjectSync";
        public static string _integration_sync_service_display = "Project Integration Synchronize";
        public static string _integration_sync_service_description = "Automatic synchronize data between POS and HUB.";

        //
        public const eSyncType _syncType = eSyncType.ByDirectSql;
        public const eSyncSettingType _syncSettingType = eSyncSettingType.DataXml;
        public const string _admin = "Admin";

        public static Configuration_View _localconfig = null;

        /// <summary>
        /// Get From User Logon
        /// </summary>
        public static string _userid = "Admin";
        public static string _username = "Administrator";
        public static string _userid2 = "test";
        public static string _username2 = "test";

        public static ConnectType _originalConnectType = ConnectType.Microsoft_SQL_Server;
        public static ConnectType _destinationConnectType = ConnectType.SAP_Business_One;
        public static DataConnection _dataConnection = DataConnection.NONE;

        public static string _originalDisplayConnectName = "Original Database";
        public static string _destinationDisplayConnectName = "Destination Database";
        public static string _product_key = "F5B3ACC2-6D0D-408D-9572-66A6A13DF0D8";
        public const string _msg_invalid_product_key = "[ICC Alert] Please upgrade to new version. Contact admin to be support.";
        public const string _msg_setting_connection = "[ICC Alert] Please settings connection to database. Contact admin to be support.";


    }
}
