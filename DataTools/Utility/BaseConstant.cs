﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTools
{
    public class BaseConstant
    {
        public static string _connection_string = null;
        public static ConnectionInfo_View _local_conn = null;
        public static string _printer_name = "";
        public static string _user_id = "admin";
        public static string SERVER = null;
        public static string DATABASE = null;
        public static string DBUSER = null;
        public static string DBPASS = null;
        public static string SERVERTYPE = null;
        public static string LICENSE = null;
        public static string SAPUSER = null;
        public static string SAPPASS = null;


        public const string _admin = "Admin";
        public const string _ini_section_connection = "ConnectionInfo";
        public const string _ini_section_sapconnection = "SAPConnectionInfo";

        public const string _format_date = "dd-MM-yyyy";
        public const string _format_time = "HH:mm:ss";
        public const string _format_datetime = "dd-MM-yyyy HH:mm:ss";
        public const string _format_amount = "#,###";
        public const string _format_quantity = "#,###";
        public const string _format_number = "#";
        public const string _format_day = "dd";
        public const string _format_hours = "HH:mm";

        public const string _format_date_db = "yyyy-MM-dd";
        public const string _format_datetime_db = "yyyy-MM-dd HH:mm:ss";
        public static string _ini_file = System.AppDomain.CurrentDomain.BaseDirectory + System.Windows.Forms.Application.ProductName + ".INI";

    }

    public class ConnectionInfo_View
    {
        public string DATABASE { get; set; }
        public string DBPASS { get; set; }
        public string DBUSER { get; set; }
        public int INTERVAL { get; set; }
        public string LICENSENAME { get; set; }
        public string SAPPASS { get; set; }
        public string SAPUSER { get; set; }
        public string SERVER { get; set; }
        public string SERVERTYPE { get; set; }
    }

    public class FIELDINFO_VIEW
    {
        public string TableName { get; set; }
        public int FieldId { get; set; }
        public string FieldName { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string EditType { get; set; }
        public int? Size { get; set; }
        public int? EditSize { get; set; }
        public bool isYesNo { get; set; }
    }

    public class SMD_VIEW
    {
        public string Code { get; set; }
        public object Code1 { get; set; }
        public string Name { get; set; }
    }
}
