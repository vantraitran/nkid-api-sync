﻿using System;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using System.Data.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using DataTools.Models;
using DataTools.Utility;

namespace DataTools.DataAccess.Sync.FtpSync
{
    public class SAPSyncFtpEntry : SyncBase
    {
        //int _syncLogId = 0;

        FtpRequestEntry _ftp = null;
        DataSync _syncdata = null;
        SettingSync _settingSync = null;

        //List<SyncObjectCurrent> _objLog = null;

        public SAPSyncFtpEntry()
        {
            _syncdata = new DataSync();

            _settingSync = new SettingSync();
        }

        #region public functions

        public List<SyncObjectCurrent> SyncData(List<SyncObjectCurrent> syncObjs)
        {
            //Clear Log
            //ClearSyncLog();

            if (syncObjs == null || syncObjs.Count == 0) return null;

            //_objLog = new List<SyncObjectCurrent>();

            //_syncLogId = SyncLog(syncObjs[0]);

            foreach (var obj in syncObjs)
            {
                if (obj.IsServer)
                {
                    var listMachine = _settingSync.LocalMachine_Get();
                    foreach (var o in listMachine)
                    {
                        obj.MachineCode = o.MachineCode;
                        obj.StoreCode = o.StoreCode;

                        if (obj.SyncIN)
                            SyncIN(obj);
                        else
                            SyncOUT(obj);
                    }
                }
                else
                {
                    if (obj.SyncIN)
                        DownloadFile(obj);
                    else
                        SyncOUT(obj);
                }

                //Van Linh: Edit    2013-03-14 11:00
                //obj.LastedSyncDate = DateTime.Now;
            }

            //return
            //return _objLog;
            return syncObjs;
        }

        #endregion

        #region private functions

        #region Sync Out & Upload to Server
        private void SyncOUT(SyncObjectCurrent obj)
        {
            string message;
            if (obj == null)
            {
                SyncSourceLog(obj, SyncStatus.SyncOutInvalidSource, null, null, null);
                return;
            }

            //var fileName = SyncTempPath(obj) + GetDataFile(obj);
            string workingPath = SyncTempPath(obj);
            string workingFiles = GetDataFile(obj);

            var fileName = workingPath + workingFiles;

            //Lasted Sync Date
            string lastedSyncDate = "1989-09-27";
            if (obj.LastedSyncDate.HasValue)
                lastedSyncDate = obj.LastedSyncDate.Value.ToString(_formatdate_ymd);


            //obj.WhsCode ==obj.ShopCode????
            var data = _syncdata.ExcuteDataSyncOut(lastedSyncDate, obj.MachineCode, obj.StoreCode, obj.SyncSource, true, obj.SyncBy, obj, out message);
            if (checkHasError(message))
            {
                SyncSourceLog(obj, SyncStatus.SyncOutDataFail, message, null, null);
                goto syncout;
            }
            else if (data == null || data.Rows.Count == 0)
            {
                SyncSourceLog(obj, SyncStatus.GetDataEmpty, message, null, null);
                goto syncout;
            }
            else
            {
                List<SyncDataObjectLine> dataLines = null;
                if (obj.IsHeader)
                {
                    //obj.WhsCode==obj.ShopCode????
                    dataLines = _syncdata.ExcuteDataLinesSyncOut(lastedSyncDate, obj.MachineCode, obj.StoreCode, obj.SyncSource, true, obj, out message);
                    if (checkHasError(message))
                    {
                        SyncSourceLog(obj, SyncStatus.SyncOutDataLinesFail, message, null, null);
                        goto syncout;
                    }
                }

                //Linh (Mr.) : Edit 2015-10-26
                obj.LastedSyncDate = _syncdata.dbServerDate(out message);
                if (checkHasError(message))
                {
                    SyncSourceLog(obj, SyncStatus.SyncOutDataLinesFail, message, null, null);
                    goto syncout;
                }

                //2013.02.27
                List<string> listQuery = new List<string>();

                if (obj.IsDeleteBefore && obj.IsServer)
                {
                    DeleteAllFile(workingPath, out message);
                    if (checkHasError(message))
                    {
                        SyncSourceLog(obj, SyncStatus.None, message, workingPath, null);
                        goto syncout;
                    }
                }

                bool checkSave = false;

                checkSave = SaveDataToXmlFile(data, dataLines, listQuery, obj.XmlElement, fileName, obj.IsServer, out message);

                if (checkSave == false)
                {
                    SyncSourceLog(obj, SyncStatus.SyncOutDataFail, message, workingPath, workingFiles);
                    goto syncout;
                }
                else
                {
                    var zf = GetFileNameNoExt(workingFiles) + ".zip";

                    var _zipfile = workingPath + zf;
                    if (ZipFile(_zipfile, fileName, out message))
                    {
                        SyncSourceLog(obj, SyncStatus.SyncOutDataSuccess, null, workingPath, zf);
                        File.Delete(fileName);
                    }
                    else
                        SyncSourceLog(obj, SyncStatus.ZipFileFail, message, workingPath, workingFiles);

                    if (_syncdata.LastedSyncDate_Update(obj.SyncSource, obj.LastedSyncDate, out message) == false)
                        goto syncout;

                }
            }

            syncout:
            //van linh  -   2013-05-02.
            if (!obj.IsServer)
                UploadFile(obj);
            else
            {
                //trong trường hợp , pos client download file successfully & backup file. Luc nay dưới client khó có thể xóa file backup mà trên server sẽ làm việc này......$$$$$$$$$
                DeleteBackupFile(workingPath + "backup", SYNCOUT_FILEBACKUP_DAYS, out message);
            }
        }

        /// <summary>
        /// Update to database with data sync out to file
        /// Chi update 1 so truong hop nhu Sales Data
        /// </summary>
        /// <param name="obj"></param>
        private void UpdateSource_SyncStatus(SyncObjectCurrent obj)
        {

        }

        private void UploadFile(SyncObjectCurrent obj)
        {
            string message = "";
            if (obj == null) return;

            var hostName = SyncFtpPath(obj);

            _ftp = new FtpRequestEntry(obj.FtpHostName, hostName, obj.FtpUserId, obj.FtpPassword);



            var tempPath = SyncTempPath(obj);
            var backupPath = SyncBackupPath(obj);

            var listFile = GetFilesZipinDir(tempPath, out message);
            if (checkHasError(message) == false)
            {
                foreach (var file in listFile)
                {
                    _ftp.UploadFile(file.FullName, out message);
                    if (checkHasError(message) == false)
                    {
                        string newBackupFile = "";
                        BackupFile(file.Name, tempPath, backupPath, out newBackupFile, out message);
                        if (checkHasError(message) == false)
                            SyncSourceLog(obj, SyncStatus.BackupFileSuccess, message, backupPath, newBackupFile);
                        else
                            SyncSourceLog(obj, SyncStatus.BackupFileFail, message, backupPath, newBackupFile);

                    }
                    else
                    {
                        SyncSourceLog(obj, SyncStatus.UploadFileFail, message, hostName, file.Name);
                    }
                }
            }
            else
            {
                SyncSourceLog(obj, SyncStatus.ReadFileError, message, tempPath, null);
            }
        }

        #endregion

        #region Download & Sync In
        private void DownloadFile(SyncObjectCurrent obj)
        {
            //_ftp.DownloadAllFile(destPath, ".xml", out message);

            string message = "";
            if (obj == null) return;

            var hostName = SyncFtpPath(obj);

            _ftp = new FtpRequestEntry(hostName, obj.FtpUserId, obj.FtpPassword);

            var destPath = SyncTempPath(obj);

            var listFile = _ftp.GetFiles(".zip", out message);
            if (checkHasError(message) == false)
            {
                foreach (var file in listFile)
                {
                    _ftp.DownloadFile(file, destPath + file, out message);
                    if (checkHasError(message) == false)
                    {
                        _ftp.MoveFile(file, "backup/", out message);
                    }
                    else
                    {
                        SyncSourceLog(obj, SyncStatus.DownloadFileFail, message, hostName, file);
                    }
                }
            }
            else
            {
                SyncSourceLog(obj, SyncStatus.ReadFileError, message, hostName, null);
            }

            //
            SyncIN(obj);

        }

        private void SyncIN(SyncObjectCurrent obj)
        {
            string message = "";
            if (obj == null)
            {
                SyncSourceLog(obj, SyncStatus.SyncOutInvalidSource, null, null, null);
                return;
            }
            var workingPath = SyncTempPath(obj);

            //extract files
            var fileszip = GetFilesZipinDir(workingPath, out message);
            if (message != "")
            {
                SyncSourceLog(obj, SyncStatus.SystemError, message, null, null);
            }
            else
            {
                foreach (var file in fileszip)
                {
                    if (ExtractFile(file.FullName, workingPath, out message))
                    {
                        file.Delete();
                    }
                    else
                    {
                        SyncSourceLog(obj, SyncStatus.ExtractFileFail, message, null, file.Name);
                    }
                }
            }

            var files = GetFilesXmlinDir(workingPath, out message);
            if (message != "")
            {
                SyncSourceLog(obj, SyncStatus.SystemError, message, null, null);
                return;
            }
            else if (files != null && files.Count > 0)
            {
                //obj.WorkingFiles = obj.WorkingFiles + " " + files.Count.ToString() + " files found";
                foreach (var file in files)
                {
                    var workingFile = file.Name;


                    //Get Header
                    var dataObj = XElementToDataTable(file.FullName, obj.XmlElement, out message);

                    //Get List Detail
                    List<SyncDataObjectLine> dataLines = new List<SyncDataObjectLine>();
                    if (obj.IsHeader)
                    {
                        //var objLines = _syncdata.SyncSourceLevelGet(obj.SyncSource);//For DataSQL
                        var objLines = _settingSync.SyncSourceLevel_Get_Active(obj.SyncSource, out message);//For DataXML

                        if (objLines != null)
                        {
                            foreach (var line in objLines)
                            {
                                var dataLine = XElementToDataTable(file.FullName, line.XmlElement, out message);
                                if (checkHasError(message) == false)
                                    dataLines.Add(new SyncDataObjectLine() { DataSource = dataLine, SourceCode = line.SourceLine, XmlElement = line.XmlElement });
                                else
                                    SyncSourceLog(obj, SyncStatus.ReadFileError, message, workingPath, workingFile);
                            }
                        }
                    }

                    if (message == "")
                    {
                        if (_syncdata.ExcuteDataSyncIn(obj.SyncSource, obj.StoreCode, dataObj, dataLines, true, obj.SyncBy, out message))
                        {
                            SyncSourceLog(obj, SyncStatus.SyncInDataSuccess, null, workingPath, workingFile);

                            //
                            BackupFile_SyncIN(obj, workingFile);
                        }
                        else
                        {
                            SyncSourceLog(obj, SyncStatus.SyncInDataFail, message, workingPath, workingFile);
                        }
                    }
                    else
                    {
                        SyncSourceLog(obj, SyncStatus.ReadFileError, message, workingPath, workingFile);
                    }
                }
            }
            else
            {
                SyncSourceLog(obj, SyncStatus.GetDataEmpty, message, workingPath, null);
            }

        }

        private void BackupFile_SyncIN(SyncObjectCurrent obj, string fileName)
        {
            string message = "";

            var backupPath = SyncBackupPath(obj);
            var workingPath = SyncTempPath(obj);

            DeleteBackupFile(backupPath, SYNCIN_FILEBACKUP_DAYS, out message);

            //string backupFileName = "";
            //BackupFile(fileName, workingPath, backupPath, out backupFileName, out message);
            //if (message == "")
            //{
            //    SyncSourceLog(obj, SyncStatus.BackupFileSuccess, null, backupPath, backupFileName);
            //}
            //else
            //{
            //    SyncSourceLog(obj, SyncStatus.BackupFileFail, message, backupPath, backupFileName);
            //}

            var sourcefile = workingPath + fileName;

            var zf = GetFileNameNoExt(fileName) + ".zip";
            var _zipfile = backupPath + zf;

            if (ZipFile(_zipfile, sourcefile, out message))
            {
                SyncSourceLog(obj, SyncStatus.BackupFileSuccess, null, workingPath, zf);
                File.Delete(sourcefile);
            }
            else
                SyncSourceLog(obj, SyncStatus.BackupFileFail, message, workingPath, fileName);

        }
        #endregion

        #region Write log
        private void SyncSourceLog(SyncObjectCurrent obj, SyncStatus status, string syncDescription, string workingPath, string workingFiles)
        {
            obj.SyncStatus = ((int)status).ToString();
            obj.SyncStatusDesc = SyncStatusDesc(status);
            obj.SyncDescription = syncDescription;
            obj.WorkingPath = workingPath;
            obj.WorkingFiles = workingFiles;

            //SyncSourceLog(_syncLogId, obj);
        }

        private int SyncLog(SyncObjectCurrent obj)
        {
            var result = _syncdata.SyncLogModified(obj);
            if (result != null)
                return result.SyncLogId;

            return 0;
        }

        private bool SyncSourceLog(int syncLogId, SyncObjectCurrent obj)
        {
            if (syncLogId != 0)
            {
                var result = _syncdata.SyncSourceLogModified(syncLogId, obj);
                if (result != null)
                {
                    return true;
                }
            }
            return false;
        }

        private void ClearSyncLog()
        {
            _syncdata.SyncLogClear(SYNCLOG_DATA_DAYS);
        }

        #endregion

        #region get path current & filename to xml

        private string SyncFtpPath(SyncObjectCurrent obj)
        {
            if (obj != null)
            {
                var ftpHostName = obj.FtpHostName;
                if (ftpHostName == null || ftpHostName == "")
                    return null;

                if (ftpHostName.ToLower().Contains("ftp") == false)
                    ftpHostName = "ftp://" + ftpHostName;
                ftpHostName = PathReplaceSeparator(ftpHostName);

                //obj.WhsCode==obj.ShopCode????
                var machineFolder = PathReplaceSeparator(obj.MachineCode);
                //if (obj.Channel == ((int)Channel.NMS).ToString())
                //    machineFolder = "";

                var syncTypeFolder = PathReplaceSeparator(obj.SyncType);

                var syncGroupFolder = PathReplaceSeparator(obj.SyncGroup);

                var syncSourceFolder = PathReplaceSeparator(obj.SyncSource);


                //var dataFileName = PathReplaceSeparator(obj.SyncSourcePrefixName);

                var path = ftpHostName + machineFolder + syncTypeFolder + syncGroupFolder + syncSourceFolder;// +dataFileName;


                //CreateDirectoty(path);

                return path;
            }

            return "";

        }

        private string SyncTempPath(SyncObjectCurrent obj)
        {
            if (obj != null)
            {
                var tempPath = PathReplaceSeparator(obj.TempPath);
                if (tempPath == null || tempPath == "")
                    tempPath = PathReplaceSeparator("C:\\Temp");

                //obj.WhsCode==obj.ShopCode????
                var machineFolder = PathReplaceSeparator(obj.MachineCode);
                //if (obj.Channel == ((int)Channel.NMS).ToString())
                //    machineFolder = "";

                var syncTypeFolder = PathReplaceSeparator(obj.SyncType);

                var syncGroupFolder = PathReplaceSeparator(obj.SyncGroup);

                var syncSourceFolder = PathReplaceSeparator(obj.SyncSource);

                //var dataFileName = PathReplaceSeparator(obj.SyncSourcePrefixName);

                var path = tempPath + machineFolder + syncTypeFolder + syncGroupFolder + syncSourceFolder;// +dataFileName;

                CreateDirectoty(path);

                return path;
            }


            return "";

        }

        private string SyncBackupPath(SyncObjectCurrent obj)
        {
            if (obj != null)
            {
                var backupPath = PathReplaceSeparator(obj.BackupPath);
                if (backupPath == null || backupPath == "")
                    backupPath = PathReplaceSeparator("C:\\Backup");

                //obj.WhsCode==obj.ShopCode????
                var machineFolder = PathReplaceSeparator(obj.MachineCode);
                //if (obj.Channel == ((int)Channel.NMS).ToString())
                //    machineFolder = "";

                var syncTypeFolder = PathReplaceSeparator(obj.SyncType);

                var syncGroupFolder = PathReplaceSeparator(obj.SyncGroup);

                var syncSourceFolder = PathReplaceSeparator(obj.SyncSource);

                //var dataFileName = PathReplaceSeparator(obj.SyncSourcePrefixName);

                var path = backupPath + machineFolder + syncTypeFolder + syncGroupFolder + syncSourceFolder;// +dataFileName;

                CreateDirectoty(path);

                return path;
            }

            return "";

        }

        private string GetDataFile(SyncObjectCurrent obj)
        {
            if (obj != null)
            {
                string prefixName = obj.SyncSourcePrefixName;
                if (prefixName == null)
                    prefixName = obj.SyncSource;
                var dataFileName = prefixName + GenerateFormatDateTime() + ".xml";

                return dataFileName;
            }

            return "";

        }

        #endregion

        #endregion

    }
}
