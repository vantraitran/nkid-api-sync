﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;

using Microsoft.Win32;


namespace DataTools.DataAccess.Sync.FtpSync
{
    public class FtpRequestEntry : SyncBase
    {
        FtpWebRequest _requestFtp = null;

        public string FtpServer { get; set; }
        public string FtpUsername { get; set; }
        public string FtpPassword { get; set; }
        //2013-03-20    Van Linh
        public string FtpHost { get; set; }

        public FtpRequestEntry(string ftpServer, string ftpUsername, string ftpPassword)
        {
            this.FtpServer = ftpServer;
            this.FtpUsername = ftpUsername;
            this.FtpPassword = ftpPassword;
        }

        public FtpRequestEntry(string ftpHost, string ftpServer, string ftpUsername, string ftpPassword)
        {
            this.FtpServer = ftpServer;
            this.FtpUsername = ftpUsername;
            this.FtpPassword = ftpPassword;
            this.FtpHost = ftpHost;
        }

        #region File
        public void UploadFile0(string filename, out string message)
        {
            try
            {
                FileInfo fileInf = new FileInfo(filename);
                string uri = this.FtpServer + fileInf.Name;

                // Create FtpWebRequest object from the Uri provided
                _requestFtp = (FtpWebRequest)FtpWebRequest.Create(uri);
                _requestFtp.Timeout = 300000;

                // Provide the WebPermission Credintials
                _requestFtp.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);

                // By default KeepAlive is true, where the control connection
                // is not closed after a command is executed.
                _requestFtp.KeepAlive = true;

                // Specify the command to be executed.
                _requestFtp.Method = WebRequestMethods.Ftp.UploadFile;

                // Specify the data transfer type.
                _requestFtp.UseBinary = true;

                // Notify the server about the size of the uploaded file
                _requestFtp.ContentLength = fileInf.Length;

                // The buffer size is set to 2kb
                int buffLength = 2048;
                byte[] buff = new byte[buffLength];
                int contentLen;

                // Opens a file stream (System.IO.FileStream) to read the file
                // to be uploaded
                FileStream fs = fileInf.OpenRead();


                // Stream to which the file to be upload is written
                Stream strm = _requestFtp.GetRequestStream();

                // Read from the file stream 2kb at a time
                contentLen = fs.Read(buff, 0, buffLength);

                // Till Stream content ends
                while (contentLen != 0)
                {
                    // Write Content from the file stream to the FTP Upload
                    // Stream
                    strm.Write(buff, 0, contentLen);
                    contentLen = fs.Read(buff, 0, buffLength);
                }

                // Close the file stream and the Request Stream
                strm.Close();
                fs.Close();

                message = "";
            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Upload File", ex.Message);
            }
        }

        public void UploadFile(string filename, out string message)
        {
            try
            {
                CreateDirecrory1(this.FtpServer, out message);
                if (checkHasError(message))
                    return;

                FileInfo fileInf = new FileInfo(filename);

                WebClient request = new WebClient();
                
                request.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                byte[] fileData = request.UploadFile(this.FtpServer + fileInf.Name, filename);

                //
                message = "";
            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Rename File", ex.Message);
            }
        }

        public bool UploadAllFile(string localDirName, string extension, out string message)
        {
            try
            {
                message = "";
                string messageTemp = "";

                if (Directory.Exists(localDirName))
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(localDirName);
                    foreach (var file in dirInfo.GetFiles())
                    {
                        if (extension == "" || file.Extension == extension)
                        {
                            UploadFile(file.FullName, out messageTemp);
                            message = message + messageTemp + Microsoft.VisualBasic.Constants.vbNewLine;
                        }
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Rename File", ex.Message);
                return false;
            }
        }

        /// <summary>
        /// filename is not include path
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="message"></param>
        public bool DeleteFile(string filename, out string message)
        {
            try
            {
                message = "";

                string uri = this.FtpServer + filename;
                _requestFtp = (FtpWebRequest)FtpWebRequest.Create(uri);
                _requestFtp.Timeout = 300000;

                _requestFtp.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                _requestFtp.Method = WebRequestMethods.Ftp.DeleteFile;
                _requestFtp.KeepAlive = true;
                FtpWebResponse ftpResponse = (FtpWebResponse)_requestFtp.GetResponse();
                ftpResponse.Close();

                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Delete File", ex.Message);
                return false;
            }
        }

        /// <summary>
        /// filename is not include path
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="message"></param>
        public bool RenameFile(string sourceFilename, string destFilename, out string message)
        {
            try
            {
                message = "";

                string uri = this.FtpServer + sourceFilename;
                _requestFtp = (FtpWebRequest)FtpWebRequest.Create(uri);
                _requestFtp.Timeout = 300000;
                _requestFtp.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                _requestFtp.Method = WebRequestMethods.Ftp.Rename;
                _requestFtp.RenameTo = destFilename;
                FtpWebResponse ftpResponse = (FtpWebResponse)_requestFtp.GetResponse();
                ftpResponse.Close();

                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Rename File", ex.Message);
                return false;
            }
        }

        public bool DownloadFile0(string filename, string destFileName, out string message)
        {
            try
            {
                message = "";

                string uri = this.FtpServer + filename;
                _requestFtp = (FtpWebRequest)FtpWebRequest.Create(uri);
                _requestFtp.Timeout = 300000;

                _requestFtp.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);

                _requestFtp.Method = WebRequestMethods.Ftp.DownloadFile;

                //_requestFtp.KeepAlive = true;
                //_requestFtp.UsePassive = true;
                //_requestFtp.UseBinary = true;

                FtpWebResponse ftpResponse = (FtpWebResponse)_requestFtp.GetResponse();

                Stream responseStream = ftpResponse.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                using (StreamWriter destination = new StreamWriter(destFileName))
                {
                    destination.Write(reader.ReadToEnd());
                    destination.Flush();
                }

                reader.Close();
                ftpResponse.Close();

                return false;
            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Rename File", ex.Message);
                return false;
            }
        }

        public bool DownloadFile1(string filename, string destFileName, out string message)
        {
            try
            {

                message = "";

                WebClient request = new WebClient();
                request.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                byte[] fileData = request.DownloadData(this.FtpServer + filename);

                FileStream file = File.Create(destFileName);
                file.Write(fileData, 0, fileData.Length);
                file.Close();

                return true;

            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Rename File", ex.Message);
                return false;
            }
        }

        public bool DownloadFile(string filename, string destFileName, out string message)
        {
            try
            {
                message = "";
                WebClient request = new WebClient();
                request.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                request.DownloadFile(this.FtpServer + filename, destFileName);

                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Rename File", ex.Message);
                return false;
            }
        }

        public bool DownloadAllFile(string destDirectory, string extension, out string message)
        {
            try
            {
                message = "";
                string listMessage = "";

                var listFiles = GetFiles(extension, out message);
                if (!checkHasError(message))
                {
                    foreach (var file in listFiles)
                    {
                        DownloadFile(file, destDirectory + file, out listMessage);
                        message = message + listMessage + Microsoft.VisualBasic.Constants.vbNewLine;
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Rename File", ex.Message);
                return false;
            }
        }

        public long GetFileSize(string filename, out string message)
        {
            try
            {
                string uri = this.FtpServer + filename;
                _requestFtp = (FtpWebRequest)FtpWebRequest.Create(uri);
                _requestFtp.Timeout = 300000;
                _requestFtp.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                _requestFtp.Method = WebRequestMethods.Ftp.GetFileSize;
                _requestFtp.UseBinary = true;

                FtpWebResponse ftpResponse = (FtpWebResponse)_requestFtp.GetResponse();
                var size = ftpResponse.ContentLength;

                ftpResponse.Close();

                message = "";
                return size;

            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Rename File", ex.Message);
                return -1;
            }
        }

        public bool FileExists(string filename, out string message)
        {
            try
            {
                message = "";

                var listFiles = GetFiles("", out message);
                if (checkHasError(message))
                {
                    var obj = listFiles.Where(o => o == filename).ToList();
                    if (obj != null && obj.Count > 0)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Rename File", ex.Message);
                return false;
            }
        }

        public List<String> GetFiles(string extension, out string message)
        {
            List<String> result = new List<String>();
            try
            {

                _requestFtp = (FtpWebRequest)FtpWebRequest.Create(new Uri(this.FtpServer));
                _requestFtp.Timeout = 300000;
                _requestFtp.UsePassive = false;
                _requestFtp.UseBinary = true;
                _requestFtp.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                _requestFtp.Method = WebRequestMethods.Ftp.ListDirectory;
                FtpWebResponse response = (FtpWebResponse)_requestFtp.GetResponse();

                StreamReader reader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8);
                string line = "";

                while (reader.Peek() > -1)
                {
                    line = reader.ReadLine();
                    if (extension == "" || File_GetExtension(line) == extension)
                        result.Add(line);
                }

                reader.Close();
                response.Close();

                message = "";
                return result;
            }
            catch (Exception ex)
            {
                message = getFunctionError("Get Files", ex.Message);
                return null;
            }
        }

        public List<String> GetFiles(string ftpHost, string extension, out string message)
        {
            List<String> result = new List<String>();
            try
            {

                _requestFtp = (FtpWebRequest)FtpWebRequest.Create(new Uri(ftpHost));
                _requestFtp.Timeout = 300000;
                _requestFtp.UsePassive = false;
                _requestFtp.UseBinary = true;
                _requestFtp.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                _requestFtp.Method = WebRequestMethods.Ftp.ListDirectory;
                FtpWebResponse response = (FtpWebResponse)_requestFtp.GetResponse();

                StreamReader reader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8);
                string line = "";

                while (reader.Peek() > -1)
                {
                    line = reader.ReadLine();
                    if (extension == "" || File_GetExtension(line) == extension)
                        result.Add(line);
                }

                reader.Close();
                response.Close();

                message = "";
                return result;
            }
            catch (Exception ex)
            {
                message = getFunctionError("Get Files", ex.Message);
                return null;
            }
        }


        public string File_GetExtension(string fileName)
        {
            try
            {
                if (fileName != null)
                {
                    var index = fileName.LastIndexOf(".");

                    if (fileName.Length > index)
                    {
                        var obj = fileName.Substring(index);
                        return obj;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return "";
        }

        public bool MoveFile(string filename, string destDirName, out string message)
        {
            try
            {
                message = "";

                if (DirectoryExists(destDirName, out message) == false)
                    CreateDirecrory(destDirName, out message);

                string uri = this.FtpServer + filename;
                _requestFtp = (FtpWebRequest)FtpWebRequest.Create(uri);
                _requestFtp.Timeout = 300000;
                _requestFtp.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                _requestFtp.Method = WebRequestMethods.Ftp.Rename;
                _requestFtp.RenameTo = destDirName + filename;
                FtpWebResponse ftpResponse = (FtpWebResponse)_requestFtp.GetResponse();
                ftpResponse.Close();

                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Delete File", ex.Message);
                return false;
            }
        }
        #endregion

        #region Direcrory
        public bool CreateDirecrory(string dirName, out string message)
        {
            try
            {
                message = "";

                string uri = this.FtpServer + dirName;
                _requestFtp = (FtpWebRequest)FtpWebRequest.Create(uri);
                _requestFtp.Timeout = 300000;
                _requestFtp.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                _requestFtp.Method = WebRequestMethods.Ftp.MakeDirectory;
                _requestFtp.KeepAlive = true;
                FtpWebResponse ftpResponse = (FtpWebResponse)_requestFtp.GetResponse();
                ftpResponse.Close();

                return true;

            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Create Direcrory", ex.Message);
                return false;
            }
        }

        public bool CreateDirecrory(string ftpHost, string dirName, out string message)
        {
            try
            {
                message = "";

                if (DirectoryExists(ftpHost, dirName, out message))
                    return true;

                string uri = PathReplaceSeparator(ftpHost) + dirName;
                _requestFtp = (FtpWebRequest)FtpWebRequest.Create(uri);
                _requestFtp.Timeout = 300000;
                _requestFtp.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                _requestFtp.Method = WebRequestMethods.Ftp.MakeDirectory;
                _requestFtp.KeepAlive = true;
                FtpWebResponse ftpResponse = (FtpWebResponse)_requestFtp.GetResponse();
                ftpResponse.Close();

                return true;

            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Create Direcrory", ex.Message);
                return false;
            }
        }

        public bool CreateDirecrory1(string dirName, out string message)
        {
            try
            {
                /*ftp://203.113.143.246/lg_sync/SHOP/SHOPTEST/TR/*/

                string path = "";
                path = this.FtpServer.Substring(this.FtpServer.IndexOf(this.FtpHost) + this.FtpHost.Length);

                message = "";

                var arr = path.Split('/');
                if (arr != null)
                {
                    var p = this.FtpHost;
                    foreach (var s in arr)
                    {
                        if (p.EndsWith("/") == false)
                            p = p + "/";

                        if (s != null && s != "")
                        {
                            CreateDirecrory(p, s, out message);
                            if (checkHasError(message))
                                return false;

                            p = PathReplaceSeparator(p) + s;
                        }
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Create Direcrory", ex.Message);
                return false;
            }
        }

        public bool RenameDirecrory(string sourceDirName, string destDirName, out string message)
        {
            try
            {
                message = "";

                string uri = this.FtpServer + sourceDirName;
                _requestFtp = (FtpWebRequest)FtpWebRequest.Create(uri);
                _requestFtp.Timeout = 300000;
                _requestFtp.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                _requestFtp.Method = WebRequestMethods.Ftp.Rename;
                _requestFtp.RenameTo = destDirName;
                FtpWebResponse ftpResponse = (FtpWebResponse)_requestFtp.GetResponse();
                ftpResponse.Close();

                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Rename Direcrory", ex.Message);
                return false;
            }
        }

        public bool DeleteDirecrory(string dirName, out string message)
        {
            try
            {
                message = "";

                string uri = this.FtpServer + dirName;
                _requestFtp = (FtpWebRequest)FtpWebRequest.Create(uri);
                _requestFtp.Timeout = 300000;
                _requestFtp.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                _requestFtp.Method = WebRequestMethods.Ftp.RemoveDirectory;
                _requestFtp.KeepAlive = true;
                FtpWebResponse ftpResponse = (FtpWebResponse)_requestFtp.GetResponse();
                ftpResponse.Close();

                return true;

            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Delete Direcrory", ex.Message);
                return false;
            }
        }

        public void GetDirectories(out string message)
        {
            try
            {


                message = "";
            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Get Directories", ex.Message);
            }
        }

        public bool DirectoryExists(string dirName, out string message)
        {
            try
            {
                message = "";

                var listFiles = GetFiles("", out message);
                if (checkHasError(message))
                {
                    var obj = listFiles.Where(o => o == dirName).ToList();
                    if (obj != null && obj.Count > 0)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Directory Exists", ex.Message);
                return false;
            }
        }

        public bool DirectoryExists(string ftpHost, string dirName, out string message)
        {
            try
            {
                message = "";

                var listFiles = GetFiles(ftpHost, "", out message);
                if (!checkHasError(message))
                {
                    var obj = listFiles.Where(o => o == dirName).ToList();
                    if (obj != null && obj.Count > 0)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Directory Exists", ex.Message);
                return false;
            }
        }

        public bool MoveDirectory(string dirName, string destDirName, out string message)
        {
            try
            {
                message = "";

                string uri = this.FtpServer + dirName;
                _requestFtp = (FtpWebRequest)FtpWebRequest.Create(uri);
                _requestFtp.Timeout = 300000;
                _requestFtp.Credentials = new NetworkCredential(this.FtpUsername, this.FtpPassword);
                _requestFtp.Method = WebRequestMethods.Ftp.Rename;
                _requestFtp.RenameTo = destDirName + dirName;
                FtpWebResponse ftpResponse = (FtpWebResponse)_requestFtp.GetResponse();
                ftpResponse.Close();

                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("FTP Delete File", ex.Message);
                return false;
            }
        }

        #endregion

        public void Dispose()
        {
            _requestFtp = null;
        }
    }

    public class FtpFileInfo
    {
        public string FullName { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string Size { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Extension { get; set; }
        public string NameOnly { get; set; }
    }

    public class FtpDirectoryInfo
    {
        public FtpFileInfo GetFiles { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Size { get; set; }
    }
}
