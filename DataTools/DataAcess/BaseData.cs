﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Data;

using DataTools.Models;
using DataTools.Utility;

namespace DataTools.DataAccess
{
    public class BaseData : BaseUtil
    {
        public dbHub _db_hub = null;
        public string _connection_string = "";

        public BaseData()
        {
            DB_HUB();
        }

        #region Data Context
        public TransactionScope TransWithUncommitted()
        {
            return new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted });
        }

        public TransactionScope TransWithCommitted()
        {
            return new TransactionScope(TransactionScopeOption.Required, new TransactionOptions() { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted });
        }

        public dbHub DB_HUB()
        {
            var sConn = Connection_String(DataConnection.CLIENTDB);
            if (sConn != "")
            {
                _db_hub = new dbHub(sConn);
            }
            return _db_hub;
        }

        public dbHub DB_HUB(string connection_string)
        {
            if (connection_string != "")
            {
                _db_hub = new dbHub(connection_string);
            }
            return _db_hub;
        }

        public bool Connection_Test(BaseUtil.DataConnection dataconn, out string message)
        {
            message = "";
            try
            {

                daConnection daconn = new daConnection(dataconn);

                return daconn.Connect(ref message);

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }
        #endregion

        #region For Sync
        //Frequence Occurs
        public enum FrequenceOccurs
        {
            D = 1,
            W = 2,
        }

        public string FrequenceOccursDesc(FrequenceOccurs type)
        {
            switch (type)
            {
                case FrequenceOccurs.D:
                    return "Day(s)";
                case FrequenceOccurs.W:
                    return "Week(s)";
                default: return "-------";
            }
        }

        //ScheduleType
        public enum ScheduleType
        {
            Recurring = 1,
            Onetime = 2,
        }

        public string ScheduleTypeDesc(ScheduleType type)
        {
            switch (type)
            {
                case ScheduleType.Recurring:
                    return "Recurring";
                case ScheduleType.Onetime:
                    return "One time";
                default: return "-------";
            }
        }

        public List<Enum_View> FrequenceOccursGet()
        {
            var results = new List<Enum_View>();
            results.Add(new Enum_View() { Code = FrequenceOccurs.D.ToString(), Name = FrequenceOccursDesc(FrequenceOccurs.D) });
            results.Add(new Enum_View() { Code = FrequenceOccurs.W.ToString(), Name = FrequenceOccursDesc(FrequenceOccurs.W) });

            return results;
        }

        public List<Enum_View> ScheduleTypeGet()
        {
            var results = new List<Enum_View>();
            results.Add(new Enum_View() { Code = ((int)ScheduleType.Recurring).ToString(), Name = ScheduleTypeDesc(ScheduleType.Recurring) });
            results.Add(new Enum_View() { Code = ((int)ScheduleType.Onetime).ToString(), Name = ScheduleTypeDesc(ScheduleType.Onetime) });

            return results;
        }


        //TimeType
        public enum TimeType
        {
            H = 1,
            M = 2,
            S = 3,
        }

        public string TimeTypeDesc(TimeType type)
        {
            switch (type)
            {
                case TimeType.H:
                    return "Hour(s)";
                case TimeType.M:
                    return "Minute(s)";
                case TimeType.S:
                    return "Second(s)";
                default: return "-------";
            }
        }

        public List<Enum_View> TimeTypeGet()
        {
            var results = new List<Enum_View>();
            results.Add(new Enum_View() { Code = TimeType.H.ToString(), Name = TimeTypeDesc(TimeType.H) });
            results.Add(new Enum_View() { Code = TimeType.M.ToString(), Name = TimeTypeDesc(TimeType.M) });
            results.Add(new Enum_View() { Code = TimeType.S.ToString(), Name = TimeTypeDesc(TimeType.S) });

            return results;
        }
        #endregion

        public bool ExecuteSQL(string sql, out string message)
        {
            message = "";
            try
            {
                //sql = SqlReplaceSpecial(sql);
                _db_hub.ExecuteQuery<Document_View>(sql);
                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }

        public bool CheckRegisterConnection(DataConnection dataConn)
        {
            var regconn = RegConn_Get(dataConn);
            if (regconn == null) return false;

            return true;
        }

        public bool Check_Product_Key()
        {
            bool check_product = false;

            try
            {
                daConnection conn = new daConnection(_dataConnection);
                var dt = conn.GetData("IConfig", "select * from sys.tables where name='IConfig'");
                if (dt == null || dt.Rows.Count == 0)
                {
                    string message = "";
                    if (conn.ExecQuery("CREATE TABLE [dbo].[IConfig]([Code] [nvarchar](50) NOT NULL,[Name] [nvarchar](250) NULL,[Value] [nvarchar](250) NULL)", out message))
                    {
                        conn.ExecQuery(string.Format("INSERT INTO [dbo].[IConfig]([Code],[Name],[Value]) VALUES('IID','Product Key','{0}')", Utility.BaseUtil._product_key), out message);
                    }
                }

                dt = conn.GetData("IConfig", "SELECT Value FROM IConfig WHERE Code='IID' and Value='" + Utility.BaseUtil._product_key + "'");
                if (dt != null && dt.Rows.Count > 0)
                {
                    check_product = true;
                }
            }
            catch (Exception)
            {

            }

            return check_product;
        }

        public string SqlReplaceSpecial(string s)
        {
            if (s == null || s.Trim() == "") return "";

            s = s.Replace("'", "''");

            return s;
        }

        public DateTime dbDate()
        {
            var date = _db_hub.ExecuteQuery<DateTime>("select getdate()").FirstOrDefault();
            return date;
        }

        #region Store List
        public List<Store_View> StoreList_Get(out string message)
        {
            message = "";
            try
            {
                var a = (from o1 in _db_hub.StoreLists
                         select new Store_View
                         {
                             StoreCode = o1.StoreCode,
                             Description = o1.Description,
                             Address = o1.Address,
                             Inactive = o1.Inactive,
                             PriceList = o1.PriceList,

                         }).ToList();
                return a;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return null;
            }
        }

        public Store_View StoreList_Get(string StoreCode, out string message)
        {
            message = "";
            try
            {
                var a = (from o1 in _db_hub.StoreLists
                         where o1.StoreCode == StoreCode
                         select new Store_View
                         {
                             StoreCode = o1.StoreCode,
                             Description = o1.Description,
                             Address = o1.Address,
                             Inactive = o1.Inactive,
                             PriceList = o1.PriceList,
                         }).FirstOrDefault();
                return a;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return null;
            }
        }

        public List<Store_View> StoreList_Get(Boolean AddEmpty, out string message)
        {
            message = "";
            try
            {
                var a = (from o1 in _db_hub.StoreLists
                         where o1.Inactive == null || o1.Inactive == false
                         select new Store_View
                         {
                             StoreCode = o1.StoreCode,
                             Description = o1.Description,
                             Address = o1.Address,
                             Inactive = o1.Inactive,
                             PriceList = o1.PriceList,
                         }).ToList();

                if (AddEmpty)
                {
                    a.Insert(0, new Store_View() { StoreCode = null, Description = "-------" });
                }

                return a;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return null;
            }
        }

        public Boolean StoreList_Insert(List<Store_View> oList, out string message)
        {
            message = "";
            System.Data.Common.DbTransaction trans = null;
            try
            {
                if (_db_hub == null)
                    _db_hub = DB_HUB();

                if (_db_hub.Connection.State == System.Data.ConnectionState.Closed)
                {
                    _db_hub.Connection.Open();
                }
                trans = _db_hub.Connection.BeginTransaction();

                _db_hub.Transaction = trans;

                List<StoreList> odel = null;
                odel = _db_hub.StoreLists.ToList();

                _db_hub.StoreLists.DeleteAllOnSubmit(odel);
                _db_hub.StoreLists.Context.SubmitChanges();

                if (oList != null)
                {
                    List<StoreList> otemp = new List<StoreList>();
                    foreach (var o in oList)
                    {
                        StoreList os = new StoreList();
                        os.StoreCode = o.StoreCode;
                        os.Description = o.Description;
                        os.Address = o.Address;
                        os.Inactive = o.Inactive;
                        os.PriceList = o.PriceList;

                        otemp.Add(os);

                    }

                    _db_hub.StoreLists.InsertAllOnSubmit(otemp);
                    _db_hub.StoreLists.Context.SubmitChanges();
                }

                trans.Commit();
                return true;
            }
            catch (Exception ex)
            {
                if (trans != null)
                {
                    trans.Rollback();
                }
                message = ex.Message;
                return false;
            }
        }
        #endregion

        #region User Group
        public bool UserGroupValid(string groupcode, out string message)
        {
            message = "";
            try
            {
                var a = (from o1 in _db_hub.Users
                         where o1.UserGroup == groupcode
                         select new { o1.UserId }).FirstOrDefault();
                if (a != null)
                    return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }
        public List<UserGroup_View> UserGroup_Get(out string message)
        {
            message = "";
            try
            {
                var a = (from o1 in _db_hub.UserGroups
                         select new UserGroup_View
                         {
                             Code = o1.Code,
                             Name = o1.Name,
                         }).ToList();
                return a;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return null;
            }
        }

        public List<UserGroup_View> UserGroup_Get(Boolean AddEmpty, out string message)
        {
            message = "";
            try
            {
                var a = (from o1 in _db_hub.UserGroups
                         select new UserGroup_View
                         {
                             Code = o1.Code,
                             Name = o1.Name,
                         }).ToList();

                if (AddEmpty)
                {
                    a.Insert(0, new UserGroup_View() { Code = null, Name = "-------" });
                }

                return a;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return null;
            }
        }

        public Boolean UserGroup_Insert(List<UserGroup_View> oList, out string message)
        {
            message = "";
            System.Data.Common.DbTransaction trans = null;
            try
            {
                if (_db_hub == null)
                    _db_hub = DB_HUB();

                if (_db_hub.Connection.State == System.Data.ConnectionState.Closed)
                {
                    _db_hub.Connection.Open();
                }
                trans = _db_hub.Connection.BeginTransaction();

                _db_hub.Transaction = trans;

                List<UserGroup> odel = null;
                odel = _db_hub.UserGroups.ToList();

                _db_hub.UserGroups.DeleteAllOnSubmit(odel);
                _db_hub.UserGroups.Context.SubmitChanges();

                if (oList != null)
                {
                    List<UserGroup> otemp = new List<UserGroup>();
                    foreach (var o in oList)
                    {
                        UserGroup os = new UserGroup();
                        os.Code = o.Code;
                        os.Name = o.Name;

                        otemp.Add(os);

                    }

                    _db_hub.UserGroups.InsertAllOnSubmit(otemp);
                    _db_hub.UserGroups.Context.SubmitChanges();
                }

                trans.Commit();
                return true;
            }
            catch (Exception ex)
            {
                if (trans != null)
                {
                    trans.Rollback();
                }
                message = ex.Message;
                return false;
            }
        }
        #endregion
    }
}
