﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DataTools.Models;
using DataTools.DataAccess.Sync.FtpSync;
using DataTools.DataAccess.Sync.SqlSync;
using DataTools.Utility;

namespace DataTools.DataAccess.Sync
{
    public class MainSync : BaseData
    {
        private SyncFtpEntry _syncftp = null;
        private SyncSqlEntry _syncSQL = null;

        public List<SyncObjectCurrent> Sync(List<SyncObjectCurrent> syncObjs, bool isPreviewData)
        {
            if (BaseUtil._syncType == eSyncType.ByDirectSql)
            {
                _syncSQL = new SyncSqlEntry();
                _syncSQL.SyncData(syncObjs, isPreviewData);
            }
            else if (BaseUtil._syncType == eSyncType.ByFtp)
            {
                _syncftp = new SyncFtpEntry();
                _syncftp.SyncData(syncObjs);
            }

            return syncObjs;
        }

        public bool Sync(SyncObjectCurrent syncObject, DataTable dataHeader, out string message)
        {
            bool oresult = false;
            if (BaseUtil._syncType == eSyncType.ByDirectSql)
            {
                _syncSQL = new SyncSqlEntry();
                oresult= _syncSQL.SyncData(syncObject, dataHeader, out message);
            }

            return oresult;
        }




    }
}
