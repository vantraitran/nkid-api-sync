﻿using System;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using System.Data.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using DataTools.Models;
using DataTools.Utility;
using DataTools.SAPAccess;

namespace DataTools.DataAccess.Sync.SqlSync
{
    public class SAPSyncSqlEntry : SyncBase
    {
        //int _syncLogId = 0;
        DataSync _syncdata = null;
        SAPSync _sapSync = null;

        List<SyncObjectCurrent> _objLog = null;

        public SAPSyncSqlEntry()
        {
            _syncdata = new DataSync();
            _sapSync = new SAPSync();
        }

        #region public functions

        public List<SyncObjectCurrent> SyncData(List<SyncObjectCurrent> syncObjs, bool isPreviewData)
        {
            ////Clear Log
            //ClearSyncLog();

            if (syncObjs == null || syncObjs.Count == 0) return null;

            //_objLog = new List<SyncObjectCurrent>();

            //_syncLogId = SyncLog(syncObjs[0]);

            foreach (var obj in syncObjs)
            {

                //var listMachine = _syncdata.LocalMachine_Get();
                //foreach (var o in listMachine)
                //{
                //    obj.MachineCode = o.MachineCode;
                //    obj.StoreCode = o.StoreCode;
                //}

                SyncOUT(!obj.SyncIN, obj, isPreviewData);

                //Van Linh: Edit    2013-03-14 11:00
                //obj.LastedSyncDate = DateTime.Now;
            }

            //return
            //return _objLog;
            return syncObjs;
        }

        public bool SyncData(SyncObjectCurrent syncObject, DataTable dataHeader, out string message)
        {
            message = "";
            if (syncObject == null)
            {
                message = "Please choose a object to sync.";
                return false;
            }
            if (syncObject.ObjectType == null || syncObject.ObjectType == "")
            {
                if (SyncIN(syncObject.SyncIN, syncObject, dataHeader, syncObject.DataLinesPreview, out message))
                {
                    if (_syncdata.LastedSyncDate_Update(syncObject.SyncSource, syncObject.LastedSyncDate, out message) == false)
                        goto goout;

                    if (syncObject.TransactionNotification != null && syncObject.TransactionNotification.ToString() != "")
                    {
                        var daConn = new daConnection(!syncObject.SyncIN);

                        if (string.IsNullOrWhiteSpace(syncObject.KeyUpdate) == false)
                        {
                            int count = dataHeader.Rows.Count;
                            for (int n = 0; n < count; n++)
                            {
                                daConn.ExecQuery(string.Format(syncObject.TransactionNotification, dataHeader.Rows[n][syncObject.KeyUpdate].ToString()), out message);
                            }
                        }
                    }

                    AddStatusColumn(dataHeader, true, "");
                }
                else
                {
                    goto goout;
                }

                return true;
            }
            else
            {
                var oresult = _sapSync.SAPCreateObject(syncObject, dataHeader, syncObject.DataLinesPreview, !syncObject.SyncIN, out message);

                return oresult;
            }


            goout:
            AddStatusColumn(dataHeader, false, message);
            return false;
        }

        #endregion

        #region private functions

        #region Sync Out
        private bool SyncOUT(bool isLocal, SyncObjectCurrent obj, bool isPreviewData)
        {
            string message;

            if (obj == null)
            {
                SyncSourceLog(obj, SyncStatus.SyncOutInvalidSource, null, null, null);
                goto goout;
            }

            //Lasted Sync Date
            //string lastedSyncDate = "1989-09-27 00:00:00";
            //string lastedSyncDate = "01-JAN-1900 00:00:00";
            string lastedSyncDate = "15-Oct-2015 03:25:09 PM";
            if (obj.LastedSyncDate.HasValue)
            {
                lastedSyncDate = obj.LastedSyncDate.Value.ToString(_formatdate_dmony_hmst);
            }

            //var dataObj = _syncdata.ExcuteDataSyncOut(lastedSyncDate, obj.MachineCode, obj.StoreCode, obj.SyncSource, isLocal, obj.SyncBy, out message);
            var dataObj = _syncdata.ExcuteDataSyncOut(lastedSyncDate, obj.MachineCode, BaseUtil._storeCode, obj.SyncSource, isLocal, obj.SyncBy, obj, out message);
            if (checkHasError(message))
            {
                SyncSourceLog(obj, SyncStatus.SyncOutDataFail, message, null, null);
                goto goout;
            }
            else if (dataObj == null || dataObj.Rows.Count == 0)
            {
                SyncSourceLog(obj, SyncStatus.GetDataEmpty, message, null, null);
                goto goout;
            }
            else
            {
                //object maxkey = null;
                //if (string.IsNullOrWhiteSpace(obj.KeyUpdate) == false)
                //    maxkey = dataObj.Compute(string.Format("max({0})", obj.KeyUpdate), "");

                List<SyncDataObjectLine> dataLines = null;
                if (obj.IsHeader)
                {
                    //dataLines = _syncdata.ExcuteDataLinesSyncOut(lastedSyncDate, obj.MachineCode, obj.StoreCode, obj.SyncSource, isLocal, out message);
                    dataLines = _syncdata.ExcuteDataLinesSyncOut(lastedSyncDate, obj.MachineCode, BaseUtil._storeCode, obj.SyncSource, isLocal, obj, out message);
                    if (checkHasError(message))
                    {
                        SyncSourceLog(obj, SyncStatus.SyncOutDataLinesFail, message, null, null);
                        goto goout;
                    }
                }

                obj.LastedSyncDate = _syncdata.dbServerDate(out message);
                if (checkHasError(message))
                {
                    SyncSourceLog(obj, SyncStatus.SyncOutDataLinesFail, message, null, null);
                    goto goout;
                }

                if (isPreviewData == false)
                {
                    if (obj.ObjectType == null || obj.ObjectType == "")
                    {
                        if (SyncIN(isLocal, obj, dataObj, dataLines, out message))
                            SyncSourceLog(obj, SyncStatus.SyncInDataSuccess, null, null, null);
                        else
                            SyncSourceLog(obj, SyncStatus.SyncInDataFail, message, null, null);
                    }
                    else
                    {
                        if (_sapSync.SAPCreateObject(obj, dataObj, dataLines, isLocal, out message))
                            SyncSourceLog(obj, SyncStatus.SyncInDataSuccess, null, null, null);
                        else
                            SyncSourceLog(obj, SyncStatus.SyncInDataFail, message, null, null);
                    }

                }
                else
                {
                    SyncSourceLog(obj, SyncStatus.GetDataSuccess, null, null, null);
                }

                obj.DataLinesPreview = dataLines;
                obj.DataPreview = dataObj;

            }

            return true;

            goout:
            return false;
        }
        #endregion

        #region Sync In
        private bool SyncIN(bool isLocal, SyncObjectCurrent obj, DataTable dataObj, List<SyncDataObjectLine> dataLines, out string message)
        {
            message = "";
            if (obj == null)
            {
                SyncSourceLog(obj, SyncStatus.SyncOutInvalidSource, null, null, null);
                goto goout;
            }

            if (message == "")
            {
                //if (_syncdata.ExcuteDataSyncIn(obj.SyncSource, obj.StoreCode, dataObj, dataLines, isLocal, obj.SyncBy, out message))
                if (_syncdata.ExcuteDataSyncIn(obj.SyncSource, BaseUtil._storeCode, dataObj, dataLines, isLocal, obj.SyncBy, out message))
                {
                    SyncSourceLog(obj, SyncStatus.SyncInDataSuccess, null, null, null);
                }
                else
                {
                    SyncSourceLog(obj, SyncStatus.SyncInDataFail, message, null, null);
                    goto goout;
                }
            }
            else
            {
                SyncSourceLog(obj, SyncStatus.ReadFileError, message, null, null);
                goto goout;
            }

            return true;

            goout:
            return false;
        }
        #endregion

        #region Write log
        public void SyncSourceLog(SyncObjectCurrent obj, SyncStatus status, string syncDescription, string workingPath, string workingFiles)
        {
            obj.SyncStatus = ((int)status).ToString();
            obj.SyncStatusDesc = SyncStatusDesc(status);
            obj.SyncDescription = syncDescription;
            obj.WorkingPath = workingPath;
            obj.WorkingFiles = workingFiles;

            //SyncSourceLog(_syncLogId, obj);
        }

        private int SyncLog(SyncObjectCurrent obj)
        {
            var result = _syncdata.SyncLogModified(obj);
            if (result != null)
                return result.SyncLogId;

            return 0;
        }

        private bool SyncSourceLog(int syncLogId, SyncObjectCurrent obj)
        {
            if (syncLogId != 0)
            {
                var result = _syncdata.SyncSourceLogModified(syncLogId, obj);
                if (result != null)
                {
                    return true;
                }
            }
            return false;
        }

        private void ClearSyncLog()
        {
            _syncdata.SyncLogClear(SYNCLOG_DATA_DAYS);
        }

        #endregion
        #endregion

        public void AddStatusColumn(DataTable dt, bool success, string message)
        {
            if (dt != null)
            {
                if (dt.Columns.Contains("SyncStatus") == false)
                {
                    if (success)
                        dt.Columns.Add(new DataColumn() { ColumnName = "SyncStatus", DefaultValue = "Success" });
                    else
                        dt.Columns.Add(new DataColumn() { ColumnName = "SyncStatus", DefaultValue = "Fail" });
                }
                else
                {
                    dt.Columns.Remove("SyncStatus");
                    if (success)
                        dt.Columns.Add(new DataColumn() { ColumnName = "SyncStatus", DefaultValue = "Success" });
                    else
                        dt.Columns.Add(new DataColumn() { ColumnName = "SyncStatus", DefaultValue = "Fail" });
                }

                if (dt.Columns.Contains("SyncMessage") == false)
                {
                    dt.Columns.Add(new DataColumn() { ColumnName = "SyncMessage", DefaultValue = message });
                }
                else
                {
                    dt.Columns.Remove("SyncMessage");
                    dt.Columns.Add(new DataColumn() { ColumnName = "SyncMessage", DefaultValue = message });
                }
            }
        }

    }
}
