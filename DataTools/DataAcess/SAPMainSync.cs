﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DataTools.Models;
using DataTools.DataAccess.Sync.FtpSync;
using DataTools.DataAccess.Sync.SqlSync;
using DataTools.Utility;

namespace DataTools.DataAccess.Sync
{
    public class SAPMainSync : BaseData
    {
        private SAPSyncFtpEntry _syncftp = null;
        private SAPSyncSqlEntry _syncSQL = null;

        public List<SyncObjectCurrent> Sync(List<SyncObjectCurrent> syncObjs, bool isPreviewData)
        {
            if (BaseUtil._syncType == eSyncType.ByDirectSql)
            {
                _syncSQL = new SAPSyncSqlEntry();
                _syncSQL.SyncData(syncObjs, isPreviewData);
            }
            else if (BaseUtil._syncType == eSyncType.ByFtp)
            {
                _syncftp = new SAPSyncFtpEntry();
                _syncftp.SyncData(syncObjs);
            }

            return syncObjs;
        }

        //Only support ByDirectSql
        public bool Sync(SyncObjectCurrent syncObject, DataTable dataHeader, out string message)
        {
            bool oresult = false;
            if (BaseUtil._syncType == eSyncType.ByDirectSql)
            {
                _syncSQL = new SAPSyncSqlEntry();
                oresult=_syncSQL.SyncData(syncObject, dataHeader,out message);
            }

            return oresult;
        }



    }
}
