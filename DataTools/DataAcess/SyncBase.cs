﻿using System;
using System.Xml;
using System.Xml.Linq;
using System.Data.Linq;
using System.Data;
using System.Reflection;
using System.Transactions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Packaging;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;


using DataTools.Models;
using DataTools.Utility;
using System.Diagnostics;

namespace DataTools.DataAccess.Sync
{
    public class SyncBase : BaseXml
    {
        /*Delete backup data*/
        public const int SYNCIN_FILEBACKUP_DAYS = 30;
        public const int SYNCOUT_FILEBACKUP_DAYS = 30;
        public const int SYNCLOG_DATA_DAYS = 3;

        /*XML Settings*/
        private string _xmlNodeRoot = "root";
        private string _xmlNodeBO = "bo";
        private string _xmlNodeRow = "row";
        private string _serviceLogpath = @"C:/lg.log/";
        private string _serviceLogFile = @"C:/lg.log/syncService.txt";

        /**/
        public const string _us_sync_notification = "POSONE_US_SYNC_NOTIFICATION";

        public string _messageOutCurrent { get; set; }

        public void WriteEventLog(string message)
        {
            if (message == "") return;

            if (!EventLog.SourceExists("WSONE.WMS"))
            {
                EventLog.CreateEventSource("WSONE.WMS", "WSONE.WMS");
            }
            EventLog myLog = new EventLog();
            myLog.Source = "WSONE.WMS";
            myLog.WriteEntry(message);
        }

        public string SetValueSql(string value, bool isNumeric, bool isDateTime, bool isBoolean)
        {
            try
            {
                if (value == null)
                {
                    value = "NULL";
                }
                else if (value == "")
                {
                    if (isNumeric || isDateTime || isBoolean)
                        value = "NULL";
                    else
                        value = string.Format("N'{0}'", SqlReplaceSpecial(value));
                }
                else if (isNumeric)
                {

                }
                else if (isDateTime)
                {
                    var obj = Convert.ToDateTime(value);
                    value = string.Format("'{0}'", obj.ToString(_formatdate_ymd_hmsf));
                }
                else if (isBoolean)
                {
                    var obj = Convert.ToBoolean(value);
                    if (obj == false)
                        value = "0";
                    else
                        value = "1";

                }
                else
                {
                    value = StringDecode(value);
                    value = string.Format("N'{0}'", SqlReplaceSpecial(value));
                }

                return value;
            }
            catch (Exception ex)
            {
                return "NULL";
            }
        }

        public string NewLine(string s)
        {
            return "    " + s + "    " + Microsoft.VisualBasic.Constants.vbNewLine;
        }

        public DateTime? ParseDateNoTime(DateTime? date)
        {
            try
            {

                if (date.HasValue)
                    return Convert.ToDateTime(date.Value.ToString(_formatdate_ymd));

                return null;
            }
            catch
            {

            }
            return date;
        }

        public string ParseDateToSAP(object value)
        {
            try
            {
                var date = Convert.ToDateTime(value);

                return date.ToString("yyyyMMdd");
            }
            catch
            {

            }
            return "";
        }

        public bool checkHasError(string message)
        {
            if (message == "" || message == null)
            {
                return false;
            }

            return true;
        }

        public string getFunctionError(string functionName, string message)
        {
            var s = string.Format("[{0}] {1}", functionName, message);
            return s;
        }

        public string PathReplaceSeparator(string s)
        {
            if (s == null || s.Trim() == "") return "";

            if (s.StartsWith("/"))
                s = s.Substring(1);
            if (s.EndsWith("/") == false)
                s = s + "/";

            return s;
        }

        public string GetFileNameNoExt(string filename)
        {
            if (filename == null || filename.Trim() == "") return "";

            var n = filename.LastIndexOf("/");
            if (n > 0)
                n = n + 1;
            else
                n = 0;

            filename = filename.Substring(n);

            filename = filename.Substring(0, filename.LastIndexOf("."));

            return filename;
        }

        public string SqlReplaceSpecial(string s)
        {
            if (s == null || s.Trim() == "") return "";

            s = s.Replace("'", "''");

            return s;
        }

        public string StringDecode(string s)
        {
            var obj = HttpUtility.HtmlDecode(s);
            return obj;
        }

        public string StringEncode(string s)
        {
            var obj = HttpUtility.HtmlEncode(s);
            return obj;
        }

        public string AddPrefix(string s, string prexString, int leng)
        {
            for (int i = 0; i < leng; i++)
                s = prexString + s;

            s = s.Substring(s.Length - leng, leng);

            return s;
        }

        public void WriteLogFile(String s)
        {
            System.IO.StreamWriter oWrite = null;

            if (!Directory.Exists(_serviceLogpath))
                Directory.CreateDirectory(_serviceLogpath);

            if (!File.Exists(_serviceLogFile))
                oWrite = File.CreateText(_serviceLogFile);
            else
                oWrite = File.AppendText(_serviceLogFile);

            //System.IO.StreamWriter file = new System.IO.StreamWriter(_serviceLogFile, true);

            oWrite.WriteLine(s);
            oWrite.Close();
        }

        public T NewInstant<T>(T DataObject) where T : new()
        {
            T result = new T();

            if (DataObject == null) return result;

            PropertyInfo[] oProps = null;

            if (oProps == null)
            {
                oProps = ((Type)(new T()).GetType()).GetProperties();
            }

            foreach (PropertyInfo pi in oProps)
            {
                var value = pi.GetValue(DataObject, null);
                pi.SetValue(result, value, null);
            }

            return result;
        }

        public bool isNumber(object obj)
        {
            try
            {
                var o = Convert.ToDecimal(obj);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #region Enum
        public enum eSyncType2
        {
            /// <summary>
            /// OUT
            /// </summary>
            O = 1,
            /// <summary>
            /// IN
            /// </summary>
            I = 2,
        }
        public enum SyncServiceType
        {
            All = 0,
            Manual = 1,
            Automatic = 2,
        }

        public List<Enum_View> SyncServiceTypeGet()
        {
            var results = new List<Enum_View>();
            results.Add(new Enum_View() { Code = ((int)SyncServiceType.All).ToString(), Name = "-------" });
            results.Add(new Enum_View() { Code = ((int)SyncServiceType.Manual).ToString(), Name = SyncServiceType.Manual.ToString() });
            results.Add(new Enum_View() { Code = ((int)SyncServiceType.Automatic).ToString(), Name = SyncServiceType.Automatic.ToString() });

            return results;
        }

        public enum SyncConditionType
        {
            None = 0,
            MachineCode = 1,
            StoreCode = 2,
            GetDate = 3,
            IsNull = 4,
            IsNotNull = 5,
            LastedSyncDate = 6,
            Equal = 7,
            NotEqual = 8,
            BeginsWith = 9,
            EndsWith = 10,
            Contains = 11,
            NotContains = 12,
            LessThan = 13,
            LessThanOrEqualTo = 14,
            GreaterThan = 15,
            GreaterThanOrEqualTo = 16,
            In = 17,
            NotIn = 18,
        }

        public List<Enum_View> SyncConditionTypeDesc()
        {
            var results = new List<Enum_View>();
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.None).ToString(), Name = "None" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.MachineCode).ToString(), Name = "{MachineCode}" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.StoreCode).ToString(), Name = "{StoreCode}" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.GetDate).ToString(), Name = "{Get Date}" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.IsNull).ToString(), Name = "{Is Null}" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.IsNotNull).ToString(), Name = "{Is Not Null}" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.LastedSyncDate).ToString(), Name = "{Lasted Sync Date}" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.Equal).ToString(), Name = "Equal" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.NotEqual).ToString(), Name = "Not Equal" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.BeginsWith).ToString(), Name = "Begins With" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.EndsWith).ToString(), Name = "Ends With" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.Contains).ToString(), Name = "Contains" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.NotContains).ToString(), Name = "Not Contains" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.LessThan).ToString(), Name = "Less Than" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.LessThanOrEqualTo).ToString(), Name = "Less Than Or Equal To" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.GreaterThan).ToString(), Name = "Greater Than" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.GreaterThanOrEqualTo).ToString(), Name = "Greater Than Or Equal To" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.In).ToString(), Name = "In" });
            results.Add(new Enum_View() { Code = ((int)SyncConditionType.NotIn).ToString(), Name = "Not In" });

            return results;
        }

        public List<SyncObject_View> ObjectTypeList()
        {
            var results = new List<SyncObject_View>();
            results.Add(new SyncObject_View() { Code = "", Name = "" });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oChartOfAccounts).ToString(), Name = "Chart of Accounts", IsSAPB1 = true });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oBusinessPartners).ToString(), Name = "Business Partners", IsSAPB1 = true });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oItems).ToString(), Name = "Items", IsSAPB1 = true });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oJournalVouchers).ToString(), Name = "Journal Vouchers", IsSAPB1 = true });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oJournalEntries).ToString(), Name = "Journal Entry", IsSAPB1 = true });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oInvoices).ToString(), Name = "A/R Invoice", IsSAPB1 = true });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oCreditNotes).ToString(), Name = "A/R Credit Memo", IsSAPB1 = true });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oIncomingPayments).ToString(), Name = "Incoming Payments", IsSAPB1 = true });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oPurchaseInvoices).ToString(), Name = "A/P Invoice", IsSAPB1 = true });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oPurchaseCreditNotes).ToString(), Name = "A/P Credit Memo", IsSAPB1 = true });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oVendorPayments).ToString(), Name = "Outgoing Payments", IsSAPB1 = true });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oDownPayments).ToString(), Name = "A/R Down Payment Invoice", IsSAPB1 = true });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oPurchaseDownPayments).ToString(), Name = "A/P Down Payment Invoice", IsSAPB1 = true });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oProjects).ToString(), Name = "Projects", IsSAPB1 = true });
            results.Add(new SyncObject_View() { Code = ((int)SyncObject.oSendMail).ToString(), Name = "Send Mail", IsSAPB1 = false });
            return results;


        }

        public enum ConditionType
        {
            None = 0,
            Equal = 1,
            NotEqual = 2,
            BeginsWith = 3,
            EndsWith = 4,
            Contains = 5,
            Empty = 6,
        }


        //Sync Status
        public enum SyncStatus
        {
            None = -1,
            SystemError = 0,
            UploadFileFail = 1,
            DownloadFileFail = 2,
            NotFoundFile = 3,
            ReadFileError = 4,
            WriteFileError = 5,
            SyncInDataFail = 6,
            SyncOutDataFail = 7,
            SyncInDataSuccess = 8,
            SyncOutDataSuccess = 9,
            SyncOutNoData = 10,
            SyncInNoData = 11,
            SyncOutInvalidSource = 12,
            SyncInInvalidSource = 13,
            SyncOutDataLinesFail = 14,
            SyncOutDataLinesFail_SAP = 15,
            BackupFileSuccess = 16,
            BackupFileFail = 17,
            InvalidShop = 18,
            NoPropertyinSource = 19,
            ZipFileFail = 20,
            ExtractFileFail = 21,
            GetDataSuccess = 22,
            GetDataFail = 23,
            GetDataEmpty = 24,
        }

        public string SyncStatusDesc(SyncStatus type)
        {
            switch (type)
            {
                case SyncStatus.None:
                    return "-------";
                case SyncStatus.SystemError:
                    return BaseUtil.SystemError;
                case SyncStatus.UploadFileFail:
                    return BaseUtil.UploadFileFail;
                case SyncStatus.DownloadFileFail:
                    return BaseUtil.DownloadFileFail;
                case SyncStatus.NotFoundFile:
                    return BaseUtil.NotFoundFile;
                case SyncStatus.ReadFileError:
                    return BaseUtil.ReadFileError;
                case SyncStatus.WriteFileError:
                    return BaseUtil.WriteFileError;
                case SyncStatus.SyncInDataFail:
                    return BaseUtil.SyncInDataFail;
                case SyncStatus.SyncInDataSuccess:
                    return BaseUtil.SyncInDataSuccess;
                case SyncStatus.SyncOutDataFail:
                    return BaseUtil.SyncOutDataFail;
                case SyncStatus.SyncOutDataSuccess:
                    return BaseUtil.SyncOutDataSuccess;
                case SyncStatus.SyncOutNoData:
                    return BaseUtil.SyncOutNoData;
                case SyncStatus.SyncInNoData:
                    return BaseUtil.SyncInNoData;
                case SyncStatus.SyncOutInvalidSource:
                    return BaseUtil.SyncOutInvalidSource;
                case SyncStatus.SyncInInvalidSource:
                    return BaseUtil.SyncInInvalidSource;
                case SyncStatus.SyncOutDataLinesFail:
                    return BaseUtil.SyncOutDataLinesFail;
                case SyncStatus.SyncOutDataLinesFail_SAP:
                    return BaseUtil.SyncOutDataLinesFail_Sap;
                case SyncStatus.BackupFileSuccess:
                    return BaseUtil.BackupFileSuccess;
                case SyncStatus.BackupFileFail:
                    return BaseUtil.BackupFileFail;
                case SyncStatus.InvalidShop:
                    return BaseUtil.InvalidShop;
                case SyncStatus.NoPropertyinSource:
                    return BaseUtil.NoPropertyinSource;
                case SyncStatus.ZipFileFail:
                    return BaseUtil.ZipFileFail;
                case SyncStatus.ExtractFileFail:
                    return BaseUtil.ExtractFileFail;
                case SyncStatus.GetDataSuccess:
                    return "Get data successfully.";
                case SyncStatus.GetDataFail:
                    return "Get data fail. Please check data or query.";
                case SyncStatus.GetDataEmpty:
                    return "Data empty. Please check data or query.";
                default: return "-------";
            }
        }

        public List<Enum_View> SyncStatusGet()
        {
            var results = new List<Enum_View>();
            results.Add(new Enum_View() { Code = (((int)SyncStatus.None).ToString()), Name = SyncStatusDesc(SyncStatus.None) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.SystemError).ToString()), Name = ((int)SyncStatus.SystemError).ToString() + " - " + SyncStatusDesc(SyncStatus.SystemError) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.UploadFileFail).ToString()), Name = ((int)SyncStatus.UploadFileFail).ToString() + " - " + SyncStatusDesc(SyncStatus.UploadFileFail) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.DownloadFileFail).ToString()), Name = ((int)SyncStatus.DownloadFileFail).ToString() + " - " + SyncStatusDesc(SyncStatus.DownloadFileFail) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.NotFoundFile).ToString()), Name = ((int)SyncStatus.NotFoundFile).ToString() + " - " + SyncStatusDesc(SyncStatus.NotFoundFile) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.ReadFileError).ToString()), Name = ((int)SyncStatus.ReadFileError).ToString() + " - " + SyncStatusDesc(SyncStatus.ReadFileError) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.WriteFileError).ToString()), Name = ((int)SyncStatus.WriteFileError).ToString() + " - " + SyncStatusDesc(SyncStatus.WriteFileError) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.SyncInDataFail).ToString()), Name = ((int)SyncStatus.SyncInDataFail).ToString() + " - " + SyncStatusDesc(SyncStatus.SyncInDataFail) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.SyncOutDataFail).ToString()), Name = ((int)SyncStatus.SyncOutDataFail).ToString() + " - " + SyncStatusDesc(SyncStatus.SyncOutDataFail) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.SyncInDataSuccess).ToString()), Name = ((int)SyncStatus.SyncInDataSuccess).ToString() + " - " + SyncStatusDesc(SyncStatus.SyncInDataSuccess) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.SyncOutDataSuccess).ToString()), Name = ((int)SyncStatus.SyncOutDataSuccess).ToString() + " - " + SyncStatusDesc(SyncStatus.SyncOutDataSuccess) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.SyncOutNoData).ToString()), Name = ((int)SyncStatus.SyncOutNoData).ToString() + " - " + SyncStatusDesc(SyncStatus.SyncOutNoData) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.SyncInNoData).ToString()), Name = ((int)SyncStatus.SyncInNoData).ToString() + " - " + SyncStatusDesc(SyncStatus.SyncInNoData) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.SyncOutInvalidSource).ToString()), Name = ((int)SyncStatus.SyncOutInvalidSource).ToString() + " - " + SyncStatusDesc(SyncStatus.SyncOutInvalidSource) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.SyncInInvalidSource).ToString()), Name = ((int)SyncStatus.SyncInInvalidSource).ToString() + " - " + SyncStatusDesc(SyncStatus.SyncInInvalidSource) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.SyncOutDataLinesFail).ToString()), Name = ((int)SyncStatus.SyncOutDataLinesFail).ToString() + " - " + SyncStatusDesc(SyncStatus.SyncOutDataLinesFail) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.SyncOutDataLinesFail_SAP).ToString()), Name = ((int)SyncStatus.SyncOutDataLinesFail_SAP).ToString() + " - " + SyncStatusDesc(SyncStatus.SyncOutDataLinesFail_SAP) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.BackupFileSuccess).ToString()), Name = ((int)SyncStatus.BackupFileSuccess).ToString() + " - " + SyncStatusDesc(SyncStatus.BackupFileSuccess) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.BackupFileFail).ToString()), Name = ((int)SyncStatus.BackupFileFail).ToString() + " - " + SyncStatusDesc(SyncStatus.BackupFileFail) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.InvalidShop).ToString()), Name = ((int)SyncStatus.InvalidShop).ToString() + " - " + SyncStatusDesc(SyncStatus.InvalidShop) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.NoPropertyinSource).ToString()), Name = ((int)SyncStatus.NoPropertyinSource).ToString() + " - " + SyncStatusDesc(SyncStatus.NoPropertyinSource) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.ZipFileFail).ToString()), Name = ((int)SyncStatus.ZipFileFail).ToString() + " - " + SyncStatusDesc(SyncStatus.ZipFileFail) });
            results.Add(new Enum_View() { Code = (((int)SyncStatus.ExtractFileFail).ToString()), Name = ((int)SyncStatus.ExtractFileFail).ToString() + " - " + SyncStatusDesc(SyncStatus.ExtractFileFail) });

            //return results.OrderBy(o => o.Code).ToList();
            return results;
        }

        //Server Type
        public enum ServerType
        {
            PosClient = 0,
            PosServer = 1,
        }

        //Sync Type
        public enum eSyncType
        {
            SyncIn = 1,
            SyncOut = 2,
        }

        #endregion

        #region Data to XML

        //by XML Document
        public void _DataTableToXMLFile(DataTable dataObj, string fileName, out string message)
        {
            try
            {
                message = "";
                dataObj.WriteXml(fileName);

                //XDocument doc = new XDocument(
                //    new XDeclaration("1.0", "UTF-8", "yes"),
                //    new XElement("CustomerGroup",
                //        from p in dataObj.AsEnumerable()
                //        select new XElement("CustomerGroup",
                //            new XAttribute("GroupCode", p["GroupCode"]),
                //            new XElement("GroupCode", p["GroupCode"]),
                //            new XElement("GroupName", p["GroupName"]),
                //            new XElement("GroupType", p["GroupType"]))));

                //doc.Save(fileName);
            }
            catch (Exception ex)
            {
                message = getFunctionError("GenerateDatatoXml", ex.Message);
            }
        }

        public void _DataTableToXMLFile(DataTable dataObject, String xmlElement, string fileName, out string message)
        {
            try
            {
                message = "";

                XmlWriter xml = XmlWriter.Create(fileName);

                xml.WriteStartElement(_xmlNodeRoot);
                xml.WriteStartElement(_xmlNodeBO);
                xml.WriteStartElement(xmlElement);

                foreach (DataRow line in dataObject.Rows)
                {
                    xml.WriteStartElement(_xmlNodeRow);
                    foreach (DataColumn col in dataObject.Columns)
                    {
                        string colName = col.ColumnName;
                        string value;

                        if (line[colName] == null)
                            value = "";
                        else
                            value = line[colName].ToString();

                        xml.WriteStartElement(colName);
                        xml.WriteString(value);
                        xml.WriteEndElement();
                    }
                    xml.WriteEndElement();
                }

                xml.WriteEndElement();
                xml.WriteEndElement();
                xml.WriteEndElement();
                xml.Flush();

            }
            catch (Exception ex)
            {
                message = getFunctionError("GenerateDatatoXml", ex.Message);
            }
        }

        public XmlDocument DataTableToXMLFile(DataTable dataObject, String xmlElement, string fileName, out string message)
        {
            try
            {
                if (dataObject == null || dataObject.Rows.Count == 0)
                {
                    message = getFunctionError("DataTableToXMLFile-1-v2", BaseUtil.GetDataEmpty);
                    return null;
                }

                message = "";

                XmlDocument xmlDoc = new XmlDocument();

                // Write down the XML declaration
                XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
                xmlDoc.InsertBefore(xmlDeclaration, xmlDoc.DocumentElement);

                //root
                XmlElement rootNode = xmlDoc.CreateElement(_xmlNodeRoot);
                xmlDoc.AppendChild(rootNode);

                //bo
                XmlElement boNode = xmlDoc.CreateElement(_xmlNodeBO);
                rootNode.AppendChild(boNode);

                XmlElement parentNode = xmlDoc.CreateElement(xmlElement);
                boNode.AppendChild(parentNode);

                foreach (DataRow line in dataObject.Rows)
                {
                    //row
                    XmlElement rowNode = xmlDoc.CreateElement(_xmlNodeRow);
                    parentNode.AppendChild(rowNode);

                    foreach (DataColumn col in dataObject.Columns)
                    {
                        string colName = col.ColumnName;
                        string value;

                        if (line[colName] == null)
                            value = "";
                        else
                            if (col.DataType.Name == "DateTime" && line[colName].ToString().Trim() != "")
                            value = Convert.ToDateTime(line[colName].ToString()).ToString(_formatdate_ymd_hms);
                        //else if (isNumber(line[colName].ToString().Trim()))
                        //    value = Convert.ToDecimal(line[colName].ToString()).ToString(_formatNumber);
                        else
                            value = line[colName].ToString();

                        // Create the required nodes
                        XmlElement colNode = xmlDoc.CreateElement(colName);
                        // append the nodes to the parentNode without the value
                        rowNode.AppendChild(colNode);

                        // retrieve the text 
                        XmlText valueNode = xmlDoc.CreateTextNode(value);
                        // save the value of the fields into the nodes
                        colNode.AppendChild(valueNode);
                    }
                }

                //// Save to the XML file
                //xmlDoc.Save(fileName);
                return xmlDoc;
            }
            catch (Exception ex)
            {
                message = getFunctionError("GenerateDatatoXml1", ex.Message);
                return null;
            }
        }

        public XmlDocument DataTableToXMLFile(DataTable dataObjHeader, String xmlElement, List<SyncDataObjectLine> dataObjLines, string fileName, out string message)
        {
            try
            {
                if (dataObjHeader == null || dataObjHeader.Rows.Count == 0)
                {
                    message = getFunctionError("DataTableToXMLFile-1-v2", BaseUtil.GetDataEmpty);
                    return null;
                }

                message = "";

                XmlDocument xmlDoc = new XmlDocument();

                // Write down the XML declaration
                XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
                xmlDoc.InsertBefore(xmlDeclaration, xmlDoc.DocumentElement);

                //root
                XmlElement rootNode = xmlDoc.CreateElement(_xmlNodeRoot);
                xmlDoc.AppendChild(rootNode);

                foreach (DataRow header in dataObjHeader.Rows)
                {
                    //bo
                    XmlElement boNode = xmlDoc.CreateElement(_xmlNodeBO);
                    rootNode.AppendChild(boNode);

                    //Header
                    XmlElement headerNode = xmlDoc.CreateElement(xmlElement);
                    boNode.AppendChild(headerNode);

                    //row
                    XmlElement rowNode = xmlDoc.CreateElement(_xmlNodeRow);
                    headerNode.AppendChild(rowNode);

                    foreach (DataColumn col in dataObjHeader.Columns)
                    {
                        string colName = col.ColumnName;

                        string value;

                        if (header[colName] == null)
                            value = "";
                        else
                            if (col.DataType.Name == "DateTime" && header[colName].ToString().Trim() != "")
                            value = Convert.ToDateTime(header[colName].ToString()).ToString(_formatdate_ymd_hms);
                        //else if (isNumber(header[colName].ToString().Trim()))
                        //    value = Convert.ToDecimal(header[colName].ToString()).ToString(_formatNumber);
                        else
                            value = StringEncode(header[colName].ToString());

                        // Create the required nodes
                        XmlElement colNode = xmlDoc.CreateElement(colName);
                        // append the nodes to the parentNode without the value
                        rowNode.AppendChild(colNode);

                        // retrieve the text 
                        XmlText valueNode = xmlDoc.CreateTextNode(value);
                        // save the value of the fields into the nodes
                        colNode.AppendChild(valueNode);
                    }

                    //Lines

                    foreach (var source in dataObjLines)
                    {
                        if (source.DataSource != null && source.DataSource.Rows.Count > 0)
                        {
                            //begin Line
                            XmlElement lineNode = xmlDoc.CreateElement(source.XmlElement);
                            boNode.AppendChild(lineNode);



                            var lineByKey = source.DataSource.Select(string.Format("{0}='{1}'", source.LineRefenceKey, header[source.HeaderKey]));
                            foreach (var line in lineByKey)
                            {
                                //begin Line
                                //row
                                XmlElement rowNodeLine = xmlDoc.CreateElement(_xmlNodeRow);
                                lineNode.AppendChild(rowNodeLine);
                                foreach (DataColumn col in source.DataSource.Columns)
                                {
                                    string colName = col.ColumnName;

                                    string value;

                                    if (line[colName] == null)
                                        value = "";
                                    else
                                        if (col.DataType.Name == "DateTime" && line[colName].ToString().Trim() != "")
                                        value = Convert.ToDateTime(line[colName].ToString()).ToString(_formatdate_ymd_hms);
                                    //else if (isNumber(line[colName].ToString().Trim()))
                                    //    value = Convert.ToDecimal(line[colName].ToString()).ToString(_formatNumber);
                                    else
                                        value = StringEncode(line[colName].ToString());

                                    // Create the required nodes
                                    XmlElement colNodeLine = xmlDoc.CreateElement(colName);
                                    // append the nodes to the parentNode without the value
                                    rowNodeLine.AppendChild(colNodeLine);

                                    // retrieve the text 
                                    XmlText valueNodeLine = xmlDoc.CreateTextNode(value);
                                    // save the value of the fields into the nodes
                                    colNodeLine.AppendChild(valueNodeLine);
                                }

                            }
                        }
                    }
                }

                //// Save to the XML file
                //xmlDoc.Save(fileName);

                return xmlDoc;
            }
            catch (Exception ex)
            {
                message = getFunctionError("DataTableToXMLString-2", ex.Message);
                return null;
            }
        }

        //by String
        public String _DataTableToXMLString<T>(List<T> DataObject, String xmlElement, out string message)
        {
            try
            {
                message = "";
                String xml = String.Empty;

                if (DataObject == null) return xml;
                if (xmlElement == null || xmlElement.Trim() == "") return xml;

                PropertyInfo[] oProps = null;

                xml = "";
                xml = @"<?xml version=""1.0"" encoding=""utf-8""?>";
                //<root>
                xml += String.Format("<{0}>", _xmlNodeRoot);
                //<bo>
                xml += String.Format("<{0}>", _xmlNodeBO);
                //
                xml += String.Format("<{0}>", xmlElement);
                foreach (T line in DataObject)
                {
                    //<row>
                    xml += String.Format("<{0}>", _xmlNodeRow);
                    if (oProps == null)
                    {
                        oProps = ((Type)line.GetType()).GetProperties();
                    }

                    foreach (PropertyInfo pi in oProps)
                    {
                        object value;

                        if (pi.GetValue(line, null) == null)
                            value = DBNull.Value;
                        else
                            value = StringEncode(pi.GetValue(line, null).ToString());

                        xml += String.Format("<{0}>{1}</{0}>", pi.Name, value);
                    }
                    //</row>
                    xml += String.Format("</{0}>", xmlElement);
                }
                //
                xml += String.Format("</{0}>", xmlElement);
                //</bo>
                xml += String.Format("</{0}>", _xmlNodeBO);
                //</root>
                xml += String.Format("</{0}>", _xmlNodeRoot);

                return xml;
            }
            catch (Exception ex)
            {
                message = getFunctionError("DataTableToXMLString-0", ex.Message);
                return String.Empty;
            }
        }

        public String _DataTableToXMLString(DataTable dataObject, String xmlElement, out string message)
        {
            try
            {
                message = "";
                String xml = String.Empty;

                if (dataObject == null) return xml;
                if (xmlElement == null || xmlElement.Trim() == "") return xml;

                var listCol = dataObject.Columns;

                xml = @"<?xml version=""1.0"" encoding=""utf-8""?>";
                xml += String.Format("<{0}>", _xmlNodeRoot);
                xml += String.Format("<{0}>", _xmlNodeBO);
                xml += String.Format("<{0}>", xmlElement);
                foreach (DataRow line in dataObject.Rows)
                {
                    xml += String.Format("<{0}>", _xmlNodeRow);
                    foreach (DataColumn col in listCol)
                    {
                        string colName = col.ColumnName;
                        object value;

                        if (line[colName] == null)
                            value = DBNull.Value;
                        else
                            value = StringEncode(line[colName].ToString());

                        xml += String.Format("<{0}>{1}</{0}>", colName, value);
                    }
                    xml += String.Format("</{0}>", _xmlNodeRow);
                }
                xml += String.Format("</{0}>", xmlElement);
                xml += String.Format("</{0}>", _xmlNodeBO);
                xml += String.Format("</{0}>", _xmlNodeRoot);

                return xml;
            }
            catch (Exception ex)
            {
                message = getFunctionError("DataTableToXMLString-1", ex.Message);
                return String.Empty;
            }
        }

        public String _DataTableToXMLString(DataTable dataObjHeader, String xmlElement, List<SyncDataObjectLine> dataObjLines, out string message)
        {
            try
            {
                message = "";
                String xml = String.Empty;

                if (dataObjHeader == null) return xml;
                if (xmlElement == null || xmlElement.Trim() == "") return xml;

                xml = @"<?xml version=""1.0"" encoding=""utf-8""?>";
                xml += String.Format("<{0}>", _xmlNodeRoot);
                foreach (DataRow header in dataObjHeader.Rows)
                {
                    xml += String.Format("<{0}>", _xmlNodeBO);

                    //Header
                    xml += String.Format("<{0}>", xmlElement);
                    xml += String.Format("<{0}>", _xmlNodeRow);
                    foreach (DataColumn col in dataObjHeader.Columns)
                    {
                        string colName = col.ColumnName;

                        object value;

                        if (header[colName] == null)
                            value = DBNull.Value;
                        else
                            value = StringEncode(header[colName].ToString());

                        xml += String.Format("<{0}>{1}</{0}>", colName, value);
                    }
                    //End Header
                    xml += String.Format("</{0}>", _xmlNodeRow);
                    xml += String.Format("</{0}>", xmlElement);

                    //Lines

                    foreach (var source in dataObjLines)
                    {
                        if (source.DataSource != null)
                        {
                            //begin Line
                            xml += String.Format("<{0}>", source.XmlElement);
                            var lineByKey = source.DataSource.Select(string.Format("{0}='{1}'", source.LineRefenceKey, header[source.HeaderKey]));
                            foreach (var line in lineByKey)
                            {
                                //begin Line
                                xml += String.Format("<{0}>", _xmlNodeRow);
                                foreach (DataColumn col in source.DataSource.Columns)
                                {
                                    string colName = col.ColumnName;

                                    object value;

                                    if (line[colName] == null)
                                        value = DBNull.Value;
                                    else
                                        value = StringEncode(line[colName].ToString());

                                    xml += String.Format("<{0}>{1}</{0}>", colName, value);
                                }
                                xml += String.Format("</{0}>", _xmlNodeRow);

                            }
                            //end Line
                            xml += String.Format("</{0}>", source.XmlElement);
                        }
                    }
                    xml += String.Format("</{0}>", _xmlNodeBO);
                }
                xml += String.Format("</{0}>", _xmlNodeRoot);

                return xml;
            }
            catch (Exception ex)
            {
                message = getFunctionError("DataTableToXMLString-2", ex.Message);
                return String.Empty;
            }
        }

        //Save to file
        public bool _SaveDataToXmlFile(DataTable dataObj, String xmlElement, string fileName, out string message)
        {
            try
            {
                message = "";
                XmlDocument x = new XmlDocument();
                var xmlString = _DataTableToXMLString(dataObj, xmlElement, out message);


                if (checkHasError(message))
                {
                    return false;
                }
                x.LoadXml(xmlString);
                x.Save(fileName);
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("_SaveDataToXmlFile", ex.Message);
                return false;
            }
        }

        public bool SaveDataToXmlFile(DataTable dataObjHeader, List<SyncDataObjectLine> dataObjLines, List<string> listQuery, String xmlElement, string fileName, bool isServer, out string message)
        {
            var daConn = new daConnection(!isServer);
            try
            {
                message = "";
                if (dataObjLines == null || dataObjLines.Count == 0)
                {
                    XmlDocument xmlDoc = DataTableToXMLFile(dataObjHeader, xmlElement, fileName, out message);
                    if (checkHasError(message))
                    {
                        return false;
                    }
                    else
                    {

                        if (listQuery == null || listQuery.Count == 0)
                        {
                            //
                            xmlDoc.Save(fileName);

                            ////update source query ..... will
                            //DeleteFile(fileName, out message);
                            //if (checkHasError(message))
                            //    return false;

                        }
                        else
                        {

                            daConn.BeginTransaction();

                            foreach (var query in listQuery)
                            {
                                if (daConn.ExecQueryTrans(query, out message) == false)
                                    daConn.RollbackTransaction();
                            }

                            //Save to file xml
                            xmlDoc.Save(fileName);

                            //Commit
                            daConn.CommitTransaction();
                        }
                    }
                }
                else
                {
                    XmlDocument xmlDoc = DataTableToXMLFile(dataObjHeader, xmlElement, dataObjLines, fileName, out message);
                    if (checkHasError(message))
                    {
                        return false;
                    }
                    else
                    {
                        if (listQuery == null || listQuery.Count == 0)
                        {
                            //
                            xmlDoc.Save(fileName);

                            ////update source query ..... will
                            //DeleteFile(fileName, out message);
                            //if (checkHasError(message))
                            //    return false;
                        }
                        else
                        {
                            daConn.BeginTransaction();

                            foreach (var query in listQuery)
                            {
                                if (daConn.ExecQueryTrans(query, out message) == false)
                                    daConn.RollbackTransaction();
                            }

                            //Save to file xml
                            xmlDoc.Save(fileName);

                            //Commit
                            daConn.CommitTransaction();
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                daConn.RollbackTransaction();
                message = getFunctionError("SaveDataToXmlFile", ex.Message);
                return false;
            }
        }

        //SAP XML
        public String DataTableToSAPXMLString(DataTable dataObject, String xmlElement, string sapObject, string sapVersion, out string message)
        {
            try
            {
                if (dataObject == null || dataObject.Rows.Count == 0)
                {
                    message = getFunctionError("DataTableToSAPXMLString-0-v2", BaseUtil.GetDataEmpty);
                    return "";
                }

                if (xmlElement == null || xmlElement.Trim() == "")
                {
                    message = getFunctionError("DataTableToSAPXMLString-0", "Source must have xmlElement.");
                    return "";
                }

                message = "";
                String xml = String.Empty;


                xml = @"<?xml version=""1.0"" encoding=""utf-16""?>";

                xml += String.Format("<{0}>", "BOM");
                foreach (DataRow line in dataObject.Rows)
                {
                    xml += String.Format("<{0}>", "BO");
                    xml = xml + string.Format("<AdmInfo><Object>{0}</Object><Version>{1}</Version></AdmInfo>", sapObject, sapVersion);
                    xml += String.Format("<{0}>", xmlElement);
                    xml += String.Format("<{0}>", "row");
                    foreach (DataColumn col in dataObject.Columns)
                    {
                        string colName = col.ColumnName;
                        object value = line[colName] == null ? DBNull.Value : line[colName];
                        if (col.DataType.Name == "DateTime")
                            value = ParseDateToSAP(value);
                        if (col.DataType.Name == "String")
                            value = StringEncode(value.ToString());

                        xml += String.Format("<{0}>{1}</{0}>", colName, value);
                    }
                    xml += String.Format("</{0}>", "row");
                    xml += String.Format("</{0}>", xmlElement);
                    xml += String.Format("</{0}>", "BO");
                }
                xml += String.Format("</{0}>", "BOM");

                return xml;
            }
            catch (Exception ex)
            {
                message = getFunctionError("DataTableToSAPXMLString", ex.Message);
                return String.Empty;
            }
        }

        public String DataTableToSAPXMLString(DataTable dataObjHeader, String xmlElement, List<SyncDataObjectLine> dataObjLines, string sapObject, string sapVersion, out string message)
        {
            try
            {
                if (dataObjHeader == null || dataObjHeader.Rows.Count == 0)
                {
                    message = getFunctionError("DataTableToSAPXMLString-1-v2", BaseUtil.GetDataEmpty);
                    return "";
                }

                if (xmlElement == null || xmlElement.Trim() == "")
                {
                    message = getFunctionError("DataTableToSAPXMLString-1", "Source must have xmlElement.");
                    return "";
                }


                message = "";
                String xml = String.Empty;


                xml = @"<?xml version=""1.0"" encoding=""utf-16""?>";
                xml += String.Format("<{0}>", "BOM");
                foreach (DataRow header in dataObjHeader.Rows)
                {
                    xml += String.Format("<{0}>", "BO");
                    xml = xml + string.Format("<AdmInfo><Object>{0}</Object><Version>{1}</Version></AdmInfo>", sapObject, sapVersion);
                    //Header
                    xml += String.Format("<{0}>", xmlElement);
                    xml += String.Format("<{0}>", "row");
                    foreach (DataColumn col in dataObjHeader.Columns)
                    {
                        string colName = col.ColumnName;
                        object value = header[colName] == null ? DBNull.Value : header[colName];
                        if (col.DataType.Name == "DateTime")
                            value = ParseDateToSAP(value);
                        if (col.DataType.Name == "String")
                            value = StringEncode(value.ToString());

                        xml += String.Format("<{0}>{1}</{0}>", colName, value);
                    }
                    xml += String.Format("</{0}>", "row");
                    xml += String.Format("</{0}>", xmlElement);
                    //End Header

                    //Lines by Header
                    foreach (var source in dataObjLines)
                    {
                        if (source.DataSource != null)
                        {
                            //begin Line
                            xml += String.Format("<{0}>", source.XmlElement);
                            var lineByKey = source.DataSource.Select(string.Format("{0}='{1}'", source.LineRefenceKey, header[source.HeaderKey]));
                            foreach (var line in lineByKey)
                            {

                                xml += String.Format("<{0}>", "row");
                                foreach (DataColumn col in source.DataSource.Columns)
                                {
                                    string colName = col.ColumnName;
                                    object value = line[colName] == null ? DBNull.Value : line[colName];
                                    if (col.DataType.Name == "DateTime")
                                        value = ParseDateToSAP(value);
                                    if (col.DataType.Name == "String")
                                        value = StringEncode(value.ToString());

                                    xml += String.Format("<{0}>{1}</{0}>", colName, value);
                                }
                                xml += String.Format("</{0}>", "row");
                            }
                            xml += String.Format("</{0}>", source.XmlElement);
                            //end Line
                        }
                    }
                    //End Lines byHeader

                    xml += String.Format("</{0}>", "BO");
                }
                xml += String.Format("</{0}>", "BOM");

                return xml;
            }
            catch (Exception ex)
            {
                message = getFunctionError("DataTableToSAPXMLString", ex.Message);
                return String.Empty;
            }
        }

        public bool _SaveDataToSapXmlFile(DataTable dataObj, String xmlElement, string sapObject, string sapVersion, string fileName, out string message)
        {
            try
            {
                if (dataObj == null || dataObj.Rows.Count == 0)
                {
                    message = getFunctionError("_SaveDataToSapXmlFile-0-v2", BaseUtil.GetDataEmpty);
                    return false;
                }

                message = "";
                XmlDocument x = new XmlDocument();
                var xmlString = DataTableToSAPXMLString(dataObj, xmlElement, sapObject, sapVersion, out message);
                if (checkHasError(message))
                {
                    return false;
                }
                x.LoadXml(xmlString);
                x.Save(fileName);
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("SaveDataToSapXmlFile-0", ex.Message);
                return false;
            }
        }

        #endregion

        #region Generate
        public string GenerateFormatDateTime()
        {
            var s = DateTime.Now.ToString("yyyyMMddHHmmssfffffff");

            return s;
        }
        #endregion

        #region Read XML file write to database
        public bool ReadXmlFile(string fileName, string xmlElement, out string message)
        {
            try
            {
                message = "";

                //var dt = XmlToDataTable(fileName);

                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("ReadXmlFile-0", ex.Message);
            }
            return false;
        }

        public DataTable XmlToDataTable(string fileName, out string message)
        {
            try
            {
                message = "";
                DataSet ds = new DataSet();
                ds.ReadXml(fileName);
                if (ds != null && ds.Tables.Count > 0)
                    return ds.Tables[0];

                return null;
            }
            catch (Exception ex)
            {
                message = getFunctionError("XmlToDataTable-0", ex.Message);
                return null;
            }
        }

        public DataTable _XElementToDataTable(string fileName, string xmElement, out string message)
        {
            try
            {
                message = "";
                XDocument x = XDocument.Load(fileName);
                var element = x.Element(_xmlNodeRoot).Element(_xmlNodeBO).Element(xmElement).Element(_xmlNodeRow);

                DataSet ds = new DataSet();

                ds.ReadXml(new StringReader(element.ToString()));

                if (ds != null && ds.Tables.Count > 0)
                {
                    var dt = ds.Tables[0];
                    dt.TableName = xmElement;

                    return dt;
                }

                message = "No data in xml file.";
                return null;
            }
            catch (Exception ex)
            {
                message = getFunctionError("XElementToDataTable-0", ex.Message);
                return null;
            }
        }

        public DataTable XElementToDataTable(string fileName, string xmlElement, out string message)
        {
            try
            {
                message = "";

                XDocument x = XDocument.Load(fileName);

                DataTable dt = new DataTable(xmlElement);


                var o1 = x.Descendants(_xmlNodeRoot).Descendants(_xmlNodeBO).Descendants(xmlElement).ToList();

                if (o1 != null && o1.Count > 0)
                {
                    var o2 = o1.Descendants(_xmlNodeRow).ToList();
                    if (o2 != null && o2.Count > 0)
                    {
                        XElement setup = (from p in x.Descendants(_xmlNodeRoot).Descendants(_xmlNodeBO).Descendants(xmlElement).Descendants(_xmlNodeRow) select p).First();

                        DataTable dttemp = new DataTable();

                        foreach (XElement xe in setup.Descendants()) // build your DataTable
                        {
                            dt.Columns.Add(new DataColumn(xe.Name.ToString(), typeof(string))); // add columns to your dt
                        }

                        var all = from p in x.Descendants(xmlElement).Descendants(_xmlNodeRow) select p;
                        foreach (XElement xe in all)
                        {
                            DataRow dr = dt.NewRow();
                            foreach (XElement xe2 in xe.Descendants())
                            {
                                dr[xe2.Name.ToString()] = xe2.Value; //add in the values
                            }
                            dt.Rows.Add(dr);
                        }
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                message = getFunctionError("XElementToDataTable-1", ex.Message);
                return null;
            }

        }

        public DataTable XElementToDataTable_SAP(string fileName, string xmlElement, out string message)
        {
            try
            {
                message = "";

                XDocument x = XDocument.Load(fileName);

                DataTable dt = new DataTable(xmlElement);

                XElement setup = (from p in x.Descendants("BOM").Descendants("BO").Descendants(xmlElement).Descendants(_xmlNodeRow) select p).First();
                DataTable dttemp = new DataTable();

                foreach (XElement xe in setup.Descendants()) // build your DataTable
                {
                    dt.Columns.Add(new DataColumn(xe.Name.ToString(), typeof(string))); // add columns to your dt
                }

                var all = from p in x.Descendants(xmlElement).Descendants(_xmlNodeRow) select p;
                foreach (XElement xe in all)
                {
                    DataRow dr = dt.NewRow();
                    foreach (XElement xe2 in xe.Descendants())
                    {
                        dr[xe2.Name.ToString()] = xe2.Value; //add in the values
                    }
                    dt.Rows.Add(dr);
                }
                return dt;
            }
            catch (Exception ex)
            {
                message = getFunctionError("XElementToDataTable-1", ex.Message);
                return null;
            }

        }

        #endregion

        #region Directory & File
        public string GetNewBackupFile(string fileName)
        {
            string extBackup = ".bak";
            if (fileName != null)
            {
                int index = fileName.LastIndexOf(".");
                if (index > 0)
                {
                    fileName = fileName.Substring(0, index);

                    return fileName + extBackup;
                }
            }


            return fileName;

        }

        public bool CheckFile(string fileName)
        {
            try
            {

                if (File.Exists(fileName))
                    return true;
            }

            catch (Exception ex)
            {

            }
            return false;
        }

        public bool CheckDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path))
                    return true;
            }

            catch (Exception ex)
            {

            }
            return false;
        }

        public void CreateDirectoty(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        public bool DirHasFile(string path)
        {
            try
            {
                if (Directory.Exists(path))
                {
                    DirectoryInfo dir = new DirectoryInfo(path);
                    var files = dir.GetFiles("*.xml");
                    if (files != null && files.Length > 0)
                        return true;
                }
            }

            catch (Exception ex)
            {

            }
            return false;
        }

        public List<FileInfo> GetFilesXmlinDir(string path, out string message)
        {
            try
            {
                message = "";
                if (Directory.Exists(path))
                {
                    DirectoryInfo dir = new DirectoryInfo(path);
                    var files = dir.GetFiles("*.xml");
                    if (files != null)
                    {
                        var obj = (from x in files orderby x.CreationTime ascending select x);

                        return obj.ToList();
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                message = getFunctionError("GetFilesXmlinDir-0", ex.Message);
            }
            return null;
        }

        public List<FileInfo> GetFilesZipinDir(string path, out string message)
        {
            try
            {
                message = "";
                if (Directory.Exists(path))
                {
                    DirectoryInfo dir = new DirectoryInfo(path);
                    var files = dir.GetFiles("*.zip");
                    if (files != null)
                    {
                        var obj = (from x in files orderby x.CreationTime ascending select x);

                        return obj.ToList();
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                message = getFunctionError("GetFilesZipinDir-0", ex.Message);
            }
            return null;
        }

        public void MoveFile(string fileName, string sourcePath, string destinationPath, out string message)
        {
            message = "";
            try
            {
                if (!CheckDirectory(destinationPath))
                    CreateDirectoty(destinationPath);

                string sourceFile = PathReplaceSeparator(sourcePath) + fileName;
                string distFile = PathReplaceSeparator(destinationPath) + fileName;
                if (CheckFile(sourceFile))
                {
                    File.Move(sourceFile, distFile);
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("MoveFile-0", ex.Message);
            }
        }

        public void BackupFile(string fileName, string sourcePath, string destinationPath, out string newBackupFile, out string message)
        {
            message = "";
            newBackupFile = "";
            try
            {
                if (!CheckDirectory(destinationPath))
                    CreateDirectoty(destinationPath);

                DeleteBackupFile(destinationPath, SYNCOUT_FILEBACKUP_DAYS, out message);

                newBackupFile = GetNewBackupFile(fileName);
                string sourceFile = PathReplaceSeparator(sourcePath) + fileName;
                string distFile = PathReplaceSeparator(destinationPath) + newBackupFile;
                if (CheckFile(sourceFile))
                {
                    File.Move(sourceFile, distFile);
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("MoveFile-0", ex.Message);
            }
        }

        public void DeleteFile(string sourceFileName, out string message)
        {
            message = "";
            try
            {

                if (CheckFile(sourceFileName))
                {
                    File.Delete(sourceFileName);
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("MoveFile-0", ex.Message);
            }
        }

        public void DeleteAllFile(string sourcePath, out string message)
        {
            message = "";
            try
            {

                if (CheckDirectory(sourcePath))
                {
                    DirectoryInfo dir = new DirectoryInfo(sourcePath);
                    foreach (FileInfo file in dir.GetFiles())
                    {
                        File.Delete(file.FullName);
                    }
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("MoveFile-0", ex.Message);
            }
        }

        public void DeleteBackupFile(string sourcePath, int days, out string message)
        {
            message = "";
            try
            {
                if (CheckDirectory(sourcePath))
                {
                    DirectoryInfo dir = new DirectoryInfo(sourcePath);
                    foreach (FileInfo file in dir.GetFiles())
                    {
                        if ((DateTime.Now - file.CreationTime).TotalDays >= days)
                        {
                            File.Delete(file.FullName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("MoveFile-0", ex.Message);
            }
        }
        #endregion

        #region ZIP FILE
        public bool ZipFile(string zipfilename, string sourcefilename, out string message)
        {
            try
            {
                message = "";

                Package zipPackage = ZipPackage.Open(zipfilename, FileMode.Create);

                string destFilename = ".\\" + Path.GetFileName(sourcefilename);

                Uri zipPartUri = PackUriHelper.CreatePartUri(new Uri(destFilename, UriKind.Relative));

                //if (zipPackage.PartExists(zipPartUri))
                //{
                //    zipPackage.DeletePart(zipPartUri);
                //}

                PackagePart zipPackagePart = zipPackage.CreatePart(zipPartUri, "", CompressionOption.Normal);

                FileStream fileStream = new FileStream(sourcefilename, FileMode.Open, FileAccess.Read);

                Stream dest = zipPackagePart.GetStream();

                CopyStream(fileStream, dest);
                //fileStream.CopyTo(dest);

                fileStream.Flush();
                fileStream.Dispose();

                dest.Flush();
                dest.Dispose();

                zipPackage.Flush();
                zipPackage.Close();

                return true;
            }
            catch (Exception ex)
            {
                message = "ZipFile --> " + ex.Message;
            }
            return false;
        }

        public bool ExtractFile(string zipfilename, string destfolder, out string message)
        {
            try
            {
                message = "";
                Package package = ZipPackage.Open(zipfilename, FileMode.Open);

                foreach (PackagePart part in package.GetParts())
                {
                    var target = Path.GetFullPath(Path.Combine(destfolder, part.Uri.OriginalString.TrimStart('/')));
                    var targetDir = target.Remove(target.LastIndexOf('\\'));

                    if (!Directory.Exists(targetDir))
                        Directory.CreateDirectory(targetDir);

                    Stream source = part.GetStream();
                    Stream dest = File.OpenWrite(target);

                    //source.CopyTo(dest);
                    CopyStream(source, dest);

                    source.Flush();
                    source.Dispose();

                    dest.Flush();
                    dest.Dispose();

                }

                package.Flush();
                package.Close();


                return true;
            }
            catch (Exception ex)
            {
                message = "ExtractFile --> " + ex.Message;
                return false;
            }
        }

        //Copy one stream data to another
        private static void CopyStream(Stream source, Stream target)
        {
            const int bufSize = 0x1000;
            byte[] buf = new byte[bufSize];
            int bytesRead = 0;
            while ((bytesRead = source.Read(buf, 0, bufSize)) > 0)
                target.Write(buf, 0, bytesRead);
        }
        #endregion


        public DateTime dbServerDate(out string message)
        {
            message = "";
            try
            {
                var daConn = new daConnection(false);
                return daConn.dbDate();
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return Convert.ToDateTime("1900-01-01");
        }

    }
}
