﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using DataTools.Models;
using DataTools.Utility;


namespace DataTools.DataAccess
{
    public partial class daSettings : BaseData
    {
        #region Configuration Infomation
        public List<ConfigurationInfo_View> ConfigurationInfo_Get(out string message)
        {
            message = "";
            try
            {

                var a = (from o1 in _db_hub.Configurations
                         select new ConfigurationInfo_View
                         {
                             Name = o1.Name,
                             Value = o1.Value,
                             Description = o1.Description,
                         }).ToList();
                return a;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return null;
            }
        }

        public Boolean ConfigurationInfo_Insert(List<ConfigurationInfo_View> oData, out string message)
        {
            message = "";
            System.Data.Common.DbTransaction trans = null;
            try
            {
                _db_hub = DB_HUB();

                if (_db_hub.Connection.State == System.Data.ConnectionState.Closed)
                {
                    _db_hub.Connection.Open();
                }
                trans = _db_hub.Connection.BeginTransaction();

                _db_hub.Transaction = trans;

                _db_hub.ExecuteQuery<ConfigurationInfo_View>("delete from Configuration");

                var oEntity = new List<Configuration>();

                foreach (var o in oData)
                {
                    var on = new Configuration();
                    on.Name = o.Name;
                    on.Value = o.Value;
                    on.Description = o.Description;

                    oEntity.Add(on);
                }

                _db_hub.Configurations.InsertAllOnSubmit(oEntity);
                _db_hub.Configurations.Context.SubmitChanges();

                trans.Commit();
                return true;
            }
            catch (Exception ex)
            {
                if (trans != null)
                {
                    trans.Rollback();
                }
                message = ex.Message;
                return false;
            }
        }


        #endregion

    }
}
