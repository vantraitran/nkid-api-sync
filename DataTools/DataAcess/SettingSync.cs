﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml;
using System.Xml.Linq;
using System.Reflection;
using System.Web;
using System.IO;

using DataTools.Models;
using DataTools.Utility;

namespace DataTools.DataAccess.Sync
{
    public class SettingSync : SyncBase
    {
        dbHub _db = null;//FOR DATASQL

        public SettingSync()
        {
            if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
            {
                _db = new dbHub(Connection_String(DataConnection.CLIENTDB));
            }
        }

        #region Sync Configuration
        public SysConfLocal_View SyncConfig_Get_Top1(out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    using (var trans = TransWithUncommitted())
                    {
                        var result = (from x in _db.SyncConfigs
                                      select new SysConfLocal_View
                                      {
                                          FtpHostName = x.FtpHostName,
                                          FtpUserId = x.FtpUserId,
                                          FtpPassword = x.FtpPassword,
                                          MachineCode = x.MachineCode,
                                          MachineName = x.MachineName,
                                          TempPath = x.TempPath,
                                          BackupPath = x.BackupPath,
                                          IsServer = x.IsServer,
                                          StoreCode = x.StoreCode,
                                      }).FirstOrDefault();

                        return result;
                    }
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncConfig", out message);
                        if (xdata != null)
                        {
                            var otemp = (from x in xdata.Elements(_xmlNodeRow)
                                         select new SysConfLocal_View
                                         {
                                             FtpHostName = x.Element("FtpHostName").Value,
                                             FtpUserId = x.Element("FtpUserId").Value,
                                             FtpPassword = x.Element("FtpPassword").Value,
                                             MachineCode = x.Element("MachineCode").Value,
                                             MachineName = x.Element("MachineName").Value,
                                             TempPath = x.Element("TempPath").Value,
                                             BackupPath = x.Element("BackupPath").Value,
                                             IsServer = Convert.ToBoolean(x.Element("IsServer").Value),
                                             StoreCode = x.Element("StoreCode").Value,
                                         }).FirstOrDefault();
                            return otemp;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }

        public bool SyncConfig_Save(SysConfLocal_View odata, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    if (odata == null) return false;

                    using (var trans = TransWithCommitted())
                    {
                        SyncConfig obj = null;
                        obj = _db.SyncConfigs.FirstOrDefault();

                        if (obj != null)
                        {
                            obj.FtpHostName = odata.FtpHostName;
                            obj.FtpUserId = odata.FtpUserId;
                            obj.FtpPassword = odata.FtpPassword;
                            obj.MachineCode = odata.MachineCode;
                            obj.MachineName = odata.MachineName;
                            obj.TempPath = odata.TempPath;
                            obj.BackupPath = odata.BackupPath;
                            obj.IsServer = odata.IsServer;
                            obj.StoreCode = odata.StoreCode;

                            _db.SyncConfigs.Context.SubmitChanges();
                        }
                        else
                        {
                            obj = new SyncConfig();

                            obj.FtpHostName = odata.FtpHostName;
                            obj.FtpUserId = odata.FtpUserId;
                            obj.FtpPassword = odata.FtpPassword;
                            obj.MachineCode = odata.MachineCode;
                            obj.MachineName = odata.MachineName;
                            obj.TempPath = odata.TempPath;
                            obj.BackupPath = odata.BackupPath;
                            obj.IsServer = odata.IsServer;
                            obj.StoreCode = odata.StoreCode;

                            _db.SyncConfigs.InsertOnSubmit(obj);
                            _db.SyncConfigs.Context.SubmitChanges();

                        }

                        trans.Complete();
                        return true;
                    }
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    if (_dir_xmldata != "")
                        if (!Directory.Exists(_dir_xmldata))
                            Directory.CreateDirectory(_dir_xmldata);

                    DeleleAll(_path_xml_syncdata, "SyncConfig", out message);

                    if (!File.Exists(_path_xml_syncdata))
                    {

                        var xdata = new XElement("SyncConfig");


                        xdata.Add(new XElement(_xmlNodeRow,
                                new XElement("FtpHostName", odata.FtpHostName),
                                new XElement("FtpUserId", odata.FtpUserId),
                                new XElement("FtpPassword", odata.FtpPassword),
                                new XElement("MachineCode", odata.MachineCode),
                                new XElement("MachineName", odata.MachineName),
                                new XElement("TempPath", odata.TempPath),
                                new XElement("BackupPath", odata.BackupPath),
                                new XElement("IsServer", odata.IsServer),
                                new XElement("StoreCode", odata.StoreCode)
                                ));

                        XElement xdoc = new XElement(_xmlNodeRoot,
                            new XElement(_xmlNodeBO,
                            xdata));

                        xdoc.Save(_path_xml_syncdata);
                    }
                    else
                    {
                        var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                        if (xdoc != null)
                        {
                            CreateBaseNode(xdoc, "SyncConfig", out message);
                            var xdata = XElement_Get(xdoc, "SyncConfig", out message);
                            if (xdata != null)
                            {
                                xdata.Add(new XElement(_xmlNodeRow,
                                         new XElement("FtpHostName", odata.FtpHostName),
                                new XElement("FtpUserId", odata.FtpUserId),
                                new XElement("FtpPassword", odata.FtpPassword),
                                new XElement("MachineCode", odata.MachineCode),
                                new XElement("MachineName", odata.MachineName),
                                new XElement("TempPath", odata.TempPath),
                                new XElement("BackupPath", odata.BackupPath),
                                new XElement("IsServer", odata.IsServer),
                                new XElement("StoreCode", odata.StoreCode)
                                        ));

                            }

                            xdoc.Save(_path_xml_syncdata);
                        }
                    }
                    return true;
                }


            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        #endregion

        #region Sync Type
        public List<SyncType_View> SyncType_Get_All(out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    using (var trans = TransWithUncommitted())
                    {
                        var results = (from x in _db.SyncTypes
                                       select new SyncType_View
                                       {
                                           TypeCode = x.TypeCode,
                                           TypeName = x.TypeName,
                                           Remarks = x.Remarks,
                                           SyncIN = x.SyncIN,
                                       }).ToList();

                        if (results != null)
                            return results.OrderBy(o => o.TypeCode).ToList();
                    }
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncType", out message);
                        if (xdata != null)
                        {
                            var otemp = (from x in xdata.Elements(_xmlNodeRow)
                                         select new SyncType_View
                                         {
                                             TypeCode = x.Element("TypeCode").Value,
                                             TypeName = x.Element("TypeName").Value,
                                             SyncIN = Convert.ToBoolean(x.Element("SyncIN").Value),
                                             Remarks = x.Element("Remarks").Value,
                                             Inactive = Convert.ToBoolean(x.Element("Inactive").Value),
                                         }).ToList();
                            if (otemp != null)
                                return otemp.OrderBy(o => o.TypeCode).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncType_View>();
        }

        public List<SyncType_View> SyncType_Get_Active(out string message)
        {
            message = "";
            try
            {
                var otemp = SyncType_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.Inactive == false).ToList();
                    return otemp;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncType_View>();
        }

        public bool SyncType_Save(List<SyncType_View> odata, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    if (odata != null)
                    {
                        List<SyncType> list = new List<SyncType>();
                        foreach (var item in odata)
                        {
                            list.Add(new SyncType()
                            {
                                TypeCode = item.TypeCode,
                                TypeName = item.TypeName,
                                SyncIN = item.SyncIN,
                                Remarks = item.Remarks,
                                Inactive = item.Inactive,
                            });
                        }

                        var obj = _db.SyncTypes.ToList();
                        _db.SyncTypes.DeleteAllOnSubmit(obj);
                        _db.SyncTypes.InsertAllOnSubmit(list);
                        _db.SyncTypes.Context.SubmitChanges();
                        return true;
                    }

                    return false;
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    if (_dir_xmldata != "")
                        if (!Directory.Exists(_dir_xmldata))
                            Directory.CreateDirectory(_dir_xmldata);

                    DeleleAll(_path_xml_syncdata, "SyncType", out message);

                    if (!File.Exists(_path_xml_syncdata))
                    {


                        var xdata = new XElement("SyncType");

                        foreach (var oline in odata)
                        {
                            xdata.Add(new XElement(_xmlNodeRow,
                                    new XElement("TypeCode", oline.TypeCode),
                                    new XElement("TypeName", oline.TypeName),
                                    new XElement("SyncIN", oline.SyncIN),
                                    new XElement("Remarks", oline.Remarks),
                                    new XElement("Inactive", oline.Inactive)
                                    ));
                        }

                        XElement xdoc = new XElement(_xmlNodeRoot,
                            new XElement(_xmlNodeBO,
                            xdata));

                        xdoc.Save(_path_xml_syncdata);
                    }
                    else
                    {
                        var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                        if (xdoc != null)
                        {
                            CreateBaseNode(xdoc, "SyncType", out message);
                            var xdata = XElement_Get(xdoc, "SyncType", out message);
                            if (xdata != null)
                            {
                                foreach (var oline in odata)
                                {
                                    xdata.Add(new XElement(_xmlNodeRow,
                                            new XElement("TypeCode", oline.TypeCode),
                                            new XElement("TypeName", oline.TypeName),
                                            new XElement("SyncIN", oline.SyncIN),
                                            new XElement("Remarks", oline.Remarks),
                                            new XElement("Inactive", oline.Inactive)
                                            ));
                                }

                            }

                            xdoc.Save(_path_xml_syncdata);
                        }
                    }
                    return true;
                }


            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool SyncType_Delete(string typecode, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    var obj = _db.SyncTypes.Where(o => o.TypeCode == typecode).FirstOrDefault();
                    if (obj != null)
                    {
                        _db.SyncTypes.DeleteOnSubmit(obj);
                        _db.SyncTypes.Context.SubmitChanges();
                    }
                    return true;
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncType", out message);
                        if (xdata != null)
                        {
                            var xxx = (from x in xdata.Descendants(_xmlNodeRow)
                                       where x.Element("TypeCode").Value == typecode
                                       select x).ToList();


                            if (xxx != null && xxx.Count > 0)
                            {
                                foreach (var x in xxx)
                                {
                                    x.Remove();
                                }
                            }

                            xdoc.Save(_path_xml_syncdata);
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }
        #endregion

        #region Sync Group
        public List<SyncGroup_View> SyncGroup_Get_All(out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    using (var trans = TransWithUncommitted())
                    {
                        var otemp = (from x in _db.SyncGroups
                                     select new SyncGroup_View
                                     {
                                         GroupCode = x.GroupCode,
                                         GroupName = x.GroupName,
                                         TypeCode = x.TypeCode,
                                         Remarks = x.Remarks,
                                         Inactive = x.Inactive,
                                     }).ToList();

                        var otype = SyncType_Get_All(out message);
                        if (otemp != null && otype != null)
                        {
                            var oresult = (from x in otemp
                                           join y in otype on x.TypeCode equals y.TypeCode
                                           select new SyncGroup_View
                                           {
                                               TypeCode = x.TypeCode,
                                               GroupCode = x.GroupCode,
                                               GroupName = x.GroupName,
                                               Remarks = x.Remarks,
                                               Inactive = x.Inactive,
                                               udfTypeName = y.TypeName,
                                           }).ToList();
                            if (oresult != null)
                                return oresult.OrderBy(o => o.TypeCode).ToList();
                        }
                        else if (otemp != null)
                            return otemp.OrderBy(o => o.TypeCode).ToList();
                    }
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncGroup", out message);
                        if (xdata != null)
                        {
                            var otemp = (from x in xdata.Elements(_xmlNodeRow)
                                         select new SyncGroup_View
                                         {
                                             TypeCode = x.Element("TypeCode").Value,
                                             GroupCode = x.Element("GroupCode").Value,
                                             GroupName = x.Element("GroupName").Value,
                                             Remarks = x.Element("Remarks").Value,
                                             Inactive = Convert.ToBoolean(x.Element("Inactive").Value),
                                         }).ToList();


                            var otype = SyncType_Get_All(out message);
                            if (otemp != null && otype != null)
                            {
                                var oresult = (from x in otemp
                                               join y in otype on x.TypeCode equals y.TypeCode
                                               select new SyncGroup_View
                                               {
                                                   TypeCode = x.TypeCode,
                                                   GroupCode = x.GroupCode,
                                                   GroupName = x.GroupName,
                                                   Remarks = x.Remarks,
                                                   Inactive = x.Inactive,
                                                   udfTypeName = y.TypeName,
                                               }).ToList();
                                if (oresult != null)
                                    return oresult.OrderBy(o => o.TypeCode).ToList();
                            }
                            else if (otemp != null)
                                return otemp.OrderBy(o => o.TypeCode).ToList();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncGroup_View>();
        }

        public List<SyncGroup_View> SyncGroup_Get_Active(bool isLineEmpty, out string message)
        {
            message = "";
            try
            {
                var otemp = SyncGroup_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.Inactive == false).ToList();
                }

                if (isLineEmpty)
                {
                    if (otemp == null)
                        otemp = new List<SyncGroup_View>();

                    otemp.Insert(0, new SyncGroup_View() { GroupCode = "", GroupName = "-------" });
                }
                return otemp;

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncGroup_View>();
        }

        public List<SyncGroup_View> SyncGroup_Get_Active(out string message)
        {
            message = "";
            try
            {
                var otemp = SyncGroup_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.Inactive == false).ToList();
                }

                return otemp;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncGroup_View>();
        }

        public List<SyncGroup_View> SyncGroup_Get_Active(string typecode, bool isLineEmpty, out string message)
        {
            message = "";
            try
            {
                var otemp = SyncGroup_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.Inactive == false && o.TypeCode == typecode).ToList();
                }
                if (isLineEmpty)
                {
                    if (otemp == null)
                        otemp = new List<SyncGroup_View>();

                    otemp.Insert(0, new SyncGroup_View() { GroupCode = "", GroupName = "-------" });
                }
                return otemp;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncGroup_View>();
        }

        public bool SyncGroup_Save(List<SyncGroup_View> odata, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    if (odata != null)
                    {
                        List<SyncGroup> list = new List<SyncGroup>();
                        foreach (var item in odata)
                        {
                            list.Add(new SyncGroup()
                            {
                                GroupCode = item.GroupCode,
                                GroupName = item.GroupName,
                                TypeCode = item.TypeCode,
                                Remarks = item.Remarks,
                                Inactive = item.Inactive,
                            });
                        }
                        var obj = _db.SyncGroups.ToList();
                        _db.SyncGroups.DeleteAllOnSubmit(obj);
                        _db.SyncGroups.InsertAllOnSubmit(list);
                        _db.SyncGroups.Context.SubmitChanges();
                        return true;
                    }

                    return false;
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    if (_dir_xmldata != "")
                        if (!Directory.Exists(_dir_xmldata))
                            Directory.CreateDirectory(_dir_xmldata);

                    DeleleAll(_path_xml_syncdata, "SyncGroup", out message);

                    if (!File.Exists(_path_xml_syncdata))
                    {


                        var xdata = new XElement("SyncGroup");

                        foreach (var oline in odata)
                        {
                            xdata.Add(new XElement(_xmlNodeRow,
                                    new XElement("TypeCode", oline.TypeCode),
                                    new XElement("GroupCode", oline.GroupCode),
                                    new XElement("GroupName", oline.GroupName),
                                    new XElement("Remarks", oline.Remarks),
                                    new XElement("Inactive", oline.Inactive)
                                    ));
                        }

                        XElement xdoc = new XElement(_xmlNodeRoot,
                            new XElement(_xmlNodeBO,
                            xdata));

                        xdoc.Save(_path_xml_syncdata);
                    }
                    else
                    {
                        var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                        if (xdoc != null)
                        {
                            CreateBaseNode(xdoc, "SyncGroup", out message);
                            var xdata = XElement_Get(xdoc, "SyncGroup", out message);
                            if (xdata != null)
                            {
                                foreach (var oline in odata)
                                {
                                    xdata.Add(new XElement(_xmlNodeRow,
                                            new XElement("TypeCode", oline.TypeCode),
                                            new XElement("GroupCode", oline.GroupCode),
                                            new XElement("GroupName", oline.GroupName),
                                            new XElement("Remarks", oline.Remarks),
                                            new XElement("Inactive", oline.Inactive)
                                            ));
                                }

                            }

                            xdoc.Save(_path_xml_syncdata);
                        }
                    }
                    return true;
                }


            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool SyncGroup_Delete(string groupcode, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    var obj = _db.SyncGroups.Where(o => o.GroupCode == groupcode).FirstOrDefault();
                    if (obj != null)
                    {
                        _db.SyncGroups.DeleteOnSubmit(obj);
                        _db.SyncGroups.Context.SubmitChanges();
                    }
                    return true;
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncGroup", out message);
                        if (xdata != null)
                        {
                            var xxx = (from x in xdata.Descendants(_xmlNodeRow)
                                       where x.Element("GroupCode").Value == groupcode
                                       select x).ToList();


                            if (xxx != null && xxx.Count > 0)
                            {
                                foreach (var x in xxx)
                                {
                                    x.Remove();
                                }
                            }

                            xdoc.Save(_path_xml_syncdata);
                        }
                    }
                    return true;
                }



            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }
        #endregion

        #region Sync Property
        public List<SyncProperty_View> SyncProperty_Get_All(out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    using (var trans = TransWithUncommitted())
                    {
                        var otemp = (from x in _db.SyncProperties
                                     select new SyncProperty_View
                                     {
                                         BooleanType = x.BooleanType,
                                         ColumnIN = x.ColumnIN,
                                         ColumnOUT = x.ColumnOUT,
                                         DateTimeType = x.DateTimeType,
                                         Inactive = x.Inactive,
                                         IsKey = x.IsKey,
                                         NumericType = x.NumericType,
                                         Remarks = x.Remarks,
                                         SourceCode = x.SourceCode,
                                         StringType = x.StringType,
                                     }).ToList();

                        var osource = SyncSource_Get_All(out message);
                        if (otemp != null && osource != null)
                        {
                            var oresult = (from x in otemp
                                           join y in osource on x.SourceCode equals y.SourceCode
                                           select new SyncProperty_View
                                           {
                                               SourceCode = y.SourceCode,
                                               udfSourceName = y.SourceName,
                                               udfSourceIN = y.SourceIN,
                                               ColumnIN = x.ColumnIN,
                                               udfSourceOUT = y.SourceOUT,
                                               ColumnOUT = x.ColumnOUT,
                                               Remarks = x.Remarks,
                                               IsKey = x.IsKey,
                                               NumericType = x.NumericType,
                                               DateTimeType = x.DateTimeType,
                                               BooleanType = x.BooleanType,
                                               StringType = x.StringType,
                                               Inactive = x.Inactive,
                                           }).ToList();
                            if (oresult != null)
                                return oresult.OrderBy(o => o.SourceCode).ToList();
                        }
                        else if (otemp != null)
                            return otemp.OrderBy(o => o.SourceCode).ToList();
                    }
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncProperty", out message);
                        if (xdata != null)
                        {
                            var otemp = (from x in xdata.Elements(_xmlNodeRow)
                                         select new SyncProperty_View
                                         {
                                             BooleanType = Convert.ToBoolean(x.Element("BooleanType").Value),
                                             ColumnIN = x.Element("ColumnIN").Value,
                                             ColumnOUT = x.Element("ColumnOUT").Value,
                                             DateTimeType = Convert.ToBoolean(x.Element("DateTimeType").Value),
                                             Inactive = Convert.ToBoolean(x.Element("Inactive").Value),
                                             IsKey = Convert.ToBoolean(x.Element("IsKey").Value),
                                             NumericType = Convert.ToBoolean(x.Element("NumericType").Value),
                                             Remarks = x.Element("Remarks").Value,
                                             SourceCode = x.Element("SourceCode").Value,
                                             StringType = Convert.ToBoolean(x.Element("StringType").Value),
                                         }).ToList();

                            var osource = SyncSource_Get_All(out message);
                            if (otemp != null && osource != null)
                            {
                                var oresult = (from x in otemp
                                               join y in osource on x.SourceCode equals y.SourceCode
                                               select new SyncProperty_View
                                               {
                                                   SourceCode = y.SourceCode,
                                                   udfSourceName = y.SourceName,
                                                   udfSourceIN = y.SourceIN,
                                                   ColumnIN = x.ColumnIN,
                                                   udfSourceOUT = y.SourceOUT,
                                                   ColumnOUT = x.ColumnOUT,
                                                   Remarks = x.Remarks,
                                                   IsKey = x.IsKey,
                                                   NumericType = x.NumericType,
                                                   DateTimeType = x.DateTimeType,
                                                   BooleanType = x.BooleanType,
                                                   StringType = x.StringType,
                                                   Inactive = x.Inactive,
                                               }).ToList();
                                if (oresult != null)
                                    return oresult.OrderBy(o => o.SourceCode).ToList();
                            }
                            else if (otemp != null)
                                return otemp.OrderBy(o => o.SourceCode).ToList();
                        }
                    }
                }



            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncProperty_View>();
        }

        public List<SyncProperty_View> SyncProperty_Get_Active(out string message)
        {
            message = "";
            try
            {
                var otemp = SyncProperty_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.Inactive == false).ToList();
                    return otemp;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncProperty_View>();
        }

        public List<SyncProperty_View> SyncProperty_Get_Active(string sourcecode, out string message)
        {
            message = "";
            try
            {
                var otemp = SyncProperty_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.Inactive == false && o.SourceCode == sourcecode).ToList();
                    return otemp;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncProperty_View>();
        }

        public bool SyncProperty_Save(List<SyncProperty_View> odata, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    if (odata != null)
                    {
                        List<SyncProperty> list = new List<SyncProperty>();
                        foreach (var item in odata)
                        {
                            list.Add(new SyncProperty()
                            {
                                BooleanType = item.BooleanType,
                                ColumnIN = item.ColumnIN,
                                ColumnOUT = item.ColumnOUT,
                                DateTimeType = item.DateTimeType,
                                Inactive = item.Inactive,
                                IsKey = item.IsKey,
                                NumericType = item.NumericType,
                                Remarks = item.Remarks,
                                SourceCode = item.SourceCode,
                                StringType = item.StringType,
                            });
                        }

                        var obj = _db.SyncProperties.ToList();
                        _db.SyncProperties.DeleteAllOnSubmit(obj);
                        _db.SyncProperties.InsertAllOnSubmit(list);
                        _db.SyncProperties.Context.SubmitChanges();
                        return true;
                    }

                    return false;
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    if (_dir_xmldata != "")
                        if (!Directory.Exists(_dir_xmldata))
                            Directory.CreateDirectory(_dir_xmldata);

                    DeleleAll(_path_xml_syncdata, "SyncProperty", out message);

                    if (!File.Exists(_path_xml_syncdata))
                    {


                        var xdata = new XElement("SyncProperty");

                        foreach (var oline in odata)
                        {
                            xdata.Add(new XElement(_xmlNodeRow,
                                    new XElement("BooleanType", oline.BooleanType),
                                    new XElement("ColumnIN", oline.ColumnIN),
                                    new XElement("ColumnOUT", oline.ColumnOUT),
                                    new XElement("DateTimeType", oline.DateTimeType),
                                    new XElement("Inactive", oline.Inactive),
                                    new XElement("IsKey", oline.IsKey),
                                    new XElement("NumericType", oline.NumericType),
                                    new XElement("Remarks", oline.Remarks),
                                    new XElement("SourceCode", oline.SourceCode),
                                    //new XElement("SourceIN", oline.SourceIN),
                                    //new XElement("SourceName", oline.SourceName),
                                    //new XElement("SourceOUT", oline.SourceOUT),
                                    new XElement("StringType", oline.StringType)
                                    ));
                        }

                        XElement xdoc = new XElement(_xmlNodeRoot,
                            new XElement(_xmlNodeBO,
                            xdata));

                        xdoc.Save(_path_xml_syncdata);
                    }
                    else
                    {
                        var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                        if (xdoc != null)
                        {
                            CreateBaseNode(xdoc, "SyncProperty", out message);
                            var xdata = XElement_Get(xdoc, "SyncProperty", out message);
                            if (xdata != null)
                            {
                                foreach (var oline in odata)
                                {
                                    xdata.Add(new XElement(_xmlNodeRow,
                                            new XElement("BooleanType", oline.BooleanType),
                                            new XElement("ColumnIN", oline.ColumnIN),
                                            new XElement("ColumnOUT", oline.ColumnOUT),
                                            new XElement("DateTimeType", oline.DateTimeType),
                                            new XElement("Inactive", oline.Inactive),
                                            new XElement("IsKey", oline.IsKey),
                                            new XElement("NumericType", oline.NumericType),
                                            new XElement("Remarks", oline.Remarks),
                                            new XElement("SourceCode", oline.SourceCode),
                                            //new XElement("SourceIN", oline.SourceIN),
                                            //new XElement("SourceName", oline.SourceName),
                                            //new XElement("SourceOUT", oline.SourceOUT),
                                            new XElement("StringType", oline.StringType)
                                            ));
                                }

                            }

                            xdoc.Save(_path_xml_syncdata);
                        }
                    }
                    return true;
                }


            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool SyncProperty_Delete(string sourcecode, string columnin, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    var obj = _db.SyncProperties.Where(o => o.SourceCode == sourcecode && o.ColumnIN == columnin).FirstOrDefault();
                    if (obj != null)
                    {
                        _db.SyncProperties.DeleteOnSubmit(obj);
                        _db.SyncProperties.Context.SubmitChanges();
                    }
                    return true;
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncProperty", out message);
                        if (xdata != null)
                        {
                            var xxx = (from x in xdata.Descendants(_xmlNodeRow)
                                       where x.Element("SourceCode").Value == sourcecode && x.Element("ColumnIN").Value == columnin
                                       select x).ToList();


                            if (xxx != null && xxx.Count > 0)
                            {
                                foreach (var x in xxx)
                                {
                                    x.Remove();
                                }
                            }

                            xdoc.Save(_path_xml_syncdata);
                        }
                    }
                    return true;
                }



            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }
        #endregion

        #region Sync Schedule
        public List<SyncSchedule_View> SyncSchedule_Get_All(out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    using (var trans = TransWithUncommitted())
                    {
                        var results = (from x in _db.SyncSchedules
                                       join z in _db.SyncGroups on x.GroupCode equals z.GroupCode
                                       join w in _db.SyncTypes on z.TypeCode equals w.TypeCode
                                       select new SyncSchedule_View
                                       {
                                           GroupCode = x.GroupCode,
                                           udfGroupName = z.GroupName,
                                           ScheduleType = x.ScheduleType,
                                           OneTimeAtDate = x.OneTimeAtDate,
                                           OneTimeAtTime = x.OneTimeAtTime,
                                           FrequenceOccurs = x.FrequenceOccurs,
                                           FrequenceOccursEvery = x.FrequenceOccursEvery,
                                           Mon = x.Mon,
                                           Tue = x.Tue,
                                           Wed = x.Wed,
                                           Thu = x.Thu,
                                           Fri = x.Fri,
                                           Sat = x.Sat,
                                           Sun = x.Sun,
                                           IsDailyOccursOnce = x.IsDailyOccursOnce,
                                           DailyOccursOnceAt = x.DailyOccursOnceAt,
                                           DailyOccursEvery = x.DailyOccursEvery,
                                           TimeType = x.TimeType,
                                           StartAtTime = x.StartAtTime,
                                           EndAtTime = x.EndAtTime,
                                           StartDate = x.StartDate,
                                           EndDate = x.EndDate,
                                           NoEndDate = x.NoEndDate,
                                           Description = x.Description,
                                           Inactive = x.Inactive,
                                       }).ToList();

                        return results;
                    }
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncSchedule", out message);
                        if (xdata != null)
                        {
                            var otemp = (from x in xdata.Elements(_xmlNodeRow)
                                         select new SyncSchedule_View
                                         {
                                             DailyOccursEvery = Convert.ToInt32(x.Element("DailyOccursEvery").Value),
                                             DailyOccursOnceAt = string.IsNullOrEmpty(x.Element("DailyOccursOnceAt").Value) ? null : (TimeSpan?)TimeSpan.Parse(x.Element("DailyOccursOnceAt").Value),
                                             Description = x.Element("Description").Value,
                                             EndAtTime = string.IsNullOrEmpty(x.Element("EndAtTime").Value) ? null : (TimeSpan?)TimeSpan.Parse(x.Element("EndAtTime").Value),
                                             EndDate = string.IsNullOrEmpty(x.Element("EndDate").Value) ? null : (DateTime?)Convert.ToDateTime(x.Element("EndDate").Value),
                                             FrequenceOccurs = x.Element("FrequenceOccurs").Value,
                                             FrequenceOccursEvery = Convert.ToInt32(x.Element("FrequenceOccursEvery").Value),
                                             Fri = Convert.ToBoolean(x.Element("Fri").Value),
                                             GroupCode = x.Element("GroupCode").Value,
                                             Inactive = Convert.ToBoolean(x.Element("Inactive").Value),
                                             IsDailyOccursOnce = Convert.ToBoolean(x.Element("IsDailyOccursOnce").Value),
                                             Mon = Convert.ToBoolean(x.Element("Mon").Value),
                                             NoEndDate = Convert.ToBoolean(x.Element("NoEndDate").Value),
                                             OneTimeAtDate = string.IsNullOrEmpty(x.Element("OneTimeAtDate").Value) ? null : (DateTime?)Convert.ToDateTime(x.Element("OneTimeAtDate").Value),
                                             OneTimeAtTime = string.IsNullOrEmpty(x.Element("OneTimeAtTime").Value) ? null : (TimeSpan?)TimeSpan.Parse(x.Element("OneTimeAtTime").Value),
                                             Sat = Convert.ToBoolean(x.Element("Sat").Value),
                                             ScheduleType = x.Element("ScheduleType").Value,
                                             StartAtTime = string.IsNullOrEmpty(x.Element("StartAtTime").Value) ? null : (TimeSpan?)TimeSpan.Parse(x.Element("StartAtTime").Value),
                                             StartDate = string.IsNullOrEmpty(x.Element("StartDate").Value) ? null : (DateTime?)Convert.ToDateTime(x.Element("StartDate").Value),
                                             Sun = Convert.ToBoolean(x.Element("Sun").Value),
                                             Thu = Convert.ToBoolean(x.Element("Thu").Value),
                                             TimeType = x.Element("TimeType").Value,
                                             Tue = Convert.ToBoolean(x.Element("Tue").Value),
                                             Wed = Convert.ToBoolean(x.Element("Wed").Value),
                                         }).ToList();

                            var otype = SyncType_Get_All(out message);
                            var ogroup = SyncGroup_Get_All(out message);
                            if (otemp != null && otype != null && ogroup != null)
                            {
                                var oresult = (from x in otemp
                                               join y in ogroup on x.GroupCode equals y.GroupCode
                                               join z in otype on y.TypeCode equals z.TypeCode
                                               select new SyncSchedule_View
                                               {
                                                   GroupCode = x.GroupCode,
                                                   udfGroupName = y.GroupName,
                                                   ScheduleType = x.ScheduleType,
                                                   OneTimeAtDate = x.OneTimeAtDate,
                                                   OneTimeAtTime = x.OneTimeAtTime,
                                                   FrequenceOccurs = x.FrequenceOccurs,
                                                   FrequenceOccursEvery = x.FrequenceOccursEvery,
                                                   Mon = x.Mon,
                                                   Tue = x.Tue,
                                                   Wed = x.Wed,
                                                   Thu = x.Thu,
                                                   Fri = x.Fri,
                                                   Sat = x.Sat,
                                                   Sun = x.Sun,
                                                   IsDailyOccursOnce = x.IsDailyOccursOnce,
                                                   DailyOccursOnceAt = x.DailyOccursOnceAt,
                                                   DailyOccursEvery = x.DailyOccursEvery,
                                                   TimeType = x.TimeType,
                                                   StartAtTime = x.StartAtTime,
                                                   EndAtTime = x.EndAtTime,
                                                   StartDate = x.StartDate,
                                                   EndDate = x.EndDate,
                                                   NoEndDate = x.NoEndDate,
                                                   Description = x.Description,
                                                   Inactive = x.Inactive,
                                               }).ToList();
                                return oresult;
                            }
                            else
                                return otemp;
                        }
                    }
                }



            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncSchedule_View>();
        }

        public List<SyncSchedule_View> SyncSchedule_Get_Active(out string message)
        {
            message = "";
            try
            {
                var otemp = SyncSchedule_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.Inactive == false).ToList();
                    return otemp;
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncSchedule_View>();
        }

        public SyncSchedule_View SyncSchedule_GetBy_Group(string groupcode, out string message)
        {
            message = "";
            try
            {
                var otemp = SyncSchedule_Get_All(out message);
                if (otemp != null)
                {
                    var ofirst = otemp.Where(o => o.GroupCode == groupcode).FirstOrDefault();
                    return ofirst;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }

        public List<SyncSchedule_View> SyncSchedule_GetBy_Type(string typeCode, out string message)
        {
            message = "";
            try
            {
                var otemp = SyncSchedule_Get_All(out message);
                if (otemp != null)
                {
                    var otype = SyncType_Get_Active(out message);
                    var ogroup = SyncGroup_Get_Active(out message);

                    var olist = (from x in otemp
                                 join y in ogroup on x.GroupCode equals y.GroupCode
                                 join z in otype on y.TypeCode equals z.TypeCode
                                 where z.TypeCode == typeCode
                                 && x.Inactive == false
                                 select x).ToList();

                    return olist;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }


        public bool SyncSchedule_Save(SyncSchedule_View oline, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    if (oline == null) return false;

                    using (var trans = TransWithCommitted())
                    {
                        SyncSchedule obj = null;
                        obj = _db.SyncSchedules.SingleOrDefault(o => o.GroupCode == oline.GroupCode);

                        if (obj != null)
                        {

                            obj.ScheduleType = oline.ScheduleType;
                            obj.OneTimeAtDate = oline.OneTimeAtDate;
                            obj.OneTimeAtTime = oline.OneTimeAtTime;
                            obj.FrequenceOccurs = oline.FrequenceOccurs;
                            obj.FrequenceOccursEvery = oline.FrequenceOccursEvery;
                            obj.Mon = oline.Mon;
                            obj.Tue = oline.Tue;
                            obj.Wed = oline.Wed;
                            obj.Thu = oline.Thu;
                            obj.Fri = oline.Fri;
                            obj.Sat = oline.Sat;
                            obj.Sun = oline.Sun;
                            obj.IsDailyOccursOnce = oline.IsDailyOccursOnce;
                            obj.DailyOccursOnceAt = oline.DailyOccursOnceAt;
                            obj.DailyOccursEvery = oline.DailyOccursEvery;
                            obj.TimeType = oline.TimeType;
                            obj.StartAtTime = oline.StartAtTime;
                            obj.EndAtTime = oline.EndAtTime;
                            obj.StartDate = oline.StartDate;
                            obj.EndDate = oline.EndDate;
                            obj.NoEndDate = oline.NoEndDate;
                            obj.Description = oline.Description;
                            obj.Inactive = oline.Inactive;

                            _db.SyncSchedules.Context.SubmitChanges();
                        }
                        else
                        {
                            obj = new SyncSchedule();

                            obj.GroupCode = oline.GroupCode;
                            obj.ScheduleType = oline.ScheduleType;
                            obj.OneTimeAtDate = oline.OneTimeAtDate;
                            obj.OneTimeAtTime = oline.OneTimeAtTime;
                            obj.FrequenceOccurs = oline.FrequenceOccurs;
                            obj.FrequenceOccursEvery = oline.FrequenceOccursEvery;
                            obj.Mon = oline.Mon;
                            obj.Tue = oline.Tue;
                            obj.Wed = oline.Wed;
                            obj.Thu = oline.Thu;
                            obj.Fri = oline.Fri;
                            obj.Sat = oline.Sat;
                            obj.Sun = oline.Sun;
                            obj.IsDailyOccursOnce = oline.IsDailyOccursOnce;
                            obj.DailyOccursOnceAt = oline.DailyOccursOnceAt;
                            obj.DailyOccursEvery = oline.DailyOccursEvery;
                            obj.TimeType = oline.TimeType;
                            obj.StartAtTime = oline.StartAtTime;
                            obj.EndAtTime = oline.EndAtTime;
                            obj.StartDate = oline.StartDate;
                            obj.EndDate = oline.EndDate;
                            obj.NoEndDate = oline.NoEndDate;
                            obj.Description = oline.Description;
                            obj.Inactive = oline.Inactive;


                            _db.SyncSchedules.InsertOnSubmit(obj);
                            _db.SyncSchedules.Context.SubmitChanges();
                        }

                        trans.Complete();
                        return true;
                    }
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    if (_dir_xmldata != "")
                        if (!Directory.Exists(_dir_xmldata))
                            Directory.CreateDirectory(_dir_xmldata);

                    SyncSchedule_Delete(oline.GroupCode, out message);

                    if (!File.Exists(_path_xml_syncdata))
                    {


                        var xdata = new XElement("SyncSchedule");


                        xdata.Add(new XElement(_xmlNodeRow,
                                        new XElement("DailyOccursEvery", oline.DailyOccursEvery),
                                        new XElement("DailyOccursOnceAt", (oline.DailyOccursOnceAt == null ? null : oline.DailyOccursOnceAt.Value.ToString())),
                                        new XElement("Description", oline.Description),
                                        new XElement("EndAtTime", (oline.EndAtTime == null ? null : oline.EndAtTime.Value.ToString())),
                                        new XElement("EndDate", (oline.EndDate == null ? null : oline.EndDate.Value.ToString(_formatdate_ymd))),
                                        new XElement("FrequenceOccurs", oline.FrequenceOccurs),
                                        new XElement("FrequenceOccursEvery", oline.FrequenceOccursEvery),
                                        new XElement("Fri", oline.Fri),
                                        new XElement("GroupCode", oline.GroupCode),
                                        new XElement("Inactive", oline.Inactive),
                                        new XElement("IsDailyOccursOnce", oline.IsDailyOccursOnce),
                                        new XElement("Mon", oline.Mon),
                                        new XElement("NoEndDate", oline.NoEndDate),
                                        new XElement("OneTimeAtDate", (oline.OneTimeAtDate == null ? null : oline.OneTimeAtDate.Value.ToString(_formatdate_ymd))),
                                        new XElement("OneTimeAtTime", (oline.OneTimeAtTime == null ? null : oline.OneTimeAtTime.Value.ToString())),
                                        new XElement("Sat", oline.Sat),
                                        new XElement("ScheduleType", oline.ScheduleType),
                                        new XElement("StartAtTime", (oline.StartAtTime == null ? null : oline.StartAtTime.Value.ToString())),
                                        new XElement("StartDate", (oline.StartDate == null ? null : oline.StartDate.Value.ToString(_formatdate_ymd))),
                                        new XElement("Sun", oline.Sun),
                                        new XElement("Thu", oline.Thu),
                                        new XElement("TimeType", oline.TimeType),
                                        new XElement("Tue", oline.Tue),
                                        new XElement("Wed", oline.Wed)
                                ));


                        XElement xdoc = new XElement(_xmlNodeRoot,
                            new XElement(_xmlNodeBO,
                            xdata));

                        xdoc.Save(_path_xml_syncdata);
                    }
                    else
                    {
                        var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                        if (xdoc != null)
                        {
                            CreateBaseNode(xdoc, "SyncSchedule", out message);
                            var xdata = XElement_Get(xdoc, "SyncSchedule", out message);
                            if (xdata != null)
                            {

                                xdata.Add(new XElement(_xmlNodeRow,
                                            new XElement("DailyOccursEvery", oline.DailyOccursEvery),
                                            new XElement("DailyOccursOnceAt", (oline.DailyOccursOnceAt == null ? null : oline.DailyOccursOnceAt.Value.ToString())),
                                            new XElement("Description", oline.Description),
                                            new XElement("EndAtTime", (oline.EndAtTime == null ? null : oline.EndAtTime.Value.ToString())),
                                            new XElement("EndDate", (oline.EndDate == null ? null : oline.EndDate.Value.ToString(_formatdate_ymd))),
                                            new XElement("FrequenceOccurs", oline.FrequenceOccurs),
                                            new XElement("FrequenceOccursEvery", oline.FrequenceOccursEvery),
                                            new XElement("Fri", oline.Fri),
                                            new XElement("GroupCode", oline.GroupCode),
                                            new XElement("Inactive", oline.Inactive),
                                            new XElement("IsDailyOccursOnce", oline.IsDailyOccursOnce),
                                            new XElement("Mon", oline.Mon),
                                            new XElement("NoEndDate", oline.NoEndDate),
                                            new XElement("OneTimeAtDate", (oline.OneTimeAtDate == null ? null : oline.OneTimeAtDate.Value.ToString(_formatdate_ymd))),
                                            new XElement("OneTimeAtTime", (oline.OneTimeAtTime == null ? null : oline.OneTimeAtTime.Value.ToString())),
                                            new XElement("Sat", oline.Sat),
                                            new XElement("ScheduleType", oline.ScheduleType),
                                            new XElement("StartAtTime", (oline.StartAtTime == null ? null : oline.StartAtTime.Value.ToString())),
                                            new XElement("StartDate", (oline.StartDate == null ? null : oline.StartDate.Value.ToString(_formatdate_ymd))),
                                            new XElement("Sun", oline.Sun),
                                            new XElement("Thu", oline.Thu),
                                            new XElement("TimeType", oline.TimeType),
                                            new XElement("Tue", oline.Tue),
                                            new XElement("Wed", oline.Wed)
                                        ));


                            }

                            xdoc.Save(_path_xml_syncdata);
                        }
                    }
                    return true;
                }


            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool SyncSchedule_Delete(string groupcode, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    var obj = _db.SyncSchedules.Where(o => o.GroupCode == groupcode).FirstOrDefault();
                    if (obj != null)
                    {
                        _db.SyncSchedules.DeleteOnSubmit(obj);
                        _db.SyncSchedules.Context.SubmitChanges();
                    }
                    return true;
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncSchedule", out message);
                        if (xdata != null)
                        {
                            var xxx = (from x in xdata.Descendants(_xmlNodeRow)
                                       where x.Element("GroupCode").Value == groupcode
                                       select x).ToList();


                            if (xxx != null && xxx.Count > 0)
                            {
                                foreach (var x in xxx)
                                {
                                    x.Remove();
                                }
                            }

                            xdoc.Save(_path_xml_syncdata);
                        }
                    }
                    return true;
                }



            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }
        #endregion

        #region Sync Source
        public List<SyncSource_View> SyncSource_Get_All(out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    using (var trans = TransWithUncommitted())
                    {
                        var results = (from x in _db.SyncSources
                                       from y in _db.SyncGroups
                                       from z in _db.SyncTypes
                                       where x.GroupCode == y.GroupCode
                                       && y.TypeCode == z.TypeCode
                                       select new SyncSource_View
                                       {
                                           udfTypeCode = y.TypeCode,
                                           udfTypeName = z.TypeName,
                                           GroupCode = x.GroupCode,
                                           udfGroupName = y.GroupName,
                                           SourceCode = x.SourceCode,
                                           SourceName = x.SourceName,
                                           SourceIN = x.SourceIN,
                                           SourceOUT = x.SourceOUT,
                                           Remarks = x.Remarks,
                                           XmlPrefixName = x.XmlPrefixName,
                                           LastedSyncDate = x.LastedSyncDate,
                                           IsDeleteBefore = x.IsDeleteBefore,
                                           IsOnlyInsert = x.IsOnlyInsert,
                                           IsHeader = x.IsHeader,
                                           IsLine = x.IsLine,
                                           XmlElement = x.XmlElement,
                                           ObjectType = x.ObjectType,
                                           Inactive = x.Inactive,
                                           KeyUpdate = x.KeyUpdate,
                                           ObjectQuery = x.ObjectQuery,
                                           IsObjectDraft = x.IsObjectDraft,
                                           TransactionNotification = x.TransactionNotification,
                                       }).ToList();

                        if (results != null)
                            return results.OrderBy(o => o.GroupCode).ToList();
                    }
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncSource", out message);
                        if (xdata != null)
                        {
                            var otemp = (from x in xdata.Elements(_xmlNodeRow)
                                         select new SyncSource_View
                                         {
                                             GroupCode = x.Element("GroupCode").Value,
                                             Inactive = Convert.ToBoolean(x.Element("Inactive").Value),
                                             IsDeleteBefore = Convert.ToBoolean(x.Element("IsDeleteBefore").Value),
                                             IsHeader = Convert.ToBoolean(x.Element("IsHeader").Value),
                                             IsLine = Convert.ToBoolean(x.Element("IsLine").Value),
                                             IsOnlyInsert = Convert.ToBoolean(x.Element("IsOnlyInsert").Value),
                                             LastedSyncDate = string.IsNullOrEmpty(x.Element("LastedSyncDate").Value) ? null : (DateTime?)Convert.ToDateTime(x.Element("LastedSyncDate").Value),
                                             Remarks = x.Element("Remarks").Value,
                                             SourceCode = x.Element("SourceCode").Value,
                                             SourceName = x.Element("SourceName").Value,
                                             SourceIN = x.Element("SourceIN").Value,
                                             SourceOUT = x.Element("SourceOUT").Value,
                                             ObjectType = x.Element("ObjectType").Value,
                                             XmlElement = x.Element("XmlElement").Value,
                                             XmlPrefixName = x.Element("XmlPrefixName").Value,
                                             KeyUpdate = x.Element("KeyUpdate").Value,
                                             ObjectQuery = x.Element("ObjectQuery").Value,
                                             IsObjectDraft = Convert.ToBoolean(x.Element("IsObjectDraft").Value),
                                             TransactionNotification = x.Element("TransactionNotification").Value,
                                         }).ToList();

                            var otype = SyncType_Get_All(out message);
                            var ogroup = SyncGroup_Get_All(out message);
                            if (otemp != null && otype != null && ogroup != null)
                            {
                                var oresult = (from x in otemp
                                               join y in ogroup on x.GroupCode equals y.GroupCode
                                               join z in otype on y.TypeCode equals z.TypeCode
                                               select new SyncSource_View
                                               {
                                                   udfTypeCode = y.TypeCode,
                                                   udfTypeName = z.TypeName,
                                                   GroupCode = x.GroupCode,
                                                   udfGroupName = y.GroupName,
                                                   SourceCode = x.SourceCode,
                                                   SourceName = x.SourceName,
                                                   SourceIN = x.SourceIN,
                                                   SourceOUT = x.SourceOUT,
                                                   Remarks = x.Remarks,
                                                   XmlPrefixName = x.XmlPrefixName,
                                                   LastedSyncDate = x.LastedSyncDate,
                                                   IsDeleteBefore = x.IsDeleteBefore,
                                                   IsOnlyInsert = x.IsOnlyInsert,
                                                   IsHeader = x.IsHeader,
                                                   IsLine = x.IsLine,
                                                   XmlElement = x.XmlElement,
                                                   ObjectType = x.ObjectType,
                                                   Inactive = x.Inactive,
                                                   KeyUpdate = x.KeyUpdate,
                                                   ObjectQuery = x.ObjectQuery,
                                                   IsObjectDraft = x.IsObjectDraft,
                                                   TransactionNotification = x.TransactionNotification,
                                               }).ToList();
                                if (oresult != null)
                                    return oresult.OrderBy(o => o.GroupCode).ToList();
                            }
                            else if (otemp != null)
                                return otemp.OrderBy(o => o.GroupCode).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncSource_View>();
        }

        public List<SyncSource_View> SyncSource_Get_Active(out string message)
        {
            message = "";
            try
            {
                var otemp = SyncSource_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.Inactive == false && o.IsLine == false).ToList();
                    return otemp;
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncSource_View>();
        }

        public List<SyncSource_View> SyncSource_Get_Active(string groupcode, out string message)
        {
            message = "";
            try
            {
                var otemp = SyncSource_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.Inactive == false && o.GroupCode == groupcode && o.IsLine == false).ToList();
                }

                return otemp;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncSource_View>();
        }

        public List<SyncSource_View> SyncSource_Get_Active(string groupcode, bool isLineEmpty, out string message)
        {
            message = "";
            try
            {

                var otemp = SyncSource_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.Inactive == false && o.GroupCode == groupcode && o.IsLine == false).ToList();
                }

                if (isLineEmpty)
                {
                    if (otemp == null)
                        otemp = new List<SyncSource_View>();

                    otemp.Insert(0, new SyncSource_View() { SourceCode = "", SourceName = "-------" });
                }
                return otemp;

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncSource_View>();
        }

        public SyncSource_View SyncSource_Get_ByKey(string sourcecode, out string message)
        {
            message = "";
            try
            {
                var otemp = SyncSource_Get_All(out message);
                if (otemp != null)
                {
                    var ofirst = otemp.Where(o => o.Inactive == false && o.SourceCode == sourcecode).FirstOrDefault();
                    return ofirst;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }

        public bool SyncSource_Save(List<SyncSource_View> odata, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    if (odata != null)
                    {
                        List<SyncSource> list = new List<SyncSource>();
                        foreach (var x in odata)
                        {
                            list.Add(new SyncSource()
                            {
                                GroupCode = x.GroupCode,
                                SourceCode = x.SourceCode,
                                SourceName = x.SourceName,
                                SourceIN = x.SourceIN,
                                SourceOUT = x.SourceOUT,
                                Remarks = x.Remarks,
                                XmlPrefixName = x.XmlPrefixName,
                                LastedSyncDate = x.LastedSyncDate,
                                IsDeleteBefore = x.IsDeleteBefore,
                                IsOnlyInsert = x.IsOnlyInsert,
                                IsHeader = x.IsHeader,
                                IsLine = x.IsLine,
                                XmlElement = x.XmlElement,
                                ObjectType = x.ObjectType,
                                Inactive = x.Inactive,
                                KeyUpdate = x.KeyUpdate,
                                ObjectQuery = x.ObjectQuery,
                                IsObjectDraft = x.IsObjectDraft,
                                TransactionNotification = x.TransactionNotification,
                            });
                        }
                        var obj = _db.SyncSources.ToList();
                        _db.SyncSources.DeleteAllOnSubmit(obj);
                        _db.SyncSources.InsertAllOnSubmit(list);
                        _db.SyncSources.Context.SubmitChanges();
                        return true;
                    }

                    return false;
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    if (_dir_xmldata != "")
                        if (!Directory.Exists(_dir_xmldata))
                            Directory.CreateDirectory(_dir_xmldata);

                    DeleleAll(_path_xml_syncdata, "SyncSource", out message);

                    if (!File.Exists(_path_xml_syncdata))
                    {


                        var xdata = new XElement("SyncSource");

                        foreach (var oline in odata)
                        {
                            xdata.Add(new XElement(_xmlNodeRow,
                                    new XElement("GroupCode", oline.GroupCode),
                                    new XElement("Inactive", oline.Inactive),
                                    new XElement("IsDeleteBefore", oline.IsDeleteBefore),
                                    new XElement("IsHeader", oline.IsHeader),
                                    new XElement("IsLine", oline.IsLine),
                                    new XElement("IsOnlyInsert", oline.IsOnlyInsert),
                                    new XElement("LastedSyncDate", oline.LastedSyncDate),
                                    new XElement("Remarks", oline.Remarks),
                                    new XElement("SourceCode", oline.SourceCode),
                                    new XElement("SourceIN", oline.SourceIN),
                                    new XElement("SourceName", oline.SourceName),
                                    new XElement("SourceOUT", oline.SourceOUT),
                                    new XElement("KeyUpdate", oline.KeyUpdate),
                                    new XElement("XmlElement", oline.XmlElement),
                                    new XElement("ObjectType", oline.ObjectType),
                                    new XElement("XmlPrefixName", oline.XmlPrefixName),
                                    new XElement("ObjectQuery", oline.ObjectQuery),
                                    new XElement("IsObjectDraft", oline.IsObjectDraft),
                                    new XElement("TransactionNotification", oline.TransactionNotification)
                                    ));
                        }

                        XElement xdoc = new XElement(_xmlNodeRoot,
                            new XElement(_xmlNodeBO,
                            xdata));

                        xdoc.Save(_path_xml_syncdata);
                    }
                    else
                    {
                        var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                        if (xdoc != null)
                        {
                            CreateBaseNode(xdoc, "SyncSource", out message);
                            var xdata = XElement_Get(xdoc, "SyncSource", out message);
                            if (xdata != null)
                            {
                                foreach (var oline in odata)
                                {
                                    xdata.Add(new XElement(_xmlNodeRow,
                                            new XElement("GroupCode", oline.GroupCode),
                                            new XElement("Inactive", oline.Inactive),
                                            new XElement("IsDeleteBefore", oline.IsDeleteBefore),
                                            new XElement("IsHeader", oline.IsHeader),
                                            new XElement("IsLine", oline.IsLine),
                                            new XElement("IsOnlyInsert", oline.IsOnlyInsert),
                                            new XElement("LastedSyncDate", oline.LastedSyncDate),
                                            new XElement("Remarks", oline.Remarks),
                                            new XElement("SourceCode", oline.SourceCode),
                                            new XElement("SourceIN", oline.SourceIN),
                                            new XElement("SourceName", oline.SourceName),
                                            new XElement("SourceOUT", oline.SourceOUT),
                                            new XElement("KeyUpdate", oline.KeyUpdate),
                                            new XElement("XmlElement", oline.XmlElement),
                                            new XElement("ObjectType", oline.ObjectType),
                                            new XElement("XmlPrefixName", oline.XmlPrefixName),
                                            new XElement("ObjectQuery", oline.ObjectQuery),
                                            new XElement("IsObjectDraft", oline.IsObjectDraft),
                                            new XElement("TransactionNotification", oline.TransactionNotification)
                                            ));
                                }

                            }

                            xdoc.Save(_path_xml_syncdata);
                        }
                    }
                    return true;
                }


            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool LastedSyncDate_Update(object sourcecode, DateTime? syncDate, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    var obj = _db.SyncSources.Where(o => o.SourceCode == sourcecode).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.LastedSyncDate = syncDate;
                        _db.SyncSources.Context.SubmitChanges();
                    }
                    return true;
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncSource", out message);
                        if (xdata != null)
                        {
                            var xsource = (from x in xdata.Descendants(_xmlNodeRow)
                                           where x.Element("SourceCode").Value.Trim().ToLower() == sourcecode.ToString().Trim().ToLower()
                                           select x).FirstOrDefault();
                            if (xsource != null)
                            {
                                xsource.Element("LastedSyncDate").Value = syncDate.Value.ToString(_formatdate_ymd_hms);
                                xdoc.Save(_path_xml_syncdata);
                                return true;
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool SyncSource_Delete(string sourcecode, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    var obj = _db.SyncSources.Where(o => o.SourceCode == sourcecode).FirstOrDefault();
                    if (obj != null)
                    {
                        _db.SyncSources.DeleteOnSubmit(obj);
                        _db.SyncSources.Context.SubmitChanges();
                    }
                    return true;
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncSource", out message);
                        if (xdata != null)
                        {
                            var xxx = (from x in xdata.Descendants(_xmlNodeRow)
                                       where x.Element("SourceCode").Value == sourcecode
                                       select x).ToList();


                            if (xxx != null && xxx.Count > 0)
                            {
                                foreach (var x in xxx)
                                {
                                    x.Remove();
                                }
                            }

                            xdoc.Save(_path_xml_syncdata);
                        }
                    }
                    return true;
                }



            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }
        #endregion

        #region Sync Source Level
        public List<SyncSourceLevel_View> SyncSourceLevel_Get_All(out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    using (var trans = TransWithUncommitted())
                    {
                        var results = (from x in _db.SyncSourceLevels
                                       join y in _db.SyncSources on x.SourceLine equals y.SourceCode
                                       select new SyncSourceLevel_View
                                       {
                                           SourceHeader = x.SourceHeader,
                                           SourceLine = x.SourceLine,
                                           HeaderKey = x.HeaderKey,
                                           LineReferenceKey = x.LineReferenceKey,
                                           XmlElement = y.XmlElement,
                                       }).ToList();
                        if (results != null)
                            return results.OrderBy(o => o.SourceHeader).ToList();
                    }
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncSourceLevel", out message);
                        if (xdata != null)
                        {
                            var otemp = (from x in xdata.Elements(_xmlNodeRow)
                                         select new SyncSourceLevel_View
                                         {
                                             HeaderKey = x.Element("HeaderKey").Value,
                                             Inactive = Convert.ToBoolean(x.Element("Inactive").Value),
                                             LineReferenceKey = x.Element("LineReferenceKey").Value,
                                             SourceHeader = x.Element("SourceHeader").Value,
                                             SourceLine = x.Element("SourceLine").Value,
                                         }).ToList();
                            if (otemp != null)
                                return otemp.OrderBy(o => o.SourceHeader).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncSourceLevel_View>();
        }

        public List<SyncSourceLevel_View> SyncSourceLevel_Get_Active(out string message)
        {
            message = "";
            try
            {
                var otemp = SyncSourceLevel_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.Inactive == false).ToList();
                    return otemp;
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncSourceLevel_View>();
        }

        public List<SyncSourceLevel_View> SyncSourceLevel_Get_Active(string sourceheader, out string message)
        {
            message = "";
            try
            {
                var otemp = SyncSourceLevel_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.Inactive == false && o.SourceHeader == sourceheader).ToList();
                    return otemp;
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncSourceLevel_View>();
        }

        public bool SyncSourceLevel_Save(List<SyncSourceLevel_View> odata, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    if (odata != null)
                    {
                        List<SyncSourceLevel> list = new List<SyncSourceLevel>();
                        foreach (var x in odata)
                        {
                            list.Add(new SyncSourceLevel()
                            {
                                HeaderKey = x.HeaderKey,
                                Inactive = x.Inactive,
                                LineReferenceKey = x.LineReferenceKey,
                                SourceHeader = x.SourceHeader,
                                SourceLine = x.SourceLine,
                            });
                        }
                        var obj = _db.SyncSourceLevels.ToList();
                        _db.SyncSourceLevels.DeleteAllOnSubmit(obj);
                        _db.SyncSourceLevels.InsertAllOnSubmit(list);
                        _db.SyncSourceLevels.Context.SubmitChanges();
                        return true;
                    }

                    return false;
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    if (_dir_xmldata != "")
                        if (!Directory.Exists(_dir_xmldata))
                            Directory.CreateDirectory(_dir_xmldata);

                    DeleleAll(_path_xml_syncdata, "SyncSourceLevel", out message);

                    if (!File.Exists(_path_xml_syncdata))
                    {


                        var xdata = new XElement("SyncSourceLevel");

                        foreach (var oline in odata)
                        {
                            xdata.Add(new XElement(_xmlNodeRow,
                                    new XElement("HeaderKey", oline.HeaderKey),
                                    new XElement("Inactive", oline.Inactive),
                                    new XElement("LineReferenceKey", oline.LineReferenceKey),
                                    new XElement("SourceHeader", oline.SourceHeader),
                                    new XElement("SourceLine", oline.SourceLine)
                                    ));
                        }

                        XElement xdoc = new XElement(_xmlNodeRoot,
                            new XElement(_xmlNodeBO,
                            xdata));

                        xdoc.Save(_path_xml_syncdata);
                    }
                    else
                    {
                        var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                        if (xdoc != null)
                        {
                            CreateBaseNode(xdoc, "SyncSourceLevel", out message);
                            var xdata = XElement_Get(xdoc, "SyncSourceLevel", out message);
                            if (xdata != null)
                            {
                                foreach (var oline in odata)
                                {
                                    xdata.Add(new XElement(_xmlNodeRow,
                                            new XElement("HeaderKey", oline.HeaderKey),
                                            new XElement("Inactive", oline.Inactive),
                                            new XElement("LineReferenceKey", oline.LineReferenceKey),
                                            new XElement("SourceHeader", oline.SourceHeader),
                                            new XElement("SourceLine", oline.SourceLine)
                                            ));
                                }

                            }

                            xdoc.Save(_path_xml_syncdata);
                        }
                    }
                    return true;
                }


            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool SyncSourceLevel_Delete(string sourceheader, string sourceline, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    var obj = _db.SyncSourceLevels.Where(o => o.SourceHeader == sourceheader && o.SourceLine == sourceline).FirstOrDefault();
                    if (obj != null)
                    {
                        _db.SyncSourceLevels.DeleteOnSubmit(obj);
                        _db.SyncSourceLevels.Context.SubmitChanges();
                    }
                    return true;
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncSourceLevel", out message);
                        if (xdata != null)
                        {
                            var xxx = (from x in xdata.Descendants(_xmlNodeRow)
                                       where x.Element("SourceHeader").Value == sourceheader && x.Element("SourceLine").Value == sourceline
                                       select x).ToList();


                            if (xxx != null && xxx.Count > 0)
                            {
                                foreach (var x in xxx)
                                {
                                    x.Remove();
                                }
                            }

                            xdoc.Save(_path_xml_syncdata);
                        }
                    }
                    return true;
                }



            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }
        #endregion

        #region Sync Condition
        public List<SyncCondition_View> SyncCondition_Get_All(out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    using (var trans = TransWithUncommitted())
                    {
                        var results = (from x in _db.SyncConditions
                                       join y in _db.SyncSources on x.SourceCode equals y.SourceCode
                                       select new SyncCondition_View
                                       {
                                           ConditionType = x.ConditionType,
                                           ConditionValue = x.ConditionValue,
                                           ConditionType1 = x.ConditionType1,
                                           ConditionValue1 = x.ConditionValue1,
                                           ConditionType2 = x.ConditionType2,
                                           ConditionValue2 = x.ConditionValue2,
                                           ConditionType3 = x.ConditionType3,
                                           ConditionValue3 = x.ConditionValue3,
                                           ConditionType4 = x.ConditionType4,
                                           ConditionValue4 = x.ConditionValue4,
                                           ConditionType5 = x.ConditionType5,
                                           ConditionValue5 = x.ConditionValue5,
                                           ConditionType6 = x.ConditionType6,
                                           ConditionValue6 = x.ConditionValue6,
                                           ConditionType7 = x.ConditionType7,
                                           ConditionValue7 = x.ConditionValue7,
                                           Inactive = x.Inactive,
                                           SourceCode = x.SourceCode,
                                           SourceColumn = x.SourceColumn,
                                       }).ToList();
                        if (results != null)
                            return results.OrderBy(o => o.SourceCode).ToList();
                    }
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncCondition", out message);
                        if (xdata != null)
                        {
                            var otemp = (from x in xdata.Elements(_xmlNodeRow)
                                         select new SyncCondition_View
                                         {
                                             ConditionType = x.Element("ConditionType").Value,
                                             ConditionValue = x.Element("ConditionValue").Value,
                                             ConditionType1 = x.Element("ConditionType1").Value,
                                             ConditionValue1 = x.Element("ConditionValue1").Value,
                                             ConditionType2 = x.Element("ConditionType2").Value,
                                             ConditionValue2 = x.Element("ConditionValue2").Value,
                                             ConditionType3 = x.Element("ConditionType3").Value,
                                             ConditionValue3 = x.Element("ConditionValue3").Value,
                                             ConditionType4 = x.Element("ConditionType4").Value,
                                             ConditionValue4 = x.Element("ConditionValue4").Value,
                                             ConditionType5 = x.Element("ConditionType5").Value,
                                             ConditionValue5 = x.Element("ConditionValue5").Value,
                                             ConditionType6 = x.Element("ConditionType6").Value,
                                             ConditionValue6 = x.Element("ConditionValue6").Value,
                                             ConditionType7 = x.Element("ConditionType7").Value,
                                             ConditionValue7 = x.Element("ConditionValue7").Value,
                                             Inactive = Convert.ToBoolean(x.Element("Inactive").Value),
                                             SourceCode = x.Element("SourceCode").Value,
                                             SourceColumn = x.Element("SourceColumn").Value,
                                         }).ToList();
                            if (otemp != null)
                                return otemp.OrderBy(o => o.SourceCode).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncCondition_View>();
        }

        public List<SyncCondition_View> SyncCondition_Get_Active(out string message)
        {
            message = "";
            try
            {
                var otemp = SyncCondition_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.Inactive == false).ToList();
                    return otemp;
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncCondition_View>();
        }

        public List<SyncCondition_View> SyncCondition_Get_Active(string sourcecode, out string message)
        {
            message = "";
            try
            {
                var otemp = SyncCondition_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.Inactive == false && o.SourceCode == sourcecode).ToList();
                    return otemp;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncCondition_View>();
        }

        public bool SyncCondition_Save(List<SyncCondition_View> odata, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    if (odata != null)
                    {
                        List<SyncCondition> list = new List<SyncCondition>();
                        foreach (var x in odata)
                        {
                            list.Add(new SyncCondition()
                            {
                                ConditionType = x.ConditionType,
                                ConditionValue = x.ConditionValue,
                                ConditionType1 = x.ConditionType1,
                                ConditionValue1 = x.ConditionValue1,
                                ConditionType2 = x.ConditionType2,
                                ConditionValue2 = x.ConditionValue2,
                                ConditionType3 = x.ConditionType3,
                                ConditionValue3 = x.ConditionValue3,
                                ConditionType4 = x.ConditionType4,
                                ConditionValue4 = x.ConditionValue4,
                                ConditionType5 = x.ConditionType5,
                                ConditionValue5 = x.ConditionValue5,
                                ConditionType6 = x.ConditionType6,
                                ConditionValue6 = x.ConditionValue6,
                                ConditionType7 = x.ConditionType7,
                                ConditionValue7 = x.ConditionValue7,
                                Inactive = x.Inactive,
                                SourceCode = x.SourceCode,
                                SourceColumn = x.SourceColumn,
                            });
                        }
                        var obj = _db.SyncConditions.ToList();
                        _db.SyncConditions.DeleteAllOnSubmit(obj);
                        _db.SyncConditions.InsertAllOnSubmit(list);
                        _db.SyncConditions.Context.SubmitChanges();
                        return true;
                    }

                    return false;
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {

                }

                if (_dir_xmldata != "")
                    if (!Directory.Exists(_dir_xmldata))
                        Directory.CreateDirectory(_dir_xmldata);

                DeleleAll(_path_xml_syncdata, "SyncCondition", out message);

                if (!File.Exists(_path_xml_syncdata))
                {


                    var xdata = new XElement("SyncCondition");

                    foreach (var oline in odata)
                    {
                        xdata.Add(new XElement(_xmlNodeRow,
                                new XElement("ConditionType", oline.ConditionType),
                                new XElement("ConditionValue", oline.ConditionValue),
                                new XElement("ConditionType1", oline.ConditionType1),
                                new XElement("ConditionValue1", oline.ConditionValue1),
                                new XElement("ConditionType2", oline.ConditionType2),
                                new XElement("ConditionValue2", oline.ConditionValue2),
                                new XElement("ConditionType3", oline.ConditionType3),
                                new XElement("ConditionValue3", oline.ConditionValue3),
                                new XElement("ConditionType4", oline.ConditionType4),
                                new XElement("ConditionValue4", oline.ConditionValue4),
                                new XElement("ConditionType5", oline.ConditionType5),
                                new XElement("ConditionValue5", oline.ConditionValue5),
                                new XElement("ConditionType6", oline.ConditionType6),
                                new XElement("ConditionValue6", oline.ConditionValue6),
                                new XElement("ConditionType7", oline.ConditionType7),
                                new XElement("ConditionValue7", oline.ConditionValue7),
                                new XElement("Inactive", oline.Inactive),
                                new XElement("SourceCode", oline.SourceCode),
                                new XElement("SourceColumn", oline.SourceColumn)
                                ));
                    }

                    XElement xdoc = new XElement(_xmlNodeRoot,
                        new XElement(_xmlNodeBO,
                        xdata));

                    xdoc.Save(_path_xml_syncdata);
                }
                else
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        CreateBaseNode(xdoc, "SyncCondition", out message);
                        var xdata = XElement_Get(xdoc, "SyncCondition", out message);
                        if (xdata != null)
                        {
                            foreach (var oline in odata)
                            {
                                xdata.Add(new XElement(_xmlNodeRow,
                                        new XElement("ConditionType", oline.ConditionType),
                                        new XElement("ConditionValue", oline.ConditionValue),
                                        new XElement("ConditionType1", oline.ConditionType1),
                                        new XElement("ConditionValue1", oline.ConditionValue1),
                                        new XElement("ConditionType2", oline.ConditionType2),
                                        new XElement("ConditionValue2", oline.ConditionValue2),
                                        new XElement("ConditionType3", oline.ConditionType3),
                                        new XElement("ConditionValue3", oline.ConditionValue3),
                                        new XElement("ConditionType4", oline.ConditionType4),
                                        new XElement("ConditionValue4", oline.ConditionValue4),
                                        new XElement("ConditionType5", oline.ConditionType5),
                                        new XElement("ConditionValue5", oline.ConditionValue5),
                                        new XElement("ConditionType6", oline.ConditionType6),
                                        new XElement("ConditionValue6", oline.ConditionValue6),
                                        new XElement("ConditionType7", oline.ConditionType7),
                                        new XElement("ConditionValue7", oline.ConditionValue7),
                                        new XElement("Inactive", oline.Inactive),
                                        new XElement("SourceCode", oline.SourceCode),
                                        new XElement("SourceColumn", oline.SourceColumn)
                                        ));
                            }

                        }

                        xdoc.Save(_path_xml_syncdata);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool SyncCondition_Delete(string sourcecode, string column, out string message)
        {
            message = "";
            try
            {
                if (BaseUtil._syncSettingType == eSyncSettingType.DataSql)
                {
                    var obj = _db.SyncConditions.Where(o => o.SourceCode == sourcecode && o.SourceColumn == column).FirstOrDefault();
                    if (obj != null)
                    {
                        _db.SyncConditions.DeleteOnSubmit(obj);
                        _db.SyncConditions.Context.SubmitChanges();
                    }
                    return true;
                }
                else if (BaseUtil._syncSettingType == eSyncSettingType.DataXml)
                {
                    var xdoc = XDoc_Get(_path_xml_syncdata, out message);
                    if (xdoc != null)
                    {
                        var xdata = XElement_Get(xdoc, "SyncCondition", out message);
                        if (xdata != null)
                        {
                            var xxx = (from x in xdata.Descendants(_xmlNodeRow)
                                       where x.Element("SourceCode").Value == sourcecode && x.Element("SourceColumn").Value == column
                                       select x).ToList();


                            if (xxx != null && xxx.Count > 0)
                            {
                                foreach (var x in xxx)
                                {
                                    x.Remove();
                                }
                            }

                            xdoc.Save(_path_xml_syncdata);
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }
        #endregion

        #region MACHINE LIST
        public List<SyncMachine_View> LocalMachine_Get()
        {
            //Get all WhsCode of SHOP
            var obj = (from x in _db.ps_uv_get_machine_for_syncs
                       select new SyncMachine_View
                       {
                           MachineCode = x.MachineCode,
                           MachineName = x.MachineName,
                           StoreCode = x.StoreCode,
                       }).ToList();

            return obj;
        }
        #endregion
    }
}
