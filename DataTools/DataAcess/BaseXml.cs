﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Xml;
using System.Xml.Linq;
using System.Reflection;
using System.Web;
using System.IO;

using DataTools.Models;
using DataTools.Utility;

namespace DataTools.DataAccess
{
    public class BaseXml : BaseData
    {
        /*XML Settings*/
        public const string _xmlNodeRoot = "root";
        public const string _xmlNodeBO = "bo";
        public const string _xmlNodeRow = "row";

        public string _dir_xmldata = @"xmldata";
        public string _path_xml_mainlog = @"xmldata/mainlog.xml";
        public string _path_xml_synclog = @"xmldata/synclog.xml";
        public string _path_xml_syncdata = @"xmldata/syncdata.xml";

        public BaseXml()
        {
            _dir_xmldata = System.AppDomain.CurrentDomain.BaseDirectory + "/xmldata";
            _path_xml_mainlog = System.AppDomain.CurrentDomain.BaseDirectory + @"/xmldata/mainlog.xml";
            _path_xml_synclog = System.AppDomain.CurrentDomain.BaseDirectory + @"/xmldata/synclog.xml";
            _path_xml_syncdata = System.AppDomain.CurrentDomain.BaseDirectory + @"/xmldata/syncdata.xml";
        }

        public XDocument XDoc_Get(string path_xmldata, out string message)
        {
            message = "";
            try
            {
                if (File.Exists(path_xmldata))
                {
                    XDocument xdoc = XDocument.Load(path_xmldata);
                    if (xdoc != null)
                    {
                        return xdoc;
                    }
                    else
                        message = "Couldn't load file.";
                }
                else
                    message = "File not found.";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }

        public XElement XElement_Get(XDocument xdoc, out string message)
        {
            message = "";
            try
            {
                if (xdoc != null)
                {
                    XElement xroot = xdoc.Element(_xmlNodeRoot);
                    if (xroot != null)
                    {
                        XElement xbo = xroot.Element(_xmlNodeBO);
                        if (xbo != null)
                        {
                            return xbo;
                        }
                        else
                            message = "Invalid bo node.";
                    }
                    else
                        message = "Invalid root node.";
                }
                else
                    message = "Couldn't load file.";

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }

        public XElement XElement_Get(XDocument xdoc, string sourcename, out string message)
        {
            message = "";
            try
            {
                if (xdoc != null)
                {
                    XElement xroot = xdoc.Element(_xmlNodeRoot);
                    if (xroot != null)
                    {
                        XElement xbo = xroot.Element(_xmlNodeBO);
                        if (xbo != null)
                        {
                            XElement xdata = xbo.Element(sourcename);
                            if (xdata != null)
                                return xdata;
                            else
                                message = "Invalid data node.";
                        }
                        else
                            message = "Invalid bo node.";
                    }
                    else
                        message = "Invalid root node.";
                }
                else
                    message = "Couldn't load file.";

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }

        public bool DeleleAll(string path_xmldata, string sourcename, out string message)
        {
            message = "";
            try
            {
                var xdoc = XDoc_Get(path_xmldata, out message);
                if (xdoc != null)
                {
                    var xdata = XElement_Get(xdoc, sourcename, out message);
                    if (xdata != null)
                    {
                        xdata.RemoveNodes();
                        xdoc.Save(path_xmldata);
                        return true;
                    }
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool CreateBaseNode(XDocument xdoc, string sourcename, out string message)
        {
            message = "";
            try
            {
                if (xdoc != null)
                {
                    if (xdoc.Element(_xmlNodeRoot) == null)
                    {
                        xdoc.Add(new XElement(_xmlNodeRoot));
                    }
                    else if (xdoc.Element(_xmlNodeRoot).Element(_xmlNodeBO) == null)
                    {
                        xdoc.Element(_xmlNodeRoot).Add(new XElement(_xmlNodeBO));
                    }
                    else if (xdoc.Element(_xmlNodeRoot).Element(_xmlNodeBO).Element(sourcename) == null)
                    {
                        xdoc.Element(_xmlNodeRoot).Element(_xmlNodeBO).Add(new XElement(sourcename));
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        #region Sync Log
        public List<SyncLog_View> SyncLog_Get_All(out string message)
        {
            message = "";
            try
            {
                var xdoc = XDoc_Get(_path_xml_synclog, out message);
                if (xdoc != null)
                {
                    var xdata = XElement_Get(xdoc, "SyncLog", out message);
                    if (xdata != null)
                    {
                        var otemp = (from x in xdata.Elements(_xmlNodeRow)
                                     select new SyncLog_View
                                     {
                                         IsManual = string.IsNullOrEmpty(x.Element("IsManual").Value) ? false : Convert.ToBoolean(x.Element("IsManual").Value),
                                         Description = x.Element("Description").Value,
                                         SyncBy = x.Element("SyncBy").Value,
                                         SyncDate = string.IsNullOrEmpty(x.Element("SyncDate").Value) ? DateTime.MinValue : Convert.ToDateTime(x.Element("SyncDate").Value),
                                     }).ToList();
                        return otemp;
                    }
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncLog_View>();
        }

        public List<SyncLog_View> SyncLog_GetBy_Type_Date(bool? ismanual, DateTime? fromdate, DateTime? todate, out string message)
        {
            message = "";
            try
            {
                var otemp = SyncLog_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => (fromdate == null || o.SyncDate.Date >= fromdate.Value.Date)
                        && (todate == null || o.SyncDate.Date <= todate.Value.Date)
                        && (ismanual == null || o.IsManual == ismanual)).ToList();
                    return otemp;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<SyncLog_View>();
        }

        public bool SyncLog_Save(string userid, string description, bool isManual, out string message)
        {
            message = "";
            try
            {
                //var currdir_xmldata = System.AppDomain.CurrentDomain.BaseDirectory + "/" + _dir_xmldata;
                //var _curr_path_xml_synclog = System.AppDomain.CurrentDomain.BaseDirectory + "/" + _path_xml_synclog;

                if (_dir_xmldata != "")
                    if (!Directory.Exists(_dir_xmldata))
                        Directory.CreateDirectory(_dir_xmldata);

                if (!File.Exists(_path_xml_synclog))
                {

                    var xdata = new XElement("SyncLog");
                    SyncLog_View oline = new SyncLog_View() { SyncDate = DateTime.Now, SyncBy = userid, Description = description, IsManual = isManual };

                    xdata.Add(new XElement(_xmlNodeRow,
                            new XElement("SyncDate", oline.SyncDate.ToString(BaseUtil._formatdate_ymd_hms)),
                            new XElement("SyncBy", oline.SyncBy),
                            new XElement("Description", oline.Description),
                            new XElement("IsManual", oline.IsManual)
                            ));

                    XElement xdoc = new XElement(_xmlNodeRoot,
                        new XElement(_xmlNodeBO,
                        xdata));

                    xdoc.Save(_path_xml_synclog);
                }
                else
                {
                    var xdoc = XDoc_Get(_path_xml_synclog, out message);
                    if (message != "")
                    {
                        return false;
                    }
                    if (xdoc != null)
                    {
                        CreateBaseNode(xdoc, "SyncLog", out message);
                        if (message != "")
                        {
                            return false;
                        }
                        var xdata = XElement_Get(xdoc, "SyncLog", out message);
                        if (message != "")
                        {
                            return false;
                        }
                        if (xdata != null)
                        {
                            SyncLog_View oline = new SyncLog_View() { SyncDate = DateTime.Now, SyncBy = userid, Description = description, IsManual = isManual };

                            xdata.Add(new XElement(_xmlNodeRow,
                                    new XElement("SyncDate", oline.SyncDate.ToString(BaseUtil._formatdate_ymd_hms)),
                                    new XElement("SyncBy", oline.SyncBy),
                                    new XElement("Description", oline.Description),
                                    new XElement("IsManual", oline.IsManual)
                                    ));

                        }

                        xdoc.Save(_path_xml_synclog);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool SyncLog_Delete_All(out string message)
        {
            message = "";
            try
            {

                var xdoc = XDoc_Get(_path_xml_synclog, out message);
                if (xdoc != null)
                {
                    var xdata = XElement_Get(xdoc, "SyncLog", out message);
                    if (xdata != null)
                    {
                        var xxx = (from x in xdata.Descendants(_xmlNodeRow)
                                   select x).ToList();


                        if (xxx != null && xxx.Count > 0)
                        {
                            foreach (var x in xxx)
                            {
                                x.Remove();
                            }
                        }

                        xdoc.Save(_path_xml_synclog);
                    }
                }
                return true;

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }
        #endregion

        #region Main Log
        public List<Log_View> MainLog_Get_All(out string message)
        {
            message = "";
            try
            {
                var xdoc = XDoc_Get(_path_xml_mainlog, out message);
                if (xdoc != null)
                {
                    var xdata = XElement_Get(xdoc, "MainLog", out message);
                    if (xdata != null)
                    {
                        var otemp = (from x in xdata.Elements(_xmlNodeRow)
                                     select new Log_View
                                     {
                                         Date = string.IsNullOrEmpty(x.Element("Date").Value) ? DateTime.MinValue : Convert.ToDateTime(x.Element("Date").Value),
                                         Time = string.IsNullOrEmpty(x.Element("Time").Value) ? TimeSpan.MinValue : TimeSpan.Parse(x.Element("Time").Value),
                                         Description = x.Element("Description").Value,
                                         MessageType = x.Element("MessageType").Value,
                                         Read = string.IsNullOrEmpty(x.Element("Read").Value) ? false : Convert.ToBoolean(x.Element("Read").Value),
                                         UserId = x.Element("UserId").Value,
                                     }).ToList();
                        return otemp;
                    }
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<Log_View>();
        }

        public List<Log_View> MainLog_GetBy_Type(eMessageType msgtype, out string message)
        {
            message = "";
            try
            {
                var otemp = MainLog_Get_All(out message);
                if (otemp != null)
                {
                    otemp = otemp.Where(o => o.MessageType == msgtype.ToString()).ToList();
                    return otemp;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return new List<Log_View>();
        }

        public bool MainLog_Save(string userid, string description, eMessageType msgtype, out string message)
        {
            message = "";
            try
            {
                if (_dir_xmldata != "")
                    if (!Directory.Exists(_dir_xmldata))
                        Directory.CreateDirectory(_dir_xmldata);



                if (!File.Exists(_path_xml_mainlog))
                {

                    var xdata = new XElement("MainLog");

                    xdata.Add(new XElement(_xmlNodeRow,
                            new XElement("Date", DateTime.Now.ToString(BaseUtil._formatdate_ymd_hms)),
                            new XElement("Time", DateTime.Now.ToString(BaseUtil._formattime)),
                            new XElement("UserId", userid),
                            new XElement("Description", description),
                            new XElement("MessageType", msgtype.ToString()),
                            new XElement("Read", false)
                            ));

                    XElement xdoc = new XElement(_xmlNodeRoot,
                        new XElement(_xmlNodeBO,
                        xdata));

                    xdoc.Save(_path_xml_mainlog);
                }
                else
                {
                    var xdoc = XDoc_Get(_path_xml_mainlog, out message);
                    if (xdoc != null)
                    {
                        CreateBaseNode(xdoc, "MainLog", out message);
                        var xdata = XElement_Get(xdoc, "MainLog", out message);
                        if (xdata != null)
                        {
                            xdata.Add(new XElement(_xmlNodeRow,
                           new XElement("Date", DateTime.Now.ToString(BaseUtil._formatdate_ymd_hms)),
                           new XElement("Time", DateTime.Now.ToString(BaseUtil._formattime)),
                           new XElement("UserId", userid),
                           new XElement("Description", description),
                            new XElement("MessageType", msgtype.ToString()),
                           new XElement("Read", false)
                           ));
                        }

                        xdoc.Save(_path_xml_mainlog);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool MainLog_Delete_All(out string message)
        {
            message = "";
            try
            {

                var xdoc = XDoc_Get(_path_xml_mainlog, out message);
                if (xdoc != null)
                {
                    var xdata = XElement_Get(xdoc, "MainLog", out message);
                    if (xdata != null)
                    {
                        var xxx = (from x in xdata.Descendants(_xmlNodeRow)
                                   select x).ToList();


                        if (xxx != null && xxx.Count > 0)
                        {
                            foreach (var x in xxx)
                            {
                                x.Remove();
                            }
                        }

                        xdoc.Save(_path_xml_mainlog);
                    }
                }
                return true;

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }
        #endregion

        

    }
}
