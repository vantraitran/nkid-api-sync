﻿using System;
using System.Data;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using DataTools.Models;
using DataTools.Utility;

namespace DataTools.DataAccess.Sync
{
    public class blSyncDataOne : BaseXml
    {
        string message = "";
        public blSyncDataOne()
        {

        }

        public List<SyncObjectCurrent> SyncData(List<SyncObjectCurrent> syncObjs, bool isPreviewData)
        {
            if (syncObjs == null || syncObjs.Count == 0)
            {
                return syncObjs;
            }
            var oResult = new List<SyncObjectCurrent>();

            var daSync = new MainSync();
            daSync.Sync(syncObjs, isPreviewData);

            return syncObjs;
        }

        public bool SyncData(SyncObjectCurrent syncObject, DataTable dataHeader, out string message)
        {
            message = "";

            if (syncObject == null)
            {
                message = "Please choose a object to sync.";
                return false;
            }

            bool oresult = false;
            var daSync = new MainSync();
            oresult = daSync.Sync(syncObject, dataHeader, out message);

            return oresult;
        }

        private void ExportCustomerToXml(SyncObjectCurrent oSync)
        {
            string xmlPathFile = oSync.TempPath;
            string xmlFileName = oSync.SyncSource + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff") + ".xml";
            var xmlFullName = xmlPathFile + xmlFileName;
            try
            {
                daConnection daConn = new daConnection(true);

                //⦁	CREATE FROM POS:    0
                //⦁	UPDATE FROM POS:    2
                //⦁	DA NHAN FROM CRM:   1
                //⦁	XUAT XML:   3-->CAP NHAT 1

                var dt = daConn.GetData("Customer", "select * from Customer_Master_Data WHERE STATUS=3");
                if (dt != null && dt.Rows.Count > 0)
                {
                    oSync.DataPreview = dt;
                    if (CreateXmlFile(xmlFullName, dt, out message))
                    {
                        string sql = "";

                        //Excute data từng phần-->Vì tránh bộ nhớ không đủ
                        int rowtimes = 500;

                        //header
                        int count = dt.Rows.Count;
                        int index = (count / rowtimes) + 1;

                        for (int n = 0; n < index; n++)
                        {
                            sql = "";
                            for (int m = n * rowtimes; m < (n + 1) * rowtimes; m++)
                            {
                                if (m < count)
                                    sql = sql + string.Format("\n UPDATE Customer_Master_Data SET STATUS=1 WHERE Cust_sid='{0}' and Cust_id='{1}' ", dt.Rows[m]["Cust_sid"], dt.Rows[m]["Cust_id"]);
                                else
                                    break;
                            }

                            if (daConn.ExecQuery(sql, out message) == false)
                            {
                                oSync.SyncStatusDesc = message;
                                oSync.SyncDescription = message;
                                if (File.Exists(xmlFullName))
                                {
                                    File.Delete(xmlFullName);
                                }
                                return;
                            }
                        }

                        //OK
                        oSync.SyncStatusDesc = "Export successfully.";
                        oSync.SyncDescription = "Export successfully.";
                    }
                    else
                    {
                        oSync.SyncStatusDesc = message;
                        oSync.SyncDescription = message;
                    }
                }
                else
                {
                    oSync.SyncStatusDesc = "Not found any data.";
                    oSync.SyncDescription = "Not found any data.";
                }


            }
            catch (Exception ex)
            {
                oSync.SyncDescription = "[Exception]" + ex.Message;
                if (File.Exists(xmlFullName))
                {
                    File.Delete(xmlFullName);
                }
            }
        }

        public bool CreateXmlFile(string _path_xml, DataTable odata, out string message)
        {
            message = "";
            try
            {
                if (odata == null || odata.Rows.Count == 0)
                    return false;

                var xdata = new XElement("CUSTOMERS");

                foreach (DataRow orow in odata.Rows)
                {
                    var xe = new XElement("CUSTOMER");
                    xe.Add(new XAttribute("cust_sid", orow["cust_sid"]));
                    xe.Add(new XAttribute("cust_id", orow["cust_id"]));
                    xe.Add(new XAttribute("first_name", orow["first_name"]));
                    xe.Add(new XAttribute("last_name", orow["last_name"]));
                    xe.Add(new XAttribute("title", orow["title"]));
                    xe.Add(new XAttribute("max_disc_perc", orow["max_disc_perc"]));
                    xe.Add(new XAttribute("info1", orow["info1"]));
                    xe.Add(new XAttribute("info2", orow["info2"]));
                    xe.Add(new XAttribute("company_name", orow["company_name"]));
                    xe.Add(new XAttribute("email_addr", orow["email_addr"]));
                    xe.Add(new XAttribute("sbs_no", orow["sbs_no"]));
                    xe.Add(new XAttribute("primary_clerk_sbs_no", orow["primary_clerk_sbs_no"]));
                    xe.Add(new XAttribute("modifiedby_sbs_no", orow["modifiedby_sbs_no"]));
                    xe.Add(new XAttribute("home_sbs_no", orow["home_sbs_no"]));
                    xe.Add(new XAttribute("createdby_sbs_no", orow["createdby_sbs_no"]));
                    xe.Add(new XAttribute("store_no", orow["store_no"]));
                    xe.Add(new XAttribute("home_store_no", orow["home_store_no"]));
                    xe.Add(new XAttribute("created_date", FormatDateTimeZone(orow["created_date"])));       //2015-08-26T17:17:43+07:00
                    xe.Add(new XAttribute("modified_date", FormatDateTimeZone(orow["modified_date"])));     //2015-08-26T17:17:43+07:00
                    xe.Add(new XAttribute("cms_post_date", FormatDateTimeZone(orow["cms_post_date"])));     //2015-08-26T17:17:43+07:00
                    xe.Add(new XAttribute("price_lvl", orow["price_lvl"]));
                    xe.Add(new XAttribute("udf1_date", FormatDateTimeZone(orow["udf1_date"])));     //2015-08-26T17:17:43+07:00
                    xe.Add(new XAttribute("udf2_date", FormatDateTimeZone(orow["udf2_date"])));     //2015-08-26T17:17:43+07:00
                    xe.Add(new XAttribute("sec_lvl", orow["sec_lvl"]));
                    xe.Add(new XAttribute("primary_clerk_name", orow["primary_clerk_name"]));
                    xe.Add(new XAttribute("orig_controller", orow["orig_controller"]));
                    xe.Add(new XAttribute("modifiedby_empl_name", orow["modifiedby_empl_name"]));
                    xe.Add(new XAttribute("detax", orow["detax"]));
                    xe.Add(new XAttribute("cust_type", orow["cust_type"]));
                    xe.Add(new XAttribute("createdby_empl_name", orow["createdby_empl_name"]));
                    xe.Add(new XAttribute("controller", orow["controller"]));
                    xe.Add(new XAttribute("cms", orow["cms"]));
                    xe.Add(new XAttribute("check_limit", orow["check_limit"]));
                    xe.Add(new XAttribute("ar_flag", orow["ar_flag"]));
                    xe.Add(new XAttribute("allow_post", orow["allow_post"]));
                    xe.Add(new XAttribute("allow_phone", orow["allow_phone"]));
                    xe.Add(new XAttribute("allow_email", orow["allow_email"]));
                    xe.Add(new XAttribute("accept_checks", orow["accept_checks"]));
                    xe.Add(new XAttribute("active", orow["active"]));

                    var xe_addrs = new XElement("CUST_ADDRESSS");
                    var xe_addr = new XElement("CUST_ADDRESS");
                    xe_addr.Add(new XAttribute("phone1", orow["phone1"]));
                    xe_addr.Add(new XAttribute("address1", orow["address1"]));
                    xe_addr.Add(new XAttribute("address2", orow["address2"]));
                    xe_addr.Add(new XAttribute("address3", orow["address3"]));
                    xe_addr.Add(new XAttribute("address4", orow["address4"]));
                    xe_addr.Add(new XAttribute("address5", orow["address5"]));
                    xe_addr.Add(new XAttribute("address6", orow["address6"]));
                    xe_addr.Add(new XAttribute("shipping", orow["shipping"]));
                    xe_addr.Add(new XAttribute("include_post", orow["include_post"]));
                    xe_addr.Add(new XAttribute("include_phone", orow["include_phone"]));
                    xe_addr.Add(new XAttribute("include_email", orow["include_email"]));
                    xe_addr.Add(new XAttribute("default_addr", orow["default_addr"]));
                    xe_addr.Add(new XAttribute("addr_no", orow["addr_no"]));
                    xe_addr.Add(new XAttribute("active", orow["active"]));
                    xe_addrs.Add(xe_addr);

                    //UDF3
                    var xe_supps = new XElement("CUST_SUPPLS");
                    var xe_supp = new XElement("CUST_SUPPL");
                    xe_supp.Add(new XAttribute("udf_no", "3"));
                    xe_supp.Add(new XAttribute("udf_value", orow["Udf_No_3"]));
                    xe_supps.Add(xe_supp);
                    //UDF
                    xe_supp = new XElement("CUST_SUPPL");
                    xe_supp.Add(new XAttribute("udf_no", "4"));
                    xe_supp.Add(new XAttribute("udf_value", orow["Udf_No_4"]));
                    xe_supps.Add(xe_supp);
                    //UDF
                    xe_supp = new XElement("CUST_SUPPL");
                    xe_supp.Add(new XAttribute("udf_no", "5"));
                    xe_supp.Add(new XAttribute("udf_value", orow["Udf_No_5"]));
                    xe_supps.Add(xe_supp);
                    //UDF
                    xe_supp = new XElement("CUST_SUPPL");
                    xe_supp.Add(new XAttribute("udf_no", "6"));
                    xe_supp.Add(new XAttribute("udf_value", orow["Udf_No_6"]));
                    xe_supps.Add(xe_supp);
                    //UDF
                    xe_supp = new XElement("CUST_SUPPL");
                    xe_supp.Add(new XAttribute("udf_no", "7"));
                    xe_supp.Add(new XAttribute("udf_value", orow["Udf_No_7"]));
                    xe_supps.Add(xe_supp);
                    //UDF
                    xe_supp = new XElement("CUST_SUPPL");
                    xe_supp.Add(new XAttribute("udf_no", "8"));
                    xe_supp.Add(new XAttribute("udf_value", orow["Udf_No_8"]));
                    xe_supps.Add(xe_supp);

                    xe.Add(xe_addrs);
                    xe.Add(xe_supps);
                    xdata.Add(xe);
                }

                XElement xdoc = new XElement("DOCUMENT", xdata);

                xdoc.Save(_path_xml);
                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }
    }
}
