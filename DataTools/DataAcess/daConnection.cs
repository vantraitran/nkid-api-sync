﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.OracleClient;

using DataTools.Models;
using DataTools.Utility;
using SAPbobsCOM;

namespace DataTools.DataAccess
{
    public class daConnection : BaseUtil
    {
        public SqlConnection oSqlConn;
        public OracleConnection oArcConn;

        public SqlTransaction oSqlTran;
        public OracleTransaction oOracTran;

        private DataConnection dataConn;
        private string connType;
        public string message = "";

        public daConnection(DataConnection dataConn)
        {
            this.dataConn = dataConn;
            SetConnect();
        }

        public daConnection(bool isLocal)
        {
            if (isLocal)
                this.dataConn = DataConnection.CLIENTDB;
            else
                this.dataConn = DataConnection.SERVERDB;

            SetConnect();
        }

        public daConnection(string sqlConnectionString)
        {
            this.dataConn = DataConnection.NONE;
            this.connType = ConnectType.Microsoft_SQL_Server.ToString();
            oSqlConn = new SqlConnection(sqlConnectionString);
        }

        public void SetConnect()
        {
            try
            {
                if (this.dataConn == DataConnection.NONE)
                {
                    this.connType = ConnectType.Microsoft_SQL_Server.ToString();//Hiện tại đang fix vì Connection đang lưu trong file .ini chưa có thông tin này.

                    var sConn = string.Format("Data Source={0};Initial Catalog={1};User ID={2}; Password={3}", BaseConstant.SERVER
                                 , BaseConstant.DATABASE, BaseConstant.DBUSER, BaseConstant.DBPASS);
                    oSqlConn = new SqlConnection(sConn);
                }
                else
                {
                    var regconn = RegConn_Get(this.dataConn);
                    this.connType = regconn.CONNECTTYPE;
                    if (this.connType == ConnectType.Microsoft_SQL_Server.ToString())
                    {
                        var sConn = string.Format("data source={0}; Initial Catalog={1};user id={2};password={3};", regconn.SERVER, regconn.DATABASE, regconn.DBUSER, regconn.DBPASS);
                        oSqlConn = new SqlConnection(sConn);
                    }
                    else if (this.connType == ConnectType.Oracle_Database.ToString())
                    {
                        var sConn = string.Format("data source={0}; user id={1};password={2};", regconn.SERVER, regconn.DBUSER, regconn.DBPASS);
                        oArcConn = new OracleConnection(sConn);
                    }
                    else if (this.connType == ConnectType.SAP_Business_One.ToString())
                    {
                        var sConn = string.Format("data source={0}; Initial Catalog={1};user id={2};password={3};", regconn.SERVER, regconn.DATABASE, regconn.DBUSER, regconn.DBPASS);
                        oSqlConn = new SqlConnection(sConn);
                    }
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
        }

        public bool Connect(ref string message)
        {
            try
            {
                var regconn = RegConn_Get(this.dataConn);
                if (regconn.CONNECTTYPE == ConnectType.Microsoft_SQL_Server.ToString())
                {
                    var sConn = string.Format("data source={0}; Initial Catalog={1};user id={2};password={3};", regconn.SERVER, regconn.DATABASE, regconn.DBUSER, regconn.DBPASS);
                    oSqlConn = new SqlConnection(sConn);
                    if (oSqlConn.State == ConnectionState.Open)
                    {
                        oSqlConn.Close();
                    }
                    oSqlConn.Open();
                    return true;
                }

                else if (regconn.CONNECTTYPE == ConnectType.Oracle_Database.ToString())
                {
                    var sConn = string.Format("data source={0}; user id={1};password={2};", regconn.SERVER, regconn.DBUSER, regconn.DBPASS);
                    oArcConn = new OracleConnection(sConn);
                    if (oArcConn.State == ConnectionState.Open)
                    {
                        oArcConn.Close();
                    }
                    oArcConn.Open();
                    return true;
                }

                else if (regconn.CONNECTTYPE == ConnectType.SAP_Business_One.ToString())
                {

                    var oCompany = new Company();

                    oCompany.Server = regconn.SERVER;
                    oCompany.LicenseServer = regconn.LICENSENAME;
                    oCompany.DbPassword = regconn.DBPASS;
                    oCompany.CompanyDB = regconn.DATABASE;

                    if (regconn.SERVERTYPE == "MSSQL_2008")
                    {
                        oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2008;
                    }
                    else if (regconn.SERVERTYPE == "MSSQL_2012")
                    {
                        oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                    }
                    else if (regconn.SERVERTYPE == "MSSQL_2014")
                    {
                        oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2014;
                    }
                    else if (regconn.SERVERTYPE == "MSSQL_2016")
                    {
                        //oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2016;
                    }
                    else if (regconn.SERVERTYPE == "HANADB")
                    {
                        oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                    }
                    oCompany.DbUserName = regconn.DBUSER;
                    oCompany.UserName = regconn.SAPUSER;
                    oCompany.Password = regconn.SAPPASS;

                    if (!oCompany.Connected)
                    {
                        int i = oCompany.Connect();
                        if (i != 0)
                        {
                            oCompany.GetLastError(out i, out message);
                            return false;
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public DataTable GetData(string tablename, string sql)
        {

            DataTable dt = new DataTable(tablename);

            if (this.connType == ConnectType.Microsoft_SQL_Server.ToString() || this.connType == ConnectType.SAP_Business_One.ToString())
            {
                SqlDataAdapter da = null;
                if (oSqlConn.State == ConnectionState.Closed)
                {
                    oSqlConn.Open();
                }
                da = new SqlDataAdapter(sql, oSqlConn);
                da.Fill(dt);
                da.Dispose();
                da = null;

                oSqlConn.Close();
                //oSqlConn = null;
            }
            else if (this.connType == ConnectType.Oracle_Database.ToString())
            {
                OracleDataAdapter da = null;
                if (oArcConn.State == ConnectionState.Closed)
                {
                    oArcConn.Open();
                }
                da = new OracleDataAdapter(sql, oArcConn);
                da.Fill(dt);
                da.Dispose();
                da = null;

                oArcConn.Close();
                //oArcConn = null;
            }
            return dt;

        }

        public bool ExecQuery(string sql, out string message)
        {
            message = "";
            try
            {
                if (this.connType == ConnectType.Microsoft_SQL_Server.ToString())
                {
                    if (oSqlConn.State == ConnectionState.Closed)
                    {
                        oSqlConn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = oSqlConn;
                    cmd.CommandTimeout = 900;
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();

                    oSqlConn.Close();
                    //oSqlConn = null;

                    return true;
                }
                else if (this.connType == ConnectType.Oracle_Database.ToString())
                {
                    if (oArcConn.State == ConnectionState.Closed)
                    {
                        oArcConn.Open();
                    }

                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = oArcConn;
                    cmd.CommandTimeout = 900;
                    cmd.CommandText = sql;
                    cmd.ExecuteNonQuery();

                    oArcConn.Close();
                    //oArcConn = null;

                    return true;
                }

            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool ExecQueryTrans(string sql, out string message)
        {
            message = "";
            try
            {
                if (this.connType == ConnectType.Microsoft_SQL_Server.ToString())
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = oSqlConn;
                    cmd.CommandTimeout = 900;
                    cmd.CommandText = sql;
                    cmd.Transaction = oSqlTran;
                    cmd.ExecuteNonQuery();

                    return true;
                }
                else if (this.connType == ConnectType.Oracle_Database.ToString())
                {
                    OracleCommand cmd = new OracleCommand();
                    cmd.Connection = oArcConn;
                    cmd.CommandTimeout = 900;
                    cmd.CommandText = sql;
                    cmd.Transaction = oOracTran;
                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            catch (Exception ex)
            {
                message = "[ExecQueryTrans]" + ex.Message;
            }
            return false;
        }

        public bool BeginTransaction()
        {
            try
            {
                if (this.connType == ConnectType.Microsoft_SQL_Server.ToString())
                {
                    if (oSqlConn.State == ConnectionState.Closed)
                    {
                        oSqlConn.Open();
                    }
                    oSqlTran = oSqlConn.BeginTransaction();
                    return true;
                }
                else if (this.connType == ConnectType.Oracle_Database.ToString())
                {
                    if (oArcConn.State == ConnectionState.Closed)
                    {
                        oArcConn.Open();
                    }
                    oOracTran = oArcConn.BeginTransaction();
                    return true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool CommitTransaction()
        {
            try
            {
                if (this.connType == ConnectType.Microsoft_SQL_Server.ToString())
                {
                    if (oSqlTran != null)
                    {
                        oSqlTran.Commit();
                        oSqlTran.Dispose();
                        oSqlTran = null;

                        oSqlConn.Close();
                        //oSqlConn = null;
                    }

                    return true;
                }
                else if (this.connType == ConnectType.Oracle_Database.ToString())
                {
                    if (oOracTran != null)
                    {
                        oOracTran.Commit();
                        oOracTran.Dispose();
                        oOracTran = null;

                        oArcConn.Close();
                        //oArcConn = null;
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool RollbackTransaction()
        {
            try
            {
                if (this.connType == ConnectType.Microsoft_SQL_Server.ToString())
                {
                    if (oSqlTran != null)
                    {
                        oSqlTran.Rollback();
                        oSqlTran.Dispose();
                        oSqlTran = null;

                        oSqlConn.Close();
                        //oSqlConn = null;
                    }

                    return true;
                }
                else if (this.connType == ConnectType.Oracle_Database.ToString())
                {
                    if (oOracTran != null)
                    {
                        oOracTran.Rollback();
                        oOracTran.Dispose();
                        oOracTran = null;

                        oArcConn.Close();
                        //oArcConn = null;
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public DateTime dbDate()
        {
            try
            {
                DataTable dt = new DataTable("GetDate");

                if (this.connType == ConnectType.Microsoft_SQL_Server.ToString())
                {
                    SqlDataAdapter da = null;
                    if (oSqlConn.State == ConnectionState.Closed)
                    {
                        oSqlConn.Open();
                    }
                    da = new SqlDataAdapter("SELECT getdate() FROM DUAL;", oSqlConn);
                    da.Fill(dt);
                    da.Dispose();
                    da = null;

                    oSqlConn.Close();
                }
                else if (this.connType == ConnectType.Oracle_Database.ToString())
                {
                    OracleDataAdapter da = null;
                    if (oArcConn.State == ConnectionState.Closed)
                    {
                        oArcConn.Open();
                    }
                    da = new OracleDataAdapter("SELECT sysdate FROM DUAL", oArcConn);
                    da.Fill(dt);
                    da.Dispose();
                    da = null;

                    oArcConn.Close();
                }

                if (dt != null && dt.Rows.Count > 0)
                    return Convert.ToDateTime(dt.Rows[0][0]);
            }
            catch (Exception)
            {

            }
            return DateTime.Now;

        }
    }
}
