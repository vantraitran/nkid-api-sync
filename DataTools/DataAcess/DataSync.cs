﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Configuration;
using System.Reflection;

using DataTools.DataAccess;
using DataTools.Models;
using DataTools.Utility;


namespace DataTools.DataAccess.Sync
{
    public class DataSync : SyncBase
    {
        dbHub _db = null;
        SettingSync _sourcexml = null;

        public DataSync()
        {
            _db = new dbHub(Connection_String(DataConnection.CLIENTDB));
            _sourcexml = new SettingSync();
        }

        #region OUTDATA
        private string GenerateSyncOut(string lastedSyncDate, string machineCode, string storeCode, string sourceCode, out string message)
        {
            try
            {
                message = "";
                string structSource = "";
                //var syncSource = _sourcedata.SyncSourceGet(sourceCode);       //FOR DATA SQL
                var syncSource = _sourcexml.SyncSource_Get_ByKey(sourceCode, out message);         //FOR DATA XML

                if (syncSource == null)
                {
                    message = getFunctionError("GenerateSyncOut", BaseUtil.SyncOutInvalidSource);
                    return "";
                }
                else
                {
                    //string lastedSyncDate = "1989-09-27";
                    //if (syncSource.LastedSyncDate.HasValue)
                    //    lastedSyncDate = syncSource.LastedSyncDate.Value.ToString(_formatdate_ymd);

                    //Get list Columns to select
                    //var syncProperties = _sourcedata.SyncPropertyGet(syncSource.SourceCode);        //FOR DATA SQL
                    var syncProperties = _sourcexml.SyncProperty_Get_Active(syncSource.SourceCode, out message);         //FOR DATA XML
                    if (syncProperties == null || syncProperties.Count == 0)
                    {
                        message = getFunctionError("GenerateSyncOut", BaseUtil.NoPropertyinSource);
                        return "";
                    }

                    var listColString = "";
                    foreach (var col in syncProperties)
                    {
                        listColString = listColString + string.Format(",{0} as \"{1}\" ", col.ColumnOUT, col.ColumnIN);
                    }
                    if (listColString.Length > 0)
                        listColString = listColString.Substring(1);

                    //Get list conditions
                    var conditionString = "";

                    //var syncConditions = _sourcedata.SyncConditionGet(syncSource.SourceCode);     //FOR DATA SQL
                    var syncConditions = _sourcexml.SyncCondition_Get_Active(syncSource.SourceCode, out message);        //FOR DATA XML

                    if (syncConditions == null || syncConditions.Count == 0)
                    {
                        message = "";
                        conditionString = "";
                    }
                    else
                    {
                        foreach (var col in syncConditions)
                        {
                            string tempCond = "";

                            tempCond = tempCond + GeneateCondition(col.ConditionType, col.SourceColumn, machineCode, storeCode, lastedSyncDate, col.ConditionValue);
                            tempCond = tempCond + GeneateCondition(col.ConditionType1, col.SourceColumn, machineCode, storeCode, lastedSyncDate, col.ConditionValue1);
                            tempCond = tempCond + GeneateCondition(col.ConditionType2, col.SourceColumn, machineCode, storeCode, lastedSyncDate, col.ConditionValue2);
                            tempCond = tempCond + GeneateCondition(col.ConditionType3, col.SourceColumn, machineCode, storeCode, lastedSyncDate, col.ConditionValue3);
                            tempCond = tempCond + GeneateCondition(col.ConditionType4, col.SourceColumn, machineCode, storeCode, lastedSyncDate, col.ConditionValue4);
                            tempCond = tempCond + GeneateCondition(col.ConditionType5, col.SourceColumn, machineCode, storeCode, lastedSyncDate, col.ConditionValue5);
                            tempCond = tempCond + GeneateCondition(col.ConditionType6, col.SourceColumn, machineCode, storeCode, lastedSyncDate, col.ConditionValue6);
                            tempCond = tempCond + GeneateCondition(col.ConditionType7, col.SourceColumn, machineCode, storeCode, lastedSyncDate, col.ConditionValue7);

                            //each line is conditon AND
                            //each column from 1 to 7 is condition OR
                            if (tempCond.Length > 3)
                                tempCond = tempCond.Substring(3);
                            conditionString = conditionString + string.Format(" and ({0})", tempCond);
                        }
                        if (conditionString.Length > 7)
                            conditionString = "where " + conditionString.Trim().Substring(3);
                        else
                            conditionString = "";
                    }

                    //final query
                    structSource = NewLine(structSource + string.Format("select {0} from {1} {2}", listColString, syncSource.SourceOUT, conditionString));
                }

                //for ORACLE
                structSource = structSource.ToUpper();

                return structSource;
            }
            catch (Exception ex)
            {
                message = getFunctionError("GenerateSyncOut", ex.Message);
                return null;
            }
        }

        private string GeneateCondition(string ConditionType, string SourceColumn, string machineCode, string storeCode, string lastedSyncDate, string ConditionValue)
        {
            string tempCond = "";
            if (ConditionType != null && ConditionType.Trim() != "" && ConditionType != ((int)SyncConditionType.None).ToString())
            {
                if (ConditionType == ((int)SyncConditionType.MachineCode).ToString())
                    tempCond = tempCond + string.Format("or {0} = N'{1}' ", SourceColumn, machineCode);
                else if (ConditionType == ((int)SyncConditionType.StoreCode).ToString())
                    tempCond = tempCond + string.Format("or {0} = N'{1}' ", SourceColumn, storeCode);
                else if (ConditionType == ((int)SyncConditionType.GetDate).ToString())
                    tempCond = tempCond + string.Format("or {0} = '{1}' ", SourceColumn, DateTime.Now.ToString("dd-MMM-yyyy"));
                else if (ConditionType == ((int)SyncConditionType.IsNull).ToString())
                    tempCond = tempCond + string.Format("or {0} is null ", SourceColumn);
                else if (ConditionType == ((int)SyncConditionType.IsNotNull).ToString())
                    tempCond = tempCond + string.Format("or {0} is not null ", SourceColumn);
                else if (ConditionType == ((int)SyncConditionType.LastedSyncDate).ToString())
                    tempCond = tempCond + string.Format("or {0} >= '{1}' ", SourceColumn, lastedSyncDate);
                else if (ConditionType == ((int)SyncConditionType.Equal).ToString())
                    tempCond = tempCond + string.Format("or {0} = N'{1}' ", SourceColumn, SqlReplaceSpecial(ConditionValue));
                else if (ConditionType == ((int)SyncConditionType.NotEqual).ToString())
                    tempCond = tempCond + string.Format("or {0} <> N'{1}' ", SourceColumn, SqlReplaceSpecial(ConditionValue));
                else if (ConditionType == ((int)SyncConditionType.BeginsWith).ToString())
                    tempCond = tempCond + string.Format("or {0} like N'{1}%' ", SourceColumn, SqlReplaceSpecial(ConditionValue));
                else if (ConditionType == ((int)SyncConditionType.EndsWith).ToString())
                    tempCond = tempCond + string.Format("or {0} like N'%{1}' ", SourceColumn, SqlReplaceSpecial(ConditionValue));
                else if (ConditionType == ((int)SyncConditionType.Contains).ToString())
                    tempCond = tempCond + string.Format("or {0} like N'%{1}%' ", SourceColumn, SqlReplaceSpecial(ConditionValue));
                else if (ConditionType == ((int)SyncConditionType.NotContains).ToString())
                    tempCond = tempCond + string.Format("or {0} not like N'%{1}%' ", SourceColumn, SqlReplaceSpecial(ConditionValue));
                else if (ConditionType == ((int)SyncConditionType.LessThan).ToString())
                    tempCond = tempCond + string.Format("or {0} < '{1}' ", SourceColumn, SqlReplaceSpecial(ConditionValue));
                else if (ConditionType == ((int)SyncConditionType.LessThanOrEqualTo).ToString())
                    tempCond = tempCond + string.Format("or {0} <= '{1}' ", SourceColumn, SqlReplaceSpecial(ConditionValue));
                else if (ConditionType == ((int)SyncConditionType.GreaterThan).ToString())
                    tempCond = tempCond + string.Format("or {0} > '{1}' ", SourceColumn, SqlReplaceSpecial(ConditionValue));
                else if (ConditionType == ((int)SyncConditionType.GreaterThanOrEqualTo).ToString())
                    tempCond = tempCond + string.Format("or {0} >= '{1}' ", SourceColumn, SqlReplaceSpecial(ConditionValue));
                else if (ConditionType == ((int)SyncConditionType.In).ToString())
                    tempCond = tempCond + string.Format("or {0} in ({1}) ", SourceColumn, ConditionValue);
                else if (ConditionType == ((int)SyncConditionType.NotIn).ToString())
                    tempCond = tempCond + string.Format("or {0} not in ({1}) ", SourceColumn, ConditionValue);

            }

            return tempCond;
        }

        public DataTable ExcuteDataSyncOut(string lastedSyncDate, string machineCode, string storeCode, string sourceCode, bool isLocal, string userid, SyncObjectCurrent obj, out string message)
        {
            try
            {
                message = "";
                string sql = "";

                if (obj.ObjectType != null && obj.ObjectType != "")
                    sql = obj.ObjectQuery;
                else
                    sql = GenerateSyncOut(lastedSyncDate, obj.MachineCode, storeCode, sourceCode, out message);

                if (sql != null && sql != "" && !checkHasError(message))
                {
                    obj.SqlScriptOut = sql;
                    var daConn = new daConnection(isLocal);
                    var dt = daConn.GetData(sourceCode, sql);
                    return dt;
                }

                return null;
            }
            catch (Exception ex)
            {
                message = getFunctionError("ExcuteDataSyncOut", ex.Message);
                return null;
            }
        }

        public List<SyncDataObjectLine> ExcuteDataLinesSyncOut(string lastedSyncDate, string machineCode, string storeCode, string sourceCode, bool isLocal, SyncObjectCurrent obj, out string message)
        {
            try
            {
                message = "";


                List<SyncDataObjectLine> listData = new List<SyncDataObjectLine>();
                //var syncSourceLevel = _sourcedata.SyncSourceLevelGet(sourceCode);         //FOR DATA SQL
                var syncSourceLevel = _sourcexml.SyncSourceLevel_Get_Active(sourceCode, out message);         //FOR DATA XML
                foreach (var source in syncSourceLevel)
                {
                    var sourceLine = _sourcexml.SyncSource_Get_ByKey(source.SourceLine, out message);         //GET SOURCE LINE
                    string sql = "";

                    if (sourceLine.ObjectType != null && sourceLine.ObjectType != "")
                        sql = sourceLine.ObjectQuery;
                    else
                        sql = GenerateSyncOut(lastedSyncDate, machineCode, storeCode, source.SourceLine, out message);

                    if (sql != null && sql != "" && !checkHasError(message))
                    {
                        var daConn = new daConnection(isLocal);
                        var dt = daConn.GetData(source.SourceLine, sql);
                        listData.Add(new SyncDataObjectLine() { DataSource = dt, HeaderKey = source.HeaderKey, LineRefenceKey = source.LineReferenceKey, SourceCode = source.SourceLine, XmlElement = source.XmlElement, SourceName = sourceLine.SourceName, ObjectQuery = sourceLine.ObjectQuery });
                    }
                }
                return listData;
            }
            catch (Exception ex)
            {
                message = getFunctionError("ExcuteDataLinesSyncOut", ex.Message);
                return null;
            }
        }
        #endregion

        #region INDATA
        private string GenerateSyncIn(string sourceCode, DataTable dataSource, out string message)
        {
            try
            {
                message = "";
                if (dataSource == null || dataSource.Rows.Count == 0)
                {
                    message = getFunctionError("GenerateSyncIn", BaseUtil.GetDataEmpty);
                    return "";
                }

                StringBuilder temp = new StringBuilder();

                //string structSource = "";
                //var syncSource =_sourcedata. SyncSourceGet(sourceCode);         //FOR DATA SQL
                var syncSource = _sourcexml.SyncSource_Get_ByKey(sourceCode, out message);          //FOR DATA XML

                if (syncSource == null)
                {
                    message = getFunctionError("GenerateSyncIn", BaseUtil.SyncInInvalidSource);
                    return "";
                }
                else
                {
                    //var syncProperties =_sourcedata. SyncPropertyGet(syncSource.SourceCode);         //FOR DATA SQL
                    var syncProperties = _sourcexml.SyncProperty_Get_Active(syncSource.SourceCode, out message);         //FOR DATA XML
                    if (syncProperties == null || syncProperties.Count == 0)
                    {
                        message = getFunctionError("GenerateSyncIn", BaseUtil.NoPropertyinSource);
                        return "";
                    }


                    //structSource = "";
                    temp.Append("");
                    if (syncSource.IsOnlyInsert)
                    {
                        foreach (DataRow row in dataSource.Rows)
                        {
                            //Generate column insert
                            var listColString = "";
                            foreach (var col in syncProperties)
                            {
                                listColString = listColString + string.Format(",{0} ", col.ColumnIN);
                            }
                            if (listColString.Length > 0)
                                listColString = listColString.Substring(1);

                            //Generate values insert
                            var listValueString = "";
                            foreach (var col in syncProperties)
                            {
                                var value = row[col.ColumnIN].ToString();
                                value = SetValueSql(value, col.NumericType, col.DateTimeType, col.BooleanType);
                                listValueString = listValueString + string.Format(",{0} ", value);
                            }
                            listValueString = listValueString.Substring(1);

                            temp.Append(NewLine(string.Format("insert into {0}({1})", syncSource.SourceIN, listColString)));
                            temp.Append(NewLine(string.Format("values ({0})", listValueString)));
                        }
                    }
                    else if (syncSource.IsDeleteBefore)
                    {
                        temp.Append(NewLine(string.Format("delete from {0} ", syncSource.SourceIN)));
                        foreach (DataRow row in dataSource.Rows)
                        {
                            //Generate column insert
                            var listColString = "";
                            foreach (var col in syncProperties)
                            {
                                listColString = listColString + string.Format(",{0} ", col.ColumnIN);
                            }
                            if (listColString.Length > 0)
                                listColString = listColString.Substring(1);

                            //Generate values insert
                            var listValueString = "";
                            foreach (var col in syncProperties)
                            {
                                var value = row[col.ColumnIN].ToString();
                                value = SetValueSql(value, col.NumericType, col.DateTimeType, col.BooleanType);
                                listValueString = listValueString + string.Format(",{0} ", value);
                            }
                            listValueString = listValueString.Substring(1);

                            temp.Append(NewLine(string.Format("insert into {0}({1})", syncSource.SourceIN, listColString)));
                            temp.Append(NewLine(string.Format("values ({0})", listValueString)));
                        }
                    }
                    else
                    {
                        var propertyKey = syncProperties.Where(o => o.IsKey).ToList();
                        var propertyNoKey = syncProperties.Where(o => o.IsKey == false).ToList();
                        if (propertyKey != null && propertyKey.Count > 0)
                        {
                            foreach (DataRow row in dataSource.Rows)
                            {
                                //Generate condition (where column is primary key)
                                var keyString = "";
                                foreach (var col in propertyKey)
                                {
                                    keyString = keyString + string.Format("and {0}={1} ", col.ColumnIN, SetValueSql(row[col.ColumnIN].ToString(), col.NumericType, col.DateTimeType, col.BooleanType));
                                }
                                if (keyString.Length > 0)
                                    keyString = keyString.Substring(3);

                                //Generate update column (where column is no primary key)
                                var keyUpdateString = "";
                                foreach (var col in propertyNoKey)
                                {
                                    keyUpdateString = keyUpdateString + string.Format(",{0}={1} ", col.ColumnIN, SetValueSql(row[col.ColumnIN].ToString(), col.NumericType, col.DateTimeType, col.BooleanType));
                                }
                                if (keyUpdateString.Length > 0)
                                    keyUpdateString = keyUpdateString.Substring(1);

                                //Generate column insert
                                var listColString = "";
                                foreach (var col in syncProperties)
                                {
                                    listColString = listColString + string.Format(",{0} ", col.ColumnIN);
                                }
                                if (listColString.Length > 0)
                                    listColString = listColString.Substring(1);

                                //Generate values insert
                                var listValueString = "";
                                foreach (var col in syncProperties)
                                {
                                    var value = row[col.ColumnIN].ToString();
                                    value = SetValueSql(value, col.NumericType, col.DateTimeType, col.BooleanType);
                                    listValueString = listValueString + string.Format(",{0} ", value);
                                }
                                listValueString = listValueString.Substring(1);

                                temp.Append(NewLine(string.Format("if exists (select 1 from {0} where {1})", syncSource.SourceIN, keyString)));
                                temp.Append(NewLine(string.Format("begin")));
                                temp.Append(NewLine(string.Format("update {0} ", syncSource.SourceIN)));
                                temp.Append(NewLine(string.Format("set {0}", keyUpdateString)));
                                temp.Append(NewLine(string.Format("where {0}", keyString)));
                                temp.Append(NewLine(string.Format("end")));
                                temp.Append(NewLine(string.Format("else")));
                                temp.Append(NewLine(string.Format("begin")));
                                temp.Append(NewLine(string.Format("insert into {0}({1})", syncSource.SourceIN, listColString)));
                                temp.Append(NewLine(string.Format("values ({0})", listValueString)));
                                temp.Append(NewLine(string.Format("end")));
                            }
                        }
                    }
                }

                return temp.ToString();
            }
            catch (Exception ex)
            {
                message = getFunctionError("GenerateSyncIn", ex.Message);
                return null;
            }
        }

        private string GenerateLineSyncIn(string sourceCode, DataRow lineSource, out string message)
        {
            try
            {
                message = "";

                string structSource = "";
                //var syncSource =_sourcedata. SyncSourceGet(sourceCode);         //FOR DATA SQL
                var syncSource = _sourcexml.SyncSource_Get_ByKey(sourceCode, out message);         //FOR DATA XML
                if (syncSource == null)
                {
                    message = getFunctionError("GenerateLineSyncIn", BaseUtil.SyncInInvalidSource);
                    return "";
                }
                else
                {
                    //var syncProperties =_sourcedata. SyncPropertyGet(syncSource.SourceCode);         //FOR DATA SQL
                    var syncProperties = _sourcexml.SyncProperty_Get_Active(syncSource.SourceCode, out message);         //FOR DATA XML
                    if (syncProperties == null || syncProperties.Count == 0)
                    {
                        message = getFunctionError("GenerateLineSyncIn", BaseUtil.NoPropertyinSource);
                        return "";
                    }

                    structSource = "";
                    if (syncSource.IsOnlyInsert)
                    {

                        //Generate column insert
                        var listColString = "";
                        foreach (var col in syncProperties)
                        {
                            listColString = listColString + string.Format(",{0} ", col.ColumnIN);
                        }
                        if (listColString.Length > 0)
                            listColString = listColString.Substring(1);

                        //Generate values insert
                        var listValueString = "";
                        foreach (var col in syncProperties)
                        {
                            var value = lineSource[col.ColumnIN].ToString();
                            value = SetValueSql(value, col.NumericType, col.DateTimeType, col.BooleanType);
                            listValueString = listValueString + string.Format(",{0} ", value);
                        }
                        listValueString = listValueString.Substring(1);

                        structSource = structSource + NewLine(string.Format("insert into {0}({1})", syncSource.SourceIN, listColString));
                        structSource = structSource + NewLine(string.Format("values ({0})", listValueString));

                    }
                    else if (syncSource.IsDeleteBefore)
                    {
                        //Generate column insert
                        var listColString = "";
                        foreach (var col in syncProperties)
                        {
                            listColString = listColString + string.Format(",{0} ", col.ColumnIN);
                        }
                        if (listColString.Length > 0)
                            listColString = listColString.Substring(1);

                        //Generate values insert
                        var listValueString = "";
                        foreach (var col in syncProperties)
                        {
                            var value = lineSource[col.ColumnIN].ToString();
                            value = SetValueSql(value, col.NumericType, col.DateTimeType, col.BooleanType);
                            listValueString = listValueString + string.Format(",{0} ", value);
                        }
                        listValueString = listValueString.Substring(1);

                        structSource = structSource + NewLine(string.Format("insert into {0}({1})", syncSource.SourceIN, listColString));
                        structSource = structSource + NewLine(string.Format("values ({0})", listValueString));

                    }
                    else
                    {
                        var propertyKey = syncProperties.Where(o => o.IsKey).ToList();
                        var propertyNoKey = syncProperties.Where(o => o.IsKey == false).ToList();
                        if (propertyKey != null && propertyKey.Count > 0)
                        {

                            //Generate condition (where column is primary key)
                            var keyString = "";
                            foreach (var col in propertyKey)
                            {
                                keyString = keyString + string.Format("and {0}={1} ", col.ColumnIN, SetValueSql(lineSource[col.ColumnIN].ToString(), col.NumericType, col.DateTimeType, col.BooleanType));
                            }
                            if (keyString.Length > 0)
                                keyString = keyString.Substring(3);

                            //Generate update column (where column is no primary key)
                            var keyUpdateString = "";
                            foreach (var col in propertyNoKey)
                            {
                                keyUpdateString = keyUpdateString + string.Format(",{0}={1} ", col.ColumnIN, SetValueSql(lineSource[col.ColumnIN].ToString(), col.NumericType, col.DateTimeType, col.BooleanType));
                            }
                            if (keyUpdateString.Length > 0)
                                keyUpdateString = keyUpdateString.Substring(1);

                            //Generate column insert
                            var listColString = "";
                            foreach (var col in syncProperties)
                            {
                                listColString = listColString + string.Format(",{0} ", col.ColumnIN);
                            }
                            if (listColString.Length > 0)
                                listColString = listColString.Substring(1);

                            //Generate values insert
                            var listValueString = "";
                            foreach (var col in syncProperties)
                            {
                                var value = lineSource[col.ColumnIN].ToString();
                                value = SetValueSql(value, col.NumericType, col.DateTimeType, col.BooleanType);
                                listValueString = listValueString + string.Format(",{0} ", value);
                            }
                            listValueString = listValueString.Substring(1);

                            //
                            structSource = structSource + NewLine(string.Format("if exists (select 1 from {0} where {1})", syncSource.SourceIN, keyString));
                            structSource = structSource + NewLine(string.Format("begin"));
                            structSource = structSource + NewLine(string.Format("update {0} ", syncSource.SourceIN));
                            structSource = structSource + NewLine(string.Format("set {0}", keyUpdateString));
                            structSource = structSource + NewLine(string.Format("where {0}", keyString));
                            structSource = structSource + NewLine(string.Format("end"));
                            structSource = structSource + NewLine(string.Format("else"));
                            structSource = structSource + NewLine(string.Format("begin"));
                            structSource = structSource + NewLine(string.Format("insert into {0}({1})", syncSource.SourceIN, listColString));
                            structSource = structSource + NewLine(string.Format("values ({0})", listValueString));
                            structSource = structSource + NewLine(string.Format("end"));
                        }
                    }
                }

                return structSource;
            }
            catch (Exception ex)
            {
                message = getFunctionError("GenerateLineSyncIn", ex.Message);
                return null;
            }
        }

        private string GenerateSyncIn_bak(string sourceCode, DataTable dataSource, out string message)
        {
            try
            {
                message = "";
                if (dataSource == null || dataSource.Rows.Count == 0)
                {
                    message = getFunctionError("_GenerateSyncIn2", BaseUtil.GetDataEmpty);
                    return "";
                }
                var n = dataSource.Rows.Count;

                for (var j = n - 1; j >= n / 2; j--)
                {
                    dataSource.Rows.RemoveAt(j);
                }

                StringBuilder temp = new StringBuilder();

                //string structSource = "";
                //var syncSource =_sourcedata. SyncSourceGet(sourceCode);         //FOR DATA SQL
                var syncSource = _sourcexml.SyncSource_Get_ByKey(sourceCode, out message);         //FOR DATA XML

                if (syncSource != null)
                {
                    //var syncProperties =_sourcedata. SyncPropertyGet(syncSource.SourceCode);         //FOR DATA SQL
                    var syncProperties = _sourcexml.SyncProperty_Get_Active(syncSource.SourceCode, out message);         //FOR DATA XML
                    if (syncProperties == null || syncProperties.Count == 0)
                    {
                        message = getFunctionError("_GenerateSyncIn2", BaseUtil.NoPropertyinSource);
                        return "";
                    }

                    temp.Append("");
                    if (syncSource.IsOnlyInsert)
                    {
                        foreach (DataRow row in dataSource.Rows)
                        {
                            //Generate column insert
                            var listColString = "";
                            foreach (var col in syncProperties)
                            {
                                listColString = listColString + string.Format(",{0} ", col.ColumnIN);
                            }
                            if (listColString.Length > 0)
                                listColString = listColString.Substring(1);

                            //Generate values insert
                            var listValueString = "";
                            foreach (var col in syncProperties)
                            {
                                var value = row[col.ColumnIN].ToString();
                                value = SetValueSql(value, col.NumericType, col.DateTimeType, col.BooleanType);
                                listValueString = listValueString + string.Format(",{0} ", value);
                            }
                            listValueString = listValueString.Substring(1);

                            temp.Append(NewLine(string.Format("insert into {0}({1})", syncSource.SourceIN, listColString)));
                            temp.Append(NewLine(string.Format("values ({0})", listValueString)));
                        }
                    }
                    else if (syncSource.IsDeleteBefore)
                    {
                        temp.Append(NewLine(string.Format("delete from {0} ", syncSource.SourceIN)));
                        foreach (DataRow row in dataSource.Rows)
                        {
                            //Generate column insert
                            var listColString = "";
                            foreach (var col in syncProperties)
                            {
                                listColString = listColString + string.Format(",{0} ", col.ColumnIN);
                            }
                            if (listColString.Length > 0)
                                listColString = listColString.Substring(1);

                            //Generate values insert
                            var listValueString = "";
                            foreach (var col in syncProperties)
                            {
                                var value = row[col.ColumnIN].ToString();
                                value = SetValueSql(value, col.NumericType, col.DateTimeType, col.BooleanType);
                                listValueString = listValueString + string.Format(",{0} ", value);
                            }
                            listValueString = listValueString.Substring(1);

                            temp.Append(NewLine(string.Format("insert into {0}({1})", syncSource.SourceIN, listColString)));
                            temp.Append(NewLine(string.Format("values ({0})", listValueString)));
                        }
                    }
                    else
                    {
                        var propertyKey = syncProperties.Where(o => o.IsKey).ToList();
                        var propertyNoKey = syncProperties.Where(o => o.IsKey == false).ToList();
                        if (propertyKey != null && propertyKey.Count > 0)
                        {
                            foreach (DataRow row in dataSource.Rows)
                            {
                                //Generate condition (where column is primary key)
                                var keyString = "";
                                foreach (var col in propertyKey)
                                {
                                    keyString = keyString + string.Format("and {0}={1} ", col.ColumnIN, SetValueSql(row[col.ColumnIN].ToString(), col.NumericType, col.DateTimeType, col.BooleanType));
                                }
                                if (keyString.Length > 0)
                                    keyString = keyString.Substring(3);

                                //Generate update column (where column is no primary key)
                                var keyUpdateString = "";
                                foreach (var col in propertyNoKey)
                                {
                                    keyUpdateString = keyUpdateString + string.Format(",{0}={1} ", col.ColumnIN, SetValueSql(row[col.ColumnIN].ToString(), col.NumericType, col.DateTimeType, col.BooleanType));
                                }
                                if (keyUpdateString.Length > 0)
                                    keyUpdateString = keyUpdateString.Substring(1);

                                //Generate column insert
                                var listColString = "";
                                foreach (var col in syncProperties)
                                {
                                    listColString = listColString + string.Format(",{0} ", col.ColumnIN);
                                }
                                if (listColString.Length > 0)
                                    listColString = listColString.Substring(1);

                                //Generate values insert
                                var listValueString = "";
                                foreach (var col in syncProperties)
                                {
                                    var value = row[col.ColumnIN].ToString();
                                    value = SetValueSql(value, col.NumericType, col.DateTimeType, col.BooleanType);
                                    listValueString = listValueString + string.Format(",{0} ", value);
                                }
                                listValueString = listValueString.Substring(1);

                                temp.Append(NewLine(string.Format("if exists (select 1 from {0} where {1})", syncSource.SourceIN, keyString)));
                                temp.Append(NewLine(string.Format("begin")));
                                temp.Append(NewLine(string.Format("update {0} ", syncSource.SourceIN)));
                                temp.Append(NewLine(string.Format("set {0}", keyUpdateString)));
                                temp.Append(NewLine(string.Format("where {0}", keyString)));
                                temp.Append(NewLine(string.Format("end")));
                                temp.Append(NewLine(string.Format("else")));
                                temp.Append(NewLine(string.Format("begin")));
                                temp.Append(NewLine(string.Format("insert into {0}({1})", syncSource.SourceIN, listColString)));
                                temp.Append(NewLine(string.Format("values ({0})", listValueString)));
                                temp.Append(NewLine(string.Format("end")));
                            }
                        }
                    }
                }

                return temp.ToString();
            }
            catch (Exception ex)
            {
                message = getFunctionError("GenerateSyncIn-0", ex.Message);
                return null;
            }
        }

        private string GenerateSyncIn(string sourceCode, DataRow[] rowSource, int index, out string message)
        {
            try
            {
                message = "";
                if (rowSource == null || rowSource.Length == 0)
                {
                    message = getFunctionError("GenerateSyncIn", BaseUtil.GetDataEmpty);
                    return "";
                }

                StringBuilder temp = new StringBuilder();

                //var syncSource =_sourcedata. SyncSourceGet(sourceCode);         //FOR DATA SQL
                var syncSource = _sourcexml.SyncSource_Get_ByKey(sourceCode, out message);         //FOR DATA XML

                if (syncSource != null)
                {
                    //var syncProperties =_sourcedata. SyncPropertyGet(syncSource.SourceCode);         //FOR DATA SQL
                    var syncProperties = _sourcexml.SyncProperty_Get_Active(syncSource.SourceCode, out message);         //FOR DATA XML
                    if (syncProperties == null || syncProperties.Count == 0)
                    {
                        message = getFunctionError("GenerateSyncIn", BaseUtil.NoPropertyinSource);
                        return "";
                    }

                    temp.Append("");
                    if (syncSource.IsOnlyInsert)
                    {
                        foreach (DataRow row in rowSource)
                        {
                            var listColString = "";
                            var listValueString = "";
                            foreach (var col in syncProperties)
                            {
                                //Generate column insert
                                listColString = listColString + string.Format(",{0} ", col.ColumnIN);

                                //Generate values insert
                                var value = row[col.ColumnIN].ToString();
                                value = SetValueSql(value, col.NumericType, col.DateTimeType, col.BooleanType);
                                listValueString = listValueString + string.Format(",{0} ", value);
                            }
                            if (listColString.Length > 0)
                                listColString = listColString.Substring(1);
                            if (listValueString.Length > 0)
                                listValueString = listValueString.Substring(1);

                            temp.Append(NewLine(string.Format("insert into {0}({1})", syncSource.SourceIN, listColString)));
                            temp.Append(NewLine(string.Format("values ({0})", listValueString)));
                        }
                    }
                    else if (syncSource.IsDeleteBefore)
                    {
                        if (index == 0)
                            temp.Append(NewLine(string.Format("delete from {0} ", syncSource.SourceIN)));

                        foreach (DataRow row in rowSource)
                        {
                            var listColString = "";
                            var listValueString = "";
                            foreach (var col in syncProperties)
                            {
                                //Generate column insert
                                listColString = listColString + string.Format(",{0} ", col.ColumnIN);

                                //Generate values insert
                                var value = row[col.ColumnIN].ToString();
                                value = SetValueSql(value, col.NumericType, col.DateTimeType, col.BooleanType);
                                listValueString = listValueString + string.Format(",{0} ", value);
                            }
                            if (listColString.Length > 0)
                                listColString = listColString.Substring(1);

                            if (listValueString.Length > 0)
                                listValueString = listValueString.Substring(1);

                            temp.Append(NewLine(string.Format("insert into {0}({1})", syncSource.SourceIN, listColString)));
                            temp.Append(NewLine(string.Format("values ({0})", listValueString)));
                        }
                    }
                    else
                    {
                        var propertyKey = syncProperties.Where(o => o.IsKey).ToList();
                        var propertyNoKey = syncProperties.Where(o => o.IsKey == false).ToList();
                        if (propertyKey != null && propertyKey.Count > 0)
                        {
                            foreach (DataRow row in rowSource)
                            {
                                //Generate condition (where column is primary key)
                                var keyString = "";
                                foreach (var col in propertyKey)
                                {
                                    keyString = keyString + string.Format("and {0}={1} ", col.ColumnIN, SetValueSql(row[col.ColumnIN].ToString(), col.NumericType, col.DateTimeType, col.BooleanType));
                                }
                                if (keyString.Length > 0)
                                    keyString = keyString.Substring(3);

                                //Generate update column (where column is no primary key)
                                var keyUpdateString = "";
                                foreach (var col in propertyNoKey)
                                {
                                    keyUpdateString = keyUpdateString + string.Format(",{0}={1} ", col.ColumnIN, SetValueSql(row[col.ColumnIN].ToString(), col.NumericType, col.DateTimeType, col.BooleanType));
                                }
                                if (keyUpdateString.Length > 0)
                                    keyUpdateString = keyUpdateString.Substring(1);


                                //Generate values insert
                                var listColString = "";
                                var listValueString = "";
                                foreach (var col in syncProperties)
                                {
                                    //Generate column insert
                                    listColString = listColString + string.Format(",{0} ", col.ColumnIN);

                                    var value = row[col.ColumnIN].ToString();
                                    value = SetValueSql(value, col.NumericType, col.DateTimeType, col.BooleanType);
                                    listValueString = listValueString + string.Format(",{0} ", value);
                                }

                                if (listColString.Length > 0)
                                    listColString = listColString.Substring(1);

                                if (listValueString.Length > 0)
                                    listValueString = listValueString.Substring(1);

                                temp.Append(NewLine(string.Format("if exists (select 1 from {0} where {1})", syncSource.SourceIN, keyString)));
                                temp.Append(NewLine(string.Format("begin")));
                                temp.Append(NewLine(string.Format("update {0} ", syncSource.SourceIN)));
                                temp.Append(NewLine(string.Format("set {0}", keyUpdateString)));
                                temp.Append(NewLine(string.Format("where {0}", keyString)));
                                temp.Append(NewLine(string.Format("end")));
                                temp.Append(NewLine(string.Format("else")));
                                temp.Append(NewLine(string.Format("begin")));
                                temp.Append(NewLine(string.Format("insert into {0}({1})", syncSource.SourceIN, listColString)));
                                temp.Append(NewLine(string.Format("values ({0})", listValueString)));
                                temp.Append(NewLine(string.Format("end")));
                            }
                        }
                        else
                        {
                            message = getFunctionError("GenerateSyncIn", "Not found any Primary Key on this source.");
                            return null;
                        }
                    }
                }

                return temp.ToString();
            }
            catch (Exception ex)
            {
                message = getFunctionError("GenerateSyncIn", ex.Message);
                return null;
            }
        }

        public bool ExcuteDataSyncIn(string sourceCode, string storecode, DataTable dataSource, List<SyncDataObjectLine> objLines, bool isLocal, string userid, out string message)
        {
            var daConn = new daConnection(isLocal);
            try
            {
                message = "";



                daConn.BeginTransaction();

                //Excute data từng phần-->Vì tránh bộ nhớ không đủ
                int rowtimes = 2500;

                //header
                int count = dataSource.Rows.Count;
                int index = (count / rowtimes) + 1;

                for (int n = 0; n < index; n++)
                {
                    List<DataRow> listRow = new List<DataRow>();
                    for (int m = n * rowtimes; m < (n + 1) * rowtimes; m++)
                    {
                        if (m < count)
                            listRow.Add(dataSource.Rows[m]);
                        else
                            break;
                    }

                    var sql = GenerateSyncIn(sourceCode, listRow.ToArray(), n, out message);
                    if (checkHasError(message) == true)
                    {
                        daConn.RollbackTransaction();
                        return false;
                    }
                    if (daConn.ExecQueryTrans(sql, out message) == false)
                        return false;
                }

                //line
                if (objLines != null && objLines.Count > 0)
                {
                    foreach (var obj in objLines)
                    {
                        if (obj.DataSource != null && obj.DataSource.Rows.Count > 0)
                        {
                            //edit  2014-04-22
                            var syncSourceLevel = _sourcexml.SyncSourceLevel_Get_Active(sourceCode, out message);         //FOR DATA XML
                            if (syncSourceLevel == null || syncSourceLevel.Count == 0)
                            {
                                message = getFunctionError("ExcuteDataSyncIn", "Not found any soure level.");
                                daConn.RollbackTransaction();
                                return false;
                            }

                            var sl = syncSourceLevel.Where(o => o.SourceLine == obj.SourceCode).FirstOrDefault();
                            if (sl != null)
                            {
                                DataTable olines_temp = obj.DataSource.Clone();

                                foreach (DataRow rtemp in dataSource.Rows)
                                {

                                    var ot = obj.DataSource.Select(string.Format("{0}='{1}'", sl.LineReferenceKey, rtemp[sl.HeaderKey]));
                                    ot.CopyToDataTable(olines_temp, LoadOption.Upsert);
                                }

                                if (olines_temp != null && olines_temp.Rows.Count > 0)
                                {
                                    int countLine = olines_temp.Rows.Count;
                                    int lineIndex = (countLine / rowtimes) + 1;

                                    for (int n = 0; n < lineIndex; n++)
                                    {
                                        List<DataRow> listRow = new List<DataRow>();
                                        for (int m = n * rowtimes; m < (n + 1) * rowtimes; m++)
                                        {
                                            if (m < countLine)
                                                listRow.Add(olines_temp.Rows[m]);
                                            else
                                                break;
                                        }

                                        var sql = GenerateSyncIn(obj.SourceCode, listRow.ToArray(), n, out message);
                                        if (checkHasError(message) == true)
                                        {
                                            daConn.RollbackTransaction();
                                            return false;
                                        }
                                        if (daConn.ExecQueryTrans(sql, out message) == false)
                                            return false;
                                    }
                                }
                            }
                            else
                            {
                                message = getFunctionError("ExcuteDataSyncIn", "Not found soure level.");
                                daConn.RollbackTransaction();
                                return false;
                            }
                            //end  2014-04-22
                        }
                    }
                }


                daConn.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("ExcuteDataSyncIn", ex.Message);
                daConn.RollbackTransaction();
                return false;
            }
        }

        public bool LastedSyncDate_Update(object sourcecodce, DateTime? syncDate, out string message)
        {
            message = "";
            try
            {
                return _sourcexml.LastedSyncDate_Update(sourcecodce, syncDate, out message);
            }
            catch (Exception ex)
            {
                message = getFunctionError("LastedSyncDate_Update", ex.Message);
                return false;
            }
        }

        #endregion

        #region SYNCLOG
        public List<SyncLog_View> SyncLogGet(DateTime? fromDate, DateTime? toDate, bool? isManual)
        {

            fromDate = ParseDateNoTime(fromDate);
            toDate = ParseDateNoTime(toDate);
            using (var trans = TransWithUncommitted())
            {
                var results = (from x in _db.SyncLogs
                               where (fromDate.HasValue == false || x.SyncDate >= fromDate)
                               && (toDate.HasValue == false || x.SyncDate < toDate.Value.AddDays(1))
                               && (isManual == null || x.IsManual == isManual)
                               orderby x.SyncLogId descending
                               select new SyncLog_View
                               {
                                   SyncLogId = x.SyncLogId,
                                   SyncDate = x.SyncDate,
                                   SyncBy = x.SyncBy,
                                   Description = x.Description,
                                   IsManual = x.IsManual,
                               }).ToList();

                return results;
            }
        }

        public List<SyncLogLine_View> SyncSourceLogGet(int syncLogId, string status)
        {
            using (var trans = TransWithUncommitted())
            {
                var results = (from x in _db.SyncLogLines
                               where x.SyncLogId == syncLogId
                               && (status == "" || status == "-1" || x.SyncStatus == status)
                               orderby x.Id descending
                               select new SyncLogLine_View
                               {
                                   Id = x.Id,
                                   SyncLogId = x.SyncLogId,
                                   GroupCode = x.GroupCode,
                                   GroupName = x.GroupName,
                                   SourceCode = x.SoruceCode,
                                   SourceName = x.SourceName,
                                   SyncStatus = x.SyncStatus,
                                   SyncStatusDesc = x.SyncStatusDesc,
                                   SyncDate = x.SyncDate,
                                   Description = x.Description,
                                   WorkingPath = x.WorkingPath,
                                   WorkingFiles = x.WorkingFiles,
                               }).ToList();

                return results;
            }
        }

        public void SyncLogClear(int days)
        {
            using (var trans = TransWithCommitted())
            {

                string sql_lines = string.Format("delete o1 from SyncLogLines o1 join SyncLog o2 on o1.SyncLogId=o2.SyncLogId where DATEDIFF(d, o2.SyncDate,getdate())>={0}", days);
                string sql = string.Format("delete from synclog where DATEDIFF(d, SyncDate,getdate())>={0}", days);

                _db.ExecuteQuery<SyncLogLine_View>(sql_lines).FirstOrDefault();
                _db.ExecuteQuery<SyncLog_View>(sql).FirstOrDefault();

                trans.Complete();

            }
        }

        public SyncLog SyncLogModified(SyncObjectCurrent obj)
        {
            if (obj == null) return null;

            using (var trans = TransWithCommitted())
            {
                var result = new SyncLog();

                result.IsManual = obj.IsManual;
                result.SyncBy = obj.SyncBy;
                result.SyncDate = DateTime.Now;
                result.Description = result.Description;

                _db.SyncLogs.InsertOnSubmit(result);
                _db.SyncLogs.Context.SubmitChanges();

                trans.Complete();
                return result;

            }
        }

        public SyncLogLine SyncSourceLogModified(int syncLogId, SyncObjectCurrent obj)
        {
            if (obj == null) return null;

            using (var trans = TransWithCommitted())
            {
                var result = new SyncLogLine();

                result.SyncLogId = syncLogId;
                result.WorkingPath = obj.WorkingPath;
                result.WorkingFiles = obj.WorkingFiles;
                result.SyncDate = DateTime.Now;
                result.Description = obj.SyncDescription;
                result.GroupCode = obj.SyncGroup;
                result.GroupName = obj.SyncGroupName;
                result.SoruceCode = obj.SyncSource;
                result.SourceName = obj.SyncSourceName;
                result.SyncStatus = obj.SyncStatus;
                result.SyncStatusDesc = obj.SyncStatusDesc;

                _db.SyncLogLines.InsertOnSubmit(result);
                _db.SyncLogLines.Context.SubmitChanges();

                trans.Complete();
                return result;

            }
        }
        #endregion
    }
}
