﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTools.Models.POSAPI
{
    public class OrderStatus
    {
        public string SONUM { get; set; }
        public Nullable<System.DateTime> SODate { get; set; }
        public string Status { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Phone { get; set; }
        public decimal? TotalAmount { get; set; }
    }

    public class OrderStatusHandle
    {
        ApiDbContext db = new ApiDbContext();

        public List<OrderStatus> GetOrderStatus(out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("ICC_US_API_GET_ORDERSTATUS");
                var lstotherdata = db.Database.SqlQuery<OrderStatus>(sql).ToList();
                return lstotherdata;
            }
            catch (Exception ex)
            {
                message = ex.Message;

            }
            return null;
        }

        public List<OrderStatus> GetOrderStatusbySONum(string SONum, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("ICC_US_API_GET_ORDERSTATUS_bySONum {0}",SONum);
                var lstotherdata = db.Database.SqlQuery<OrderStatus>(sql).ToList();
                return lstotherdata;
            }
            catch (Exception ex)
            {
                message = ex.Message;

            }
            return null;
        }
    }

}
