namespace DataTools.Models.POSAPI
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ApiDbContext : DbContext
    {
        public ApiDbContext()
            : base("name=SAPDbContext")
        {
        }

        public virtual DbSet<DataTools.Models.POSAPI.DraftData> DraftDatas { get; set; }
        public virtual DbSet<DataTools.Models.POSAPI.DraftDataLines> DraftDataLines { get; set; }
        public virtual DbSet<DataTools.Models.POSAPI.DraftDataPay> DraftDataPays { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<ApiDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}
