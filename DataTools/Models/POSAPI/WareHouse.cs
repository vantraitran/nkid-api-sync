﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTools.Models.POSAPI
{
    public class Store
    {
        public string StoreCode { get; set; }
        public string StoreName { get; set; }
        public string AreaCode { get; set; }
    }

    public class StoreHandle
    {
        ApiDbContext db = new ApiDbContext();

        public List<Store> GetWareHouse(out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("ICC_US_API_GET_WAREHOUSE");
                var listwh = db.Database.SqlQuery<Store>(sql).ToList();
                return listwh;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                
            }
            return null;
        }
    }
}
