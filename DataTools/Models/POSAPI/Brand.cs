﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTools.Models.POSAPI
{
    public class Brand
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int Inactive { get; set; }
    }

    public class BrandHandle
    {
        ApiDbContext db = new ApiDbContext();
        public List<Brand> GetBrand(out string message)
        {
            message = "";
            try
            {
                string sql = String.Format("ICC_US_API_GET_BRAND_ALL");
                var listBrand = db.Database.SqlQuery<Brand>(sql).ToList();
                return listBrand;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return null;
        }
    }
}
