﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTools.Models.POSAPI
{
    public class OtherData
    {
        public string Code { get; set; }
        public string Name { get; set; }
        //public int Inactive { get; set; }
        //public string Group { get; set; }
    }

    public class OtherDataHandle
    {
        ApiDbContext db = new ApiDbContext();

        public List<OtherData> GetOtherData(out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("ICC_US_API_GET_OTHERDATA");
                var lstotherdata = db.Database.SqlQuery<OtherData>(sql).ToList();
                return lstotherdata;
            }
            catch (Exception ex)
            {
                message = ex.Message;

            }
            return null;
        }
        public List<OtherData> GetOtherData(string Group, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("ICC_US_API_GET_OTHERDATA_WITH_GROUP @Group='{0}'", Group);
                var lstotherdata = db.Database.SqlQuery<OtherData>(sql).ToList();
                return lstotherdata;
            }
            catch (Exception ex)
            {
                message = ex.Message;

            }
            return null;
        }


    }

}
