﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTools.Models.POSAPI
{
    public class Product
    {
        public string ProductCode { get; set; }
        public string SKUCode { get; set; }
        public string ProductName { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string Material { get; set; }
        public string Category { get; set; }
        public decimal PriceBefDisc { get; set; }
        public decimal DiscountPercent { get; set; }
        public decimal PriceAfDisc { get; set; }
        public int Year { get; set; }
        public string Country { get; set; }
        public string Remarks { get; set; }
        public string Season { get; set; }
        public int Instock { get; set; }
        public int Inactive { get; set; }
        public string Gender { get; set; }
        public string Collection { get; set; }


    }

    public class ProductHandle
    {
        ApiDbContext db = new ApiDbContext();
        public List<Product> GetArtical(DateTime updateDate, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("ICC_US_API_GET_ITEMS_ALL '{0}'", updateDate.ToShortDateString());
                var listArt = db.Database.SqlQuery<Product>(sql).ToList();
                return listArt;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }

        public List<Product> GetArtical(DateTime updateDate,string v2, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("ICC_US_API_GET_ITEMS_ALL_v2 '{0}'", updateDate.ToShortDateString());
                var listArt = db.Database.SqlQuery<Product>(sql).ToList();
                return listArt;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }


        public List<Product> GetArticalbyCode(string SKUCode, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("ICC_US_API_GET_ITEMSTOCK '{0}'", SKUCode);
                var listArt = db.Database.SqlQuery<Product>(sql).ToList();
                return listArt;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }

    }
}
