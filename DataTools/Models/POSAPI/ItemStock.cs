﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace DataTools.Models.POSAPI
{
    public class StoreItemStock
    {
        public string AreaCode { get; set; }
        public string StoreCode { get; set; }
        public string SKUCode { get; set; }
        public decimal Instock { get; set; }
    }

    [DataContract]
    public class AreaItemStock
    {

        /// <summary>
        /// 
        /// </summary>
        /// 
        [DataMember]
        public string AreaCode { get; set; }
        [DataMember]
        public string SKUCode { get; set; }
        [DataMember]
        public decimal Instock { get; set; }
    }

    public class ItemStockHandle
    {
        ApiDbContext db = new ApiDbContext();
        public List<AreaItemStock> GetItemStockByArea(out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("ICC_US_API_GET_ITEMSTOCK_BY_AREA");
                var result = db.Database.SqlQuery<AreaItemStock>(sql).ToList();
                return result;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }

        public List<StoreItemStock> GetItemStockByStore(out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("ICC_US_API_GET_ITEMSTOCK_BY_STORES");
                var result = db.Database.SqlQuery<StoreItemStock>(sql).ToList();
                return result;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }
    }

}
