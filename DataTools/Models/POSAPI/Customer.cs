﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTools.Models.POSAPI
{
    public class Customer
    {
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public DateTime? Birthday { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Address { get; set; }
        public string PersonId { get; set; }
        public double EwalletPoint { get; set; }
        public string ShipCity { get; set; }
        public string ShipState { get; set; }
        public string ShipAddress { get; set; }
        public string ShipPhone { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }

    public class CustomerHandle
    {
        ApiDbContext db = new ApiDbContext();
        public Customer GetCustomer(DateTime lastUpdateDate, out string message)
        {
            message = "";
            try
            {
                string sql = String.Format("ICC_US_API_GET_CUSTOMERS '{0}'", lastUpdateDate);
                var cus = db.Database.SqlQuery<Customer>(sql).FirstOrDefault();
                return cus;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return null;
        }

        public Customer GetCustomerbyPhone(string Phone1, out string message)
        {
            message = "";
            try
            {
                string sql = String.Format("ICC_US_API_GET_CUSTOMERS_BYPHONE '{0}'", Phone1);
                var cus = db.Database.SqlQuery<Customer>(sql).FirstOrDefault();
                return cus;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return null;
        }

        public int CreateCustomer(Customer data, out string bpcode,out string message)
        {
            message = "";
            bpcode = "";
            try
            {
                if (data.Phone.Length > 7)
                {
                    string sql = string.Format("select 1 from POSONE_CLIENT_ONLINE.dbo.BusinessPartners with(nolock) where phone1 like '%{0}%'", data.Phone);
                    int a = db.Database.SqlQuery<int>(sql).FirstOrDefault(); 
                    if(a>0)
                    {
                        message = "This Phone Number is already exists!";
                        return -1;
                    }
                    sql = String.Format("ICC_US_API_CREATE_CUSTOMER @CustomerName='{0}',@MobilePhone='{1}',@Email='{2}',@Address='{3}',@PersonalID='{4}',@BirthDay='{5}'"
                    , data.CustomerName
                    , data.Phone
                    , data.Email
                    , data.Address
                    , data.PersonId
                    ,data.Birthday
                    );

                    var cus = db.Database.ExecuteSqlCommand(sql);

                    sql = string.Format("select bpcode from POSONE_CLIENT_ONLINE.dbo.BusinessPartners with(nolock) where phone1 like '%{0}%'", data.Phone);
                    bpcode = db.Database.SqlQuery<string>(sql).FirstOrDefault();
                    return 0;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return -1;
        }
    }
}
