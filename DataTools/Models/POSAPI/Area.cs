﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace DataTools.Models.POSAPI
{
    public class Area
    {
        [Description("Get the data from our service. It will requires a key.")]
        public string AreaCode { get; set; }
        public string AreaName { get; set; }
        //public int Inactive { get; set; }
    }

    public class AreaHandle
    {
        ApiDbContext db = new ApiDbContext();
        public List<Area> GetArea(out string message)
        {
            message = "";
            try
            {
                string sql = String.Format("ICC_US_API_GET_AREA");
                var listBrand = db.Database.SqlQuery<Area>(sql).ToList();
                return listBrand;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return null;
        }
    }
}
