﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace DataTools.Models.POSAPI
{
    public class SaleOrder
    {
        public string SONum { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Address { get; set; }
        public DateTime? SODate { get; set; }
        public string Receiver { get; set; }
        public string ShipCity { get; set; }
        public string ShipState { get; set; }
        public string ShipAddress { get; set; }
        public string ShipPhone { get; set; }
        public string Remarks { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? DisAmount { get; set; }
        public decimal? DisPercent { get; set; }
        public int ProgramCode { get; set; }
        public string ProgramName { get; set; }
        public decimal? EWalletAmount { get; set; }
        public string PaymentMethod { get; set; }
        public List<SaleOrderLines> SOLines { get; set; }
        public List<SaleOrderPayment> SOPayment { get; set; }
    }

    public class SaleOrderPromo
    {
        public string SONum { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Address { get; set; }
        public DateTime? SODate { get; set; }
        public string Receiver { get; set; }
        public string ShipCity { get; set; }
        public string ShipState { get; set; }
        public string ShipAddress { get; set; }
        public string ShipPhone { get; set; }
        public string Remarks { get; set; }
        public decimal? TotalAmount { get; set; }
        public decimal? DisAmount { get; set; }
        public decimal? DisPercent { get; set; }
        public int ProgramCode { get; set; }
        public string ProgramName { get; set; }
        public decimal? EWalletAmount { get; set; }
        public List<SaleOrderLines> SOLines { get; set; }
    }

    public class SaleOrderLines
    {
        public string SKUCode { get; set; }
        public string ProductName { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? PriceBefDisc { get; set; }
        public decimal? Discount { get; set; }
        public decimal? DiscountPer { get; set; }
        public decimal? PriceAfDisc { get; set; }
        public decimal? LineTotal { get; set; }
        public string ProgramCode { get; set; }
        public string ProgramName { get; set; }
        public string DisType { get; set; } 

        /// <summary>
        /// Y or N
        /// </summary>
        public string IsTranportFee { get; set; }
    }

    [DataContract]
    public class SaleOrderPayment
    {
        [DataMember]
        public string PaymentType { get; set; }
        public string CreCardNo { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public string CreCardType { get; set; }
        [DataMember]
        public decimal? Amount { get; set; }
    }


    public class ICC_US_PM_GETPROMOTIONResult
    {

        public System.Nullable<bool> DfltSelected{ get; set; }

        public System.Nullable<int> PMID{ get; set; }

        public System.Nullable<int> PMType{ get; set; }

        public string PMCode{ get; set; }

        public string PMName{ get; set; }

        public string Description{ get; set; }

        public System.Nullable<decimal> DiscountPercent{ get; set; }

        public System.Nullable<decimal> DiscountAmount{ get; set; }

        public string ItemCode{ get; set; }

        public string ItemName{ get; set; }

        public System.Nullable<decimal> Quantity{ get; set; }

        public System.Nullable<decimal> FreeQty{ get; set; }

        public System.Nullable<decimal> InStock{ get; set; }

        public System.Nullable<decimal> LineDiscPercent{ get; set; }

        public System.Nullable<decimal> LineDiscAmount{ get; set; }

        public System.Nullable<decimal> LineDiscPrice{ get; set; }

        public System.Nullable<decimal> LineFixPrice{ get; set; }

        public System.Nullable<char> Editable{ get; set; }

        public System.Nullable<int> RowBackColor{ get; set; }

        public string PMCode2{ get; set; }

        public string ProgramType{ get; set; }

        public string ProgTypeFldMapping{ get; set; }

        public System.Nullable<char> ProgTypeFldMapOnLines{ get; set; }

        public string ProgTypeFldMapDfltVal{ get; set; }

        public string ProgramCode{ get; set; }

        public string ExcludePMCode{ get; set; }
    }



        public class SaleOrderHandle
    {
        ApiDbContext db = new ApiDbContext();
        ApiDbContext db_sv = new ApiDbContext();

        public SaleOrder GetSaleOrder(string CustomerCode, string Phone, string SONum, out string message)
        {

            message = "";
            try
            {
                SaleOrder result = new SaleOrder();

                //get header
                string sql = string.Format("[ICC_US_API_GET_SalesOrders] '{0}','{1}','{2}'", CustomerCode, Phone, SONum);
                result = db.Database.SqlQuery<SaleOrder>(sql).FirstOrDefault();

                //get lines
                result.SOLines = new List<SaleOrderLines>();
                sql = string.Format("[ICC_US_API_GET_SalesOrders_Lines] '{0}','{1}','{2}'", CustomerCode, Phone, SONum);
                result.SOLines.AddRange(db.Database.SqlQuery<SaleOrderLines>(sql).ToList());

                return result;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return null;
        }

        public int PostSaleOrder(SaleOrder so, out string message)
        {
            db.Database.Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["HOnline"].ConnectionString;
            if (db.Database.Connection.State == System.Data.ConnectionState.Closed) db.Database.Connection.Open();
            var tran = db.Database.BeginTransaction();
            message = "";
            try
            {
                //check valid bill
                if (!CheckValid(so,out message))
                {
                    return -1;
                }
                DraftData draftData = new DraftData();
                draftData.SONum = so.SONum;
                draftData.BPCode = so.CustomerID;
                draftData.BPName = so.CustomerName;
                draftData.BPPhone = so.Phone;
                draftData.BPAddNo = so.Address;
                draftData.BPProvinceCode = so.City;
                draftData.BPDistrictCode = so.State;
                draftData.SODate = so.SODate;
                draftData.DEReceiver = so.Receiver;
                draftData.DEAddr = so.ShipAddress;
                draftData.DEMobile = so.ShipPhone;
                draftData.DocRemarks = so.Remarks;
                //field mac dinh he thong
                var StoreCode = db.Database.SqlQuery<string>("select Value from Configuration where Name='StoreCode'").FirstOrDefault();
                draftData.RevenueStore = StoreCode;
                draftData.StoreCode = draftData.RevenueStore;
                var lastdockey = db.Database.SqlQuery<int>("select top 1 dockey from DraftData with(nolock) order by dockey desc").FirstOrDefault();
                draftData.DocKey = lastdockey + 1;
                draftData.CreateBy = "webapi";
                draftData.CreateDate = DateTime.Today;
                draftData.DocStatus = "P";
                draftData.DocDate = DateTime.Today;

                //them
                draftData.TotalAmount = so.TotalAmount;
                draftData.DisAmount = so.DisAmount;
                draftData.DisPercent = so.DisPercent;
                draftData.PMType = so.ProgramCode;
                draftData.PMCode2 = so.ProgramName;
                draftData.PaymentMethod = so.PaymentMethod;

                //lines
                Int16 i = 1;
                List<DraftDataLines> lines = new List<DraftDataLines>();
                foreach (var item in so.SOLines)
                {

                    DraftDataLines line = new DraftDataLines();
                    line.DocKey = lastdockey + 1;
                    line.RevenueStore = StoreCode;
                    line.StoreCode = StoreCode;
                    line.WhsCode = StoreCode;
                    line.ItemCode = item.SKUCode;
                    line.ItemName = item.ProductName;
                    line.Quantity = item.Quantity;
                    line.SizeCode = item.Size;
                    line.ColorCode = item.Color;
                    line.UnitPrice = item.PriceAfDisc;
                    line.FullPrice=item.PriceBefDisc;
                    line.LineTotal = item.LineTotal;
                    line.DisPercent = item.DiscountPer;
                    line.DisAmount = item.Discount;
                    line.PMType = item.ProgramCode;
                    line.PMCode2 = item.ProgramName;
                    line.LineId = i;

                    db.DraftDataLines.Add(line);
                    i++;
                }

                //Pays
                if(so.EWalletAmount>0)
                {
                    if (!UpdatePoint(so, out message)) goto rollback;
                    DraftDataPay pay = new DraftDataPay();
                    pay.DocKey = lastdockey + 1;
                    pay.RevenueStore = StoreCode;
                    pay.StoreCode = StoreCode;
                    pay.PayType = "L";
                    pay.Amount = so.EWalletAmount * 1000;

                    db.DraftDataPays.Add(pay);
                }
                i = 1;
                List<DraftDataPay> pays = new List<DraftDataPay>();
                foreach (var item in so.SOPayment)
                {
                    DraftDataPay pay = new DraftDataPay();
                    pay.DocKey = lastdockey + 1;
                    pay.RevenueStore = StoreCode;
                    pay.StoreCode = StoreCode;
                    pay.PayType = item.PaymentType;
                    if (so.PaymentMethod=="COD" && item.PaymentType=="C")
                        pay.PayType = "Tr";
                    pay.CreCardNo = item.CreCardNo;
                    pay.BankCode = item.BankCode;
                    pay.BankName = item.BankName;
                    pay.CreCardType = item.CreCardType;
                    pay.Amount = item.Amount;
                    pay.LineId = i;

                    db.DraftDataPays.Add(pay);
                    i++;
                }

                db.DraftDatas.Add(draftData);
                //db.DraftDataLines.AddRange(lines);
                db.SaveChanges();
                tran.Commit();


                return 0;
            }

            catch (Exception ex)
            {
                message = ex.Message;
            }
            rollback:
            tran.Rollback();
            return -1;
        }

        private bool UpdatePoint(SaleOrder so, out string message)
        {
            message = "";
            try
            {
                db_sv.Database.Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["HServer"].ConnectionString;
                if (db.Database.Connection.State == System.Data.ConnectionState.Closed) db.Database.Connection.Open();
                string sSQL = string.Format("select 1 from BusinessPartners with(nolock) where bpcode='{0}' and isnull(LoyaltyPoints,0) < {1}", so.CustomerID, so.EWalletAmount);
                int i = db_sv.Database.SqlQuery<int>(sSQL).FirstOrDefault();
                if(i>0)
                {
                    message = "Not enough LoyaltyPoints!";
                    return false;
                }
                sSQL = string.Format("Update BusinessPartners set [LoyaltyPoints]=[LoyaltyPoints] - {0} where BPCODE='{1}'", so.EWalletAmount, so.CustomerID);
                i = db_sv.Database.ExecuteSqlCommand(sSQL);
                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        private bool CheckValid(SaleOrder so, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("select 1 from BusinessPartners with(nolock) where bpcode = '{0}'", so.CustomerID);
                int a = db.Database.SqlQuery<int>(sql).FirstOrDefault();
                if (a <= 0)
                {
                    message = "CustomerID not exists!";
                    return false;
                }
                if(so.SOLines.Where(o=>o.Quantity<=0).ToList().Count>0)
                {
                    message = "SO lines quantity 0!";
                    return false;
                }
                if (so.SOLines.Count<1)
                {
                    message = "SO lines empty!";
                    return false;
                }
                if (so.TotalAmount<=0)
                {
                    message = "SO total amount must be greater than 0!";
                    return false;
                }
                if (so.TotalAmount != so.SOPayment.Sum(o=>o.Amount) + (Convert.ToInt32(so.EWalletAmount) *1000))
                {
                    message = "Total Payment not equal Total Bill!";
                    return false;
                }
                if (so.TotalAmount != (so.SOLines.Sum(o => o.LineTotal)- Convert.ToInt32(so.DisAmount)))
                {
                    message = "Total Line not equal Total Bill!";
                    return false;
                }

                if (db.DraftDatas.Where(o=>o.SONum==so.SONum).ToList().Count>0)
                {
                    message = "SONum already exists";
                    return false;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
            return true;
        }
        

        public SaleOrderPromo PostSaleOrderPromo(SaleOrderPromo data, out string message)
        {
            db.Database.Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["HOnline"].ConnectionString;
            if (db.Database.Connection.State == System.Data.ConnectionState.Closed) db.Database.Connection.Open();
            //var tran = db.Database.BeginTransaction();

            message = "";
            List<ICC_US_PM_GETPROMOTIONResult> _promotionList = null;
            SaleOrderPromo result = new SaleOrderPromo();
            result = data;
            var StoreCode = db.Database.SqlQuery<string>("select Value from Configuration where Name='StoreCode'").FirstOrDefault();
            try
            {
                //get special price before get promotion
                foreach(SaleOrderLines line in result.SOLines)
                {
                    if (line.SKUCode != "DeliveryFee")
                    {
                        var priceresult = GetPromotionPrice(StoreCode, line.SKUCode, data.CustomerID, DateTime.Now, Convert.ToDecimal(line.Quantity), out message);
                        if (priceresult != null)
                        {
                            line.PriceBefDisc = priceresult.FullPrice;
                            line.PriceAfDisc = priceresult.UnitPrice;
                            line.Discount = priceresult.DisPercent;
                            line.DisType = priceresult.DisType;
                            line.LineTotal = priceresult.UnitPrice * line.Quantity;
                        }
                        else
                        {
                            message = "Cannot get Price!";
                            //tran.Rollback();
                            return null;
                        }
                    }
                }
                
                _totalbill = result.SOLines.Sum(o => o.LineTotal);
                result.DisAmount = 0;
                result.DisPercent = 0;
                //tuong duong PMType
                result.ProgramCode = 0;
                //PMCode2
                result.ProgramName = "";
                //get promotion
                string listitem = GetListItems(result.SOLines);

                string sSQL = string.Format("[ICC_US_PM_GETPROMOTION] @DocDate='{0}', @ListItems='{1}' , @TotalBefVAT={2}, @TotalAfVAT={3}",DateTime.Now, listitem, _totalbill, _totalbill);
                _promotionList = db.Database.SqlQuery<ICC_US_PM_GETPROMOTIONResult>(sSQL).ToList();
                _promotionList = ReturnPromotion(_promotionList, out message);

                if (_promotionList != null) //apply promotion
                {
                    ApplyPromotion(_promotionList, ref result, out message);
                }
                //get lai total sau khi giam bill
                result.TotalAmount = result.SOLines.Sum(o => o.LineTotal) - result.DisAmount;
                //tran.Commit();
                return result;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            //tran.Rollback();
            return result;
        }

        private void ApplyPromotion(List<ICC_US_PM_GETPROMOTIONResult> promotionList, ref SaleOrderPromo result, out string message)
        {
            message = "";
            try
            {
                //promotion
                List<ICC_US_PM_GETPROMOTIONResult> _pmDiscountLine = new List<ICC_US_PM_GETPROMOTIONResult>();
                List<ICC_US_PM_GETPROMOTIONResult> _pmDiscountBill = new List<ICC_US_PM_GETPROMOTIONResult>();
                List<ICC_US_PM_GETPROMOTIONResult> _pmFreeItems = new List<ICC_US_PM_GETPROMOTIONResult>();
                foreach (ICC_US_PM_GETPROMOTIONResult pm in promotionList)
                {
                    //if (Convert.ToBoolean(pm.DfltSelected))
                    //{
                        if (pm.PMType == 31)//free item
                        {
                            _pmFreeItems.Add(pm);
                        }
                        if (pm.PMType == 20)//discount line
                        {
                            _pmDiscountLine.Add(pm);
                        }
                        //discount bill
                        if (pm.PMType == 10)
                        {
                            _pmDiscountBill.Add(pm);
                        }
                    //}
                }

                foreach (ICC_US_PM_GETPROMOTIONResult pmDiscline in _pmDiscountLine)
                {
                    ApplyPromotion_DiscountLine(ref result,pmDiscline, Convert.ToInt32(pmDiscline.PMType));
                }
                //Apply for free item
                foreach (ICC_US_PM_GETPROMOTIONResult pmFreeItem in _pmFreeItems)
                {
                    if (pmFreeItem.FreeQty > 0)
                        ApplyPromotion_FreeItems(ref result, pmFreeItem, out message);
                }
                //Apply for discount by bill
                foreach (ICC_US_PM_GETPROMOTIONResult pmDiscBill in _pmDiscountBill)
                {
                    ApplyPromotion_DiscountBill(ref result, pmDiscBill, out message);
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
        }


        //programcode ~pmtype
        //programname ~pmcode2
        private void ApplyPromotion_DiscountLine(ref SaleOrderPromo result,ICC_US_PM_GETPROMOTIONResult pmdiscline, int pmtype = -1)
        {
            foreach (SaleOrderLines saleline in result.SOLines)
            {
                if (saleline.SKUCode == pmdiscline.ItemCode && (saleline.ProgramCode == pmtype.ToString() || saleline.ProgramCode == ""))
                {
                    if (pmdiscline.LineDiscAmount != null) saleline.Discount = pmdiscline.LineDiscAmount;
                    else if (pmdiscline.LineDiscPercent != null)
                    {
                        saleline.DiscountPer = (pmdiscline.LineDiscPercent + (saleline.DiscountPer == null ? 0 : saleline.DiscountPer)) - ((pmdiscline.LineDiscPercent * (saleline.DiscountPer == null ? 0 : saleline.DiscountPer)) / 100);
                    }
                    else if (pmdiscline.LineDiscPrice != null)
                    {
                        //saleline.DisAmount = pmdiscline.LineDiscPrice * saleline.Quantity;
                        decimal linedispricetopercent = Convert.ToDecimal(pmdiscline.LineDiscPrice / saleline.PriceAfDisc) * 100;
                        saleline.DiscountPer = (linedispricetopercent + saleline.DiscountPer) - ((linedispricetopercent * saleline.DiscountPer) / 100);
                    }
                    else if (pmdiscline.LineFixPrice != null) saleline.PriceAfDisc = pmdiscline.LineFixPrice;
                    else if (pmdiscline.PMType == 31) saleline.PriceAfDisc = 0;

                    decimal? amountdiscPer = saleline.DiscountPer != null ? saleline.PriceAfDisc * saleline.Quantity * saleline.DiscountPer / 100 : 0;
                    decimal? amountdiscAmt = saleline.Discount != null ? saleline.Discount : 0;
                    //apply lam tron o day
                    //saleline.DisPercent = amountdiscPer;
                    saleline.Discount = Math.Round(Convert.ToDecimal(amountdiscPer), 0);
                   
                    //saleline.AmountAfDis = Math.Round(saleline.AmountAfDis);
                    saleline.LineTotal = saleline.PriceAfDisc * saleline.Quantity - saleline.Discount;
                    saleline.ProgramName += pmdiscline.PMCode2 + ",";
                    saleline.ProgramCode = pmdiscline.PMType.ToString();

                    //if(saleline.PMType==31)
                    //{
                    //    saleline.DisAmount = saleline.UnitPrice * saleline.Quantity;
                    //    saleline.AmountAfDis = 0;
                    //    saleline.LineTotal = 0;
                    //}
                }
            }
        }

        private void ApplyPromotion_FreeItems(ref SaleOrderPromo result, ICC_US_PM_GETPROMOTIONResult pmFreeItem, out string message)
        {
            try
            {
                SaleOrderLines saleline = new SaleOrderLines();
                saleline.SKUCode = pmFreeItem.ItemCode;
                saleline.ProductName = pmFreeItem.ItemName;
                saleline.ProgramCode = pmFreeItem.PMType.ToString();
                saleline.ProgramName = pmFreeItem.PMCode2;
                var item= Item_Get(pmFreeItem.ItemCode, out message);
                if (item == null)
                {
                    message = "Cannot get item promotion!";
                    return;
                }
                saleline.Size = item.Size;
                saleline.Color = item.Color;
                saleline.Quantity = pmFreeItem.FreeQty;
                saleline.LineTotal = 0;
                saleline.PriceAfDisc = 0;
                saleline.PriceBefDisc = 0;

                result.SOLines.Add(saleline);
                

                //gan lai value =0 cho itemcode do; vi la hang khuyen mai
                //DocumentLines_View saleline = (from o in ListSaleLine
                //                               where o.ItemCode == itemChoose.ItemCode
                //                               select o).FirstOrDefault();

                //saleline.LineTotal = 0;
                //saleline.AmountAfDis = 0;
                //saleline.UnitPrice = 0;

                //update item duoc tang pmcode2
                //lay itemcode cua itemduoc tang
                //var itempromo = _listPmView.Where(o => o.PMType == 30 && o.PMCode2 == itemChoose.PMCode2).FirstOrDefault();
                //var itempromoinSalelist = ListSaleLine.Where(o => o.ItemCode == itempromo.ItemCode).FirstOrDefault();


            }
            catch (Exception e)
            {
                message = e.Message;
            }
        }

        private void ApplyPromotion_DiscountBill(ref SaleOrderPromo result, ICC_US_PM_GETPROMOTIONResult pmDiscBill, out string message)
        {
            message = "";
            try
            {
                //get amount after discount by line
                result.TotalAmount = result.TotalAmount = result.SOLines.Sum(o => o.LineTotal);
                //amount discount = amount + percent
                //decimal promotiondiscamt = blSale.RoundNumeric(Convert.ToDecimal(pmDiscBill.DiscountAmount + pmDiscBill.DiscountPercent * TotalAfDis/100),out message);
                if (pmDiscBill.DiscountAmount != null && pmDiscBill.DiscountAmount != 0)
                   result.DisPercent = ((result.DisPercent + (pmDiscBill.DiscountAmount / result.TotalAmount)) - (result.DisPercent * (pmDiscBill.DiscountAmount / result.TotalAmount) / 100)) * 100;
                else if (pmDiscBill.DiscountPercent != null)
                    result.DisPercent = (result.DisPercent + pmDiscBill.DiscountPercent) - (result.DisPercent * pmDiscBill.DiscountPercent / 100);
                //_disc_amount = promotiondiscamt;

                result.DisAmount= Math.Round( Convert.ToDecimal((result.DisPercent != null ? result.TotalAmount * result.DisPercent / 100 : 0)),0);
                result.ProgramCode = Convert.ToInt32(pmDiscBill.PMType);
                result.ProgramName += pmDiscBill.PMCode2 + ",";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
        }
        private decimal? _totalbill = 0;
        private List<ICC_US_PM_GETPROMOTIONResult> ReturnPromotion(List<ICC_US_PM_GETPROMOTIONResult> promotionList, out string message)
        {
            message = "";
            try
            {
                List<ICC_US_PM_GETPROMOTIONResult> result = new List<ICC_US_PM_GETPROMOTIONResult>();
                //get the first promotion
                string firstpm = promotionList.First().PMCode2;
                if(CheckMultiplePromo(promotionList, firstpm))
                {
                    message = "Please recheck Promotion for Online Store!";
                    return null;
                }

                if (promotionList.Where(o=>o.ExcludePMCode!=null).ToList().Count>0)
                {
                    message = "Please recheck Exluded Promotion for Online Store!";
                    return null;
                }

                result = promotionList;
                   
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DocumentLines_View Item_Get(string itemCode, out string message)
        {
            message = "";
            try
            {
                //db.Database.Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["HOnline"].ConnectionString;
                //if (db.Database.Connection.State == System.Data.ConnectionState.Closed) db.Database.Connection.Open();
                string sSQL = string.Format("Select * from Items with(nolock) where itemcode ='{0}'", itemCode);
                var saledata = db.Database.SqlQuery<DocumentLines_View>(sSQL).FirstOrDefault();
                if (saledata != null)
                {
                    return saledata;
                }
                return null;
            }
            catch (Exception e)
            {
                message = e.Message;
                return null;
            }
        }

        private bool CheckMultiplePromo(List<ICC_US_PM_GETPROMOTIONResult> promotionList, string firstpm)
        {
             if(promotionList.Where(o=>o.PMCode2==firstpm && o.PMType==31).ToList().Count>1)
                return true;
            return false;
        }

        private string GetListItems(List<SaleOrderLines> sOLines)
        {
            string listItem = "";
            foreach (SaleOrderLines saleline in sOLines)
            {
                listItem += string.Format("{0} {1}", saleline.SKUCode, saleline.Quantity) + ",";
            }
            return listItem;
        }

        public DocumentLines_View GetPromotionPrice(string storecode, string ItemCode, string BPCode, DateTime Date, decimal Qty, out string message)
        {
            message = "";
            try
            {
                string sSQL = string.Format("GetPromotionPrice '{0}','{1}','{2}','{3}',{4}", storecode, ItemCode, BPCode, Date.ToString(_formatdatetime_db), Qty);
                var getpro = db.Database.SqlQuery<DocumentLines_View>(sSQL).FirstOrDefault();
                return getpro;
            }
            catch (Exception ex)
            {
                message = FunctionGennerate("GetPromotionPrice", ex.Message);
                return null;
            }
        }


        public string FunctionGennerate(string funcname, string message)
        {
            return string.Format("[{0}]{1}", funcname, message);
        }

        public const string _formatdatetime_db = "yyyy-MM-dd HH:mm:ss";
    }
}
