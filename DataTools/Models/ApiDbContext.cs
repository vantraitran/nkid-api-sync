namespace DataTools.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ApiDbContext : DbContext
    {
        public ApiDbContext()
            : base("name=ApiDbContext")
        {
        }

        public virtual DbSet<DataTools.Models.TGNH.DraftData> DraftDatas { get; set; }
        public virtual DbSet<DataTools.Models.TGNH.DraftDataLine> DraftDataLines { get; set; }
        public virtual DbSet<DataTools.Models.TGNH.DraftDataPay> DraftDataPays { get; set; }
        public virtual DbSet<DataTools.Models.TGNH.BusinessPartner> BusinessPartners { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<ApiDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}
