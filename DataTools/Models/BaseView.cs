﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DataTools.Models
{
    [Serializable]
    public class Parameter
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public bool Output { get; set; }
        public object OutputValue { get; set; }

    }

    public class CreCardDisc_View
    {
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public string CardTypeCode { get; set; }
        public string CardTypeName { get; set; }
        public decimal DiscPercent { get; set; }
        public string StoreCode { get; set; }
        public bool? Inactive { get; set; }
    }

    public class UserGroup_View
    {
        public string Code { get; set; }
        public String Name { get; set; }
    }

    public class Language_View
    {
        public String Code { get; set; }
        public String Code2 { get; set; }
        public String Name { get; set; }
        public String ForeignName { get; set; }
        public String FlagImg { get; set; }
        public bool IsDefault { get; set; }
    }

    public class Report_View
    {
        public string Value { get; set; }
        public string Para { get; set; }

    }

    #region Item Master Data
    public class Item_View
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string FrgnName { get; set; }
        public int? ItmsGrpCod { get; set; }
        public string VatGroupSa { get; set; }
        public string VatGroupPu { get; set; }
        public string CodeBars { get; set; }
        public char? GiftItem { get; set; }
        public decimal? PriceByPoint { get; set; }
        public char? PrchseItem { get; set; }
        public char? SellItem { get; set; }
        public char? InvntItem { get; set; }
        public string BuyUnitMsr { get; set; }
        public string SalUnitMsr { get; set; }
        public string InvntryUoM { get; set; }
        public int? FirmCode { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public char? Inactive { get; set; }
        public decimal? Price { get; set; }
        public string DefaultWhs { get; set; }
        public string WhsName { get; set; }
        public decimal? OnHand { get; set; }
        public string U_Cap4 { get; set; }
        public int? PluCode { get; set; }
        public string DfltWhs { get; set; }
        public string ItmsGrpCod1 { get; set; }
        public string ItmsGrpNam1 { get; set; }
        public string ItmsGrpCod2 { get; set; }
        public string ItmsGrpNam2 { get; set; }
        public string ItmsGrpCod3 { get; set; }
        public string ItmsGrpNam3 { get; set; }
        public string ColorCode { get; set; }
        public string ColorName { get; set; }
        public string Size { get; set; }
        public string BrandCode { get; set; }
        public string BrandName { get; set; }
        public string MaterialCode { get; set; }
        public string MaterialName { get; set; }
        public string TypeCode { get; set; }
        


    }
    #endregion

    // Item Groups
    public class ItemGroup_View
    {
        public string ItmsGrpCod { get; set; }
        public string ItmsGrpNam { get; set; }
        public string ParentCode { get; set; }
    }

    public class Store_View
    {
        public string StoreCode { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public bool? Inactive { get; set; }
        public int? PriceList { get; set; }
    }


    #region Sales
    //Sale Data Pays
    public class SalesDataPays_vew
    {
        public string StoreCode { get; set; }
        public int DocKey { get; set; }
        public int LineId { get; set; }
        public string PayType { get; set; }
        public string PayTypeDesc { get; set; }
        public string CardNum { get; set; }
        public decimal? CardMileage { get; set; }
        public decimal? CardBalance { get; set; }
        public string CardGrade { get; set; }
        public string CardGradeName { get; set; }
        public string CardType { get; set; }
        public string EventCode { get; set; }
        public string CreCardNo { get; set; }
        public string BankName { get; set; }
        public string CreCardType { get; set; }
        public decimal? Amount { get; set; }
        public string EventType { get; set; }

        public string CreditCardERP { get; set; }
        public decimal Proportion_VND_tXu { get; set; }
        //Zakka
        public string VoucherCode { get; set; }
        public decimal? Remain { get; set; }
        public string BankCode { get; set; }
    }

    //Sale Data History
    public class _SalesDataHis_vew
    {
        public int DocKey { get; set; }
        public DateTime? DocDate { get; set; }
        public DateTime? DueDate { get; set; }
        public decimal? TotalAmount { get; set; }
        public string CardNum { get; set; }
        public string BPCode { get; set; }
        public string BPName { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
    }
    #endregion

    //Shift
    public class Shift_vew
    {
        public int Id { get; set; }
        public int DocKey { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? TotalAmount { get; set; }
        public int? BaseKey { get; set; }
        public string BPName { get; set; }
        public decimal? CashAmt { get; set; }
        public decimal? VoucherAmt { get; set; }
        public decimal? CCardAmt { get; set; }
        public decimal? MCardAmt { get; set; }
        public decimal? PointAmt { get; set; }
        public string UserId { get; set; }
        public string ShiftTime { get; set; }
        public DateTime? DocDate { get; set; }

    }

    public class Cash_Info_View
    {
        public DateTime? BeginTime { get; set; }
        public decimal? SalesAmount { get; set; }
        public decimal? CurentBalace { get; set; }
        public decimal? Amount { get; set; }
    }

    #region Business Partner
    public class BusinessPartner_View
    {
        [Key, Column(Order = 0)]
        public string BPCode{ get; set; }

        public string BPName{ get; set; }

        public string BPFrgnName{ get; set; }

        public System.Nullable<short> BPGrpCode{ get; set; }

        public string BPType{ get; set; }

        public string Phone1{ get; set; }

        public string Phone2{ get; set; }

        public string Gender{ get; set; }

        public System.Nullable<System.DateTime> Birthday{ get; set; }

        public string Email{ get; set; }

        public string Remarks{ get; set; }

        public System.Nullable<int> District{ get; set; }

        public System.Nullable<int> Province{ get; set; }

        public string Address{ get; set; }

        public string CntctPrsn{ get; set; }

        public string Fax{ get; set; }

        public string Website{ get; set; }

        public string MobilePhone{ get; set; }

        public string FatherCard{ get; set; }

        public string MemberCard{ get; set; }

        //public System.Nullable<decimal> Mileage{ get; set; }

        public string IdNum{ get; set; }

        public System.Nullable<char> IsForeigner{ get; set; }

        public System.Nullable<bool> Inactive{ get; set; }

        public System.Nullable<int> PriceList{ get; set; }

        public string StoreCode{ get; set; }

        public System.Nullable<System.DateTime> CreatedDate{ get; set; }

        public string CreatedBy{ get; set; }

        public System.Nullable<System.DateTime> UpdatedDate{ get; set; }

        public string UpdatedBy{ get; set; }

        public System.Nullable<char> isSync{ get; set; }

        public string SyncBy{ get; set; }

        public System.Nullable<System.DateTime> SyncDate{ get; set; }

        public string FirstName{ get; set; }

        public string MiddleName{ get; set; }

        public string LastName{ get; set; }

        public string AddNo{ get; set; }

        public string Street{ get; set; }

        public string Ward{ get; set; }

        public string MemberStatus{ get; set; }

        public string MemberType{ get; set; }

        public string MemberLevel{ get; set; }

        public System.Nullable<double> MemberPoints{ get; set; }

        public System.Nullable<double> LoyaltyPoints{ get; set; }
        public System.Nullable<decimal> Revenue { get; set; }


    }


    //Business Partner-Contact Person
    public class BPContactPerson_View
    {
        public string CardCode { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string Position { get; set; }//
        public string Address { get; set; }
        public string Tel1 { get; set; }
        public string Tel2 { get; set; }//
        public string Cellolar { get; set; }//  
        public string Fax { get; set; }//
        public string E_Mail { get; set; }//  
        public char? Active { get; set; }// 
    }

    //Business Partner-Address
    public class BPAddress_View
    {
        public string CardCode { get; set; }
        public string Address { get; set; }
        public string Street { get; set; }
        public string Block { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string County { get; set; }//
        public string State { get; set; }
        public string Country { get; set; }
        public string StreetNo { get; set; }//
        public string Building { get; set; }// 
        public char? AdresType { get; set; }
    }

    //Business Partner-Bank
    public class BPBank_View
    {
        public string CardCode { get; set; }
        public string BankCode { get; set; }
        public string Account { get; set; }
        public string AcctName { get; set; }
        public string SwiftNum { get; set; }
        public string Branch { get; set; }
        public string Country { get; set; }
    }

    #endregion

    //Cost Centers
    public class CostCenter_View
    {
        public string PrcCode { get; set; }
        public string PrcName { get; set; }
        public DateTime? EffectiveFrom { get; set; }
    }

    //Warehouse
    public class Warehouse_View
    {
        public string WhsCode { get; set; }
        public string WhsName { get; set; }
        public int? Location { get; set; }
        public string Location_Description { get; set; }
        public string BalInvntAc { get; set; }  //  inventory account-stock account
        public string SaleCostAc { get; set; }  //  Cost of Goods Sold
        public string TransferAc { get; set; }  //  transfer account-allocation account
        public string PriceDifAc { get; set; }  //  Price Difference Account
    }

    //Currency
    public class Currency_View
    {
        public string CurrCode { get; set; }
        public string CurrName { get; set; }
        public string ChkName { get; set; }
        public string Chk100Name { get; set; }
        public string DocCurrCod { get; set; }
        public string FrgnName { get; set; }
        public string F100Name { get; set; }
        public short? RoundSys { get; set; }
        public short? Decimals { get; set; }
        public char? RoundPym { get; set; }
        public string ISOCurrCod { get; set; }
        public decimal? MaxInDiff { get; set; }
        public decimal? MaxOutDiff { get; set; }
        public decimal? MaxInPcnt { get; set; }
        public decimal? MaxOutPcnt { get; set; }
        public char? Locked { get; set; }

    }

    public class ChartOfAccount_View
    {
        public string AcctCode { get; set; }
        public string AcctName { get; set; }
        public char? Finanse { get; set; }//    Cash Account
        public char? LocManTran { get; set; }  //Control Account
        public char? ActType { get; set; } //Account Type
        public char? Postable { get; set; }    //Title hoặc Active Account
        public string FrgnName { get; set; }
        public short? GroupMask { get; set; }   //Assets or Liabilities or Turnover ...
        public short? Levels { get; set; }
        public string FatherNum { get; set; }

    }

    //Manufacture
    public class Manufacture_View
    {
        public short FirmCode { get; set; }
        public string FirmName { get; set; }
    }

    //Manufacture
    public class PriceList_View
    {
        public int ListNum { get; set; }
        public string ListName { get; set; }
        public decimal? Factor { get; set; }
    }

    #region DOCUMENT

    public class Document_View
    {
        public int DocKey { get; set; }
        public string DocType { get; set; }
        public string DocNum { get; set; }
        public string DocStatus { get; set; }
        public string BaseDocNum { get; set; }
        public DateTime? DocDate { get; set; }
        public DateTime? DueDate { get; set; }
        public string BPCode { get; set; }
        public string BPName { get; set; }
        public decimal? DisPercent { get; set; }
        public decimal? DisAmount { get; set; }
        public decimal? TaxAmount { get; set; }
        public decimal? TotalAmount { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public int? BaseKey { get; set; }
        public int? ShiftId { get; set; }
        public string StoreCode { get; set; }
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
        public string StorePhone { get; set; }
        public decimal? Mileage { get; set; }
        public bool isVIK { get; set; }
        public string FromWhs { get; set; }
        public string ToWhs { get; set; }
        public string Remarks { get; set; }
        public string PosNo { get; set; }
        public Boolean isChoose { get; set; }
        public string EventCode { get; set; }
        public string DocStatusDesc { get; set; }

        public decimal? ReceiptAmount { get; set; }
        public string EventType { get; set; }

        public decimal? ReceiptAmt { get; set; }
        public decimal? ChangeAmt { get; set; }

        public string CardNum { get; set; }
        public string Address { get; set; }

        public int? PriceList { get; set; }
        public string PListName { get; set; }


        public string CardType { get; set; }
        public string GradeName { get; set; }
        public decimal? Balance { get; set; }
        public decimal? Remain { get; set; }
        public decimal? TotalMileage { get; set; }
        public char? isSync { get; set; }
        public string isSyncDesc { get; set; }
        public decimal? tiNiPoint { get; set; }
        public string AmountString { get; set; }
        public string Title { get; set; }
        public decimal? EventAmount { get; set; }
        public decimal? Deposited { get; set; }//Cho form in sb
        public decimal Proportion_VND_tXu { get; set; }

        public string BPFirstName { get; set; }
        public string BPMiddleName { get; set; }
        public string BPLastName { get; set; }
        public string BPAddNo { get; set; }
        public string BPStreet { get; set; }
        public string BPWard { get; set; }
        public string BPPhone { get; set; }
        public string BPEmail { get; set; }
        public DateTime? BPBirthday { get; set; }
        public string BPGenderCode { get; set; }
        public string BPDistrictCode { get; set; }
        public string BPProvinceCode { get; set; }
        public bool? IssueInvoice { get; set; }
        public string InvCustName { get; set; }
        public string InvAddress { get; set; }
        public string InvTaxCode { get; set; }
        public string PMCode { get; set; }
        public string ReasonCode { get; set; }
        public string RefOriginNo { get; set; }
        public decimal? MemDiscPer { get; set; }
        public decimal? MemDiscAmt { get; set; }
        public decimal? CreDiscPer { get; set; }
        public decimal? CreDiscAmt { get; set; }

        public bool? SalesOnline { get; set; }
        public string DeliveryType { get; set; }
        public string DeliveryTypeDesc { get; set; }
        public string TransporterCode { get; set; }
        public string TransporterName { get; set; }

        public string TrackNo { get; set; }
        public string ReportFooter { get; set; }
        public string DocRemarks { get; set; }
        public string RefVoucher { get; set; }
        public bool? Canceled { get; set; }

        public string RefundReasonCode { get; set; }
        public string OnlineOrderNo { get; set; }
    }

    //Sale Data Lines
    public class DocumentLines_View
    {
        public string StoreCode { get; set; }
        public int DocKey { get; set; }
        public DateTime? DocDate { get; set; }
        public DateTime? DueDate { get; set; }
        public string DocStatus { get; set; }
        public string DocStatusDesc { get; set; }
        public string DocRemarks { get; set; }
        public int LineId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string UoM { get; set; }
        public decimal? ApproveQty { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? BasicQuantity { get; set; }
        public decimal? RemainQuantity { get; set; }
        public decimal? ReceiptQty { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? FullPrice { get; set; }
        public decimal? DisPercent { get; set; }
        public decimal? AmountAfDis { get; set; }
        public string VatCode { get; set; }
        public decimal? LineTotal { get; set; }
        public string BarCode { get; set; }
        public string WhsCode { get; set; }
        public string FromWhs { get; set; }
        public string ToWhs { get; set; }
        public int? BaseLine { get; set; }
        public int? BaseKey { get; set; }
        public Boolean isChoose { get; set; }

        /// <summary>
        /// Inventory Counting
        /// </summary>
        public decimal? InstockQty { get; set; }
        public decimal? ActualQty { get; set; }
        public decimal? DifferenceQty { get; set; }
        public bool isCounting { get; set; }
        public string Message { get; set; }

        /// for Header
        /// 
        public int? PriceList { get; set; }
        public string PListName { get; set; }
        public string RefVoucher { get; set; }// Số phiếu chứng từ lấy từ SAP

        /// <summary>
        /// Nganh Hang
        /// </summary>
        public string ParentCode { get; set; }
        public string BatchNumber { get; set; }
        public decimal? OriginalQty { get; set; }
        

        //JUNO
        public string ItmsGrpCod1 { get; set; }
        public string ItmsGrpNam1 { get; set; }
        public string ItmsGrpCod2 { get; set; }
        public string ItmsGrpNam2 { get; set; }
        public string ItmsGrpCod3 { get; set; }
        public string ItmsGrpNam3 { get; set; }
        public string ColorCode { get; set; }
        public string ColorName { get; set; }
        public string Size { get; set; }
        //retial
        public string Color { get; set; }

        public string BrandCode { get; set; }
        public string BrandName { get; set; }
        public string MaterialCode { get; set; }
        public string MaterialName { get; set; }
        public string TypeCode { get; set; }
        public string PMCode { get; set; }
        public string ReasonCode { get; set; }
        public string RefOriginNo { get; set; }
        public decimal? DisAmount { get; set; }

        public decimal? OpenQty { get; set; }
        public string LineStatus { get; set; }
        public string LineStatusDesc { get; set; }
        public string DisType { get; set; }
        public string itemType { get; set; }

    }

    #endregion

    public class Configuration_View
    {
        //Connection
        public string CONNECTTYPE { get; set; }
        public string SERVER { get; set; }
        public string DATABASE { get; set; }
        public string DBUSER { get; set; }
        public string DBPASS { get; set; }
        public string LICENSENAME { get; set; }
        public string SAPUSER { get; set; }
        public string SAPPASS { get; set; }
        public string SERVERTYPE { get; set; }

        //Interval
        public int INTERVAL { get; set; }

    }

    public class Log_View
    {
        public string UserId { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Time { get; set; }
        public string Description { get; set; }
        public string MessageType { get; set; }
        public bool Read { get; set; }
    }

    public class FieldInfo_View
    {
        public string TableName { get; set; }
        public int FieldId { get; set; }
        public string FieldName { get; set; }
        public string Description { get; set; }
        public char? Type { get; set; }
        public char? EditType { get; set; }
        public short? Size { get; set; }
        public short? EditSize { get; set; }
    }

    public class DataType_View
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class Card_View
    {
        public string CardNum { get; set; }
        public string CardType { get; set; }
        public string CardTypeName { get; set; }
        public string GradeCode { get; set; }
        public string GradeName { get; set; }

        public string BPCode { get; set; }
        public string BPName { get; set; }
        public string AcctCode { get; set; }

        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public char? Inactive { get; set; }
        public decimal? Balance { get; set; }
        public decimal? AccBalance { get; set; }
        public decimal? Remain { get; set; }
        public decimal? Mileage { get; set; }
        public string StoreCode { get; set; }
        public string ImpError { get; set; }
        public string status { get; set; }


    }

    public class MCardTrans_View
    {
        public int Id { get; set; }
        public string StoreCode { get; set; }
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
        public string StorePhone { get; set; }

        public string DocNum { get; set; }
        public string CardNum { get; set; }
        public DateTime? TransDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string TransCashType { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Bonus { get; set; }
        public string CardType { get; set; }
        public string PosNo { get; set; }
        public string UserId { get; set; }
        public int? ShiftId { get; set; }
        public decimal? ReceiptAmt { get; set; }
        public string DocType { get; set; }
        public string BPCode { get; set; }
        public string BPName { get; set; }
        public decimal? Total { get; set; }
        public string GradeName { get; set; }
        public decimal? Balance { get; set; }
        public decimal? BeBalance { get; set; } //So du truoc
        public string DocTypeDesc { get; set; }
        public string Address { get; set; }
        public decimal Proportion_VND_tXu { get; set; }
    }

    public class AdditionSettingsHeader_View
    {
        public int HeaderId { get; set; }
        public string Titile { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? Todate { get; set; }
    }

    public class AdditionSettingsLine_View
    {
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
        public string CardType { get; set; }
        public int LineId { get; set; }
        public decimal? FromAmount { get; set; }
        public decimal? ToAmount { get; set; }
        public decimal? AdditionValue { get; set; }
        public decimal? AdditionPercent { get; set; }
        public Boolean InActive { get; set; }
        public int HeaderID { get; set; }
    }

    public class AdditionSettingsAll_View
    {
        public string GradeCode { get; set; }
        public string CardType { get; set; }
        public int LineId { get; set; }
        public decimal? FromAmount { get; set; }
        public decimal? ToAmount { get; set; }
        public decimal? AdditionValue { get; set; }
        public decimal? AdditionPercent { get; set; }
        public Boolean InActive { get; set; }
        public int HeaderId { get; set; }
        public string Titile { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? Todate { get; set; }
    }
    public class ShiftReportView
    {
        public string CashireId { get; set; }
        public string CashireName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public decimal CashIn { get; set; }
        public decimal CashOut { get; set; }
        public decimal Cash { get; set; }
        public DateTime TranTime { get; set; }
    }
    public class Card_Customer_Info_View
    {
        public string CardNum { get; set; }
        public string CardType { get; set; }
        public string BPCode { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public char? Inactive { get; set; }
        public decimal? Balance { get; set; }
        public decimal? AccBalance { get; set; }
        public decimal? Remain { get; set; }
        public string BPName { get; set; }
    }

    public class Card_SalesReport_View
    {
        public DateTime? DocDate { get; set; }
        public DateTime? Time { get; set; }
        public string ReceiptNo { get; set; }
        public int DocKey { get; set; }
        public string CardNum { get; set; }
        public string CardType { get; set; }
        public string TranCashType { get; set; }
        public string EventCode { get; set; }
        public string EventType { get; set; }
        public string group_key { get; set; }


        public decimal? Amount { get; set; }
        public decimal? Voucher { get; set; }
        public decimal? Point { get; set; }
        public int? BaseId { get; set; }

        public int? ShiftId { get; set; }
        public string UserId { get; set; }
        public string TransType { get; set; }
        public double bonusvalue { get; set; }
        public double actualvalue { get; set; }
        public string state { get; set; }
        public string description { get; set; }
        public string TransTypeCode { get; set; }
        public string StoreCode { get; set; }
        public string StoreName { get; set; }
        public bool isNotPos { get; set; }
        public double Balance { get; set; }
        public string CardName { get; set; }
        
    }





    public class CashInOut_View
    {
        public int Id { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? DocDate { get; set; }
        public string PosNo { get; set; }
        public string UserId { get; set; }
        public string TransCashType { get; set; }
        public decimal Amount { get; set; }
        public int ShiftId { get; set; }
        public string StoreCode { get; set; }
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
        public string StorePhone { get; set; }
    }

    public class Event_View
    {

        public string EventCode { get; set; }
        public string EventStatus { get; set; }
        public string BPCode { get; set; }
        public string BPName { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public decimal? Amount { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string EventType { get; set; }
        public string EventTypeDes { get; set; }
        public decimal? Balance { get; set; }
        public Boolean? Inactive { get; set; }
        public string Description { get; set; }
        public string StoreCode { get; set; }
        public string isSync { get; set; }
        public string CardNum { get; set; }
        public string EventStatusDesc { get; set; }

    }


    public class EventPays_View
    {
        public int DocKey { get; set; }
        public DateTime? DocDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public string BPCode { get; set; }
        public string BPName { get; set; }
        public string EventCode { get; set; }
        public string DocStatus { get; set; }
        public string Remark { get; set; }
        public decimal? Amount { get; set; }

        public string DocStatusDescr { get; set; }

    }

    public class Reason_View
    {
        public string ReasonCode { get; set; }
        public string Description { get; set; }
    }

    public class Pos_View
    {
        public string PosCode { get; set; }
        public string PosName { get; set; }
        public string HardwareId { get; set; }
        public string StoreCode { get; set; }
        public string PortCom { get; set; }
        public bool Inactive { get; set; }
        public string PrinterName { get; set; }
    }

    [Serializable]
    public class AdditionMileage_View
    {
        public string GradeCode { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public decimal? Amount { get; set; }
        public decimal? AddMileage { get; set; }
        public bool Inactive { get; set; }
    }

    public class MGrade_View
    {
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
        public decimal? VIPGradePoint { get; set; }
        public decimal? VIKGradePoint { get; set; }
        public string CardCode { get; set; }
    }

    public class MGrade_View2
    {
        public string GradeCode { get; set; }
        public string GradeName { get; set; }
    }


    public class ConfigurationInfo_View
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }

    public class APosUser_View
    {
        public string UserId { get; set; }
        public string PosCode { get; set; }
        public string PosName { get; set; }
        public bool Choose { get; set; }
    }

    public class APosGItem_View
    {
        public string PosCode { get; set; }
        public string ItmsGrpCod { get; set; }
        public string ItmsGrpName { get; set; }
        public bool Choose { get; set; }
    }

    public class APosItem_View
    {
        public string StoreCode { get; set; }
        public string PosCode { get; set; }
        public string ItmsGrpCod1 { get; set; }
        public string ItmsGrpName1 { get; set; }
        public string ItmsGrpCod2 { get; set; }
        public string ItmsGrpName2 { get; set; }
        public string ItmsGrpCod3 { get; set; }
        public string ItmsGrpName3 { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public bool Choose { get; set; }
    }

    public class MileageSetting_View
    {
        public string CardType { get; set; }
        public string GradeCode { get; set; }
        public int Mileage { get; set; }
    }

    public class POSNumList_View
    {
        public string PosCode { get; set; }
        public string PosName { get; set; }
        public int Value { get; set; }
        public bool? isPoint { get; set; }
    }

    public class Caption_view
    {
        //New Common
        public String PosForItmGroup { get; set; }
        public String PosCode { get; set; }
        public String PosName { get; set; }
        public String GroupCode { get; set; }
        public String GroupName { get; set; }

        public String PosForUsers { get; set; }

        public String EventBirthday { get; set; }
        public String ExtsAmount { get; set; }
        public String DepositAmount { get; set; }
        public String BalanceAmount { get; set; }
        public String Deposit { get; set; }
        public String AddChild { get; set; }
        public String AddItem { get; set; }
        public String FinishEvent { get; set; }
        public String ImportFile { get; set; }

        public String GoodsIssList { get; set; }
        public String CancelIss { get; set; }

        public String GoodsRcptList { get; set; }
        public String CancelRcpt { get; set; }

        public String Reconcile { get; set; }
        public String Couting { get; set; }

        public String InvCountingList { get; set; }
        public String RequestDate { get; set; }

        public String RequestQty { get; set; }
        public String CloseRequest { get; set; }
        public String InvTrsfRqstList { get; set; }

        public String GoodsIssEventList { get; set; }
        public String BarCode { get; set; }
        public String DefaultWhs { get; set; }
        public String WhsName { get; set; }
        public String OnHand { get; set; }

        public String PosForNumberList { get; set; }
        public String IsPoint { get; set; }

        public String ReportsParameter { get; set; }
        public String StoreName { get; set; }

        public String PosList { get; set; }
        public String PrinterName { get; set; }
        public String HardwareId { get; set; }

        public String RcptForEventList { get; set; }

        public String RcptFrmTrnsitList { get; set; }

        public String DepositList { get; set; }

        public String ShipmentList { get; set; }

        public String TrsftiNiTourList { get; set; }

        public String TrsfToTrnsit { get; set; }
        public String TrsfToTrnsitList { get; set; }

        public String CashInOut { get; set; }
        public String CurrentCashBalance { get; set; }
        public String SalesAmt { get; set; }
        public String BeginAtTimeIn { get; set; }
        public String CashInAmt { get; set; }
        public String CashOutAmt { get; set; }
        public String SalesHistory { get; set; }
        public String ReceiptNo { get; set; }
        public String BaseOn { get; set; }
        public String Point { get; set; }
        public String ShiftReport { get; set; }
        public String BeginDate { get; set; }
        public String ShiftId { get; set; }
        public String DocTotal { get; set; }
        public String CashAmt { get; set; }
        public String VoucherAmt { get; set; }
        public String CrCardAmt { get; set; }
        public String MCardAmt { get; set; }
        public String CouponAmt { get; set; }
        public String Topup { get; set; }
        public String ReceiptAmt { get; set; }
        public String ChangeAmt { get; set; }
        public String Total { get; set; }
        public String Refund { get; set; }
        public String RefundCard { get; set; }
        public String PosScreen { get; set; }
        public String TopupHistory { get; set; }
        public String ClearScreen { get; set; }
        public String EndShift { get; set; }
        public String isSync { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public String OpenItem { get; set; }
        public String History { get; set; }
        public String RePrint { get; set; }
        public String RePrintCanBill { get; set; }

        public String CancelBill { get; set; }
        public String EnterBarcode { get; set; }
        public String CategoryName { get; set; }
        public String SubTotal { get; set; }
        public String Remain { get; set; }
        public String CreditCard { get; set; }
        public String Voucher { get; set; }
        public String Coupon { get; set; }
        public String Cash { get; set; }
        public String MCard { get; set; }
        public String Complete { get; set; }
        public String DeleteItem { get; set; }
        public String DeleteCard { get; set; }
        public String SalesData { get; set; }
        public String CrCardNo { get; set; }
        public String Bank { get; set; }
        public String BankName { get; set; }
        public String tiNiStore { get; set; }
        public String tiNiPoint { get; set; }


        public String Configuration { get; set; }
        public String SerivceInterval { get; set; }
        public String Second { get; set; }
        public String Server { get; set; }
        public String LicenseName { get; set; }
        public String Database { get; set; }
        public String DBUser { get; set; }
        public String DBPass { get; set; }
        public String ServerType { get; set; }
        public String CompnyUser { get; set; }
        public String CompnyPass { get; set; }
        public String TestConnect { get; set; }
        public String CusFather { get; set; }
        public String PhoneNo { get; set; }
        public String IDNumber { get; set; }

        public String ImportCard { get; set; }

        public String Import { get; set; }
        public String NewCustomer { get; set; }
        public String CustomerInfo { get; set; }

        public String InitialSetting { get; set; }
        public String Initial { get; set; }
        public String Install { get; set; }
        public String InitialFuncList { get; set; }
        public String SyncSetting { get; set; }
        public String Value { get; set; }
        public String Description { get; set; }
        public String AddtionMileage { get; set; }
        public String VIPMileageSetting { get; set; }
        public String VIKMileageSetting { get; set; }

        public String Keyboard { get; set; }
        public String Date { get; set; }
        public String Time { get; set; }
        public String User { get; set; }
        public String Error { get; set; }
        public String Warning { get; set; }
        public String Information { get; set; }
        public String SystemMessageLog { get; set; }
        public String LastMessageDisplay { get; set; }

        public String ShowLog { get; set; }
        public String Manual { get; set; }
        public String Automatic { get; set; }
        public String Source { get; set; }
        public String WorkingFile { get; set; }
        public String WorkingPath { get; set; }

        public String OnServer { get; set; }
        public String MachineCode { get; set; }
        public String MachineName { get; set; }
        public String HostName { get; set; }
        public String TempPath { get; set; }
        public String BackupPath { get; set; }
        public String FptServer { get; set; }

        public String RunManual { get; set; }
        public String SourceGroup { get; set; }
        public String SourceTable { get; set; }
        public String Run { get; set; }

        public String ScheduleType { get; set; }
        public String OneTimeOccurence { get; set; }
        public String Frequence { get; set; }
        public String Occurs { get; set; }
        public String RecursEvery { get; set; }
        public String DaysOrWeeksOn { get; set; }

        public String Monday { get; set; }
        public String Tuesday { get; set; }
        public String Wednesday { get; set; }
        public String Thursday { get; set; }
        public String Friday { get; set; }
        public String Saturday { get; set; }
        public String Sunday { get; set; }

        public String DailyFrequency { get; set; }
        public String OccurOneAt { get; set; }
        public String OccurEvery { get; set; }
        public String StartAt { get; set; }
        public String StartEnd { get; set; }
        public String Duration { get; set; }
        public String StartDate { get; set; }
        public String EndDate { get; set; }
        public String NoEndDate { get; set; }
        public String Summary { get; set; }

        public String ConnectonInfo { get; set; }
        public String ServiceLog { get; set; }
        public String Hide { get; set; }
        public String DataSyncLog { get; set; }
        public String Start { get; set; }
        public String Stop { get; set; }
        public String Restart { get; set; }
        public String ServiceManager { get; set; }

        /// <summary>
        /// 
        /// </summary>



        public String ItemInValid { get; set; }
        public String Meassage { get; set; }
        public String FilterBy { get; set; }


        public String RePassword { get; set; }
        public String UserGroup { get; set; }
        public String CashierId { get; set; }


        public String AdditionValue { get; set; }
        public String AdditionPercent { get; set; }
        public String LineId { get; set; }
        public String Grade { get; set; }
        public String CardType { get; set; }
        public String GradeCode { get; set; }
        public String GradeName { get; set; }
        public String Inactive { get; set; }
        public String AdditionSettingDetail { get; set; }
        public String Deny { get; set; }
        public String ReadOnly { get; set; }
        public String FullControl { get; set; }
        public String Group { get; set; }
        public String Store { get; set; }
        public String StoreCode { get; set; }
        public String CardNum { get; set; }
        public String Mileage { get; set; }
        public String CancelTopup { get; set; }
        public String Enter { get; set; }
        public String Balance { get; set; }

        //Common 1                             
        public String Title { get; set; }
        public String Detail { get; set; }
        public String Search { get; set; }
        public String ItemSearch { get; set; }
        public String CustomerSearch { get; set; }
        public String Clear { get; set; }
        public String Add { get; set; }
        public String AddNew { get; set; }
        public String Save { get; set; }
        public String SaveAsDraft { get; set; }
        public String Lock { get; set; }
        public String Edit { get; set; }
        public String Delete { get; set; }
        public String View { get; set; }
        public String ViewCancel { get; set; }
        public String Ok { get; set; }
        public String Cancel { get; set; }
        public String Close { get; set; }
        public String Back { get; set; }
        public String List { get; set; }
        public String Creation { get; set; }
        public String Confirm { get; set; }
        public String Remarks { get; set; }
        public String Status { get; set; }
        public String DocStatus { get; set; }
        public String ConfirmStatus { get; set; }
        public String Print { get; set; }
        public String Choose { get; set; }
        public String Apply { get; set; }
        public String Exit { get; set; }
        public String FunctionRights { get; set; }
        public String ReportRights { get; set; }

        //Common 2
        public String Code { get; set; }
        public String Name { get; set; }
        public String Period { get; set; }
        public String Distributor { get; set; }
        public String DistributorCode { get; set; }
        public String DistributorName { get; set; }
        public String MinValue { get; set; }
        public String Bonus { get; set; }
        public String RedInvNo { get; set; }
        public String Serial { get; set; }
        public String DocType { get; set; }
        public String Ward { get; set; }


        //Common 3
        public String Birthday { get; set; }
        public String Province { get; set; }
        public String CityProvince { get; set; }
        public String District { get; set; }
        public String Address { get; set; }
        public String AddressDetail { get; set; }
        public String Gender { get; set; }
        public String Fax { get; set; }
        public String Email { get; set; }
        public String Website { get; set; }
        public String Area { get; set; }
        public String Street { get; set; }
        public String Market { get; set; }
        public String StreetMarket { get; set; }

        //Number
        public String DocKey { get; set; }
        public String DocNum { get; set; }
        public String RefVoucher { get; set; }

        //Paymnet
        public String InvoiceNo { get; set; }
        public String InvoiceAmt { get; set; }
        public String PaidAmount { get; set; }
        public String PendingAmount { get; set; }
        public String ThisTimePayment { get; set; }
        public String TotalPayment { get; set; }

        //Purchasing
        public String Promotion { get; set; }
        public String PromotionType { get; set; }
        public String PromotionCode { get; set; }
        public String Vendor { get; set; }
        public String VendorCode { get; set; }
        public String VendorName { get; set; }
        public String DocNumber { get; set; }
        public String OrderType { get; set; }
        public String CreditLimit { get; set; }
        public String ChooseOpenPO { get; set; }
        public String PODate { get; set; }
        public String PONo { get; set; }
        public String Reason { get; set; }
        public String PaymentType { get; set; }

        //Sales
        public String Customer { get; set; }
        public String CustomerCode { get; set; }
        public String CustomerName { get; set; }
        public String ShipTo { get; set; }
        public String BillTo { get; set; }
        public String ApplyPromotion { get; set; }
        public String SODate { get; set; }
        public String SONo { get; set; }

        //Item
        public String ItemCode { get; set; }
        public String ItemName { get; set; }
        public String ItemType { get; set; }
        public String Color { get; set; }
        public String Size { get; set; }
        public String Material { get; set; }
        public String Quantity { get; set; }
        public String UnitPrice { get; set; }
        public String Discount { get; set; }
        public String DiscountPer { get; set; }
        public String DiscountAmt { get; set; }
        public String TaxCode { get; set; }
        public String TaxRate { get; set; }
        public String Amount { get; set; }
        public String Percent { get; set; }
        public String InStockQty { get; set; }
        public String Warehouse { get; set; }
        public String OrderQty { get; set; }
        public String OpenQty { get; set; }
        public String ReceiptQty { get; set; }
        public String DeliveryQty { get; set; }
        public String CreditQty { get; set; }
        public String ActualQty { get; set; }
        public String DiffQty { get; set; }
        public String ApproveQty { get; set; }
        public String LineTotal { get; set; }

        //Total
        //public String Total { get; set; }
        public String TotalBefDiscount { get; set; }
        public String TotalDiscount { get; set; }
        public String TotalAfDiscount { get; set; }
        public String VatAmount { get; set; }
        public String TotalAmount { get; set; }

        //Date
        public String PostingDate { get; set; }
        public String DeliveryDate { get; set; }
        public String ReceiptDate { get; set; }
        public String DueDate { get; set; }
        public String InvoiceDate { get; set; }
        public String DocumentDate { get; set; }
        public String DraftDate { get; set; }
        public String ConfirmDate { get; set; }
        public String FromDate { get; set; }
        public String ToDate { get; set; }

        //Phone
        public String Phone { get; set; }
        public String Phone1 { get; set; }
        public String Phone2 { get; set; }
        public String MobilePhone { get; set; }
        public String Telephone { get; set; }

        //Group Menu                                    
        public String System { get; set; }
        public String MasterData { get; set; }
        public String Purchasing { get; set; }
        public String Sales { get; set; }
        public String Inventory { get; set; }
        public String Reports { get; set; }
        public String UserManagement { get; set; }
        public String AdditionSetting { get; set; }
        public String Authorization { get; set; }
        public String TopupFree { get; set; }
        public String CardManagement { get; set; }
        public String InvTrsfRqst { get; set; }
        public String RcptFrmTrnsit { get; set; }
        public String GoodsRcpt { get; set; }
        public String GoodsIss { get; set; }
        public String InvCounting { get; set; }
        public String TrsftiNiTour { get; set; }
        public String Shipment { get; set; }
        public String Event { get; set; }
        public String EventBirth { get; set; }
        public String EventList { get; set; }

        public String EventTO { get; set; }
        public String GoodsIssEvent { get; set; }
        public String GoodsRcptEvent { get; set; }
        public String RcptVoucher { get; set; }
        public String Items { get; set; }
        public String BusinessPartner { get; set; }
        public String Users { get; set; }
        public String Pos { get; set; }
        public String Transaction { get; set; }
        public String EndDay { get; set; }
        public String PosUser { get; set; }
        public String PosItmGrp { get; set; }
        public String PosNumberLst { get; set; }
        public String GeneralSalesRpt { get; set; }
        public String StockRpt { get; set; }

        //Logon
        public String Logon { get; set; }
        public String Logout { get; set; }
        public String ChangePass { get; set; }
        public String Password { get; set; }
        public String UserId { get; set; }
        public String Language { get; set; }
        public String CardNo { get; set; }

        //Change password
        public String UserName { get; set; }
        public String CurrentPass { get; set; }
        public String NewPass { get; set; }
        public String ReNewPass { get; set; }

        //Page
        public String First { get; set; }
        public String Last { get; set; }
        public String Previous { get; set; }
        public String Next { get; set; }
        public String PageSize { get; set; }

        //
        public String Suspend { get; set; }
        public String Recall { get; set; }
        public String SalesOnline { get; set; }
        public String ClearCustomer { get; set; }
        public String Notes { get; set; }
    }
}
