﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataTools.Models.Entity
{
    public class Promotion
    {
        public string sapId { get; set; }
        public SubName name { get; set; }
        public SubName remarks { get; set; }
        public int pax { get; set; }
        public string type { get; set; }
        public DateTime effectiveStartDate { get; set; }
        public DateTime effectiveEndDate { get; set; }
        public DateTime eventStartDate { get; set; }
        public DateTime eventEndDate { get; set; }
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string Remarks1 { get; set; }
        public string Remarks2 { get; set; }

    }

    public class PromotionPaging
    {
        public Paging pagination { get; set; }
        public List<Promotion> data { get; set; }
    }
}