﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Serialization;

namespace DataTools.Models.Entity
{
    [DataContract(Namespace = "")]
    public class Customer
    {
        //public string sapId { get; set; }
        //public string name { get; set; }
        //public string email { get; set; }
        //public string phone { get; set; }
        //public string address { get; set; }
        //public string federalTaxId { get; set; }
        //public DateTime createdDate { get; set; }
        [DataMember]
        public string SalesforceID { get; set; }

        [DataMember]
        public string CardCode { get; set; }

        [DataMember]
        public string CardName { get; set; }

        [DataMember]
        public string TaxCode { get; set; }

        [DataMember]
        public string CardTaxName { get; set; }

        [DataMember]
        public string TaxAdress { get; set; }

        [DataMember]
        [Description("<b>Get list Customer Group from url:<a href='#'>/api/CustomerGroups</a></b>")]
        /// <summary>
        /// Get list Customer Group from url: /api/CustomerGroups
        /// </summary>
        public string GroupCode { get; set; }
        [DataMember]
        public string Phone { get; set; }
        [DataMember(Name = "Location")]
        [Description("<b>Location code in 'S' and 'H'</b>")]
        /// <summary>
        /// Location code in 'S' and 'H'
        /// </summary>
        public string LocationCode { get; set; }
        [DataMember]
        [Description("<b>Get list Sale Employee from url: <a href='#'>/api/SaleEmployees</a> </b>")]
        /// <summary>
        /// Get list Sale Employee from url: /api/SaleEmployees
        /// </summary>
        public string EmpCode { get; set; }
        /// <summary>
        /// Territory
        /// </summary>
        public int Territory { get; set; }
        /// <summary>
        /// OwnerCode
        /// </summary>
        public int OwnerCode { get; set; }
    }
  
    public class CustomerPaging
    {
        public Paging pagination { get; set; }
        public List<Customer> data { get; set; }
    }
    
}