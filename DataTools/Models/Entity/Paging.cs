﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTools.Models.Entity
{
    public class Paging
    {
        public int pageNum { get; set; }
        public int perPage { get; set; }
        public int totalRecord { get; set; }

    }
}
