﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataTools.Models.Entity
{
    //public class EventBooking
    //{
    //    public string sapId { get; set; }
    //    public string customerId { get; set; }
    //    public string contactPersonId { get; set; }
    //    public string staffId { get; set; }
    //    public DateTime startDate { get; set; }
    //    public DateTime endDate { get; set; }
    //    public string timeFrame { get; set; }
    //    public string startPrepareTime { get; set; }
    //    public string endPrepareTime { get; set; }
    //    public string status { get; set; }
    //    public double paymentTotal { get; set; }
    //    public double discount { get; set; }
    //    public itemSub1 themeDecor { get; set; }
    //    public itemSub1 performanceActivities { get; set; }
    //    public itemSub1 foodAndBeverage { get; set; }
    //    public List<string> promotionIds { get; set; }
    //    public List<subVenue> venues { get; set; }
    //}

    public class PackageEvent
    {
        public string sapId { get; set; }
        public SubName name { get; set; }
        public SubName desc { get; set; }
        public string type { get; set; }
        public double price { get; set; }
        public int quantity { get; set; }
        public string customerType { get; set; }
        public string program { get; set; }
        public string category { get; set; }
        public string group { get; set; }
        public string unit { get; set; }
        public string createdDate { get; set; }
        public string images { get; set; }
        public ItemSub_2 items { get; set; }
    }

    public class ItemSub_2
    {
        public string sapId { get; set; }
        public SubName name { get; set; }
        public SubName desc { get; set; }
        public string images { get; set; }
        public double price { get; set; }
        public double quantity { get; set; }
        public string customerType { get; set; }
        public string program { get; set; }
        public string category { get; set; }
        public bool isIndividual { get; set; }
        public bool isAdditional { get; set; }
        public string group { get; set; }
        public string variant { get; set; }
        public bool customItem { get; set; }
        public DateTime createdDate { get; set; }
    }

    public class ItemSub_1
    {
        public List<PackageEvent> packages { get; set; }
        public List<ItemSub_2> items { get; set; }
        public List<ItemSub_2> addMoreList { get; set; }
    }

    public class VenueEvent
    {
        public string venueId { get; set; }
        public List<string> timeFrames { get; set; }
        public DateTime startPrepareDate { get; set; }
        public DateTime endPrepareDate { get; set; }
        public string startPrepareTime { get; set; }
        public string endPrepareTime { get; set; }
        public string venueName { get; set; }
        public string venueGroup { get; set; }
    }

    public class EventBookingPaging
    {
        public Paging pagination { get; set; }
        public List<EventBooking> data { get; set; }
    }

    #region NEW
    public class EventBooking
    {
        public string sapId { get; set; }
        public string customerId { get; set; }
        public string contactPersonId { get; set; }
        public string staffId { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public DateTime createDate { get; set; }
        public string status { get; set; }
        public double paymentTotal { get; set; }
        public double discount { get; set; }
        public string type { get; set; }
        public ItemSub_1 themeDecor { get; set; }
        public ItemSub_1 performanceActivities { get; set; }
        public ItemSub_1 foodAndBeverage { get; set; }
        public List<VenueEvent> venues { get; set; }
        public double numOfGuest { get; set; }
        public string note { get; set; }
        public string additionalServices { get; set; }
        public PromotionEvent promotion { get; set; }
    }

    public class PromotionEvent
    {
        public string discount { get; set; }
        public List<string> promotionList { get; set; }
        public string request { get; set; }

    }
    #endregion
}


