﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataTools.Models.Entity
{
    public class Staff
    {
        public string sapId { get; set; }
        public string fullName { get; set; }
        public string position { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string role { get; set; }

    }

    public class StaffPaging
    {
        public Paging pagination { get; set; }
        public List<Staff> data { get; set; }
    }
}