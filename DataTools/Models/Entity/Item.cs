﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace DataTools.Models.Entity
{
    public class Item
    {
        public string sapId { get; set; }
        public SubName name { get; set; }
        public SubName desc { get; set; }
        public int price { get; set; }
        public string category { get; set; }
        public string group { get; set; }
        public string variant { get; set; }
        public bool isIndividual { get; set; }
        public string unit { get; set; }
        public string status { get; set; }
        [IgnoreDataMember]
        public bool customItem { get; set; }
        public DateTime createdDate { get; set; }
        [IgnoreDataMember]
        public string Name1 { get; set; }
        [IgnoreDataMember]
        public string Name2 { get; set; }

       
    }
    public class ItemPaging
    {
        public Paging pagination { get; set; }
        public List<Item> data { get; set; }
    }

}