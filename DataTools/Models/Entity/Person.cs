﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataTools.Models.Entity
{
    public class Person
    {
        public string sapId { get; set; }
        public string fullName { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string gender { get; set; }
        public string customerId { get; set; }
        public DateTime? birthday { get; set; }
        public DateTime? createdDate { get; set; }

    }
    public class PersonPaging
    {
        public Paging pagination { get; set; }
        public List<Person> data { get; set; }
    }
}