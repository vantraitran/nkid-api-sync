﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataTools.Models.Entity
{
    public class Venue
    {
        public string sapName { get; set; }
        public string sapId { get; set; }
        public string line { get; set; }
        public string group { get; set; }      
    }

    public class VenuePaging
    {
        public Paging pagination { get; set; }
        public List<Venue> data { get; set; }
    }
}