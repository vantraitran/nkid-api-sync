﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataTools.Models.Entity
{
    public class Package
    {
        public string sapId { get; set; }
        public SubName name { get; set; }
        public SubName desc { get; set; }
        public float price { get; set; }
        public string category { get; set; }
        public string group { get; set; }
        public bool isIndividual { get; set; }
        public string unit { get; set; }
        public string variant { get; set; }
        public bool customItem { get; set; }
        public DateTime createdDate { get; set; }

        //item id   
        List<string> itemlds { get; set; }
        public string Name1 { get; set; }
        public string Name2 { get; set; }
    }

    public class PackagePaging
    {
        public Paging pagination { get; set; }
        public List<Package> data { get; set; }
    }
}