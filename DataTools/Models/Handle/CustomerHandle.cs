﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataTools.Models.Entity;
using PagedList;
using SAPbobsCOM;

namespace DataTools.Models.Handle
{
    public class CustomerHandle
    {
        ApiDbContext db = new ApiDbContext();
        public CustomerPaging GetAll(int pageNum, int perPage, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("Sp_Customer_GetAll");
                var list1 = db.Database.SqlQuery<Customer>(sql).ToList();
                var list = list1.ToPagedList(pageNum, perPage).ToList();
                var data = new CustomerPaging();
                data.pagination = new Paging() { pageNum = pageNum, perPage = perPage, totalRecord = list1.Count };
                data.data = list;
                return data;
            }
            catch (Exception ex)
            {
                message = "[Get all Customer]" + ex.Message;
            }

            return null;
        }
        public CustomerPaging GetId(string id, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("Sp_Customer_GetId @id={0}", id);
                var list = db.Database.SqlQuery<Customer>(sql).ToList();
                var data = new CustomerPaging();
                data.pagination = null;
                data.data = list;
                return data;
            }
            catch (Exception ex)
            {
                message = "[Get Id Customer]" + ex.Message;
            }

            return null;
        }

        public CustomerPaging Post(Customer value, out string message)
        {
            message = "";
            try
            {
                CustomerPaging data = new CustomerPaging();

                //CREATE BUSINESS PARTNER IN SAP
                SAPAccess.CreateObject sap = new SAPAccess.CreateObject();
                sap.CreateBussinessPartners(value,null, out message);


                data.data = new List<Customer>() { value };
                return data;
            }
            catch (Exception ex)
            {

                message = "[Post Customer]" + ex.Message;
            }

            return null;
        }

        public CustomerPaging Put(string id, Customer value, out string message)
        {
            message = "";
            try
            {
                CustomerPaging data = new CustomerPaging();
                //// action
                //CREATE BUSINESS PARTNER IN SAP
                SAPAccess.CreateObject sap = new SAPAccess.CreateObject();
                sap.CreateBussinessPartners(value, null, out message);
                //
                data.data = new List<Customer>() { value };
                return data;
            }
            catch (Exception ex)
            {
                message = "[Put Customer]" + ex.Message;
            }
            return null;
        }
    }
}
