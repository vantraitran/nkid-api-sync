﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataTools.Models.Entity;
using PagedList;

namespace DataTools.Models.Handle
{
    public class VenueHandle
    {
        ApiDbContext db = new ApiDbContext();
        public VenuePaging GetAll(int pageNum, int perPage, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("Sp_Venue_GetAll");
                var list1 = db.Database.SqlQuery<Venue>(sql).ToList();
                var list = list1.ToPagedList(pageNum, perPage).ToList();
                var data = new VenuePaging();
                data.pagination = new Paging() { pageNum = pageNum, perPage = perPage, totalRecord = list1.Count };
                data.data = list;
                return data;
            }
            catch (Exception ex)
            {
                message = "[Get all Venue]" + ex.Message;
            }

            return null;
        }
    }
}
