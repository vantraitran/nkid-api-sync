﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataTools.Models.Entity;
using PagedList;

namespace DataTools.Models.Handle
{
    public class PromotionHandle
    {
        ApiDbContext db = new ApiDbContext();
        public PromotionPaging GetAll(int pageNum, int perPage, DateTime? createdDateFrom, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("Sp_Promotion_GetAll @createdDate='{0}'", createdDateFrom.HasValue ? createdDateFrom.Value.ToString("yyyy-MM-dd") : "1900-01-01");
                var list = db.Database.SqlQuery<Promotion>(sql).ToList();
                var count = list.Count;
                list = list.ToPagedList(pageNum, perPage).ToList();
                SubName sub = null;
                foreach (var item in list)
                {
                    sub = new SubName();
                    sub.vi = item.Name1;
                    sub.en = item.Name2;
                    item.name = sub;

                    sub = new SubName();
                    sub.vi = item.Remarks1;
                    sub.en = item.Remarks2;
                    item.remarks = sub;

                }
                var data = new PromotionPaging();
                data.pagination = new Paging() { pageNum = pageNum, perPage = perPage, totalRecord = count };
                data.data = list;
                return data;
            }
            catch (Exception ex)
            {
                message = "[Get all Promotion]" + ex.Message;
            }

            return null;
        }
        public PromotionPaging GetId(string id, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("Sp_Promotion_GetId @id={0}", id);
                var list = db.Database.SqlQuery<Promotion>(sql).ToList();
                SubName sub = null;
                foreach (var item in list)
                {
                    sub = new SubName();
                    sub.vi = item.Name1;
                    sub.en = item.Name2;
                    item.name = sub;

                    sub = new SubName();
                    sub.vi = item.Remarks1;
                    sub.en = item.Remarks2;
                    item.remarks = sub;

                }
                var data = new PromotionPaging();
                data.pagination = null;
                data.data = list;
                return data;
            }
            catch (Exception ex)
            {
                message = "[Get Id Promotion]" + ex.Message;
            }

            return null;
        }



    }
}

