﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataTools.Models.Entity;

namespace DataTools.Models.Handle
{
    public class EventBookingHandle
    {
        ApiDbContext db = new ApiDbContext();
        

        public EventBookingPaging Post(EventBooking value, out string message)
        {
            message = "";
            try
            {
                EventBookingPaging data = new EventBookingPaging();

                data.data = new List<EventBooking>() { value };
                SAPAccess.CreateObject sap = new SAPAccess.CreateObject();
                sap.CreateSalesQuotation(value, out message);
                

                    return data;

            }
            catch (Exception ex)
            {

                message = "[Post EventBooking]" + ex.Message;
            }

            return null;
        }

        public EventBookingPaging Put(string  id, EventBooking value, out string message)
        {
            message = "";
            try
            {
                EventBookingPaging data = new EventBookingPaging();
                data.data = new List<EventBooking>() { value };
                SAPAccess.CreateObject sap = new SAPAccess.CreateObject();
                sap.CreateSalesQuotation(value, out message);
                return data;
            }
            catch (Exception ex)
            {
                message = "[Put EventBooking]" + ex.Message;
            }
            return null;
        }
    }
}
