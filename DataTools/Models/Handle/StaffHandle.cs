﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataTools.Models.Entity;
using PagedList;

namespace DataTools.Models.Handle
{
    public class StaffHandle
    {
        ApiDbContext db = new ApiDbContext();
        public StaffPaging GetAll(int pageNum, int perPage, DateTime? createdDateFrom, out string message)
        {
            message = "";
            int total = 0;
            try
            {
                string sql = string.Format("Sp_Staff_GetAll @createdDate='{0}'",createdDateFrom.HasValue?createdDateFrom.Value.ToString("yyyy-MM-dd"):"0001-01-01");
                var list1 = db.Database.SqlQuery<Staff>(sql).ToList();
                total = list1.Count();
                var list = list1.ToPagedList(pageNum, perPage).ToList();
                var data = new StaffPaging();
                data.pagination = new Paging() { pageNum = pageNum, perPage = perPage, totalRecord = total };
                data.data = list;
                return data;
            }
            catch (Exception ex)
            {
                message = "[Get all Staff]" + ex.Message;
            }

            return null;
        }
        public StaffPaging GetId(string  id, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("Sp_Staff_GetId @id={0}", id);
                var list = db.Database.SqlQuery<Staff>(sql).ToList();
                var data = new StaffPaging();
                data.data = list;
                return data;
            }
            catch (Exception ex)
            {
                message = "[Get Id Staff]" + ex.Message;
            }

            return null;
        }

    }
}
