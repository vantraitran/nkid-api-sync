﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataTools.Models.Entity;
using PagedList;

namespace DataTools.Models.Handle
{
    public class ItemHandle
    {
        ApiDbContext db = new ApiDbContext();
        public ItemPaging GetAll(int pageNum, int perPage, DateTime? createdDateFrom, out string message)
        {
            message = "";
            int total = 0;
            try
            {
                string sql = string.Format("Sp_Item_GetAll @createdDate='{0}'", createdDateFrom.HasValue ? createdDateFrom.Value.ToString("yyyy-MM-dd") : "0001-01-01");
                var list1 = db.Database.SqlQuery<Item>(sql).ToList();
                total = list1.Count();
                var list = list1.ToPagedList(pageNum, perPage).ToList();
                SubName sub = null;
                foreach (var item in list)
                {
                    sub = new SubName();
                    sub.vi = item.Name1;
                    sub.en = item.Name2;
                    item.name = sub;
                }
                var data = new ItemPaging();
                data.pagination = new Paging() { pageNum = pageNum, perPage = perPage, totalRecord = total };
                data.data = list;
                return data;
            }
            catch (Exception ex)
            {
                message = "[Get all Item]" + ex.Message;
            }

            return null;
        }
        public ItemPaging GetId(string id, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("Sp_Item_GetId @id={0}", id);
                var list = db.Database.SqlQuery<Item>(sql).ToList();
                SubName sub = new SubName();
                foreach (var item in list)
                {
                    sub.vi = item.Name1;
                    sub.en = item.Name2;
                    item.name = sub;
                }
                var data = new ItemPaging();
                data.pagination = null;
                data.data = list;
                return data;
            }
            catch (Exception ex)
            {
                message = "[Get Id Item]" + ex.Message;
            }

            return null;
        }

        public ItemPaging Post(Item value, out string message)
        {
            message = "";
            try
            {
                ItemPaging data = new ItemPaging();
                //value.createdDate = DateTime.Now;
                //data.data = new List<Customer>() { value };
                data.data.Add(value);

                return data;
            }
            catch (Exception ex)
            {

                message = "[Post Item]" + ex.Message;
            }

            return null;
        }

        public ItemPaging Put(string id, Item value, out string message)
        {
            message = "";
            try
            {
                ItemPaging data = new ItemPaging();
                //// action
                //
                //
                //
                data.data = new List<Item>() { value };
                return data;
            }
            catch (Exception ex)
            {
                message = "[Put Item]" + ex.Message;
            }
            return null;
        }
    }
}

