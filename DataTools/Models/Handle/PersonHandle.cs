﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataTools.Models;
using DataTools.Models.Entity;
using DataTools.Models.Handle;
using PagedList;

namespace DataTools.Models.Handle
{
    public class PersonHandle
    {
        ApiDbContext db = new ApiDbContext();
        public PersonPaging GetAll(int pageNum, int perPage, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("Sp_Person_GetAll");
                var list1 = db.Database.SqlQuery<Person>(sql).ToList();
                var list = list1.ToPagedList(pageNum, perPage).ToList();
                var data = new PersonPaging();
                data.pagination = new Paging() { pageNum = pageNum, perPage = perPage, totalRecord = list1.Count };
                data.data = list;
                return data;
            }
            catch (Exception ex)
            {
                message = "[Get all Person]" + ex.Message;
            }

            return null;
        }
        public PersonPaging GetId(string id, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("Sp_Person_GetId @id={0}", id);
                var list = db.Database.SqlQuery<Person>(sql).ToList();
                var data = new PersonPaging();
                data.pagination = null;
                data.data = list;
                return data;
            }
            catch (Exception ex)
            {
                message = "[Get Id Person]" + ex.Message;
            }

            return null;
        }

        public PersonPaging Post(Person value, out string message)
        {
            message = "";
            try
            {
                PersonPaging data = new PersonPaging();
                // ADD CONTACT PERSON
                SAPAccess.CreateObject sap = new SAPAccess.CreateObject();
                if (sap.CreateContactPersion(value, out message))
                {
                    string res = "";
                    try
                    {
                        string sql = string.Format("select convert(varchar(20),CntctCode) from OCPR where Name='{0}'", value.fullName);
                        res = db.Database.SqlQuery<string>(sql).FirstOrDefault();
                        value.sapId = res;
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    
                    
                    data.data = new List<Person>() { value };

                    return data;
                }
            }
            catch (Exception ex)
            {

                message = "[Post ContactPerson]" + ex.Message;
            }

            return null;
        }

        public PersonPaging Put(string id, Person value, out string message)
        {
            message = "";
            try
            {
                PersonPaging data = new PersonPaging();
                // ADD CONTACT PERSON
                SAPAccess.CreateObject sap = new SAPAccess.CreateObject();
                sap.CreateContactPersion(value, out message);
                data.data = new List<Person>() { value };

                return data;
            }
            catch (Exception ex)
            {
                message = "[Putv ContactPerson]" + ex.Message;
            }
            return null;
        }
    }
}
