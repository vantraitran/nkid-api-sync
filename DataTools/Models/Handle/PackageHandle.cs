﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataTools.Models.Entity;
using PagedList;

namespace DataTools.Models.Handle
{
    public class PackageHandle
    {
        ApiDbContext db = new ApiDbContext();
        public PackagePaging GetAll(int pageNum, int perPage,DateTime? createdDateFrom, out string message)
        {
            message = "";
            int total = 0;
            try
            {
                string sql = string.Format("Sp_Package_GetAll @createdDate='{0}'", createdDateFrom.HasValue ? createdDateFrom.Value.ToString("yyyy-MM-dd") : "0001-01-01");
                var list1 = db.Database.SqlQuery<Package>(sql).ToList();
                total = list1.Count();
                var list = list1.ToPagedList(pageNum, perPage).ToList();
                SubName sub = null;
                foreach (var item in list)
                {
                    sub = new SubName();
                    sub.vi = item.Name1;
                    sub.en = item.Name2;
                    item.name = sub;
                }
                var data = new PackagePaging();
                data.pagination = new Paging() { pageNum = pageNum, perPage = perPage, totalRecord =total};
                data.data = list;
                return data;
            }
            catch (Exception ex)
            {
                message = "[Get all Package]" + ex.Message;
            }

            return null;
        }
        public PackagePaging GetId(string id, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("Sp_Package_GetId @id={0}", id);
                var list = db.Database.SqlQuery<Package>(sql).ToList();
                SubName sub = null;
                foreach (var item in list)
                {
                    sub = new SubName();
                    sub.vi = item.Name1;
                    sub.en = item.Name2;
                    item.name = sub;
                }
                var data = new PackagePaging();
                data.pagination = null;
                data.data = list;
                return data;
            }
            catch (Exception ex)
            {
                message = "[Get Id Package]" + ex.Message;
            }

            return null;
        }

        public PackagePaging Post(Package value, out string message)
        {
            message = "";
            try
            {
                PackagePaging data = new PackagePaging();
                //value.createdDate = DateTime.Now;
                data.data = new List<Package>() { value };

                return data;
            }
            catch (Exception ex)
            {

                message = "[Post Package]" + ex.Message;
            }

            return null;
        }

        public PackagePaging Put(string id, Package value, out string message)
        {
            message = "";
            try
            {
                PackagePaging data = new PackagePaging();
                //// action
                //
                //
                //
                data.data = new List<Package>() { value };
                return data;
            }
            catch (Exception ex)
            {
                message = "[Put Package]" + ex.Message;
            }
            return null;
        }
    }
}

