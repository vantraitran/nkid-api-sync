﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DataTools.Models
{
    public class SyncDataObjectLine
    {
        public string SourceCode { get; set; }
        public string XmlElement { get; set; }
        public DataTable DataSource { get; set; }
        public string HeaderKey { get; set; }
        public string LineRefenceKey { get; set; }
        public string SourceName { get; set; }
        public string ObjectQuery { get; set; }
    }

    public class SyncObjectCurrent
    {
        public bool IsServer { get; set; }
        public string FtpHostName { get; set; }
        public string FtpUserId { get; set; }
        public string FtpPassword { get; set; }
        public string MachineCode { get; set; }
        public string StoreCode { get; set; }
        public string TempPath { get; set; }
        public string BackupPath { get; set; }
        public DateTime SyncDate { get; set; }
        public string SyncBy { get; set; }//UserId 
        public string SyncDescription { get; set; }
        public bool IsManual { get; set; }
        public bool SyncIN { get; set; }

        public string SyncType { get; set; }
        public string SyncGroup { get; set; }
        public string SyncGroupName { get; set; }
        public string SyncSource { get; set; }
        public string SyncSourceName { get; set; }
        public string SyncSourcePrefixName { get; set; }
        public string WorkingPath { get; set; }
        public string WorkingFiles { get; set; }
        public string SyncStatus { get; set; }
        public string SyncStatusDesc { get; set; }
        public bool IsDeleteBefore { get; set; }
        public bool IsOnlyInsert { get; set; }
        public bool IsHeader { get; set; }
        public bool IsLine { get; set; }
        public string XmlElement { get; set; }
        public DateTime? LastedSyncDate { get; set; }

        public string ObjectType { get; set; }
        public string KeyUpdate { get; set; }
        public object DataPreview { get; set; }

        public string SqlScriptOut { get; set; }
        public string ObjectQuery { get; set; }
        public bool IsObjectDraft { get; set; }
        public string TransactionNotification { get; set; }
        public List<SyncDataObjectLine> DataLinesPreview { get; set; }

        public bool isRunning { get; set; }
    }

    public class SysConfLocal_View
    {
        public string FtpHostName { get; set; }
        public string FtpUserId { get; set; }
        public string FtpPassword { get; set; }
        public string MachineCode { get; set; }
        public string MachineName { get; set; }
        public string TempPath { get; set; }
        public string BackupPath { get; set; }
        public bool IsServer { get; set; }
        public string StoreCode { get; set; }
    }

    public class SyncType_View
    {
        public string TypeCode { get; set; }
        public string TypeName { get; set; }
        public string Remarks { get; set; }
        public bool SyncIN { get; set; }
        public bool Inactive { get; set; }
    }

    public class SyncGroup_View
    {
        public string GroupCode { get; set; }
        public string TypeCode { get; set; }
        public string GroupName { get; set; }
        public string Remarks { get; set; }
        public string udfTypeName { get; set; }
        public bool Inactive { get; set; }
    }

    public class SyncSource_View
    {
        public string udfTypeCode { get; set; }
        public string udfTypeName { get; set; }
        public string SourceCode { get; set; }
        public string SourceName { get; set; }
        public string GroupCode { get; set; }
        public string udfGroupName { get; set; }
        public string SourceIN { get; set; }
        public string SourceOUT { get; set; }
        public string Remarks { get; set; }
        public DateTime? LastedSyncDate { get; set; }
        public string XmlPrefixName { get; set; }
        //public short ServerType { get; set; }
        public bool IsDeleteBefore { get; set; }
        public bool IsOnlyInsert { get; set; }
        public bool IsHeader { get; set; }
        public bool IsLine { get; set; }

        //public bool IsSapAddon { get; set; }
        //public string SapObject { get; set; }
        //public string SapVersion { get; set; }

        /// <summary>
        /// SAP-->ObjType
        /// Acumatica-->....????
        /// </summary>
        public string ObjectType { get; set; }

        public string XmlElement { get; set; }
        public string KeyUpdate { get; set; }

        public bool Inactive { get; set; }

        //2016-03-04
        public string ObjectQuery { get; set; }
        public bool IsObjectDraft { get; set; }
        public string TransactionNotification { get; set; }
    }

    public class SyncProperty_View
    {
        public string SourceCode { get; set; }
        public string udfSourceName { get; set; }
        public string udfSourceIN { get; set; }
        public string ColumnIN { get; set; }
        public string udfSourceOUT { get; set; }
        public string ColumnOUT { get; set; }
        public string Remarks { get; set; }
        public bool IsKey { get; set; }
        public bool NumericType { get; set; }
        public bool DateTimeType { get; set; }
        public bool BooleanType { get; set; }
        public bool StringType { get; set; }
        public bool Inactive { get; set; }
    }

    public class SyncSourceLevel_View
    {
        public string SourceHeader { get; set; }
        public string SourceLine { get; set; }
        public string XmlElement { get; set; }
        public string HeaderKey { get; set; }
        public string LineReferenceKey { get; set; }
        public bool Inactive { get; set; }
    }

    public class SyncCondition_View
    {
        public string SourceCode { get; set; }
        public string SourceColumn { get; set; }
        public string ConditionType { get; set; }
        public string ConditionValue { get; set; }
        public string ConditionType1 { get; set; }
        public string ConditionValue1 { get; set; }
        public string ConditionType2 { get; set; }
        public string ConditionValue2 { get; set; }
        public string ConditionType3 { get; set; }
        public string ConditionValue3 { get; set; }
        public string ConditionType4 { get; set; }
        public string ConditionValue4 { get; set; }
        public string ConditionType5 { get; set; }
        public string ConditionValue5 { get; set; }
        public string ConditionType6 { get; set; }
        public string ConditionValue6 { get; set; }
        public string ConditionType7 { get; set; }
        public string ConditionValue7 { get; set; }
        public bool Inactive { get; set; }
    }

    public class SyncQuery_View
    {
        public string SourceCode { get; set; }
        public string QueryString { get; set; }
        public bool Inactive { get; set; }
    }

    public class SyncSchedule_View
    {
        public string GroupCode { get; set; }//Primary
        public string udfGroupName { get; set; }//Primary
        public string ScheduleType { get; set; }//1-Run One Time; 2-Recurring Every
        public DateTime? OneTimeAtDate { get; set; }
        public TimeSpan? OneTimeAtTime { get; set; }
        public string FrequenceOccurs { get; set; }//D-Day; W-Week
        public int FrequenceOccursEvery { get; set; }
        public bool Mon { get; set; }
        public bool Tue { get; set; }
        public bool Wed { get; set; }
        public bool Thu { get; set; }
        public bool Fri { get; set; }
        public bool Sat { get; set; }
        public bool Sun { get; set; }
        public bool IsDailyOccursOnce { get; set; }
        public TimeSpan? DailyOccursOnceAt { get; set; }
        public int DailyOccursEvery { get; set; }
        public string TimeType { get; set; }//H-Hour; M-Minute; S-Second
        public TimeSpan? StartAtTime { get; set; }
        public TimeSpan? EndAtTime { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool NoEndDate { get; set; }
        public string Description { get; set; }
        public bool Inactive { get; set; }

    }

    public class SyncLog_View
    {
        public int SyncLogId { get; set; }
        public DateTime SyncDate { get; set; }
        public string SyncBy { get; set; }
        public string Description { get; set; }
        public bool IsManual { get; set; }
    }

    public class SyncLogLine_View
    {
        public int Id { get; set; }
        public int SyncLogId { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string SourceCode { get; set; }
        public string SourceName { get; set; }
        public string SyncStatus { get; set; }
        public string SyncStatusDesc { get; set; }
        public DateTime SyncDate { get; set; }
        public string Description { get; set; }
        public string WorkingPath { get; set; }
        public string WorkingFiles { get; set; }
    }

    public class Enum_View
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class SyncObject_View
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public bool IsSAPB1 { get; set; }
    }

    public class SyncMachine_View
    {
        public string MachineCode { get; set; }
        public string MachineName { get; set; }
        public string StoreCode { get; set; }
    }

}
