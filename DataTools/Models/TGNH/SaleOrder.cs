﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTools.Models.TGNH
{
    public class SaleOrder
    {
        public string SONum { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string Tinh_TP { get; set; }
        public string Quan_Huyen { get; set; }
        public DateTime? SODate { get; set; }
        public DateTime? SOConfirmDate { get; set; }
        public string Receiver { get; set; }
        public string ShipAddress { get; set; }
        public string ShipMobile { get; set; }
        public string Remarks { get; set; }
        public string BillNo { get; set; }
        public decimal? TotalAmount { get; set; }
        public string CreateBy { get; set; }
        public string EmpCreate { get; set; }
        public string EmpConfirm { get; set; }
        public string KenhBanHang { get; set; }
        public string OnlineOrderNo { get; set; }


        public List<SaleOrderLines> SOLines { get; set; }
        public List<SaleOrderPays> SOPays { get; set; }

    }

    public class SaleOrderLines
    {
        public string ItemCode { get; set; }
        public decimal? Quantity { get; set; }
        public string LineRemarks { get; set; }
        public decimal? Price { get; set; }
        public decimal? LineTotal { get; set; }
    }

    public class SaleOrderPays
    {
        public string PayType { get; set; }
        public string CreCardNo { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public string CreCardType { get; set; }
        public decimal? Amount { get; set; }
    }

   

    public class SaleOrderHandle
    {
        ApiDbContext db = new ApiDbContext();
        public SaleOrder GetSaleOrder(string CustomerCode, string Phone, string SONum, out string message)
        {

            message = "";
            try
            {
                SaleOrder result = new SaleOrder();
                
                //get header
                string sql = string.Format("[ICC_US_API_GET_SalesOrders] '{0}','{1}','{2}'", CustomerCode, Phone,SONum);
                result = db.Database.SqlQuery<SaleOrder>(sql).FirstOrDefault();

                //get lines
                result.SOLines = new List<SaleOrderLines>();
                sql = string.Format("[ICC_US_API_GET_SalesOrders_Lines] '{0}','{1}','{2}'", CustomerCode, Phone, SONum);
                result.SOLines.AddRange(db.Database.SqlQuery<SaleOrderLines>(sql).ToList());

                return result;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            
            return null;
        }

        public int PostSaleOrder(SaleOrder so, out string message)
        {
            db.Database.Connection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["CMSOnline"].ConnectionString;
            if (db.Database.Connection.State == System.Data.ConnectionState.Closed) db.Database.Connection.Open();
            var tran = db.Database.BeginTransaction();

            message = "";
            try
            {
                BusinessPartner bp = GetorInsertNew(so, out message);
                if (bp == null) return -1;

                DraftData draftData = new DraftData();
                draftData.SONum = so.SONum;
                draftData.BPCode = bp.BPCode;
                draftData.BPName = bp.BPName;
                draftData.BPPhone = bp.Phone1;
                draftData.BPAddNo = bp.Address;
                draftData.BPProvinceCode = bp.Province.ToString();
                draftData.BPDistrictCode = bp.District.ToString();
                draftData.SODate = so.SODate;
                draftData.SOConfirmDate = so.SOConfirmDate;
                draftData.DEReceiver = so.Receiver;
                draftData.DEAddr = so.ShipAddress;
                draftData.DEMobile = so.ShipMobile;
                draftData.DocRemarks = so.Remarks;
                draftData.BillNo = so.BillNo;
                draftData.SOCreateBy = so.CreateBy;
                draftData.EmpCreate = so.EmpCreate;
                draftData.EmpConfirm = so.EmpConfirm;
                draftData.KenhBanHang = so.KenhBanHang;
                //them moi
                draftData.OnlineOrderNo = so.OnlineOrderNo;
                //field mac dinh he thong
                var StoreCode = db.Database.SqlQuery<string>("select Value from Configuration where Name='StoreCode'").FirstOrDefault();
                draftData.RevenueStore = StoreCode;
                draftData.StoreCode = draftData.RevenueStore;
                var lastdockey= db.Database.SqlQuery<int>("select max(dockey) from DraftData").FirstOrDefault();
                draftData.DocKey = lastdockey+1;
                draftData.CreateBy = "webapi";
                draftData.CreateDate = DateTime.Today;
                draftData.DocStatus = "P";
                draftData.DocDate = DateTime.Today;

                draftData.POSOrderNo = draftData.DocKey + "webapi" + StoreCode;

                db.DraftDatas.Add(draftData);


                //lines
                Int16 i = 1;
                List<DraftDataLine> lines = new List<DraftDataLine>();
                foreach (var item in so.SOLines)
                {

                    DraftDataLine line = new DraftDataLine();
                    line.DocKey = lastdockey + 1;
                    line.RevenueStore = StoreCode;
                    line.StoreCode = line.RevenueStore;
                    line.WhsCode = line.StoreCode;
                    string s = string.Format("select ItemCode from Items where CodeBars='{0}'", item.ItemCode);
                    string getitemcode= db.Database.SqlQuery<string>(s).FirstOrDefault();
                    //line.ItemCode = item.ItemCode;
                    line.ItemCode = getitemcode;
                    line.Quantity = item.Quantity;
                    line.LineRemarks = item.LineRemarks;
                    line.UnitPrice = item.Price;
                    line.LineTotal = item.LineTotal;
                    line.LineId = i;


                    db.DraftDataLines.Add(line);
                    i++;
                }
                i = 1;
                //Pays
                List<DraftDataPay> pays = new List<DraftDataPay>();
                foreach (var item in so.SOPays)
                {
                    DraftDataPay pay = new DraftDataPay();
                    pay.DocKey = lastdockey + 1;
                    pay.RevenueStore = StoreCode;
                    pay.StoreCode = StoreCode;
                    pay.PayType = item.PayType;
                    pay.CreCardNo = item.CreCardNo;
                    pay.BankCode = item.BankCode;
                    pay.BankName = item.BankName;
                    pay.CreCardType = item.CreCardType;
                    pay.Amount = item.Amount;
                    pay.LineId = i;

                    db.DraftDataPays.Add(pay);
                    i++;
                }


                //db.DraftDatas.Add(draftData);
                //db.DraftDataLines.AddRange(lines);
                //db.DraftDataPays.AddRange(pays);
                db.SaveChanges();
                tran.Commit();
                

                return 0;
            }

            catch (Exception ex)
            {
                message = ex.Message;
            }
            tran.Rollback();
            return -1;
        }

        private BusinessPartner GetorInsertNew(SaleOrder so, out string message)
        {
            message = "";
            try
            {
            string sql = string.Format("select * from BusinessPartners with(nolock) where Phone1='{0}'", so.Mobile);
            var bp = db.Database.SqlQuery<BusinessPartner>(sql).FirstOrDefault();
                


                if (bp != null)
                    return bp;

                //khong co tao moi
                BusinessPartner bpnew = new BusinessPartner();
                bpnew.BPCode = BP_GetLastCusCode(out message);
                bpnew.BPName = so.CustomerName;
                bpnew.Phone1 = so.Mobile;
                bpnew.BPType = "C";
                bpnew.Province = Convert.ToInt32(so.Tinh_TP);
                bpnew.District = Convert.ToInt32(so.Quan_Huyen);
                bpnew.Address = so.Address;
                bpnew.CreatedDate = DateTime.Now;
                bpnew.CreatedBy = "webapi";
                bpnew.UpdatedDate = DateTime.Now.AddHours(2);
                bpnew.UpdatedBy = "webapi";
                bpnew.MemberType = "M";
                bpnew.MemberLevel = "Member";
                bpnew.StoreCode = StoreCode;

                db.BusinessPartners.Add(bpnew);

                db.SaveChanges();



            return db.Database.SqlQuery<BusinessPartner>(sql).FirstOrDefault();
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            message = "No Customer Found!";
            return null;
        }


        string StoreCode = "O001";

        public String BP_GetLastCusCode( out string message)
        {
            

            string sCuscode = "000000000000000000000000000000000";
            sCuscode = sCuscode.Substring(0, 9);
            //string StoreCode = "O001";

            try
            {
                message = "";
                string resuilt = null;
                    //var temp = db_temp.ExecuteQuery<BusinessPartner_View>(string.Format("select max(RIGHT(bpcode,{0})) as 'bpcode' from BusinessPartners where isnumeric(RIGHT(bpcode,{0}))=1", _docnum_lenght)).FirstOrDefault();
                    //khong can dung bptype
                    //var obj = db_temp.BusinessPartners.Where(o => o.BPCode.Contains(BaseUtil._storeCode + bptype.ToString())).ToList();
                    var obj = db.BusinessPartners.Where(o => o.BPCode.Contains(StoreCode)).ToList();
                    var temp = obj.Max(o => o.BPCode.Substring(o.BPCode.Length - 9, 9));
                    if (temp != null)
                    {
                        resuilt = temp;
                    }
                    if (resuilt != null && isNumeric(resuilt))
                    {
                        sCuscode = (Convert.ToInt32(resuilt) + 1).ToString();
                    }
                    else
                    {
                        sCuscode = (Convert.ToInt32(sCuscode) + 1).ToString();
                    }
                
            }
            catch (Exception ex)
            {
                message = ex.Message;
                sCuscode = (Convert.ToInt32(sCuscode) + 1).ToString();
            }
            //return BaseUtil._storeCode + bptype.ToString() + sCuscode;
            return StoreCode + sCuscode.PadLeft(9, '0');


        }


        public bool isNumeric(object s)
        {
            try
            {
                decimal d = Convert.ToDecimal(s);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
