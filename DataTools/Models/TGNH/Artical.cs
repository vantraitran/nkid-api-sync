﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTools.Models.TGNH
{
    public class Artical
    {
        public string IDWeb { get; set; }
        public string ItemCode { get; set; }
        public string BarCode { get; set; }
        public string ItemName { get; set; }
        public string Brand { get; set; }
        public string Gender { get; set; }
        public string Category { get; set; }
        public string GroupNotes { get; set; }
        public string Group { get; set; }
        public string Capacity { get; set; }
        public string NamPhatHanh { get; set; }
        public string NhaPhaChe { get; set; }
        public string MadeIn { get; set; }
        public string Size { get; set; }
        public decimal Price { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int Inactive { get; set; }

    }

    public class ArticalHandle
    {
        ApiDbContext db = new ApiDbContext();
        public List<Artical> GetArtical(DateTime updateDate, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("ICC_US_API_GET_ITEMS_ALL '{0}'", updateDate.ToShortDateString());
                var listArt = db.Database.SqlQuery<Artical>(sql).ToList();
                return listArt;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }


        public int UpdateArtical(Artical data, out string message)
        {
            message = "";
            try
            {
                //do update udf nen ko can goi sap
                //SAPAccess.CreateObject sap = new SAPAccess.CreateObject();
                //sap.UpdateArtical(data, out message);

                string sql = string.Format("ICC_US_API_POST_ITEM_UPDATE '{0}','{1}','{2}','{3}'", data.IDWeb, data.GroupNotes, data.NamPhatHanh, data.NhaPhaChe);
                int i =db.Database.ExecuteSqlCommand(sql);

                if(i==0)
                {
                    message = "IDWeb not exist.";
                    return -1;
                }

                return 0;
            }
            catch (Exception ex)
            {
                message = ex.Message;                
            }
            return -1;
        }
    }
}
