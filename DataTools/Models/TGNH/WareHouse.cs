﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTools.Models.TGNH
{
    public class WareHouse
    {
        public string WhsCode { get; set; }
        public string WhsName { get; set; }
        public string Address { get; set; }
    }

    public class WareHouseHandle
    {
        ApiDbContext db = new ApiDbContext();

        public List<WareHouse> GetWareHouse(out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("ICC_US_API_GET_WAREHOUSE");
                var listwh = db.Database.SqlQuery<WareHouse>(sql).ToList();
                return listwh;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                
            }
            return null;
        }
    }
}
