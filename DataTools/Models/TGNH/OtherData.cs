﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTools.Models.TGNH
{
    public class OtherData
    {
            public string Code { get; set; }
            public string Name { get; set; }
            public int Inactive { get; set; }
            public string Group { get; set; }
            public DateTime? CreateDate { get; set; }
            public DateTime? UpdateDate { get; set; }
    }

        public class OtherDataHandle
        {
            ApiDbContext db = new ApiDbContext();

            public List<OtherData> GetOtherData(out string message)
            {
                message = "";
                try
                {
                    string sql = string.Format("ICC_US_API_GET_OTHERDATA");
                    var lstotherdata = db.Database.SqlQuery<OtherData>(sql).ToList();
                    return lstotherdata;
                }
                catch (Exception ex)
                {
                    message = ex.Message;

                }
                return null;
            }
        }
    
}
