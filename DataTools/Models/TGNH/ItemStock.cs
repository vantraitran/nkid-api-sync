﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
namespace DataTools.Models.TGNH
{
    //[Newtonsoft.Json.JsonObject(Title = "root")]
    

    public class ItemStock
    {
        //[JsonProperty(TypeNameHandling = TypeNameHandling.None)]
        public string BarCode { get; set; }
        public string WhsCode { get; set; }
        public int Piece { get; set; }
    }
    public class ItemStock2
    {
        //[JsonProperty(TypeNameHandling = TypeNameHandling.None)]
        public string msg { get; set; }
        public string code { get; set; }
    }
    public class ItemStockHandle
    {
        ApiDbContext db = new ApiDbContext();
        public List<ItemStock> GetItemStock(string BarCode, int Type, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("ICC_US_API_GET_ITEMSTOCK '{0}',{1}", BarCode, Type);
                var result = db.Database.SqlQuery<ItemStock>(sql).ToList();
                return result;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }

        public List<ItemStock2> GetItemStock2(string BarCode, int Type, out string message)
        {
            message = "";
            try
            {
                string sql = string.Format("ICC_US_API_GET_ITEMSTOCK '{0}',{1}", BarCode, Type);
                var result = db.Database.SqlQuery<ItemStock2>(sql).ToList();
                
                return result;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return null;
        }
    }

}
