﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataTools.Models.TGNH
{
    public class Customer
    {
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string PersonId { get; set; }
        public string CardLevel { get; set; }
        public decimal Point { get; set; }
        public decimal LoyaltyPoint { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }

    public class CustomerHandle
    {
        ApiDbContext db = new ApiDbContext();
        public Customer GetCustomer(string CustomerCode,string Phone,out string message)
        {
            message = "";
            try
            {
                string sql = String.Format("ICC_US_API_GET_CUSTOMERS '{0}','{1}'", CustomerCode,Phone);
                var cus = db.Database.SqlQuery<Customer>(sql).FirstOrDefault();
                return cus;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return null;
        }
    }
}
