﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SAPbobsCOM;
using DataTools.Models;
using DataTools.Utility;

namespace DataTools.SAPAccess
{
    public class BaseSAPDI : BaseSAPData
    {
        public int _oError = 0;
        public string _oErrorMessage = "";
        public int _linenum = 0;
        public bool _isAddNew = false;

        static SAPbobsCOM.Company staticCompany_SAP;

        public SAPbobsCOM.Company oCompany_SAP;
        public SAPbobsCOM.CompanyService oCmpSrv;

        private SAPbobsCOM.Documents oDocument = null;
        private SAPbobsCOM.StockTransfer oInvTrfs = null;
        private SAPbobsCOM.InventoryCounting oInvCounting = null;
        private SAPbobsCOM.ProductionOrders oPrdctOder = null;

        private SAPbobsCOM.Payments oPayment = null;
        private SAPbobsCOM.JournalEntries oJE = null;
        private SAPbobsCOM.BusinessPartners oBP = null;

        private SAPbobsCOM.ProjectsService oServiceProj = null;
        private SAPbobsCOM.ProjectParams oProjParams = null;
        private SAPbobsCOM.Project oProj = null;

        public BaseSAPDI()
        {
            //Company_ATC();
            //Company_SAP();
        }

        public SAPbobsCOM.Company Company_SAP(out string message)
        {
            message = "";
            if (staticCompany_SAP == null || staticCompany_SAP.Connected == false)
            {
                var obj = RegConn_Get(DataConnection.SERVERDB);

                if (obj != null)
                {
                    staticCompany_SAP = new Company();

                    staticCompany_SAP.Server = obj.SERVER;
                    staticCompany_SAP.LicenseServer = obj.LICENSENAME;
                    staticCompany_SAP.DbPassword = obj.DBPASS;
                    staticCompany_SAP.CompanyDB = obj.DATABASE;

                    if (obj.SERVERTYPE == "MSSQL_2008")
                        staticCompany_SAP.DbServerType = BoDataServerTypes.dst_MSSQL2008;
                    else if (obj.SERVERTYPE == "MSSQL_2012")
                        staticCompany_SAP.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                    else if (obj.SERVERTYPE == "MSSQL_2014")
                        staticCompany_SAP.DbServerType = BoDataServerTypes.dst_MSSQL2014;
                    else if (obj.SERVERTYPE == "MSSQL_2016")
                    {
                        //staticCompany_SAP.DbServerType = BoDataServerTypes.dst_MSSQL2016;
                    }
                    else if (obj.SERVERTYPE == "HANADB")
                        staticCompany_SAP.DbServerType = BoDataServerTypes.dst_HANADB;

                    staticCompany_SAP.DbUserName = obj.DBUSER;
                    staticCompany_SAP.UserName = obj.SAPUSER;
                    staticCompany_SAP.Password = obj.SAPPASS;
                }
            }
            if (!staticCompany_SAP.Connected)
            {
                int i = staticCompany_SAP.Connect();
                staticCompany_SAP.GetLastError(out i, out message);
            }

            return staticCompany_SAP;
        }

        public int SAPConnect(string sapUser, string sapPass, out string message)
        {
            message = "";

            var oCompany = new Company();

            var server = System.Configuration.ConfigurationManager.AppSettings["SERVER"];
            var licenseServer = System.Configuration.ConfigurationManager.AppSettings["LICENSESERVER"];
            var serverType = System.Configuration.ConfigurationManager.AppSettings["SERVERTYPE"];
            var dbName = System.Configuration.ConfigurationManager.AppSettings["DBNAME"];
            var dbUser = System.Configuration.ConfigurationManager.AppSettings["DBUSER"];
            var dbPass = System.Configuration.ConfigurationManager.AppSettings["DBPASS"];
            //var sapUser = System.Configuration.ConfigurationManager.AppSettings["COMPANYUSER"];
            //var sapPass = System.Configuration.ConfigurationManager.AppSettings["COMPANYPASS"];

            oCompany.Server = server;
            oCompany.LicenseServer = licenseServer;
            oCompany.CompanyDB = dbName;
            oCompany.DbUserName = dbUser;
            oCompany.DbPassword = dbPass;

            if (serverType == "MSSQL_2008")
                oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2008;
            else if (serverType == "MSSQL_2012")
                oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
            else if (serverType == "MSSQL_2014")
                oCompany.DbServerType = BoDataServerTypes.dst_MSSQL2014;
            else if (serverType == "MSSQL_2016")
            {
                //oStaticCompany.DbServerType = BoDataServerTypes.dst_MSSQL2016;
            }
            else if (serverType == "HANADB")
                oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;

            oCompany.UserName = sapUser;
            oCompany.Password = sapPass;

            int i = 0;
            if (!oCompany.Connected)
            {
                i = oCompany.Connect();
                oCompany.GetLastError(out i, out message);
            }
            
            return i;
        }

        public SAPbobsCOM.BoYesNoEnum ConvertYesNo(object obj)
        {
            if (obj != null && obj.ToString() == "Y")
            {
                return BoYesNoEnum.tYES;
            }
            else
            {
                return BoYesNoEnum.tNO;
            }
        }

        public SAPbobsCOM.BoInventorySystem ConvertInventorySystem(object obj)
        {
            if (obj == null || obj.ToString() == "A")
            {
                return BoInventorySystem.bis_MovingAverage;
            }
            else if (obj != null && obj.ToString() == "S")
            {
                return BoInventorySystem.bis_Standard;
            }
            else
            {
                //  "S"
                return BoInventorySystem.bis_FIFO;
            }
        }

        public SAPbobsCOM.BoManageMethod ConvertManageMethod(object obj)
        {
            if (obj != null && obj.ToString() == "R")
            {
                return BoManageMethod.bomm_OnReleaseOnly;
            }
            else
            {
                return BoManageMethod.bomm_OnEveryTransaction;
            }
        }

        public SAPbobsCOM.BoAccountTypes ConvertAccountTypes(object obj)
        {
            if (obj == null || obj.ToString() == "I")
            {
                return BoAccountTypes.at_Revenues;
            }
            else if (obj != null && obj.ToString() == "E")
            {
                return BoAccountTypes.at_Expenses;
            }
            else
            {
                //  "N"
                return BoAccountTypes.at_Other;
            }
        }

        public SAPbobsCOM.BoGLMethods ConvertGLMethods(object obj)
        {
            if (obj == null || obj.ToString() == "W")
            {
                return BoGLMethods.glm_WH;
            }
            else if (obj != null && obj.ToString() == "C")
            {
                return BoGLMethods.glm_ItemClass;
            }
            else
            {
                //  "C"
                return BoGLMethods.glm_ItemLevel;
            }
        }

        public bool ConnectionSAP_Test(out string message)
        {
            message = "";
            try
            {
                var oCom = Company_SAP(out message);
                if (oCom.Connected) return true;

                return false;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }

        public void StartSapTransaction()
        {
            string message = "";
            oCompany_SAP = Company_SAP(out message);
            oCompany_SAP.StartTransaction();
        }

        public bool CommitSapTransaction(out string message)
        {
            message = "";
            try
            {
                if (oCompany_SAP.InTransaction)
                    oCompany_SAP.EndTransaction(BoWfTransOpt.wf_Commit);

                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }

        public void RollbackSapTransaction()
        {
            if (oCompany_SAP.InTransaction)
                oCompany_SAP.EndTransaction(BoWfTransOpt.wf_RollBack);
        }

        public bool CreateUDT(Company oCompany, Recordset oRecordset, string tableName, string tableDesc, List<FIELDINFO_VIEW> listField, out string message)
        {
            message = "";
            try
            {
                if (oRecordset == null)
                    oRecordset = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                oRecordset.DoQuery(string.Format("select 1 from sys.tables where name='@{0}'", tableName));
                if (oRecordset.RecordCount == 0)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
                    oRecordset = null;
                    GC.Collect();

                    var oUDT = (SAPbobsCOM.UserTablesMD)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables);
                    oUDT.TableName = tableName;
                    oUDT.TableDescription = tableDesc;
                    oUDT.TableType = BoUTBTableType.bott_NoObject;

                    // Adding the Field to the Table
                    int lRetCode = oUDT.Add();

                    // Check for errors
                    if (lRetCode != 0)
                    {
                        oCompany.GetLastError(out lRetCode, out message);
                    }
                    else
                    {
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oUDT);
                        oUDT = null;
                        GC.Collect();

                        CreateUDF(oCompany, oRecordset, listField, out message);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public void CreateUDF(Company oCompany, Recordset oRecordset, FIELDINFO_VIEW obj, out string message)
        {
            try
            {

                message = "";

                if (obj != null)
                {
                    if (oRecordset == null)
                        oRecordset = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                    oRecordset.DoQuery(string.Format("select TableID, FieldID, AliasID from CUFD where TableID='{0}' and AliasID='{1}'", obj.TableName, obj.FieldName));

                    var oUserFieldsMD = (SAPbobsCOM.UserFieldsMD)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields);


                    if (oRecordset.RecordCount > 0)
                    {
                        if (oUserFieldsMD.GetByKey(oRecordset.Fields.Item("TableID").Value, Convert.ToInt32(oRecordset.Fields.Item("FieldID").Value)))
                        {
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
                            oRecordset = null;
                            GC.Collect();

                            if (obj.isYesNo)
                            {
                                oUserFieldsMD.ValidValues.Value = "N";
                                oUserFieldsMD.ValidValues.Description = "No";
                                oUserFieldsMD.ValidValues.Add();


                                oUserFieldsMD.ValidValues.Value = "Y";
                                oUserFieldsMD.ValidValues.Description = "Yes";
                                oUserFieldsMD.ValidValues.Add();
                            }

                            oUserFieldsMD.Description = obj.Description;
                            if (obj.EditSize.HasValue)
                                oUserFieldsMD.EditSize = obj.EditSize.Value;

                            // Adding the Field to the Table
                            int lRetCode = oUserFieldsMD.Update();

                            // Check for errors
                            if (lRetCode != 0)
                            {
                                oCompany.GetLastError(out lRetCode, out message);
                            }
                        }
                    }
                    else
                    {
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
                        oRecordset = null;
                        GC.Collect();

                        oUserFieldsMD.Name = obj.FieldName;
                        oUserFieldsMD.Type = (SAPbobsCOM.BoFieldTypes)ConvertFieldType(obj.Type);
                        if (obj.Size.HasValue)
                            oUserFieldsMD.Size = obj.Size.Value;
                        oUserFieldsMD.Description = obj.Description;
                        //oUserFieldsMD.SubType = SAPbobsCOM.BoFldSubTypes.st_Image;
                        //oUserFieldsMD.LinkedTable = "UT1";
                        oUserFieldsMD.TableName = obj.TableName;
                        if (obj.EditSize.HasValue)
                            oUserFieldsMD.EditSize = obj.EditSize.Value;

                        if (obj.isYesNo)
                        {
                            oUserFieldsMD.ValidValues.Value = "N";
                            oUserFieldsMD.ValidValues.Description = "No";
                            oUserFieldsMD.ValidValues.Add();


                            oUserFieldsMD.ValidValues.Value = "Y";
                            oUserFieldsMD.ValidValues.Description = "Yes";
                            oUserFieldsMD.ValidValues.Add();
                        }

                        // Adding the Field to the Table
                        int lRetCode = oUserFieldsMD.Add();

                        // Check for errors
                        if (lRetCode != 0)
                        {
                            oCompany.GetLastError(out lRetCode, out message);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            oRecordset = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
        }

        public void CreateUDF(Company oCompany, Recordset oRecordset, List<FIELDINFO_VIEW> listField, out string message)
        {
            try
            {

                message = "";

                if (listField != null)
                {
                    foreach (var obj in listField)
                    {
                        if (oRecordset == null)
                            oRecordset = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                        oRecordset.DoQuery(string.Format("select TableID, FieldID, AliasID from CUFD where TableID='{0}' and AliasID='{1}'", obj.TableName, obj.FieldName));

                        var oUserFieldsMD = (SAPbobsCOM.UserFieldsMD)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields);


                        if (oRecordset.RecordCount > 0)
                        {
                            if (oUserFieldsMD.GetByKey(oRecordset.Fields.Item("TableID").Value, Convert.ToInt32(oRecordset.Fields.Item("FieldID").Value)))
                            {
                                System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
                                oRecordset = null;
                                GC.Collect();

                                oUserFieldsMD.Description = obj.Description;
                                if (obj.EditSize.HasValue)
                                    oUserFieldsMD.EditSize = obj.EditSize.Value;


                                if (obj.isYesNo)
                                {
                                    oUserFieldsMD.ValidValues.Value = "N";
                                    oUserFieldsMD.ValidValues.Description = "No";
                                    oUserFieldsMD.ValidValues.Add();


                                    oUserFieldsMD.ValidValues.Value = "Y";
                                    oUserFieldsMD.ValidValues.Description = "Yes";
                                    oUserFieldsMD.ValidValues.Add();
                                }

                                // Adding the Field to the Table
                                int lRetCode = oUserFieldsMD.Update();

                                // Check for errors
                                if (lRetCode != 0)
                                {
                                    oCompany.GetLastError(out lRetCode, out message);
                                }
                            }
                        }
                        else
                        {
                            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRecordset);
                            oRecordset = null;
                            GC.Collect();

                            oUserFieldsMD.Name = obj.FieldName;
                            oUserFieldsMD.Type = (SAPbobsCOM.BoFieldTypes)ConvertFieldType(obj.Type);
                            if (obj.Size.HasValue)
                                oUserFieldsMD.Size = obj.Size.Value;
                            oUserFieldsMD.Description = obj.Description;
                            //oUserFieldsMD.SubType = SAPbobsCOM.BoFldSubTypes.st_Image;
                            //oUserFieldsMD.LinkedTable = "UT1";
                            oUserFieldsMD.TableName = obj.TableName;
                            if (obj.EditSize.HasValue)
                                oUserFieldsMD.EditSize = obj.EditSize.Value;


                            if (obj.isYesNo)
                            {
                                oUserFieldsMD.ValidValues.Value = "N";
                                oUserFieldsMD.ValidValues.Description = "No";
                                oUserFieldsMD.ValidValues.Add();


                                oUserFieldsMD.ValidValues.Value = "Y";
                                oUserFieldsMD.ValidValues.Description = "Yes";
                                oUserFieldsMD.ValidValues.Add();
                            }

                            // Adding the Field to the Table
                            int lRetCode = oUserFieldsMD.Add();

                            // Check for errors
                            if (lRetCode != 0)
                            {
                                oCompany.GetLastError(out lRetCode, out message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
        }

        public void RemoveUDF(Company oCompany, FIELDINFO_VIEW fld, out string message)
        {
            try
            {
                message = "";

                if (fld != null)
                {
                    var oUserFieldsMD = (SAPbobsCOM.UserFieldsMD)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields);

                    oUserFieldsMD.GetByKey(fld.TableName, fld.FieldId);

                    // Adding the Field to the Table
                    int lRetCode = oUserFieldsMD.Remove();

                    // Check for errors
                    if (lRetCode != 0)
                    {
                        oCompany.GetLastError(out lRetCode, out message);
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
        }

        public enum BoFieldTypes
        {
            db_Alpha = 0,
            db_Memo = 1,
            db_Numeric = 2,
            db_Date = 3,
            db_Float = 4,
        }

        public enum BoFldSubTypes
        {
            st_None = 0,
            st_Phone = 35,
            st_Percentage = 37,
            st_Address = 63,
            st_Link = 66,
            st_Image = 73,
            st_Measurement = 77,
            st_Price = 80,
            st_Quantity = 81,
            st_Rate = 82,
            st_Sum = 83,
            st_Time = 84,
        }

        public List<FIELDINFO_VIEW> UDF_GET(Company oCompany)
        {

            var oRecordset = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            List<FIELDINFO_VIEW> obj = null;

            oRecordset.DoQuery(string.Format("select * from CUFD"));

            if (oRecordset.RecordCount > 0)
            {
                obj = new List<FIELDINFO_VIEW>();

                while (oRecordset.EoF == false)
                {
                    obj.Add(
                        new FIELDINFO_VIEW()
                        {
                            TableName = oRecordset.Fields.Item("TableID").Value,
                            FieldName = oRecordset.Fields.Item("AliasID").Value,
                            FieldId = oRecordset.Fields.Item("FieldID").Value,
                            Description = oRecordset.Fields.Item("Descr").Value,
                            EditSize = oRecordset.Fields.Item("EditSize").Value,
                            Size = oRecordset.Fields.Item("SizeID").Value,
                            Type = oRecordset.Fields.Item("TypeID").Value,
                            EditType = oRecordset.Fields.Item("EditType").Value,
                        });

                    oRecordset.MoveNext();
                }
            }


            return obj;
        }

        public object BoFieldTypes_Get()
        {
            List<SMD_VIEW> listobj = new List<SMD_VIEW>();
            listobj.Add(new SMD_VIEW() { Code = "A", Name = BoFieldTypes.db_Alpha.ToString() });
            listobj.Add(new SMD_VIEW() { Code = "M", Name = BoFieldTypes.db_Memo.ToString() });
            listobj.Add(new SMD_VIEW() { Code = "N", Name = BoFieldTypes.db_Numeric.ToString() });
            listobj.Add(new SMD_VIEW() { Code = "D", Name = BoFieldTypes.db_Date.ToString() });
            listobj.Add(new SMD_VIEW() { Code = "B", Name = BoFieldTypes.db_Float.ToString() });

            return listobj;
        }

        public BoFieldTypes ConvertFieldType(object o)
        {
            var f = o.ToString();
            if (f == "M")
            {
                return BoFieldTypes.db_Memo;
            }
            else if (f == "N")
            {
                return BoFieldTypes.db_Numeric;

            }
            else if (f == "D")
            {
                return BoFieldTypes.db_Date;

            }
            else if (f == "B")
            {
                return BoFieldTypes.db_Float;

            }
            else
            {
                return BoFieldTypes.db_Alpha;
            }
        }
    }
}
