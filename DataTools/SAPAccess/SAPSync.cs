﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.Mail;

using DataTools.Models;
using DataTools.DataAccess;
using DataTools.DataAccess.Sync;

using SAPbobsCOM;

namespace DataTools.SAPAccess
{

    public class SAPSync : SyncBase
    {

        public int oError = 0;
        public string oErrorMessage = "";
        public int lineNum = 0;
        public bool isAddNew = false;

        static SAPbobsCOM.Company oStaticCompany;

        public SAPbobsCOM.Company oCompany;
        public SAPbobsCOM.CompanyService oCmpSrv;

        private SAPbobsCOM.ChartOfAccounts oChartOfAccount = null;
        private SAPbobsCOM.Items oItem = null;
        private SAPbobsCOM.BusinessPartners oBP = null;

        private SAPbobsCOM.Documents oDocument = null;
        private SAPbobsCOM.StockTransfer oInvTrfs = null;
        private SAPbobsCOM.InventoryCounting oInvCounting = null;
        private SAPbobsCOM.ProductionOrders oPrdctOder = null;

        private SAPbobsCOM.Payments oPayment = null;
        private SAPbobsCOM.JournalEntries oJE = null;
        private SAPbobsCOM.JournalVouchers oJV = null;
        private SAPbobsCOM.ProjectsService oServiceProj = null;
        private SAPbobsCOM.ProjectParams oProjParams = null;
        private SAPbobsCOM.Project oProj = null;

        public SAPbobsCOM.Company Company_SAP(out string message)
        {
            message = "";
            if (oStaticCompany == null || oStaticCompany.Connected == false)
            {
                var obj = RegConn_Get(DataConnection.SERVERDB);

                if (obj != null)
                {
                    oStaticCompany = new Company();

                    oStaticCompany.Server = obj.SERVER;
                    oStaticCompany.LicenseServer = obj.LICENSENAME;
                    oStaticCompany.DbPassword = obj.DBPASS;
                    oStaticCompany.CompanyDB = obj.DATABASE;

                    if (obj.SERVERTYPE == "MSSQL_2008")
                        oStaticCompany.DbServerType = BoDataServerTypes.dst_MSSQL2008;
                    else if (obj.SERVERTYPE == "MSSQL_2012")
                        oStaticCompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                    else if (obj.SERVERTYPE == "MSSQL_2014")
                        oStaticCompany.DbServerType = BoDataServerTypes.dst_MSSQL2014;
                    else if (obj.SERVERTYPE == "MSSQL_2016")
                    {
                        //oStaticCompany.DbServerType = BoDataServerTypes.dst_MSSQL2016;
                    }
                    else if (obj.SERVERTYPE == "HANADB")
                        oStaticCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                    oStaticCompany.DbUserName = obj.DBUSER;
                    oStaticCompany.UserName = obj.SAPUSER;
                    oStaticCompany.Password = obj.SAPPASS;
                }
            }
            if (!oStaticCompany.Connected)
            {
                int i = oStaticCompany.Connect();
                oStaticCompany.GetLastError(out i, out message);
            }

            return oStaticCompany;
        }

        public void StartSapTransaction()
        {
            string message = "";
            oCompany = Company_SAP(out message);
            oCompany.StartTransaction();
        }

        public bool CommitSapTransaction(out string message)
        {
            message = "";
            try
            {
                if (oCompany.InTransaction)
                    oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }

        public void RollbackSapTransaction()
        {
            if (oCompany.InTransaction)
                oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
        }

        public void AddStatusColumn(DataTable dt)
        {
            if (dt != null)
            {
                if (dt.Columns.Contains("SyncStatus") == false)
                {
                    dt.Columns.Add("SyncStatus");
                }

                if (dt.Columns.Contains("SyncMessage") == false)
                {
                    dt.Columns.Add("SyncMessage");
                }
            }
        }

        public bool SAPCreateObject(SyncObjectCurrent obj, DataTable dataSource, List<SyncDataObjectLine> dataLines, bool isLocal, out string message)
        {
            bool oresult = false;
            var daConn = new daConnection(isLocal);
            try
            {
                message = "";

                if (obj.ObjectType == ((int)SyncObject.oItems).ToString())
                {
                    oresult = CreateItemMasterData(obj, dataSource, out message);
                }
                else if (obj.ObjectType == ((int)SyncObject.oChartOfAccounts).ToString())
                {
                    oresult = CreateChartofAccounts(obj, dataSource, out message);
                }
                else if (obj.ObjectType == ((int)SyncObject.oBusinessPartners).ToString())
                {
                    oresult = CreateBussinessPartners(obj, dataSource, dataLines, out message);
                }
                else if (obj.ObjectType == ((int)SyncObject.oJournalVouchers).ToString())
                {
                    oresult = CreateJournalVouchers(obj, dataSource, dataLines, out message);
                }
                else if (obj.ObjectType == ((int)SyncObject.oJournalEntries).ToString())
                {
                    oresult = CreateJournalEntry(obj, dataSource, dataLines, out message);
                }
                else if (obj.ObjectType == ((int)SyncObject.oInvoices).ToString())
                {
                    oresult = CreateARInvoice(obj, dataSource, dataLines, out message);
                }
                else if (obj.ObjectType == ((int)SyncObject.oCreditNotes).ToString())
                {
                    oresult = CreateARCreditMemo(obj, dataSource, dataLines, out message);
                }
                else if (obj.ObjectType == ((int)SyncObject.oIncomingPayments).ToString())
                {
                    oresult = CreateIncomingPayment(obj, dataSource, dataLines, out message);
                }
                else if (obj.ObjectType == ((int)SyncObject.oPurchaseInvoices).ToString())
                {
                    oresult = CreateAPInvoice(obj, dataSource, dataLines, out message);
                }
                else if (obj.ObjectType == ((int)SyncObject.oPurchaseCreditNotes).ToString())
                {
                    oresult = CreateAPCreditMemo(obj, dataSource, dataLines, out message);
                }
                else if (obj.ObjectType == ((int)SyncObject.oVendorPayments).ToString())
                {
                    oresult = CreateOutgoingPayment(obj, dataSource, dataLines, out message);
                }
                else if (obj.ObjectType == ((int)SyncObject.oDownPayments).ToString())
                {
                    oresult = CreateARDownPaymentInvoice(obj, dataSource, dataLines, out message);
                }
                else if (obj.ObjectType == ((int)SyncObject.oPurchaseDownPayments).ToString())
                {
                    oresult = CreateAPDownPaymentInvoice(obj, dataSource, dataLines, out message);
                }
                else if (obj.ObjectType == ((int)SyncObject.oProjects).ToString())
                {
                    oresult = CreateProjects(obj, dataSource, out message);
                }
                else if (obj.ObjectType == ((int)SyncObject.oSendMail).ToString())
                {
                    oresult = SendMail(obj, dataSource, out message);
                }
                return oresult;
            }
            catch (Exception ex)
            {
                message = getFunctionError("SAPCreateObject", ex.Message);
                return false;
            }
        }

        public bool CreateChartofAccounts(SyncObjectCurrent obj, DataTable dataSource, out string message)
        {
            message = "";
            string columnName = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {

                #region Database
                //AcctCode nvarchar    no
                //AcctName    nvarchar no
                //CurrTotal numeric no
                //EndTotal    numeric no
                //Finanse char no
                //Groups nvarchar    no
                //Budget  char no
                //Frozen  char no
                //Free_2  char no
                //Postable    char no
                //Fixed   char no
                //Levels smallint    no
                //ExportCode  nvarchar no
                //GrpLine int no
                //FatherNum nvarchar    no
                //AccntntCod  nvarchar no
                //CashBox char no
                //GroupMask smallint    no
                //RateTrans   char no
                //TaxIncome   char no
                //ExmIncome   char no
                //ExtrMatch   int no
                //IntrMatch   int no
                //ActType char no
                //Transfered  char no
                //BlncTrnsfr  char no
                //OverType    char no
                //OverCode nvarchar    no
                //SysMatch    int no
                //PrevYear    char no
                //ActCurr nvarchar    no
                //RateDifAct  nvarchar no
                //SysTotal numeric no
                //FcTotal numeric no
                //Protected   char no
                //RealAcct    char no
                //Advance char no
                //CreateDate datetime    no
                //UpdateDate  datetime no
                //FrgnName nvarchar    no
                //Details nvarchar no
                //ExtraSum numeric no
                //Project nvarchar no
                //RevalMatch  char no
                //DataSource  char no
                //LocMth  char no
                //MTHCounter  int no
                //BNKCounter  int no
                //UserSign smallint    no
                //LocManTran  char no
                //LogInstanc  int no
                //ObjType nvarchar    no
                //ValidFor    char no
                //ValidFrom datetime    no
                //ValidTo datetime no
                //ValidComm nvarchar    no
                //FrozenFor   char no
                //FrozenFrom datetime    no
                //FrozenTo    datetime no
                //FrozenComm nvarchar    no
                //Counter int no
                #endregion

                string acctCode = "";
                object value;
                bool isUpdate = false;

                oCompany = Company_SAP(out message);
                foreach (DataRow row in dataSource.Rows)
                {
                    oChartOfAccount = oCompany.GetBusinessObject(BoObjectTypes.oChartOfAccounts);

                    if (dataSource.Columns.Contains("acctCode") == false)
                    {
                        message = getFunctionError("[CreateChartofAccounts]", "Not found field AcctCode.");
                        return false;
                    }
                    else
                    {
                        acctCode = row["acctCode"].ToString();
                        if (oChartOfAccount.GetByKey(acctCode))
                            isUpdate = true;
                        else
                            isUpdate = false;
                    }

                    oChartOfAccount.Code = acctCode;

                    foreach (DataColumn column in dataSource.Columns)
                    {
                        value = row[column.ColumnName];
                        columnName = column.ColumnName;
                        if (value != null && value.ToString() != "")
                        {
                            if (column.ColumnName.ToLower() == "Postable".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.ActiveAccount = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.ActiveAccount = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "FormatCode".ToLower())
                            {
                                oChartOfAccount.FormatCode = value.ToString();
                            }
                            if (column.ColumnName.ToLower() == "AcctName".ToLower())
                                oChartOfAccount.Name = value.ToString();
                            if (column.ColumnName.ToLower() == "FrgnName".ToLower())
                                oChartOfAccount.ForeignName = value.ToString();
                            if (column.ColumnName.ToLower() == "FatherNum".ToLower())
                                oChartOfAccount.FatherAccountKey = value.ToString();
                            if (column.ColumnName.ToLower() == "ActType".ToLower())
                            {
                                if (value.ToString() == "N")
                                    oChartOfAccount.AccountType = BoAccountTypes.at_Other;
                                else if (value.ToString() == "I")
                                    oChartOfAccount.AccountType = BoAccountTypes.at_Revenues;
                                else if (value.ToString() == "E")
                                    oChartOfAccount.AccountType = BoAccountTypes.at_Expenses;
                            }
                            //if (column.ColumnName.ToLower() == "Groups".ToLower()) oChartOfAccount = value.ToString();
                            if (column.ColumnName.ToLower() == "DfltVat".ToLower())
                                oChartOfAccount.DefaultVatGroup = value.ToString();
                            if (column.ColumnName.ToLower() == "AccntntCod".ToLower())
                                oChartOfAccount.ExternalCode = value.ToString();
                            if (column.ColumnName.ToLower() == "ActCurr".ToLower())
                                oChartOfAccount.AcctCurrency = value.ToString();
                            if (column.ColumnName.ToLower() == "ValidFor".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.ValidFor = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.ValidFor = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "ValidFrom".ToLower() && value.ToString() != "") oChartOfAccount.ValidFrom = (DateTime)value;
                            if (column.ColumnName.ToLower() == "ValidTo".ToLower() && value.ToString() != "") oChartOfAccount.ValidTo = (DateTime)value;
                            if (column.ColumnName.ToLower() == "ValidComm".ToLower()) oChartOfAccount.ValidRemarks = value.ToString();
                            if (column.ColumnName.ToLower() == "FrozenFor".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.FrozenFor = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.FrozenFor = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "FrozenFrom".ToLower() && value.ToString() != "") oChartOfAccount.FrozenFrom = (DateTime)value;
                            if (column.ColumnName.ToLower() == "FrozenTo".ToLower() && value.ToString() != "") oChartOfAccount.FrozenTo = (DateTime)value;
                            if (column.ColumnName.ToLower() == "FrozenComm".ToLower()) oChartOfAccount.FrozenRemarks = value.ToString();
                            if (column.ColumnName.ToLower() == "Details".ToLower())
                                oChartOfAccount.Details = value.ToString();
                            //if (column.ColumnName.ToLower() == "Category".ToLower() && value.ToString() != "")
                            //    oChartOfAccount.Category = (int)value;
                            if (column.ColumnName.ToLower() == "Protected".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.Protected = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.Protected = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "Finanse".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.CashAccount = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.CashAccount = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "RevalMatch".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.RevaluationCoordinated = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.RevaluationCoordinated = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "RealAcct".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.ReconciledAccount = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.ReconciledAccount = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "LocManTran".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.LockManualTransaction = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.LockManualTransaction = BoYesNoEnum.tNO;
                            }
                            //if (column.ColumnName.ToLower() == "CfwRlvnt".ToLower())
                            //{
                            //    if (value.ToString() == "Y")
                            //        oChartOfAccount. = BoYesNoEnum.tYES;
                            //    else
                            //        oChartOfAccount.TaxLiableAccount = BoYesNoEnum.tNO;
                            //}
                            if (column.ColumnName.ToLower() == "Budget".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.BudgetAccount = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.BudgetAccount = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "MultiLink".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.AllowMultipleLinking = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.AllowMultipleLinking = BoYesNoEnum.tNO;
                            }

                            else if (column.ColumnName.StartsWith("U_"))
                            {
                                COA_UDF(oChartOfAccount, column, value);
                            }
                        }
                    }

                    if (isUpdate)
                        oError = oChartOfAccount.Update();
                    else
                        oError = oChartOfAccount.Add();

                    if (oError == 0)
                    {
                        row["SyncStatus"] = "Success";
                        row["SyncMessage"] = "";
                        if (obj.TransactionNotification != null && obj.TransactionNotification.ToString() != "")
                        {
                            //obj.TransactionNotification = "update OACT set U_isSync='Y' where acctCode='{0}'";
                            daConn.ExecQuery(string.Format(obj.TransactionNotification, acctCode), out message);
                        }
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        row["SyncStatus"] = "Fail";
                        row["SyncMessage"] = string.Format("{0}", message);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("[CreateChartofAccounts][Field: " + columnName + "]", ex.Message);
                return false;
            }
        }

        public bool CreateItemMasterData(SyncObjectCurrent obj, DataTable dataSource, out string message)
        {
            message = "";
            string columnName = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {

                #region Database
                //ItemCode nvarchar
                //ItemName nvarchar
                //FrgnName nvarchar
                //ItmsGrpCod smallint
                //CstGrpCode smallint
                //VatGourpSa nvarchar
                //CodeBars nvarchar
                //VATLiable   char
                //PrchseItem  char
                //SellItem    char
                //InvntItem   char
                //OnHand  numeric
                //IsCommited  numeric
                //OnOrder numeric
                //IncomeAcct  nvarchar
                //ExmptIncom  nvarchar
                //MaxLevel    numeric
                //DfltWH  nvarchar
                //CardCode    nvarchar
                //SuppCatNum  nvarchar
                //BuyUnitMsr  nvarchar
                //nj5hNumInBuy    numeric
                //ReorderQty  numeric
                //MinLevel    numeric
                //LstEvlPric  numeric
                //LstEvlDate  datetime
                //CustomPer   numeric
                //Canceled    char
                //MnufctTime  int
                //WholSlsTax  char
                //RetilrTax   char
                //SpcialDisc  numeric
                //DscountCod  smallint
                //TrackSales  char
                //SalUnitMsr  nvarchar
                //NumInSale   numeric
                //Consig  numeric
                //QueryGroup  int
                //Counted numeric
                //OpenBlnc    numeric
                //EvalSystem  char
                //UserSign    smallint
                //FREE    char
                //PicturName  nvarchar
                //Transfered  char
                //BlncTrnsfr  char
                //UserText    ntext
                //SerialNum   nvarchar
                //CommisPcnt  numeric
                //CommisSum   numeric
                //CommisGrp   smallint
                //TreeType    char
                //TreeQty numeric
                //LastPurPrc  numeric
                //LastPurCur  nvarchar
                //LastPurDat  datetime
                //ExitCur nvarchar
                //ExitPrice   numeric
                //ExitWH  nvarchar
                //AssetItem   char
                //WasCounted  char
                //ManSerNum   char
                //SHeight1    numeric
                //SHght1Unit  smallint
                //SHeight2    numeric
                //SHght2Unit  smallint
                //SWidth1 numeric
                //SWdth1Unit  smallint
                //SWidth2 numeric
                //SWdth2Unit  smallint
                //SLength1    numeric
                //SLen1Unit   smallint
                //Slength2    numeric
                //SLen2Unit   smallint
                //SVolume numeric
                //SVolUnit    smallint
                //SWeight1    numeric
                //SWght1Unit  smallint
                //SWeight2    numeric
                //SWght2Unit  smallint
                //BHeight1    numeric
                //BHght1Unit  smallint
                //BHeight2    numeric
                //BHght2Unit  smallint
                //BWidth1 numeric
                //BWdth1Unit  smallint
                //BWidth2 numeric
                //BWdth2Unit  smallint
                //BLength1    numeric
                //BLen1Unit   smallint
                //Blength2    numeric
                //BLen2Unit   smallint
                //BVolume numeric
                //BVolUnit    smallint
                //BWeight1    numeric
                //BWght1Unit  smallint
                //BWeight2    numeric
                //BWght2Unit  smallint
                //FixCurrCms  nvarchar
                //FirmCode    smallint
                //LstSalDate  datetime
                //QryGroup1   char
                //QryGroup2   char
                //QryGroup3   char
                //QryGroup4   char
                //QryGroup5   char
                //QryGroup6   char
                //QryGroup7   char
                //QryGroup8   char
                //QryGroup9   char
                //QryGroup10  char
                //QryGroup11  char
                //QryGroup12  char
                //QryGroup13  char
                //QryGroup14  char
                //QryGroup15  char
                //QryGroup16  char
                //QryGroup17  char
                //QryGroup18  char
                //QryGroup19  char
                //QryGroup20  char
                //QryGroup21  char
                //QryGroup22  char
                //QryGroup23  char
                //QryGroup24  char
                //QryGroup25  char
                //QryGroup26  char
                //QryGroup27  char
                //QryGroup28  char
                //QryGroup29  char
                //QryGroup30  char
                //QryGroup31  char
                //QryGroup32  char
                //QryGroup33  char
                //QryGroup34  char
                //QryGroup35  char
                //QryGroup36  char
                //QryGroup37  char
                //QryGroup38  char
                //QryGroup39  char
                //QryGroup40  char
                //QryGroup41  char
                //QryGroup42  char
                //QryGroup43  char
                //QryGroup44  char
                //QryGroup45  char
                //QryGroup46  char
                //QryGroup47  char
                //QryGroup48  char
                //QryGroup49  char
                //QryGroup50  char
                //QryGroup51  char
                //QryGroup52  char
                //QryGroup53  char
                //QryGroup54  char
                //QryGroup55  char
                //QryGroup56  char
                //QryGroup57  char
                //QryGroup58  char
                //QryGroup59  char
                //QryGroup60  char
                //QryGroup61  char
                //QryGroup62  char
                //QryGroup63  char
                //QryGroup64  char
                //CreateDate  datetime
                //UpdateDate  datetime
                //ExportCode  nvarchar
                //SalFactor1  numeric
                //SalFactor2  numeric
                //SalFactor3  numeric
                //SalFactor4  numeric
                //PurFactor1  numeric
                //PurFactor2  numeric
                //PurFactor3  numeric
                //PurFactor4  numeric
                //SalFormula  nvarchar
                //PurFormula  nvarchar
                //VatGroupPu  nvarchar
                //AvgPrice    numeric
                //PurPackMsr  nvarchar
                //PurPackUn   numeric
                //SalPackMsr  nvarchar
                //SalPackUn   numeric
                //SCNCounter  smallint
                //ManBtchNum  char
                //ManOutOnly  char
                //DataSource  char
                //validFor    char
                //validFrom   datetime
                //validTo datetime
                //frozenFor   char
                //frozenFrom  datetime
                //frozenTo    datetime
                //BlockOut    char
                //ValidComm   nvarchar
                //FrozenComm  nvarchar
                //LogInstanc  int
                //ObjType nvarchar
                //SWW nvarchar
                //Deleted char
                //DocEntry    int
                //ExpensAcct  nvarchar
                //FrgnInAcct  nvarchar
                //ShipType    smallint
                //GLMethod    char
                //ECInAcct    nvarchar
                //FrgnExpAcc  nvarchar
                //ECExpAcc    nvarchar
                //TaxType char
                //ByWh    char
                //WTLiable    char
                //ItemType    char
                //WarrntTmpl  nvarchar
                //BaseUnit    nvarchar
                //CountryOrg  nvarchar
                //StockValue  numeric
                //Phantom char
                //IssueMthd   char
                //FREE1   char
                //PricingPrc  numeric
                //MngMethod   char
                //ReorderPnt  numeric
                //InvntryUom  nvarchar
                //PlaningSys  char
                //PrcrmntMtd  char
                //OrdrIntrvl  smallint
                //OrdrMulti   numeric
                //MinOrdrQty  numeric
                //LeadTime    int
                //IndirctTax  char
                //TaxCodeAR   nvarchar
                //TaxCodeAP   nvarchar
                //OSvcCode    int
                //ISvcCode    int
                //ServiceGrp  int
                //NCMCode int
                //MatType nvarchar
                //MatGrp  int
                //ProductSrc  nvarchar
                //ServiceCtg  int
                //ItemClass   char
                //Excisable   char
                //ChapterID   int
                //NotifyASN   nvarchar
                //ProAssNum   nvarchar
                //AssblValue  numeric
                //DNFEntry    int
                //UserSign2   smallint
                //Spec    nvarchar
                //TaxCtg  nvarchar
                //Series  smallint
                //Number  int
                //FuelCode    int
                //BeverTblC   nvarchar
                //BeverGrpC   nvarchar
                //BeverTM int
                //Attachment  ntext
                //AtcEntry    int
                //ToleranDay  int
                //UgpEntry    int
                //PUoMEntry   int
                //SUoMEntry   int
                //IUoMEntry   int
                //IssuePriBy  smallint
                //AssetClass  nvarchar
                //AssetGroup  nvarchar
                //InventryNo  nvarchar
                //Technician  int
                //Employee    int
                //Location    int
                //StatAsset   char
                //Cession char
                //DeacAftUL   char
                //AsstStatus  char
                //CapDate datetime
                //AcqDate datetime
                //RetDate datetime
                //GLPickMeth  char
                //NoDiscount  char
                //MgrByQty    char
                //AssetRmk1   nvarchar
                //AssetRmk2   nvarchar
                //AssetAmnt1  numeric
                //AssetAmnt2  numeric
                //DeprGroup   nvarchar
                //AssetSerNo  nvarchar
                //CntUnitMsr  nvarchar
                //NumInCnt    numeric
                //INUoMEntry  int
                //OneBOneRec  char
                //RuleCode    nvarchar
                //ScsCode nvarchar
                //SpProdType  nvarchar
                //IWeight1    numeric
                //IWght1Unit  smallint
                //IWeight2    numeric
                //IWght2Unit  smallint
                //CompoWH char
                #endregion

                string itemCode = "";
                object value;
                bool isUpdate = false;

                oCompany = Company_SAP(out message);
                foreach (DataRow row in dataSource.Rows)
                {
                    oItem = oCompany.GetBusinessObject(BoObjectTypes.oItems);
                    if (dataSource.Columns.Contains("ItemCode") == false)
                    {
                        message = getFunctionError("[CreateItemMasterData]", "Not found field ItemCode.");
                        return false;
                    }
                    else
                    {
                        itemCode = row["ItemCode"].ToString();
                        if (oItem.GetByKey(itemCode))
                            isUpdate = true;
                        else
                            isUpdate = false;
                    }

                    oItem.ItemCode = itemCode;

                    foreach (DataColumn column in dataSource.Columns)
                    {
                        columnName = column.ColumnName;
                        value = row[column.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (column.ColumnName.ToLower() == "ItemName".ToLower()) oItem.ItemName = value.ToString();
                            else if (column.ColumnName.ToLower() == "frgnName".ToLower()) oItem.ForeignName = value.ToString();
                            else if (column.ColumnName.ToLower() == "itmsGrpCod".ToLower()) oItem.ItemsGroupCode = (Int16)value;
                            else if (column.ColumnName.ToLower() == "Frozen".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.Frozen = BoYesNoEnum.tYES;
                                else
                                    oItem.Frozen = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "FrozenFrom".ToLower()) oItem.FrozenFrom = (DateTime)value;
                            else if (column.ColumnName.ToLower() == "FrozenComm".ToLower()) oItem.FrozenRemarks = value.ToString();
                            else if (column.ColumnName.ToLower() == "FrozenTo".ToLower()) oItem.FrozenTo = (DateTime)value;
                            else if (column.ColumnName.ToLower() == "GLMethod".ToLower())
                            {
                                if (value.ToString() == "C")
                                    oItem.GLMethod = BoGLMethods.glm_ItemClass;
                                else if (value.ToString() == "W")
                                    oItem.GLMethod = BoGLMethods.glm_WH;
                                else
                                    oItem.GLMethod = BoGLMethods.glm_ItemLevel;
                            }
                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.InventoryNumber = value.ToString();
                            else if (column.ColumnName.ToLower() == "InvntryUom".ToLower()) oItem.InventoryUOM = value.ToString();
                            else if (column.ColumnName.ToLower() == "IUoMEntry".ToLower()) oItem.InventoryUoMEntry = (int)value;
                            else if (column.ColumnName.ToLower() == "UgpEntry".ToLower()) oItem.UoMGroupEntry = (int)value;
                            else if (column.ColumnName.ToLower() == "Valid".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.Valid = BoYesNoEnum.tYES;
                                else
                                    oItem.Valid = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "ValidFrom".ToLower()) oItem.ValidFrom = (DateTime)value;
                            else if (column.ColumnName.ToLower() == "ValidComm".ToLower()) oItem.ValidRemarks = value.ToString();
                            else if (column.ColumnName.ToLower() == "ValidTo".ToLower()) oItem.ValidTo = (DateTime)value;
                            else if (column.ColumnName.ToLower() == "PrchseItem".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.PurchaseItem = BoYesNoEnum.tYES;
                                else
                                    oItem.PurchaseItem = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "VATGroupPu".ToLower()) oItem.PurchaseVATGroup = value.ToString();
                            else if (column.ColumnName.ToLower() == "Sellitem".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.SalesItem = BoYesNoEnum.tYES;
                                else
                                    oItem.SalesItem = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "InvntItem".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.InventoryItem = BoYesNoEnum.tYES;
                                else
                                    oItem.InventoryItem = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "VATGourpSa".ToLower()) oItem.SalesVATGroup = value.ToString();
                            else if (column.ColumnName.ToLower() == "EvalSystem".ToLower())
                            {
                                if (value.ToString() == "S")
                                    oItem.CostAccountingMethod = BoInventorySystem.bis_Standard;
                                else if (value.ToString() == "F")
                                    oItem.CostAccountingMethod = BoInventorySystem.bis_FIFO;
                                else if (value.ToString() == "A")
                                    oItem.CostAccountingMethod = BoInventorySystem.bis_MovingAverage;
                                else
                                    oItem.CostAccountingMethod = BoInventorySystem.bis_SNB;
                            }






                            else if (column.ColumnName.ToLower() == "AssetClass".ToLower()) oItem.AssetClass = value.ToString();
                            else if (column.ColumnName.ToLower() == "AssetGroup".ToLower()) oItem.AssetGroup = value.ToString();
                            else if (column.ColumnName.ToLower() == "AssetItem".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.AssetItem = BoYesNoEnum.tYES;
                                else
                                    oItem.AssetItem = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "AssetSerNo".ToLower()) oItem.AssetSerialNumber = value.ToString();
                            else if (column.ColumnName.ToLower() == "AvgPrice".ToLower()) oItem.AvgStdPrice = (double)(decimal)value;
                            else if (column.ColumnName.ToLower() == "CodeBars".ToLower()) oItem.BarCode = value.ToString();
                            else if (column.ColumnName.ToLower() == "BaseUnit".ToLower()) oItem.BaseUnitName = value.ToString();
                            else if (column.ColumnName.ToLower() == "CapDate".ToLower()) oItem.CapitalizationDate = (DateTime)value;
                            else if (column.ColumnName.ToLower() == "Cession".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.Cession = BoYesNoEnum.tYES;
                                else
                                    oItem.Cession = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "CommisGrp".ToLower()) oItem.CommissionGroup = (Int16)value;
                            else if (column.ColumnName.ToLower() == "CommisPcnt".ToLower()) oItem.CommissionPercent = (double)(decimal)value;
                            else if (column.ColumnName.ToLower() == "CommisSum".ToLower()) oItem.CommissionSum = (double)(decimal)value;

                            else if (column.ColumnName.ToLower() == "CstGrpCode".ToLower()) oItem.CustomsGroupCode = (Int16)value;
                            else if (column.ColumnName.ToLower() == "ExportCode".ToLower()) oItem.DataExportCode = value.ToString();
                            else if (column.ColumnName.ToLower() == "DeacAftUL".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.DeactivateAfterUsefulLife = BoYesNoEnum.tYES;
                                else
                                    oItem.DeactivateAfterUsefulLife = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "INUoMEntry".ToLower()) oItem.DefaultCountingUoMEntry = (int)value;

                            else if (column.ColumnName.ToLower() == "PUoMEntry".ToLower()) oItem.DefaultPurchasingUoMEntry = (int)value;
                            else if (column.ColumnName.ToLower() == "BuyUnitMsr".ToLower()) oItem.PurchaseUnit = value.ToString();
                            else if (column.ColumnName.ToLower() == "NumInBuy".ToLower()) oItem.PurchaseItemsPerUnit = (double)(decimal)value;

                            else if (column.ColumnName.ToLower() == "SUoMEntry".ToLower()) oItem.DefaultSalesUoMEntry = (int)value;
                            else if (column.ColumnName.ToLower() == "SalUnitMsr".ToLower()) oItem.SalesUnit = value.ToString();
                            else if (column.ColumnName.ToLower() == "NumInSale".ToLower()) oItem.SalesItemsPerUnit = (double)(decimal)value;

                            else if (column.ColumnName.ToLower() == "DfltWH".ToLower()) oItem.DefaultWarehouse = value.ToString();
                            else if (column.ColumnName.ToLower() == "Technician".ToLower()) oItem.Technician = (int)value;


                            else if (column.ColumnName.ToLower() == "IssueMthd".ToLower())
                            {
                                if (value.ToString() == "B")
                                    oItem.IssueMethod = BoIssueMethod.im_Backflush;
                                else
                                    oItem.IssueMethod = BoIssueMethod.im_Manual;
                            }
                            else if (column.ColumnName.ToLower() == "IssuePriBy".ToLower())
                            {
                                if (value.ToString() == "")
                                    oItem.IssuePrimarilyBy = IssuePrimarilyByEnum.ipbBinLocations;
                                else
                                    oItem.IssuePrimarilyBy = IssuePrimarilyByEnum.ipbSerialAndBatchNumbers;
                            }
                            else if (column.ColumnName.ToLower() == "ItemClass".ToLower())
                            {
                                if (value.ToString() == "2")
                                    oItem.ItemClass = ItemClassEnum.itcMaterial;
                                else
                                    oItem.ItemClass = ItemClassEnum.itcService;
                            }
                            else if (column.ColumnName.ToLower() == "CountryOrg".ToLower()) oItem.ItemCountryOrg = value.ToString();
                            else if (column.ColumnName.ToLower() == "ItemType".ToLower())
                            {
                                if (value.ToString() == "T")
                                    oItem.ItemType = ItemTypeEnum.itTravel;
                                else if (value.ToString() == "I")
                                    oItem.ItemType = ItemTypeEnum.itItems;
                                else if (value.ToString() == "L")
                                    oItem.ItemType = ItemTypeEnum.itLabor;
                                else
                                    oItem.ItemType = ItemTypeEnum.itFixedAssets;
                            }
                            else if (column.ColumnName.ToLower() == "Location".ToLower()) oItem.Location = (int)value;
                            else if (column.ColumnName.ToLower() == "ManBtchNum".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.ManageBatchNumbers = BoYesNoEnum.tYES;
                                else
                                    oItem.ManageBatchNumbers = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "ManSerNum".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.ManageSerialNumbers = BoYesNoEnum.tYES;
                                else
                                    oItem.ManageSerialNumbers = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "ManOutOnly".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.ManageSerialNumbersOnReleaseOnly = BoYesNoEnum.tYES;
                                else
                                    oItem.ManageSerialNumbersOnReleaseOnly = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "ByWh".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.ManageStockByWarehouse = BoYesNoEnum.tYES;
                                else
                                    oItem.ManageStockByWarehouse = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "FirmCode".ToLower()) oItem.Manufacturer = (Int16)value;
                            //else if (column.ColumnName.ToLower() == "MatType".ToLower())
                            //{
                            //    if (value.ToString() == "")
                            //        oItem.MaterialType = BoMaterialTypes.mt_RawMaterial;
                            //    else
                            //        oItem.MaterialType = BoMaterialTypes.mt_FinishedGoods;
                            //}
                            else if (column.ColumnName.ToLower() == "MinLevel".ToLower()) oItem.MaxInventory = (double)(decimal)value;
                            else if (column.ColumnName.ToLower() == "MaxLevel".ToLower()) oItem.MinInventory = (double)(decimal)value;
                            else if (column.ColumnName.ToLower() == "MinOrdrQty".ToLower()) oItem.MinOrderQuantity = (double)(decimal)value;
                            else if (column.ColumnName.ToLower() == "NoDiscount".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.NoDiscounts = BoYesNoEnum.tYES;
                                else
                                    oItem.NoDiscounts = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "PicturName".ToLower()) oItem.Picture = value.ToString();
                            else if (column.ColumnName.ToLower() == "PlaningSys".ToLower())
                            {
                                if (value.ToString() == "M")
                                    oItem.PlanningSystem = BoPlanningSystem.bop_MRP;
                                else
                                    oItem.PlanningSystem = BoPlanningSystem.bop_None;
                            }
                            //else if (column.ColumnName.ToLower() == "CardCode".ToLower())
                            //{
                            //    oItem.PreferredVendors.BPCode = value.ToString();
                            //    oItem.PreferredVendors.Add();
                            //}
                            else if (column.ColumnName.ToLower() == "PrcrmntMtd".ToLower())
                            {
                                if (value.ToString() == "B")
                                    oItem.ProcurementMethod = BoProcurementMethod.bom_Buy;
                                else
                                    oItem.ProcurementMethod = BoProcurementMethod.bom_Make;
                            }
                            //else if (column.ColumnName.ToLower() == "ProductSrc".ToLower()) oItem.ProductSource = (int)value;
                            //else if (column.ColumnName.ToLower() == "".ToLower())
                            //{
                            //    oItem.Projects.Project = value.ToString();
                            //    oItem.Projects.Add();
                            //}

                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.PurchaseItemsPerUnit = (double)value;
                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.PurchaseUnit = value.ToString();

                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.SalesItemsPerUnit = (int)value;
                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.SalesUnit = value.ToString();
                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.SerialNum = value.ToString();
                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.Series = (int)value;
                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.ServiceGroup = (int)value;
                            else if (column.ColumnName.ToLower() == "ShipType".ToLower()) oItem.ShipType = (int)value;
                            else if (column.ColumnName.ToLower() == "StatAsset".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.StatisticalAsset = BoYesNoEnum.tYES;
                                else
                                    oItem.StatisticalAsset = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "SWW".ToLower()) oItem.SWW = value.ToString();
                            //else if (column.ColumnName.ToLower() == "".ToLower())
                            //{
                            //    if (value.ToString() == "Y")
                            //        oItem.TaxType = BoTaxTypes.tt_Yes;
                            //    else
                            //        oItem.TaxType = BoTaxTypes.tt_No;
                            //}
                            //else if (column.ColumnName.ToLower() == "".ToLower())
                            //{
                            //    oItem.UnitOfMeasurements.UoMEntry = 1;
                            //    oItem.UnitOfMeasurements.Add();
                            //}
                            //else if (column.ColumnName.ToLower() == "Valid".ToLower())
                            //{
                            //    oItem.WhsInfo.CostAccount = "";
                            //    oItem.WhsInfo.Add();
                            //}

                            //UDF   
                            //SAPbobsCOM.BoFieldTypes
                            //SAPbobsCOM.BoFldSubTypes
                            //else if (column.ColumnName == "U_Menucode") oItem.UserFields.Fields.Item("U_Menucode").Value = value.ToString();
                            //else if (column.ColumnName == "U_Menuname") oItem.UserFields.Fields.Item("U_Menuname").Value = value.ToString();

                            else if (column.ColumnName.StartsWith("U_"))
                            {
                                Item_UDF(oItem, column, value);
                            }
                        }
                    }

                    if (isUpdate)
                        oError = oItem.Update();
                    else
                        oError = oItem.Add();

                    if (oError == 0)
                    {
                        row["SyncStatus"] = "Success";
                        row["SyncMessage"] = "";
                        if (obj.TransactionNotification != null && obj.TransactionNotification.ToString() != "")
                        {
                            //obj.TransactionNotification = "update OITM set U_isSync='Y' where itemCode='{0}'";
                            daConn.ExecQuery(string.Format(obj.TransactionNotification, itemCode), out message);
                        }
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        row["SyncStatus"] = "Fail";
                        row["SyncMessage"] = message;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("[CreateItemMasterData][Field: " + columnName + "]", ex.Message);
                return false;
            }
        }

        public bool CreateBussinessPartners(SyncObjectCurrent obj, DataTable dataSource, List<SyncDataObjectLine> dataLines, out string message)
        {
            message = "";
            string columnName = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {
                #region OCRD
                //CardCode
                //CardName
                //CardType
                //GroupCode
                //CmpPrivate
                //Address
                //ZipCode
                //MailAddres
                //MailZipCod
                //Phone1
                //Phone2
                //Fax
                //CntctPrsn
                //Notes
                //Balance
                //ChecksBal
                //DNotesBal
                //OrdersBal
                //GroupNum
                //CreditLine
                //DebtLine
                //Discount
                //VatStatus
                //LicTradNum
                //DdctStatus
                //DdctPrcnt
                //ValidUntil
                //Chrctrstcs
                //ExMatchNum
                //InMatchNum
                //ListNum
                //DNoteBalFC
                //OrderBalFC
                //DNoteBalSy
                //OrderBalSy
                //Transfered
                //BalTrnsfrd
                //IntrstRate
                //Commission
                //CommGrCode
                //Free_Text
                //SlpCode
                //PrevYearAc
                //Currency
                //RateDifAct
                //BalanceSys
                //BalanceFC
                //Protected
                //Cellular
                //AvrageLate
                //City
                //County
                //Country
                //MailCity
                //MailCounty
                //MailCountr
                //E_Mail
                //Picture
                //DflAccount
                //DflBranch
                //BankCode
                //AddID
                //Pager
                //FatherCard
                //CardFName
                //FatherType
                //QryGroup1
                //QryGroup2
                //QryGroup3
                //QryGroup4
                //QryGroup5
                //QryGroup6
                //QryGroup7
                //QryGroup8
                //QryGroup9
                //QryGroup10
                //QryGroup11
                //QryGroup12
                //QryGroup13
                //QryGroup14
                //QryGroup15
                //QryGroup16
                //QryGroup17
                //QryGroup18
                //QryGroup19
                //QryGroup20
                //QryGroup21
                //QryGroup22
                //QryGroup23
                //QryGroup24
                //QryGroup25
                //QryGroup26
                //QryGroup27
                //QryGroup28
                //QryGroup29
                //QryGroup30
                //QryGroup31
                //QryGroup32
                //QryGroup33
                //QryGroup34
                //QryGroup35
                //QryGroup36
                //QryGroup37
                //QryGroup38
                //QryGroup39
                //QryGroup40
                //QryGroup41
                //QryGroup42
                //QryGroup43
                //QryGroup44
                //QryGroup45
                //QryGroup46
                //QryGroup47
                //QryGroup48
                //QryGroup49
                //QryGroup50
                //QryGroup51
                //QryGroup52
                //QryGroup53
                //QryGroup54
                //QryGroup55
                //QryGroup56
                //QryGroup57
                //QryGroup58
                //QryGroup59
                //QryGroup60
                //QryGroup61
                //QryGroup62
                //QryGroup63
                //QryGroup64
                //DdctOffice
                //CreateDate
                //UpdateDate
                //ExportCode
                //DscntObjct
                //DscntRel
                //SPGCounter
                //SPPCounter
                //DdctFileNo
                //SCNCounter
                //MinIntrst
                //DataSource
                //OprCount
                //ExemptNo
                //Priority
                //CreditCard
                //CrCardNum
                //CardValid
                //UserSign
                //LocMth
                //validFor
                //validFrom
                //validTo
                //frozenFor
                //frozenFrom
                //frozenTo
                //sEmployed
                //MTHCounter
                //BNKCounter
                //DdgKey
                //DdtKey
                //ValidComm
                //FrozenComm
                //chainStore
                //DiscInRet
                //State1
                //State2
                //VatGroup
                //LogInstanc
                //ObjType
                //Indicator
                //ShipType
                //DebPayAcct
                //ShipToDef
                //Block
                //MailBlock
                //Password
                //ECVatGroup
                //Deleted
                //IBAN
                //DocEntry
                //FormCode
                //Box1099
                //PymCode
                //BackOrder
                //PartDelivr
                //DunnLevel
                //DunnDate
                //BlockDunn
                //BankCountr
                //CollecAuth
                //DME
                //InstrucKey
                //SinglePaym
                //ISRBillId
                //PaymBlock
                //RefDetails
                //HouseBank
                //OwnerIdNum
                //PyBlckDesc
                //HousBnkCry
                //HousBnkAct
                //HousBnkBrn
                //ProjectCod
                //SysMatchNo
                //VatIdUnCmp
                //AgentCode
                //TolrncDays
                //SelfInvoic
                //DeferrTax
                //LetterNum
                //MaxAmount
                //FromDate
                //ToDate
                //WTLiable
                //CrtfcateNO
                //ExpireDate
                //NINum
                //AccCritria
                //WTCode
                //Equ
                //HldCode
                //ConnBP
                //MltMthNum
                //TypWTReprt
                //VATRegNum
                //RepName
                //Industry
                //Business
                //WTTaxCat
                //IsDomestic
                //IsResident
                //AutoCalBCG
                //OtrCtlAcct
                //AliasName
                //Building
                //MailBuildi
                //BoEPrsnt
                //BoEDiscnt
                //BoEOnClct
                //UnpaidBoE
                //ITWTCode
                //DunTerm
                //ChannlBP
                //DfTcnician
                //Territory
                //BillToDef
                //DpmClear
                //IntrntSite
                //LangCode
                //HousActKey
                //Profession
                //CDPNum
                //DflBankKey
                //BCACode
                //UseShpdGd
                //RegNum
                //VerifNum
                //BankCtlKey
                //HousCtlKey
                //AddrType
                //InsurOp347
                //MailAddrTy
                //StreetNo
                //MailStrNo
                //TaxRndRule
                //VendTID
                //ThreshOver
                //SurOver
                //VendorOcup
                //OpCode347
                //DpmIntAct
                //ResidenNum
                //UserSign2
                //PlngGroup
                //VatIDNum
                //Affiliate
                //MivzExpSts
                //HierchDdct
                //CertWHT
                //CertBKeep
                //WHShaamGrp
                //IndustryC
                //DatevAcct
                //DatevFirst
                //GTSRegNum
                //GTSBankAct
                //GTSBilAddr
                //HsBnkSwift
                //HsBnkIBAN
                //DflSwift
                //AutoPost
                //IntrAcc
                //FeeAcc
                //CpnNo
                //NTSWebSite
                //DflIBAN
                //Series
                //Number
                //EDocExpFrm
                //TaxIdIdent
                //Attachment
                //AtcEntry
                //DiscRel
                //NoDiscount
                //SCAdjust
                //DflAgrmnt
                //GlblLocNum
                //SenderID
                //RcpntID
                //MainUsage
                //SefazCheck
                //free312
                //free313
                //DateFrom
                //DateTill
                //RelCode
                //OKATO
                //OKTMO
                //KBKCode
                //TypeOfOp
                //OwnerCode
                //MandateID
                //SignDate
                //Remark1
                //ConCerti
                //TpCusPres
                //RoleTypCod
                //BlockComm
                //EmplymntCt
                //ExcptnlEvt
                //ExpnPrfFnd
                #endregion
                #region CRD1
                //Address nvarchar
                //CardCode nvarchar
                //Street nvarchar
                //Block nvarchar
                //ZipCode nvarchar
                //City nvarchar
                //County nvarchar
                //Country nvarchar
                //State nvarchar
                //UserSign smallint
                //LogInstanc  int
                //ObjType nvarchar
                //LicTradNum  nvarchar
                //LineNum int
                //TaxCode nvarchar
                //Building    ntext
                //AdresType   char
                //Address2    nvarchar
                //Address3    nvarchar
                //AddrType    nvarchar
                //StreetNo    nvarchar
                //AltCrdName  nvarchar
                //AltTaxId    nvarchar
                //TaxOffice   nvarchar
                //GlblLocNum  nvarchar
                //Ntnlty  nvarchar
                #endregion
                #region OCPR
                //CntctCode   int
                //CardCode    nvarchar
                //Name    nvarchar
                //Position    nvarchar
                //Address nvarchar
                //Tel1    nvarchar
                //Tel2    nvarchar
                //Cellolar    nvarchar
                //Fax nvarchar
                //E_MailL nvarchar
                //Pager   nvarchar
                //Notes1  nvarchar
                //Notes2  nvarchar
                //DataSource  char
                //UserSign    smallint
                //Password    nvarchar
                //LogInstanc  int
                //ObjType nvarchar
                //BirthPlace  nvarchar
                //BirthDate   datetime
                //Gender  char
                //Profession  nvarchar
                //updateDate  datetime
                //updateTime  int
                //Title   nvarchar
                //BirthCity   nvarchar
                //Active  char
                //FirstName   nvarchar
                //MiddleName  nvarchar
                //LastName    nvarchar
                //BirthState  nvarchar
                //ResidCity   nvarchar
                //ResidCntry  nvarchar
                //ResidState  nvarchar
                //NFeRcpn char
                //EmlGrpCode  nvarchar
                //BlockComm   char
                //FiscalCode  nvarchar
                //CtyPrvsYr   nvarchar
                //SttPrvsYr   nvarchar
                //CtyCdPrvsY  nvarchar
                //CtyCurYr    nvarchar
                //SttCurYr    nvarchar
                //CtyCdCurYr  nvarchar
                #endregion

                string cardCode = "";
                object value;
                bool isUpdate = false;

                oCompany = Company_SAP(out message);
                //FOR LINES
                var linesAddrress = dataLines[0].DataSource;
                var linesContactPerson = dataLines[1].DataSource;
                foreach (DataRow row in dataSource.Rows)
                {
                    oBP = oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartners);
                    if (dataSource.Columns.Contains("CardCode") == false)
                    {
                        message = getFunctionError("[CreateBussinessPartners]", "Not found field CardCode.");
                        return false;
                    }
                    else
                    {
                        cardCode = row["cardCode"].ToString();
                        if (oBP.GetByKey(cardCode))
                            isUpdate = true;
                        else
                            isUpdate = false;
                    }
                    oBP.CardCode = cardCode;

                    foreach (DataColumn column in dataSource.Columns)
                    {
                        columnName = column.ColumnName;
                        value = row[column.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (column.ColumnName.ToLower() == "cardName".ToLower()) oBP.CardName = value.ToString();
                            if (column.ColumnName.ToLower() == "cardFName".ToLower()) oBP.CardForeignName = value.ToString();
                            //if (column.ColumnName.ToLower() == "GroupCode".ToLower()) oBP.GroupCode = (Int16)value;
                            if (column.ColumnName.ToLower() == "CardType".ToLower())
                            {
                                if (value.ToString() == "C")
                                    oBP.CardType = BoCardTypes.cCustomer;
                                else if (value.ToString() == "S")
                                    oBP.CardType = BoCardTypes.cSupplier;
                                else
                                    oBP.CardType = BoCardTypes.cLid;
                            }
                            if (column.ColumnName.ToLower() == "Address".ToLower()) oBP.Address = value.ToString();
                            if (column.ColumnName.ToLower() == "ZipCode".ToLower()) oBP.ZipCode = value.ToString();
                            if (column.ColumnName.ToLower() == "MailAddres".ToLower()) oBP.MailAddress = value.ToString();
                            if (column.ColumnName.ToLower() == "Phone1".ToLower()) oBP.Phone1 = value.ToString();
                            if (column.ColumnName.ToLower() == "Phone2".ToLower()) oBP.Phone2 = value.ToString();
                            if (column.ColumnName.ToLower() == "Fax".ToLower()) oBP.Fax = value.ToString();
                            if (column.ColumnName.ToLower() == "CntctPrsn".ToLower()) oBP.ContactPerson = value.ToString();
                            if (column.ColumnName.ToLower() == "Notes".ToLower()) oBP.Notes = value.ToString();
                            if (column.ColumnName.ToLower() == "Currency".ToLower()) oBP.Currency = value.ToString();
                            if (column.ColumnName.ToLower() == "LicTradNum".ToLower()) oBP.FederalTaxID = value.ToString();
                            if (column.ColumnName.ToLower() == "Free_Text".ToLower()) oBP.FreeText = value.ToString();
                            if (column.ColumnName.ToLower() == "Slpcode".ToLower()) oBP.SalesPersonCode = (int)value;
                            else if (column.ColumnName.StartsWith("U_"))
                            {
                                BP_UDF(oBP, column, value);
                            }
                        }
                    }


                    //FOR ADDRESS
                    var linesAddressByHeader = linesAddrress.Select(string.Format("CardCode='{0}'", cardCode));
                    if (linesAddressByHeader != null)
                    {
                        oBP.Addresses.Delete();
                        foreach (DataRow rowline in linesAddressByHeader)
                        {
                            foreach (DataColumn columnline in linesAddrress.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "Street".ToLower()) oBP.Addresses.Street = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Address".ToLower()) oBP.Addresses.AddressName = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Address2".ToLower()) oBP.Addresses.AddressName2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Address3".ToLower()) oBP.Addresses.AddressName3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "AdresType".ToLower())
                                    {
                                        if (value.ToString() == "B")
                                            oBP.Addresses.AddressType = BoAddressType.bo_BillTo;
                                        else
                                            oBP.Addresses.AddressType = BoAddressType.bo_ShipTo;
                                    }
                                    if (columnline.ColumnName.ToLower() == "Block".ToLower()) oBP.Addresses.Block = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "ZipCode".ToLower()) oBP.Addresses.ZipCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "City".ToLower()) oBP.Addresses.City = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "County".ToLower()) oBP.Addresses.County = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Country".ToLower()) oBP.Addresses.Country = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "State".ToLower()) oBP.Addresses.State = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Building".ToLower()) oBP.Addresses.BuildingFloorRoom = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "AddrType".ToLower()) oBP.Addresses.TypeOfAddress = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "StreetNo".ToLower()) oBP.Addresses.StreetNo = value.ToString();
                                }
                            }
                            oBP.Addresses.Add();
                        }
                    }

                    //FOR CONTACT PERSON
                    var linesContactByHeader = linesContactPerson.Select(string.Format("CardCode='{0}'", cardCode));
                    if (linesContactByHeader != null)
                    {
                        oBP.ContactEmployees.Delete();
                        foreach (DataRow rowline in linesContactByHeader)
                        {
                            foreach (DataColumn columnline in linesContactPerson.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "Active".ToLower())
                                    {
                                        if (value.ToString() == "Y")
                                            oBP.ContactEmployees.Active = BoYesNoEnum.tYES;
                                        else
                                            oBP.ContactEmployees.Active = BoYesNoEnum.tNO;
                                    }
                                    if (columnline.ColumnName.ToLower() == "Address".ToLower()) oBP.ContactEmployees.Address = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "BirthCity".ToLower()) oBP.ContactEmployees.CityOfBirth = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "BirthDate".ToLower()) oBP.ContactEmployees.DateOfBirth = (DateTime)value;

                                    if (columnline.ColumnName.ToLower() == "EmlGrpCode".ToLower()) oBP.ContactEmployees.EmailGroupCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "E_MailL".ToLower()) oBP.ContactEmployees.E_Mail = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Fax".ToLower()) oBP.ContactEmployees.Fax = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "FirstName".ToLower()) oBP.ContactEmployees.FirstName = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Gender".ToLower())
                                    {
                                        if (value.ToString() == "M")
                                            oBP.ContactEmployees.Gender = BoGenderTypes.gt_Male;
                                        else if (value.ToString() == "F")
                                            oBP.ContactEmployees.Gender = BoGenderTypes.gt_Female;
                                        else
                                            oBP.ContactEmployees.Gender = BoGenderTypes.gt_Undefined;
                                    }
                                    if (columnline.ColumnName.ToLower() == "LastName".ToLower()) oBP.ContactEmployees.LastName = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "MiddleName".ToLower()) oBP.ContactEmployees.MiddleName = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Cellolar".ToLower()) oBP.ContactEmployees.MobilePhone = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Name".ToLower()) oBP.ContactEmployees.Name = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Pager".ToLower()) oBP.ContactEmployees.Pager = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Password".ToLower()) oBP.ContactEmployees.Password = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Tel1".ToLower()) oBP.ContactEmployees.Phone1 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Tel2".ToLower()) oBP.ContactEmployees.Phone2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "BirthPlace".ToLower()) oBP.ContactEmployees.PlaceOfBirth = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Position".ToLower()) oBP.ContactEmployees.Position = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Profession".ToLower()) oBP.ContactEmployees.Profession = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Notes1".ToLower()) oBP.ContactEmployees.Remarks1 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Notes2".ToLower()) oBP.ContactEmployees.Remarks2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Title".ToLower()) oBP.ContactEmployees.Title = value.ToString();
                                }
                            }
                            oBP.ContactEmployees.Add();
                        }
                    }

                    if (isUpdate)
                        oError = oBP.Update();
                    else
                        oError = oBP.Add();

                    if (oError == 0)
                    {
                        row["SyncStatus"] = "Success";
                        row["SyncMessage"] = "";
                        if (obj.TransactionNotification != null && obj.TransactionNotification.ToString() != "")
                        {
                            //obj.TransactionNotification = "update OCRD set U_isSync='Y' where cardCode='{0}'";
                            daConn.ExecQuery(string.Format(obj.TransactionNotification, cardCode), out message);
                        }
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        row["SyncStatus"] = "Fail";
                        row["SyncMessage"] = message;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("[CreateBussinessPartners][Field: " + columnName + "]", ex.Message);
                return false;
            }
        }

        public bool CreateJournalVouchers(SyncObjectCurrent obj, DataTable dataSource, List<SyncDataObjectLine> dataLines, out string message)
        {
            message = "";
            string columnName = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {
                #region OBTD
                //BatchNum    int
                //Status  char
                //NumOfTrans  smallint
                //DateID  datetime
                //LocTotal    numeric
                //FcTotal numeric
                //SysTotal    numeric
                //MemoID  nvarchar
                //UserSign    smallint
                //Remarks nvarchar
                #endregion
                #region OBTF
                //BatchNum    int
                //TransId int
                //BtfStatus   char
                //TransType   nvarchar
                //BaseRef nvarchar
                //RefDate datetime
                //Memo    nvarchar
                //Ref1    nvarchar
                //Ref2    nvarchar
                //CreatedBy   int
                //LocTotal    numeric
                //FcTotal numeric
                //SysTotal    numeric
                //TransCode   nvarchar
                //OrignCurr   nvarchar
                //TransRate   numeric
                //BtfLine int
                //TransCurr   nvarchar
                //Project nvarchar
                //DueDate datetime
                //TaxDate datetime
                //PCAddition  char
                //FinncPriod  int
                //DataSource  char
                //UpdateDate  datetime
                //CreateDate  datetime
                //UserSign    smallint
                //UserSign2   smallint
                //RefndRprt   char
                //LogInstanc  int
                //ObjType nvarchar
                //Indicator   nvarchar
                //AdjTran char
                //RevSource   char
                //StornoDate  datetime
                //StornoToTr  int
                //AutoStorno  char
                //Corisptivi  char
                //VatDate datetime
                //StampTax    char
                //Series  smallint
                //Number  int
                //AutoVAT char
                //DocSeries   smallint
                //FolioPref   nvarchar
                //FolioNum    int
                //CreateTime  smallint
                //BlockDunn   char
                //ReportEU    char
                //Report347   char
                //Printed char
                //DocType nvarchar
                //AttNum  int
                //GenRegNo    char
                //RG23APart2  int
                //RG23CPart2  int
                //MatType int
                //Creator nvarchar
                //Approver    nvarchar
                //Location    int
                //SeqCode smallint
                //Serial  int
                //SeriesStr   nvarchar
                //SubStr  nvarchar
                //AutoWT  char
                //WTSum   numeric
                //WTSumSC numeric
                //WTSumFC numeric
                //WTApplied   numeric
                //WTAppliedS  numeric
                //WTAppliedF  numeric
                //BaseAmnt    numeric
                //BaseAmntSC  numeric
                //BaseAmntFC  numeric
                //BaseVtAt    numeric
                //BaseVtAtSC  numeric
                //BaseVtAtFC  numeric
                //VersionNum  nvarchar
                //BaseTrans   int
                //ResidenNum  char
                //OperatCode  char
                //Ref3    nvarchar
                //SSIExmpt    char
                //SignMsg ntext
                //SignDigest  ntext
                //CertifNum   nvarchar
                //KeyVersion  int
                //CUP int
                //CIG int
                //SupplCode   nvarchar
                //SPSrcType   int
                //SPSrcID int
                //SPSrcDLN    int
                //DeferedTax  char
                //AgrNo   int
                //SeqNum  int
                //ECDPosTyp   char
                //RptPeriod   nvarchar
                //RptMonth    datetime
                //ExTransId   int
                #endregion
                #region BTF1
                //TransId int
                //Line_ID int
                //Account nvarchar
                //Debit   numeric
                //Credit  numeric
                //SYSCred numeric
                //SYSDeb  numeric
                //FCDebit numeric
                //FCCredit    numeric
                //FCCurrency  nvarchar
                //DueDate datetime
                //SourceID    int
                //SourceLine  smallint
                //ShortName   nvarchar
                //IntrnMatch  int
                //ExtrMatch   int
                //ContraAct   nvarchar
                //LineMemo    nvarchar
                //Ref3Line    nvarchar
                //TransType   nvarchar
                //RefDate datetime
                //Ref2Date    datetime
                //Ref1    nvarchar
                //Ref2    nvarchar
                //CreatedBy   int
                //BaseRef nvarchar
                //Project nvarchar
                //TransCode   nvarchar
                //ProfitCode  nvarchar
                //TaxDate datetime
                //SystemRate  numeric
                //MthDate datetime
                //ToMthSum    numeric
                //UserSign    smallint
                //BatchNum    int
                //FinncPriod  int
                //RelTransId  int
                //RelLineID   int
                //RelType char
                //LogInstanc  int
                //VatGroup    nvarchar
                //BaseSum numeric
                //VatRate numeric
                //Indicator   nvarchar
                //AdjTran char
                //RevSource   char
                //ObjType nvarchar
                //VatDate datetime
                //PaymentRef  nvarchar
                //SYSBaseSum  numeric
                //MultMatch   int
                //VatLine char
                //VatAmount   numeric
                //SYSVatSum   numeric
                //Closed  char
                //GrossValue  numeric
                //CheckAbs    int
                //LineType    int
                //DebCred char
                //SequenceNr  int
                //StornoAcc   nvarchar
                //BalDueDeb   numeric
                //BalDueCred  numeric
                //BalFcDeb    numeric
                //BalFcCred   numeric
                //BalScDeb    numeric
                //BalScCred   numeric
                //IsNet   char
                //DunWizBlck  char
                //DunnLevel   int
                //DunDate datetime
                //TaxType smallint
                //TaxPostAcc  char
                //StaCode nvarchar
                //StaType int
                //TaxCode nvarchar
                //ValidFrom   datetime
                //GrossValFc  numeric
                //LvlUpdDate  datetime
                //OcrCode2    nvarchar
                //OcrCode3    nvarchar
                //OcrCode4    nvarchar
                //OcrCode5    nvarchar
                //MIEntry int
                //MIVEntry    int
                //ClsInTP int
                //CenVatCom   int
                //MatType int
                //PstngType   int
                //ValidFrom2  datetime
                //ValidFrom3  datetime
                //ValidFrom4  datetime
                //ValidFrom5  datetime
                //Location    int
                //WTaxCode    nvarchar
                //EquVatRate  numeric
                //EquVatSum   numeric
                //SYSEquSum   numeric
                //TotalVat    numeric
                //SYSTVat numeric
                //WTLiable    char
                //WTLine  char
                //WTApplied   numeric
                //WTAppliedS  numeric
                //WTAppliedF  numeric
                //WTSum   numeric
                //WTSumFC numeric
                //WTSumSC numeric
                //PayBlock    char
                //PayBlckRef  int
                //LicTradNum  nvarchar
                //InterimTyp  int
                //DprId   int
                //MatchRef    nvarchar
                //Ordered char
                //CUP int
                //CIG int
                //BPLId   int
                //BPLName nvarchar
                //VatRegNum   nvarchar
                //SLEDGERF    char
                //ExTransId   int
                //InitRef2    nvarchar
                //InitRef3Ln  nvarchar
                #endregion

                string transId = "";
                object value;

                oCompany = Company_SAP(out message);
                //FOR LINES
                var lines = dataLines[0].DataSource;
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    oJV = oCompany.GetBusinessObject(BoObjectTypes.oJournalVouchers);

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        columnName = columnheader.ColumnName;
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "transId".ToLower())
                            {
                                transId = value.ToString();
                            }
                            if (columnheader.ColumnName.ToLower() == "Ref1".ToLower()) oJV.JournalEntries.Reference = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Ref2".ToLower()) oJV.JournalEntries.Reference2 = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Ref3".ToLower()) oJV.JournalEntries.Reference3 = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "RefDate".ToLower()) oJV.JournalEntries.ReferenceDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DueDate".ToLower()) oJV.JournalEntries.DueDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower()) oJV.JournalEntries.TaxDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "Memo".ToLower()) oJV.JournalEntries.Memo = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Indicator".ToLower()) oJV.JournalEntries.Indicator = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Project".ToLower()) oJV.JournalEntries.ProjectCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TransCode".ToLower()) oJV.JournalEntries.TransactionCode = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_ReferenceKey".ToLower()) oJV.JournalEntries.UserFields.Fields.Item("U_ReferenceKey").Value = (int)value;
                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                JV_UDF(oJV, columnheader, value);
                            }
                        }
                    }

                    //FOR LINES
                    var linesByHeader = lines.Select("transId=" + transId);
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = "Not found any items line.";
                        continue;//Tiếp tục với DocEntry khác
                    }
                    else
                    {
                        foreach (DataRow rowline in linesByHeader)
                        {
                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "BPLId".ToLower()) oJV.JournalEntries.Lines.BPLID = (int)value;
                                    if (columnline.ColumnName.ToLower() == "Account".ToLower()) oJV.JournalEntries.Lines.AccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "ShortName".ToLower()) oJV.JournalEntries.Lines.ShortName = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Debit".ToLower()) oJV.JournalEntries.Lines.Debit = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "Credit".ToLower()) oJV.JournalEntries.Lines.Credit = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "LineMemo".ToLower()) oJV.JournalEntries.Lines.LineMemo = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "ContraAct".ToLower()) oJV.JournalEntries.Lines.ContraAccount = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Ref1".ToLower()) oJV.JournalEntries.Lines.Reference1 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Ref2".ToLower()) oJV.JournalEntries.Lines.Reference2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Project".ToLower()) oJV.JournalEntries.Lines.ProjectCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "ProfitCode".ToLower()) oJV.JournalEntries.Lines.CostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower()) oJV.JournalEntries.Lines.CostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower()) oJV.JournalEntries.Lines.CostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower()) oJV.JournalEntries.Lines.CostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower()) oJV.JournalEntries.Lines.CostingCode5 = value.ToString();
                                    //if (columnline.ColumnName.ToLower() == "U_ReferenceKey".ToLower()) oJV.JournalEntries.Lines.UserFields.Fields.Item("U_ReferenceKey").Value = (int)value;
                                    else if (columnline.ColumnName.StartsWith("U_"))
                                    {
                                        JV_JE_LINES_UDF(oJV.JournalEntries.Lines, columnline, value);
                                    }
                                }
                            }
                            oJV.JournalEntries.Lines.Add();
                        }
                    }

                    oError = oJV.Add();

                    if (oError == 0)
                    {
                        rowheader["SyncStatus"] = "Success";
                        rowheader["SyncMessage"] = "";
                        var referenceKey = oCompany.GetNewObjectKey();
                        referenceKey = referenceKey.Substring(0, referenceKey.IndexOf("\t"));
                        //obj.TransactionNotification = "update OJDT set U_ReferenceKey={0} where transId={1}";
                        daConn.ExecQuery(string.Format(obj.TransactionNotification, referenceKey, transId), out message);
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = message;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("[CreateJournalVouchers][Field: " + columnName + "]", ex.Message);
                return false;
            }
        }

        public bool CreateJournalEntry(SyncObjectCurrent obj, DataTable dataSource, List<SyncDataObjectLine> dataLines, out string message)
        {
            message = "";
            string columnName = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {
                #region OJDT
                //BatchNum    int
                //TransId int
                //BtfStatus   char
                //TransType   nvarchar
                //BaseRef nvarchar
                //RefDate datetime
                //Memo    nvarchar
                //Ref1    nvarchar
                //Ref2    nvarchar
                //CreatedBy   int
                //LocTotal    numeric
                //FcTotal numeric
                //SysTotal    numeric
                //TransCode   nvarchar
                //OrignCurr   nvarchar
                //TransRate   numeric
                //BtfLine int
                //TransCurr   nvarchar
                //Project nvarchar
                //DueDate datetime
                //TaxDate datetime
                //PCAddition  char
                //FinncPriod  int
                //DataSource  char
                //UpdateDate  datetime
                //CreateDate  datetime
                //UserSign    smallint
                //UserSign2   smallint
                //RefndRprt   char
                //LogInstanc  int
                //ObjType nvarchar
                //Indicator   nvarchar
                //AdjTran char
                //RevSource   char
                //StornoDate  datetime
                //StornoToTr  int
                //AutoStorno  char
                //Corisptivi  char
                //VatDate datetime
                //StampTax    char
                //Series  smallint
                //Number  int
                //AutoVAT char
                //DocSeries   smallint
                //FolioPref   nvarchar
                //FolioNum    int
                //CreateTime  smallint
                //BlockDunn   char
                //ReportEU    char
                //Report347   char
                //Printed char
                //DocType nvarchar
                //AttNum  int
                //GenRegNo    char
                //RG23APart2  int
                //RG23CPart2  int
                //MatType int
                //Creator nvarchar
                //Approver    nvarchar
                //Location    int
                //SeqCode smallint
                //Serial  int
                //SeriesStr   nvarchar
                //SubStr  nvarchar
                //AutoWT  char
                //WTSum   numeric
                //WTSumSC numeric
                //WTSumFC numeric
                //WTApplied   numeric
                //WTAppliedS  numeric
                //WTAppliedF  numeric
                //BaseAmnt    numeric
                //BaseAmntSC  numeric
                //BaseAmntFC  numeric
                //BaseVtAt    numeric
                //BaseVtAtSC  numeric
                //BaseVtAtFC  numeric
                //VersionNum  nvarchar
                //BaseTrans   int
                //ResidenNum  char
                //OperatCode  char
                //Ref3    nvarchar
                //SSIExmpt    char
                //SignMsg ntext
                //SignDigest  ntext
                //CertifNum   nvarchar
                //KeyVersion  int
                //CUP int
                //CIG int
                //SupplCode   nvarchar
                //SPSrcType   int
                //SPSrcID int
                //SPSrcDLN    int
                //DeferedTax  char
                //AgrNo   int
                //SeqNum  int
                //ECDPosTyp   char
                //RptPeriod   nvarchar
                //RptMonth    datetime
                //ExTransId   int
                #endregion
                #region JDT1
                //TransId int
                //Line_ID int
                //Account nvarchar
                //Debit   numeric
                //Credit  numeric
                //SYSCred numeric
                //SYSDeb  numeric
                //FCDebit numeric
                //FCCredit    numeric
                //FCCurrency  nvarchar
                //DueDate datetime
                //SourceID    int
                //SourceLine  smallint
                //ShortName   nvarchar
                //IntrnMatch  int
                //ExtrMatch   int
                //ContraAct   nvarchar
                //LineMemo    nvarchar
                //Ref3Line    nvarchar
                //TransType   nvarchar
                //RefDate datetime
                //Ref2Date    datetime
                //Ref1    nvarchar
                //Ref2    nvarchar
                //CreatedBy   int
                //BaseRef nvarchar
                //Project nvarchar
                //TransCode   nvarchar
                //ProfitCode  nvarchar
                //TaxDate datetime
                //SystemRate  numeric
                //MthDate datetime
                //ToMthSum    numeric
                //UserSign    smallint
                //BatchNum    int
                //FinncPriod  int
                //RelTransId  int
                //RelLineID   int
                //RelType char
                //LogInstanc  int
                //VatGroup    nvarchar
                //BaseSum numeric
                //VatRate numeric
                //Indicator   nvarchar
                //AdjTran char
                //RevSource   char
                //ObjType nvarchar
                //VatDate datetime
                //PaymentRef  nvarchar
                //SYSBaseSum  numeric
                //MultMatch   int
                //VatLine char
                //VatAmount   numeric
                //SYSVatSum   numeric
                //Closed  char
                //GrossValue  numeric
                //CheckAbs    int
                //LineType    int
                //DebCred char
                //SequenceNr  int
                //StornoAcc   nvarchar
                //BalDueDeb   numeric
                //BalDueCred  numeric
                //BalFcDeb    numeric
                //BalFcCred   numeric
                //BalScDeb    numeric
                //BalScCred   numeric
                //IsNet   char
                //DunWizBlck  char
                //DunnLevel   int
                //DunDate datetime
                //TaxType smallint
                //TaxPostAcc  char
                //StaCode nvarchar
                //StaType int
                //TaxCode nvarchar
                //ValidFrom   datetime
                //GrossValFc  numeric
                //LvlUpdDate  datetime
                //OcrCode2    nvarchar
                //OcrCode3    nvarchar
                //OcrCode4    nvarchar
                //OcrCode5    nvarchar
                //MIEntry int
                //MIVEntry    int
                //ClsInTP int
                //CenVatCom   int
                //MatType int
                //PstngType   int
                //ValidFrom2  datetime
                //ValidFrom3  datetime
                //ValidFrom4  datetime
                //ValidFrom5  datetime
                //Location    int
                //WTaxCode    nvarchar
                //EquVatRate  numeric
                //EquVatSum   numeric
                //SYSEquSum   numeric
                //TotalVat    numeric
                //SYSTVat numeric
                //WTLiable    char
                //WTLine  char
                //WTApplied   numeric
                //WTAppliedS  numeric
                //WTAppliedF  numeric
                //WTSum   numeric
                //WTSumFC numeric
                //WTSumSC numeric
                //PayBlock    char
                //PayBlckRef  int
                //LicTradNum  nvarchar
                //InterimTyp  int
                //DprId   int
                //MatchRef    nvarchar
                //Ordered char
                //CUP int
                //CIG int
                //BPLId   int
                //BPLName nvarchar
                //VatRegNum   nvarchar
                //SLEDGERF    char
                //ExTransId   int
                //InitRef2    nvarchar
                //InitRef3Ln  nvarchar
                #endregion

                string transId = "";
                object value;

                oCompany = Company_SAP(out message);
                //FOR LINES
                var lines = dataLines[0].DataSource;
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    oJE = oCompany.GetBusinessObject(BoObjectTypes.oJournalEntries);

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        columnName = columnheader.ColumnName;
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "transId".ToLower())
                            {
                                transId = value.ToString();
                            }
                            if (columnheader.ColumnName.ToLower() == "Ref1".ToLower()) oJE.Reference = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Ref2".ToLower()) oJE.Reference2 = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Ref3".ToLower()) oJE.Reference3 = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "RefDate".ToLower()) oJE.ReferenceDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DueDate".ToLower()) oJE.DueDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower()) oJE.TaxDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "Memo".ToLower()) oJE.Memo = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Indicator".ToLower()) oJE.Indicator = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Project".ToLower()) oJE.ProjectCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TransCode".ToLower()) oJE.TransactionCode = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_ReferenceKey".ToLower()) oJE.UserFields.Fields.Item("U_ReferenceKey").Value = (int)value;
                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                JE_UDF(oJE, columnheader, value);
                            }
                        }
                    }

                    //FOR LINES
                    var linesByHeader = lines.Select("transId=" + transId);
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = "Not found any items line.";
                        continue;//Tiếp tục với DocEntry khác
                    }
                    else
                    {
                        foreach (DataRow rowline in linesByHeader)
                        {
                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "BPLId".ToLower()) oJE.Lines.BPLID = (int)value;
                                    if (columnline.ColumnName.ToLower() == "Account".ToLower()) oJE.Lines.AccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "ShortName".ToLower()) oJE.Lines.ShortName = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Debit".ToLower()) oJE.Lines.Debit = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "Credit".ToLower()) oJE.Lines.Credit = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "LineMemo".ToLower()) oJE.Lines.LineMemo = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "ContraAct".ToLower()) oJE.Lines.ContraAccount = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Ref1".ToLower()) oJE.Lines.Reference1 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Ref2".ToLower()) oJE.Lines.Reference2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Project".ToLower()) oJE.Lines.ProjectCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "ProfitCode".ToLower()) oJE.Lines.CostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower()) oJE.Lines.CostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower()) oJE.Lines.CostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower()) oJE.Lines.CostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower()) oJE.Lines.CostingCode5 = value.ToString();
                                    //if (columnline.ColumnName.ToLower() == "U_ReferenceKey".ToLower()) oJE.Lines.UserFields.Fields.Item("U_ReferenceKey").Value = (int)value;
                                    else if (columnline.ColumnName.StartsWith("U_"))
                                    {
                                        JV_JE_LINES_UDF(oJE.Lines, columnline, value);
                                    }
                                }
                            }
                            oJE.Lines.Add();
                        }
                    }

                    oError = oJE.Add();

                    if (oError == 0)
                    {
                        rowheader["SyncStatus"] = "Success";
                        rowheader["SyncMessage"] = "";
                        var referenceKey = oCompany.GetNewObjectKey();
                        //obj.TransactionNotification = "update OJDT set U_ReferenceKey={0} where transId={1}";
                        daConn.ExecQuery(string.Format(obj.TransactionNotification, referenceKey, transId), out message);

                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = message;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("[CreateJournalEntry][Field: " + columnName + "]", ex.Message);
                return false;
            }
        }

        public bool CreateARInvoice(SyncObjectCurrent obj, DataTable dataSource, List<SyncDataObjectLine> dataLines, out string message)
        {
            message = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {
                #region OINV
                //DocEntry    int
                //DocNum  int
                //DocType char
                //DocStatus   char
                //ObjType nvarchar
                //DocDate datetime
                //DocDueDate  datetime
                //CardCode    nvarchar
                //CardName    nvarchar
                //Comments    nvarchar
                #endregion
                #region INV1
                //DocEntry    int
                //LineNum int
                //TargetType  int
                //TrgetEntry  int
                //BaseRef nvarchar
                //BaseType    int
                //BaseEntry   int
                //BaseLine    int
                //LineStatus  char
                //ItemCode    nvarchar
                //Dscription  nvarchar
                //Quantity    numeric
                //ShipDate    datetime
                //OpenQty numeric
                //Price   numeric
                //Currency    nvarchar
                //Rate    numeric
                //DiscPrcnt   numeric
                //LineTotal   numeric
                #endregion

                string docEntry = "";
                object value;

                oCompany = Company_SAP(out message);
                //FOR LINES
                var lines = dataLines[0].DataSource;
                var lineDPM = dataLines[1].DataSource;
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    if (obj.IsObjectDraft)
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                    else
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oInvoices);

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }
                            oDocument.DocObjectCode = BoObjectTypes.oInvoices;
                            if (columnheader.ColumnName.ToLower() == "BPLId".ToLower()) oDocument.BPL_IDAssignedToInvoice = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CardCode".ToLower()) oDocument.CardCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "DocDate".ToLower()) oDocument.DocDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocDueDate".ToLower()) oDocument.DocDueDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower()) oDocument.TaxDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocType".ToLower())
                            {
                                if (value.ToString() == "I")
                                    oDocument.DocType = BoDocumentTypes.dDocument_Items;
                                else
                                    oDocument.DocType = BoDocumentTypes.dDocument_Service;
                            }
                            if (columnheader.ColumnName.ToLower() == "DocTotal".ToLower()) oDocument.DocTotal = (double)(decimal)value;
                            if (columnheader.ColumnName.ToLower() == "Comments".ToLower()) oDocument.Comments = value.ToString();

                            if (columnheader.ColumnName.ToLower() == "NumAtCard".ToLower()) oDocument.NumAtCard = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "SlpCode".ToLower()) oDocument.SalesPersonCode = (int)value;
                            if (columnheader.ColumnName.ToLower() == "OwnerCode".ToLower()) oDocument.DocumentsOwner = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CntctCode".ToLower()) oDocument.ContactPersonCode = (int)value;

                            if (columnheader.ColumnName.ToLower() == "ShipToCode".ToLower()) oDocument.ShipToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address2".ToLower()) oDocument.Address2 = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "PayToCode".ToLower()) oDocument.PayToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address".ToLower()) oDocument.Address = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrackNo".ToLower()) oDocument.TrackingNumber = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Project".ToLower()) oDocument.Project = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Indicator".ToLower()) oDocument.Indicator = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "LicTradNum".ToLower()) oDocument.FederalTaxID = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrnspCode".ToLower()) oDocument.TransportationCode = (short)value;
                            if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower()) oDocument.JournalMemo = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "ImportEnt".ToLower()) oDocument. = value.ToString();

                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                Document_UDF(oDocument, columnheader, value);
                            }

                        }
                    }


                    //DownPayment 
                    if (lineDPM != null)
                    {
                        var linesByDPM = lineDPM.Select("DocEntry=" + docEntry);
                        if (linesByDPM != null)
                        {
                            foreach (var liDPM in linesByDPM)
                            {
                                foreach (DataColumn columnline in lineDPM.Columns)
                                {
                                    value = liDPM[columnline.ColumnName];
                                    if (value != null && value.ToString() != "")
                                    {
                                        if (columnline.ColumnName.ToLower() == "BaseDocNum".ToLower())
                                            oDocument.DownPaymentsToDraw.DocEntry = (int)value;
                                    }
                                }
                                oDocument.DownPaymentsToDraw.Add();
                            }

                        }
                    }


                    //FOR LINES
                    var linesByHeader = lines.Select("DocEntry=" + docEntry);
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = "Not found any items line.";
                        continue;//Tiếp tục với DocEntry khác
                    }
                    else
                    {
                        foreach (DataRow rowline in linesByHeader)
                        {
                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) oDocument.Lines.ItemCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Dscription".ToLower()) oDocument.Lines.ItemDescription = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "AcctCode".ToLower()) oDocument.Lines.AccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Quantity".ToLower()) oDocument.Lines.Quantity = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "UseBaseUn".ToLower())
                                    {
                                        if (value.ToString() == "Y")
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tYES;
                                        else
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tNO;
                                    }
                                    if (columnline.ColumnName.ToLower() == "UomEntry".ToLower())
                                    {
                                        oDocument.Lines.UoMEntry = (int)value;
                                    }
                                    if (columnline.ColumnName.ToLower() == "Price".ToLower()) oDocument.Lines.UnitPrice = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VatGroup".ToLower()) oDocument.Lines.VatGroup = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "PriceAfVAT".ToLower()) oDocument.Lines.PriceAfterVAT = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "OcrCode".ToLower()) oDocument.Lines.CostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower()) oDocument.Lines.CostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower()) oDocument.Lines.CostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower()) oDocument.Lines.CostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower()) oDocument.Lines.CostingCode5 = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "WhsCode".ToLower()) oDocument.Lines.WarehouseCode = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "SubCatNum".ToLower()) oDocument.Lines.SupplierCatNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "DiscPrcnt".ToLower()) oDocument.Lines.DiscountPercent = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VendorNum".ToLower()) oDocument.Lines.VendorNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "SerialNum".ToLower()) oDocument.Lines.SerialNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsAcct".ToLower()) oDocument.Lines.COGSAccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCod".ToLower()) oDocument.Lines.COGSCostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo2".ToLower()) oDocument.Lines.COGSCostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo3".ToLower()) oDocument.Lines.COGSCostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo4".ToLower()) oDocument.Lines.COGSCostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo5".ToLower()) oDocument.Lines.COGSCostingCode5 = value.ToString();

                                    else if (columnline.ColumnName.StartsWith("U_"))
                                    {
                                        Document_Lines_UDF(oDocument.Lines, columnline, value);
                                    }
                                }
                            }
                            oDocument.Lines.Add();
                        }
                    }


                    oError = oDocument.Add();

                    if (oError == 0)
                    {
                        rowheader["SyncStatus"] = "Success";
                        rowheader["SyncMessage"] = "";
                        var referenceKey = oCompany.GetNewObjectKey();
                        //var referenceKey = "";
                        oCompany.GetNewObjectCode(out referenceKey);
                        //obj.TransactionNotification = "update OINV set U_ReferenceKey={0} where DocEntry={1}";
                        daConn.ExecQuery(string.Format(obj.TransactionNotification, referenceKey, docEntry), out message);
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = message;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("CreateARInvoice", ex.Message);
                return false;
            }
        }

        public bool CreateARCreditMemo(SyncObjectCurrent obj, DataTable dataSource, List<SyncDataObjectLine> dataLines, out string message)
        {
            message = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {
                #region ORIN 
                //DocEntry    int
                //DocNum  int
                //DocType char
                //DocStatus   char
                //ObjType nvarchar
                //DocDate datetime
                //DocDueDate  datetime
                //CardCode    nvarchar
                //CardName    nvarchar
                //Comments    nvarchar
                #endregion
                #region RIN1
                //DocEntry    int
                //LineNum int
                //TargetType  int
                //TrgetEntry  int
                //BaseRef nvarchar
                //BaseType    int
                //BaseEntry   int
                //BaseLine    int
                //LineStatus  char
                //ItemCode    nvarchar
                //Dscription  nvarchar
                //Quantity    numeric
                //ShipDate    datetime
                //OpenQty numeric
                //Price   numeric
                //Currency    nvarchar
                //Rate    numeric
                //DiscPrcnt   numeric
                //LineTotal   numeric
                #endregion

                string docEntry = "";
                object value;

                oCompany = Company_SAP(out message);
                //FOR LINES
                var lines = dataLines[0].DataSource;
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    if (obj.IsObjectDraft)
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                    else
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oCreditNotes);

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }
                            oDocument.DocObjectCode = BoObjectTypes.oCreditNotes;
                            if (columnheader.ColumnName.ToLower() == "BPLId".ToLower()) oDocument.BPL_IDAssignedToInvoice = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CardCode".ToLower()) oDocument.CardCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "DocDate".ToLower()) oDocument.DocDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocDueDate".ToLower()) oDocument.DocDueDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower()) oDocument.TaxDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocType".ToLower())
                            {
                                if (value.ToString() == "I")
                                    oDocument.DocType = BoDocumentTypes.dDocument_Items;
                                else
                                    oDocument.DocType = BoDocumentTypes.dDocument_Service;
                            }
                            if (columnheader.ColumnName.ToLower() == "Comments".ToLower()) oDocument.Comments = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "NumAtCard".ToLower()) oDocument.NumAtCard = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "SlpCode".ToLower()) oDocument.SalesPersonCode = (int)value;
                            if (columnheader.ColumnName.ToLower() == "OwnerCode".ToLower()) oDocument.DocumentsOwner = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CntctCode".ToLower()) oDocument.ContactPersonCode = (int)value;

                            if (columnheader.ColumnName.ToLower() == "ShipToCode".ToLower()) oDocument.ShipToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address2".ToLower()) oDocument.Address2 = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "PayToCode".ToLower()) oDocument.PayToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address".ToLower()) oDocument.Address = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrackNo".ToLower()) oDocument.TrackingNumber = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Project".ToLower()) oDocument.Project = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Indicator".ToLower()) oDocument.Indicator = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "LicTradNum".ToLower()) oDocument.FederalTaxID = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrnspCode".ToLower()) oDocument.TransportationCode = (int)value;
                            if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower()) oDocument.JournalMemo = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "ImportEnt".ToLower()) oDocument. = value.ToString();

                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                Document_UDF(oDocument, columnheader, value);
                            }
                        }
                    }

                    //FOR LINES
                    var linesByHeader = lines.Select("DocEntry=" + docEntry);
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = "Not found any items line.";
                        continue;//Tiếp tục với DocEntry khác
                    }
                    else
                    {
                        foreach (DataRow rowline in linesByHeader)
                        {
                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) oDocument.Lines.ItemCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Dscription".ToLower()) oDocument.Lines.ItemDescription = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "AcctCode".ToLower()) oDocument.Lines.AccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Quantity".ToLower()) oDocument.Lines.Quantity = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "UseBaseUn".ToLower())
                                    {
                                        if (value.ToString() == "Y")
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tYES;
                                        else
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tNO;
                                    }
                                    if (columnline.ColumnName.ToLower() == "UomEntry".ToLower())
                                    {
                                        oDocument.Lines.UoMEntry = (int)value;
                                    }
                                    if (columnline.ColumnName.ToLower() == "Price".ToLower()) oDocument.Lines.UnitPrice = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VatGroup".ToLower()) oDocument.Lines.VatGroup = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "PriceAfVAT".ToLower()) oDocument.Lines.PriceAfterVAT = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "OcrCode".ToLower()) oDocument.Lines.CostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower()) oDocument.Lines.CostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower()) oDocument.Lines.CostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower()) oDocument.Lines.CostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower()) oDocument.Lines.CostingCode5 = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "WhsCode".ToLower()) oDocument.Lines.WarehouseCode = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "SubCatNum".ToLower()) oDocument.Lines.SupplierCatNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "DiscPrcnt".ToLower()) oDocument.Lines.CostingCode5 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "VendorNum".ToLower()) oDocument.Lines.VendorNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "SerialNum".ToLower()) oDocument.Lines.SerialNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsAcct".ToLower()) oDocument.Lines.COGSAccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCod".ToLower()) oDocument.Lines.COGSCostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo2".ToLower()) oDocument.Lines.COGSCostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo3".ToLower()) oDocument.Lines.COGSCostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo4".ToLower()) oDocument.Lines.COGSCostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo5".ToLower()) oDocument.Lines.COGSCostingCode5 = value.ToString();

                                    //if (columnline.ColumnName.ToLower() == "U_ReferenceKey".ToLower()) oDocument.Lines.UserFields.Fields.Item("U_ReferenceKey").Value = (int)value;
                                    else if (columnline.ColumnName.StartsWith("U_"))
                                    {
                                        Document_Lines_UDF(oDocument.Lines, columnline, value);
                                    }
                                }
                            }
                            oDocument.Lines.Add();
                        }
                    }


                    oError = oDocument.Add();

                    if (oError == 0)
                    {
                        rowheader["SyncStatus"] = "Success";
                        rowheader["SyncMessage"] = "";
                        var referenceKey = oCompany.GetNewObjectKey();
                        //obj.TransactionNotification = "update ORIN set U_ReferenceKey={0} where DocEntry={1}";
                        daConn.ExecQuery(string.Format(obj.TransactionNotification, referenceKey, docEntry), out message);

                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = message;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("CreateARCreditMemo", ex.Message);
                return false;
            }
        }

        public bool CreateIncomingPayment(SyncObjectCurrent obj, DataTable dataSource, List<SyncDataObjectLine> dataLines, out string message)
        {
            message = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {
                #region ORCT
                //DocEntry    int
                //DocNum  int
                //DocType char
                //Canceled    char
                //Handwrtten  char
                //Printed char
                //DocDate datetime
                //DocDueDate  datetime
                //CardCode    nvarchar
                //CardName    nvarchar
                //Address nvarchar
                //DdctPrcnt   numeric
                //DdctSum numeric
                //DdctSumFC   numeric
                //CashAcct    nvarchar
                //CashSum numeric
                //CashSumFC   numeric
                //CreditSum   numeric
                //CredSumFC   numeric
                //CheckAcct   nvarchar
                //CheckSum    numeric
                //CheckSumFC  numeric
                //TrsfrAcct   nvarchar
                //TrsfrSum    numeric
                //TrsfrSumFC  numeric
                //TrsfrDate   datetime
                //TrsfrRef    nvarchar
                //PayNoDoc    char
                //NoDocSum    numeric
                //NoDocSumFC  numeric
                //DocCurr nvarchar
                //DiffCurr    char
                //DocRate numeric
                //SysRate numeric
                //DocTotal    numeric
                //DocTotalFC  numeric
                //Ref1    nvarchar
                //Ref2    nvarchar
                //CounterRef  nvarchar
                //Comments    nvarchar
                //JrnlMemo    nvarchar
                //TransId int
                //DocTime smallint
                //ShowAtCard  char
                //SpiltTrans  char
                //CreateTran  char
                //Flags   int
                //CntctCode   int
                //DdctSumSy   numeric
                //CashSumSy   numeric
                //CredSumSy   numeric
                //CheckSumSy  numeric
                //TrsfrSumSy  numeric
                //NoDocSumSy  numeric
                //DocTotalSy  numeric
                //ObjType nvarchar
                #endregion
                #region RCT1
                //DocNum  int
                //LineID  int
                //DueDate datetime
                //CheckNum    int
                //BankCode    nvarchar
                //Branch  nvarchar
                //AcctNum nvarchar
                //Details nvarchar
                //Trnsfrable  char
                //CheckSum    numeric
                //Currency    nvarchar
                //Flags   int
                //ObjType nvarchar
                //LogInstanc  int
                //CountryCod  nvarchar
                //CheckAct    nvarchar
                //CheckAbs    int
                //BnkActKey   int
                //ManualChk   char
                #endregion
                #region RCT2
                //DocNum  int
                //InvoiceId   int
                //DocEntry    int
                //SumApplied  numeric
                //AppliedFC   numeric
                //AppliedSys  numeric
                //InvType nvarchar
                //DocRate numeric
                //Flags   int
                //IntrsStat   char
                //DocLine int
                //vatApplied  numeric
                //vatAppldFC  numeric
                //vatAppldSy  numeric
                //selfInv char
                //ObjType nvarchar
                #endregion
                #region RCT3
                //DocNum  int
                //LineID  int
                //CreditCard  smallint
                //CreditAcct  nvarchar
                //CrCardNum   nvarchar
                //CardValid   datetime
                //VoucherNum  nvarchar
                //OwnerIdNum  nvarchar
                //OwnerPhone  nvarchar
                //CrTypeCode  smallint
                //NumOfPmnts  smallint
                //FirstDue    datetime
                //FirstSum    numeric
                //AddPmntSum  numeric
                //CreditSum   numeric
                //CreditCur   nvarchar
                //CreditRate  numeric
                //ConfNum nvarchar
                //CreditType  char
                //CredPmnts   smallint
                //PlCrdStat   nvarchar
                //MagnetStr   nvarchar
                //SpiltCred   char
                //ConsolNum   int
                //ObjType nvarchar
                //LogInstanc  int
                #endregion

                string docEntry = "";
                object value;

                oCompany = Company_SAP(out message);
                //FOR LINES
                var linesCheck = dataLines[0].DataSource;
                var linesInvoice = dataLines[1].DataSource;
                var linesCreditCard = dataLines[2].DataSource;
                var linesAccounts = dataLines[3].DataSource;
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    if (obj.IsObjectDraft)
                        oPayment = oCompany.GetBusinessObject(BoObjectTypes.oPaymentsDrafts);
                    else
                        oPayment = oCompany.GetBusinessObject(BoObjectTypes.oIncomingPayments);

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }
                            oPayment.DocObjectCode = BoPaymentsObjectType.bopot_IncomingPayments;
                            if (columnheader.ColumnName.ToLower() == "BPLId".ToLower()) oPayment.BPLID = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CardCode".ToLower()) oPayment.CardCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "DocDate".ToLower()) oPayment.DocDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocType".ToLower())
                            {
                                if (value.ToString() == "C")
                                    oPayment.DocType = BoRcptTypes.rCustomer;
                                else if (value.ToString() == "S")
                                    oPayment.DocType = BoRcptTypes.rSupplier;
                                else
                                {
                                    oPayment.DocType = BoRcptTypes.rAccount;
                                }
                            }
                            //Payment on Account
                            if (columnheader.ColumnName.ToLower() == "BpAct".ToLower()) oPayment.ControlAccount = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower()) oPayment.JournalRemarks = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Comments".ToLower()) oPayment.Remarks = value.ToString();

                            //if (columnheader.ColumnName.ToLower() == "U_ReferenceKey".ToLower()) oPayment.UserFields.Fields.Item("U_ReferenceKey").Value = (int)value;
                            //if (columnheader.ColumnName.ToLower() == "U_BillNo".ToLower()) oPayment.UserFields.Fields.Item("U_BillNo").Value = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_SoPhieu".ToLower()) oPayment.UserFields.Fields.Item("U_SoPhieu").Value = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_NhaHang".ToLower()) oPayment.UserFields.Fields.Item("U_NhaHang").Value = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_NguoiNhan".ToLower()) oPayment.UserFields.Fields.Item("U_NguoiNhan").Value = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_ContractNo".ToLower()) oPayment.UserFields.Fields.Item("U_ContractNo").Value = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_DiaChi".ToLower()) oPayment.UserFields.Fields.Item("U_DiaChi").Value = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_iSRemark".ToLower()) oPayment.UserFields.Fields.Item("U_iSRemark").Value = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_Cashier".ToLower()) oPayment.UserFields.Fields.Item("U_Cashier").Value = value.ToString();

                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                Payment_UDF(oPayment, columnheader, value);
                            }

                            //FOR CASH
                            if (columnheader.ColumnName.ToLower() == "CashAcct".ToLower()) oPayment.CashAccount = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "CashSum".ToLower()) oPayment.CashSum = (double)(decimal)value;

                            //FOR BANK TRANSFER
                            if (columnheader.ColumnName.ToLower() == "TrsfrAcct".ToLower()) oPayment.TransferAccount = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrsfrDate".ToLower()) oPayment.TransferDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "TrsfrSum".ToLower()) oPayment.TransferSum = (double)(decimal)value;

                            //FOR CHECK
                            if (columnheader.ColumnName.ToLower() == "CheckAcct".ToLower()) oPayment.CheckAccount = value.ToString();

                            //FOR BANK
                            if (columnheader.ColumnName.ToLower() == "BankCode".ToLower()) oPayment.BankCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "BankAcct".ToLower()) oPayment.BankAccount = value.ToString();

                            if (columnheader.ColumnName.ToLower() == "Bcgsum".ToLower()) oPayment.BankChargeAmount = (double)(decimal)value;
                        }
                    }

                    //FOR INVOICES
                    var linesInvoiceByHeader = linesInvoice.Select("docNum=" + docEntry);
                    if (linesInvoiceByHeader != null && linesInvoiceByHeader.Length > 0)
                    {
                        foreach (DataRow rowline in linesInvoiceByHeader)
                        {
                            foreach (DataColumn columnline in linesInvoice.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "DocEntry".ToLower()) oPayment.Invoices.DocEntry = (int)value;
                                    if (columnline.ColumnName.ToLower() == "InvType".ToLower())
                                    {
                                        if (value.ToString() == "13")
                                        {
                                            oPayment.Invoices.InvoiceType = BoRcptInvTypes.it_Invoice;
                                        }
                                        else if (value.ToString() == "14")
                                        {
                                            oPayment.Invoices.InvoiceType = BoRcptInvTypes.it_CredItnote;
                                        }
                                        else if (value.ToString() == "18")
                                        {
                                            oPayment.Invoices.InvoiceType = BoRcptInvTypes.it_PurchaseInvoice;
                                        }
                                        else if (value.ToString() == "19")
                                        {
                                            oPayment.Invoices.InvoiceType = BoRcptInvTypes.it_PurchaseCreditNote;
                                        }
                                        else if (value.ToString() == "203")
                                        {
                                            oPayment.Invoices.InvoiceType = BoRcptInvTypes.it_DownPayment;
                                        }
                                        else if (value.ToString() == "204")
                                        {
                                            oPayment.Invoices.InvoiceType = BoRcptInvTypes.it_PurchaseDownPayment;
                                        }
                                    }
                                }
                            }
                            oPayment.Invoices.Add();
                        }
                    }
                    else
                    {
                        //oPayment.DocTypte = BoRcptTypes.rAccount;
                    }

                    //FOR CREDIT CARD
                    var linesCreditCardByHeader = linesCreditCard.Select("docNum=" + docEntry);
                    if (linesCreditCardByHeader != null && linesCreditCardByHeader.Length > 0)
                    {
                        foreach (DataRow rowline in linesCreditCardByHeader)
                        {
                            foreach (DataColumn columnline in linesCreditCard.Columns)
                            {
                                value = rowline[columnline.ColumnName];

                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "CardValid".ToLower()) oPayment.CreditCards.CardValidUntil = (DateTime)value;
                                    if (columnline.ColumnName.ToLower() == "VoucherNum".ToLower()) oPayment.CreditCards.VoucherNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CreditCard".ToLower()) oPayment.CreditCards.CreditCard = (Int16)value;
                                    if (columnline.ColumnName.ToLower() == "CrCardNum".ToLower()) oPayment.CreditCards.CreditCardNumber = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CreditSum".ToLower()) oPayment.CreditCards.CreditSum = (double)(decimal)value;
                                }
                            }
                            oPayment.CreditCards.Add();
                        }
                    }

                    //FOR CHECK
                    var linesCheckByHeader = linesCheck.Select("docNum=" + docEntry);
                    if (linesCheckByHeader != null && linesCheckByHeader.Length > 0)
                    {
                        foreach (DataRow rowline in linesCheckByHeader)
                        {
                            foreach (DataColumn columnline in linesCheck.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "BankCode".ToLower()) oPayment.Checks.BankCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Branch".ToLower()) oPayment.Checks.Branch = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CountryCod".ToLower()) oPayment.Checks.CountryCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "DueDate".ToLower()) oPayment.Checks.DueDate = (DateTime)value;
                                    if (columnline.ColumnName.ToLower() == "CheckSum".ToLower()) oPayment.Checks.CheckSum = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "CheckNum".ToLower()) oPayment.Checks.CheckNumber = (Int32)value;
                                    if (columnline.ColumnName.ToLower() == "AcctNum".ToLower()) oPayment.Checks.CheckAccount = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Trnsfrable".ToLower())
                                    {
                                        if (value.ToString() == "Y")
                                            oPayment.Checks.Trnsfrable = BoYesNoEnum.tYES;
                                        else
                                            oPayment.Checks.Trnsfrable = BoYesNoEnum.tNO;
                                    }
                                }
                            }
                            oPayment.Checks.Add();
                        }
                    }

                    //FOR ACCOUNTS
                    var linesAccount = linesAccounts.Select("docNum=" + docEntry);
                    if (linesAccount != null && linesAccount.Length > 0)
                    {
                        foreach (DataRow rowline in linesAccount)
                        {
                            foreach (DataColumn columnline in linesAccounts.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "AcctCode".ToLower()) oPayment.AccountPayments.AccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "AcctName".ToLower()) oPayment.AccountPayments.AccountName = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Descrip".ToLower()) oPayment.AccountPayments.Decription = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "VatGroup".ToLower()) oPayment.AccountPayments.VatGroup = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "VatAmnt".ToLower()) oPayment.AccountPayments.VatAmount = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "Project".ToLower()) oPayment.AccountPayments.ProjectCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode".ToLower()) oPayment.AccountPayments.ProfitCenter = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower()) oPayment.AccountPayments.ProfitCenter2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower()) oPayment.AccountPayments.ProfitCenter3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower()) oPayment.AccountPayments.ProfitCenter4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower()) oPayment.AccountPayments.ProfitCenter5 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "SumApplied".ToLower()) oPayment.AccountPayments.SumPaid = (double)(decimal)value;
                                }
                            }
                            oPayment.AccountPayments.Add();
                        }
                    }

                    oError = oPayment.Add();

                    if (oError == 0)
                    {
                        rowheader["SyncStatus"] = "Success";
                        rowheader["SyncMessage"] = "";
                        var referenceKey = oCompany.GetNewObjectKey();
                        //obj.TransactionNotification = "update OCRT set U_ReferenceKey={0} where DocEntry={1}";
                        daConn.ExecQuery(string.Format(obj.TransactionNotification, referenceKey, docEntry), out message);
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = message;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("CreateIncomingPayment", ex.Message);
                return false;
            }
        }

        public bool CreateAPInvoice(SyncObjectCurrent obj, DataTable dataSource, List<SyncDataObjectLine> dataLines, out string message)
        {
            message = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {
                #region OPCH 
                //DocEntry    int
                //DocNum  int
                //DocType char
                //DocStatus   char
                //ObjType nvarchar
                //DocDate datetime
                //DocDueDate  datetime
                //CardCode    nvarchar
                //CardName    nvarchar
                //Comments    nvarchar
                #endregion
                #region PCH1
                //DocEntry    int
                //LineNum int
                //TargetType  int
                //TrgetEntry  int
                //BaseRef nvarchar
                //BaseType    int
                //BaseEntry   int
                //BaseLine    int
                //LineStatus  char
                //ItemCode    nvarchar
                //Dscription  nvarchar
                //Quantity    numeric
                //ShipDate    datetime
                //OpenQty numeric
                //Price   numeric
                //Currency    nvarchar
                //Rate    numeric
                //DiscPrcnt   numeric
                //LineTotal   numeric
                //TotalFrgn   numeric
                //OpenSum numeric
                //OpenSumFC   numeric
                //VendorNum   nvarchar
                //SerialNum   nvarchar
                //WhsCode nvarchar
                //SlpCode int
                //Commission  numeric
                //TreeType    char
                //AcctCode    nvarchar
                //TaxStatus   char
                //GrossBuyPr  numeric
                //PriceBefDi  numeric
                //DocDate datetime
                //Flags   int
                //OpenCreQty  numeric
                //UseBaseUn   char
                //SubCatNum   nvarchar
                //BaseCard    nvarchar
                //TotalSumSy  numeric
                //OpenSumSys  numeric
                //InvntSttus  char
                //OcrCode nvarchar
                //Project nvarchar
                //CodeBars    nvarchar
                //VatPrcnt    numeric
                //VatGroup    nvarchar
                //PriceAfVAT  numeric
                //Height1 numeric
                //Hght1Unit   smallint
                //Height2 numeric
                //Hght2Unit   smallint
                //Width1  numeric
                //Wdth1Unit   smallint
                //Width2  numeric
                //Wdth2Unit   smallint
                //Length1 numeric
                //Len1Unit    smallint
                //length2 numeric
                //Len2Unit    smallint
                //Volume  numeric
                //VolUnit smallint
                //Weight1 numeric
                //Wght1Unit   smallint
                //Weight2 numeric
                //Wght2Unit   smallint
                //Factor1 numeric
                //Factor2 numeric
                //Factor3 numeric
                //Factor4 numeric
                //PackQty numeric
                //UpdInvntry  char
                //BaseDocNum  int
                //BaseAtCard  nvarchar
                //SWW nvarchar
                //VatSum  numeric
                //VatSumFrgn  numeric
                //VatSumSy    numeric
                //FinncPriod  int
                //ObjType nvarchar
                //LogInstanc  int
                //BlockNum    nvarchar
                //ImportLog   nvarchar
                //DedVatSum   numeric
                //DedVatSumF  numeric
                //DedVatSumS  numeric
                //IsAqcuistn  char
                //DistribSum  numeric
                //DstrbSumFC  numeric
                //DstrbSumSC  numeric
                //GrssProfit  numeric
                //GrssProfSC  numeric
                //GrssProfFC  numeric
                //VisOrder    int
                //INMPrice    numeric
                //PoTrgNum    int
                //PoTrgEntry  nvarchar
                //DropShip    char
                //PoLineNum   int
                //Address nvarchar
                //TaxCode nvarchar
                //TaxType char
                //OrigItem    nvarchar
                //BackOrdr    char
                //FreeTxt nvarchar
                //PickStatus  char
                //PickOty numeric
                //PickIdNo    int
                //TrnsCode    smallint
                //VatAppld    numeric
                //VatAppldFC  numeric
                //VatAppldSC  numeric
                //BaseQty numeric
                //BaseOpnQty  numeric
                //VatDscntPr  numeric
                //WtLiable    char
                //DeferrTax   char
                //EquVatPer   numeric
                //EquVatSum   numeric
                //EquVatSumF  numeric
                //EquVatSumS  numeric
                //LineVat numeric
                //LineVatlF   numeric
                //LineVatS    numeric
                //unitMsr nvarchar
                //NumPerMsr   numeric
                //CEECFlag    char
                //ToStock numeric
                //ToDiff  numeric
                //ExciseAmt   numeric
                //TaxPerUnit  numeric
                //TotInclTax  numeric
                //CountryOrg  nvarchar
                //StckDstSum  numeric
                //ReleasQtty  numeric
                //LineType    char
                //TranType    char
                //Text    ntext
                //OwnerCode   int
                //StockPrice  numeric
                //ConsumeFCT  char
                //LstByDsSum  numeric
                //StckINMPr   numeric
                //LstBINMPr   numeric
                //StckDstFc   numeric
                //StckDstSc   numeric
                //LstByDsFc   numeric
                //LstByDsSc   numeric
                //StockSum    numeric
                //StockSumFc  numeric
                //StockSumSc  numeric
                //StckSumApp  numeric
                //StckAppFc   numeric
                //StckAppSc   numeric
                //ShipToCode  nvarchar
                //ShipToDesc  nvarchar
                //StckAppD    numeric
                //StckAppDFC  numeric
                //StckAppDSC  numeric
                //BasePrice   char
                //GTotal  numeric
                //GTotalFC    numeric
                //GTotalSC    numeric
                //DistribExp  char
                //DescOW  char
                //DetailsOW   char
                //GrossBase   smallint
                //VatWoDpm    numeric
                //VatWoDpmFc  numeric
                //VatWoDpmSc  numeric
                //CFOPCode    nvarchar
                //CSTCode nvarchar
                //Usage   int
                //TaxOnly char
                //WtCalced    char
                //QtyToShip   numeric
                //DelivrdQty  numeric
                //OrderedQty  numeric
                //CogsOcrCod  nvarchar
                //CiOppLineN  int
                //CogsAcct    nvarchar
                //ChgAsmBoMW  char
                //ActDelDate  datetime
                //OcrCode2    nvarchar
                //OcrCode3    nvarchar
                //OcrCode4    nvarchar
                //OcrCode5    nvarchar
                //TaxDistSum  numeric
                //TaxDistSFC  numeric
                //TaxDistSSC  numeric
                //PostTax char
                //Excisable   char
                //AssblValue  numeric
                //RG23APart1  int
                //RG23APart2  int
                //RG23CPart1  int
                //RG23CPart2  int
                //CogsOcrCo2  nvarchar
                //CogsOcrCo3  nvarchar
                //CogsOcrCo4  nvarchar
                //CogsOcrCo5  nvarchar
                //LnExcised   char
                //LocCode int
                //StockValue  numeric
                //GPTtlBasPr  numeric
                //unitMsr2    nvarchar
                //NumPerMsr2  numeric
                //SpecPrice   char
                //CSTfIPI nvarchar
                //CSTfPIS nvarchar
                //CSTfCOFINS  nvarchar
                //ExLineNo    nvarchar
                //isSrvCall   char
                //PQTReqQty   numeric
                //PQTReqDate  datetime
                //PcDocType   int
                //PcQuantity  numeric
                //LinManClsd  char
                //VatGrpSrc   char
                //NoInvtryMv  char
                //ActBaseEnt  int
                //ActBaseLn   int
                //ActBaseNum  int
                //OpenRtnQty  numeric
                //AgrNo   int
                //AgrLnNum    int
                //CredOrigin  char
                //Surpluses   numeric
                //DefBreak    numeric
                //Shortages   numeric
                //UomEntry    int
                //UomEntry2   int
                //UomCode nvarchar
                //UomCode2    nvarchar
                //FromWhsCod  nvarchar
                //NeedQty char
                //PartRetire  char
                //RetireQty   numeric
                //RetireAPC   numeric
                //RetirAPCFC  numeric
                //RetirAPCSC  numeric
                //InvQty  numeric
                //OpenInvQty  numeric
                //EnSetCost   char
                //RetCost numeric
                //Incoterms   int
                //TransMod    int
                //LineVendor  nvarchar
                //DistribIS   char
                //ISDistrb    numeric
                //ISDistrbFC  numeric
                //ISDistrbSC  numeric
                //IsByPrdct   char
                //ItemType    int
                //PriceEdit   char
                //PrntLnNum   int
                //LinePoPrss  char
                //FreeChrgBP  char
                //TaxRelev    char
                //LegalText   nvarchar
                //ThirdParty  char
                //LicTradNum  nvarchar
                //InvQtyOnly  char
                //UnencReasn  int
                //ShipFromCo  nvarchar
                //ShipFromDe  nvarchar
                #endregion

                string docEntry = "";
                object value;

                oCompany = Company_SAP(out message);
                //FOR LINES
                var lines = dataLines[0].DataSource;
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    if (obj.IsObjectDraft)
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                    else
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oPurchaseInvoices);

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }
                            oDocument.DocObjectCode = BoObjectTypes.oPurchaseInvoices;
                            if (columnheader.ColumnName.ToLower() == "BPLId".ToLower()) oDocument.BPL_IDAssignedToInvoice = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CardCode".ToLower()) oDocument.CardCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "DocDate".ToLower()) oDocument.DocDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocDueDate".ToLower()) oDocument.DocDueDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower()) oDocument.TaxDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocType".ToLower())
                            {
                                if (value.ToString() == "I")
                                    oDocument.DocType = BoDocumentTypes.dDocument_Items;
                                else
                                    oDocument.DocType = BoDocumentTypes.dDocument_Service;
                            }
                            if (columnheader.ColumnName.ToLower() == "DocTotal".ToLower()) oDocument.DocTotal = (double)(decimal)value;
                            if (columnheader.ColumnName.ToLower() == "Comments".ToLower()) oDocument.Comments = value.ToString();

                            if (columnheader.ColumnName.ToLower() == "NumAtCard".ToLower()) oDocument.NumAtCard = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "SlpCode".ToLower()) oDocument.SalesPersonCode = (int)value;
                            if (columnheader.ColumnName.ToLower() == "OwnerCode".ToLower()) oDocument.DocumentsOwner = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CntctCode".ToLower()) oDocument.ContactPersonCode = (int)value;

                            if (columnheader.ColumnName.ToLower() == "ShipToCode".ToLower()) oDocument.ShipToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address2".ToLower()) oDocument.Address2 = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "PayToCode".ToLower()) oDocument.PayToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address".ToLower()) oDocument.Address = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrackNo".ToLower()) oDocument.TrackingNumber = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Project".ToLower()) oDocument.Project = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Indicator".ToLower()) oDocument.Indicator = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "LicTradNum".ToLower()) oDocument.FederalTaxID = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrnspCode".ToLower()) oDocument.TransportationCode = (int)value;
                            if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower()) oDocument.JournalMemo = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "ImportEnt".ToLower()) oDocument. = value.ToString();

                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                Document_UDF(oDocument, columnheader, value);
                            }
                        }
                    }

                    //FOR LINES
                    var linesByHeader = lines.Select("DocEntry=" + docEntry);
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = "Not found any items line.";
                        continue;//Tiếp tục với DocEntry khác
                    }
                    else
                    {
                        foreach (DataRow rowline in linesByHeader)
                        {
                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) oDocument.Lines.ItemCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Dscription".ToLower()) oDocument.Lines.ItemDescription = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "AcctCode".ToLower()) oDocument.Lines.AccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Quantity".ToLower()) oDocument.Lines.Quantity = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "UseBaseUn".ToLower())
                                    {
                                        if (value.ToString() == "Y")
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tYES;
                                        else
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tNO;
                                    }
                                    if (columnline.ColumnName.ToLower() == "UomEntry".ToLower())
                                    {
                                        oDocument.Lines.UoMEntry = (int)value;
                                    }

                                    if (columnline.ColumnName.ToLower() == "Price".ToLower()) oDocument.Lines.UnitPrice = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VatGroup".ToLower()) oDocument.Lines.VatGroup = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "PriceAfVAT".ToLower()) oDocument.Lines.PriceAfterVAT = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "OcrCode".ToLower()) oDocument.Lines.CostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower()) oDocument.Lines.CostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower()) oDocument.Lines.CostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower()) oDocument.Lines.CostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower()) oDocument.Lines.CostingCode5 = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "WhsCode".ToLower()) oDocument.Lines.WarehouseCode = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "SubCatNum".ToLower()) oDocument.Lines.SupplierCatNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "DiscPrcnt".ToLower()) oDocument.Lines.CostingCode5 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "VendorNum".ToLower()) oDocument.Lines.VendorNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "SerialNum".ToLower()) oDocument.Lines.SerialNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsAcct".ToLower()) oDocument.Lines.COGSAccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCod".ToLower()) oDocument.Lines.COGSCostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo2".ToLower()) oDocument.Lines.COGSCostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo3".ToLower()) oDocument.Lines.COGSCostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo4".ToLower()) oDocument.Lines.COGSCostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo5".ToLower()) oDocument.Lines.COGSCostingCode5 = value.ToString();

                                    //if (columnline.ColumnName.ToLower() == "U_ReferenceKey".ToLower()) oDocument.Lines.UserFields.Fields.Item("U_ReferenceKey").Value = (int)value;
                                    else if (columnline.ColumnName.StartsWith("U_"))
                                    {
                                        Document_Lines_UDF(oDocument.Lines, columnline, value);
                                    }
                                }
                            }
                            oDocument.Lines.Add();
                        }
                    }


                    oError = oDocument.Add();

                    if (oError == 0)
                    {
                        rowheader["SyncStatus"] = "Success";
                        rowheader["SyncMessage"] = "";
                        var referenceKey = oCompany.GetNewObjectKey();
                        //obj.TransactionNotification = "update OPCH set U_ReferenceKey={0} where DocEntry={1}";
                        daConn.ExecQuery(string.Format(obj.TransactionNotification, referenceKey, docEntry), out message);
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = message;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("CreateAPInvoice", ex.Message);
                return false;
            }
        }

        public bool CreateAPCreditMemo(SyncObjectCurrent obj, DataTable dataSource, List<SyncDataObjectLine> dataLines, out string message)
        {
            message = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {
                #region ORPC 
                //DocEntry    int
                //DocNum  int
                //DocType char
                //DocStatus   char
                //ObjType nvarchar
                //DocDate datetime
                //DocDueDate  datetime
                //CardCode    nvarchar
                //CardName    nvarchar
                //Comments    nvarchar
                #endregion
                #region RPC1
                //DocEntry    int
                //LineNum int
                //TargetType  int
                //TrgetEntry  int
                //BaseRef nvarchar
                //BaseType    int
                //BaseEntry   int
                //BaseLine    int
                //LineStatus  char
                //ItemCode    nvarchar
                //Dscription  nvarchar
                //Quantity    numeric
                //ShipDate    datetime
                //OpenQty numeric
                //Price   numeric
                //Currency    nvarchar
                //Rate    numeric
                //DiscPrcnt   numeric
                //LineTotal   numeric
                #endregion

                string docEntry = "";
                object value;

                oCompany = Company_SAP(out message);
                //FOR LINES
                var lines = dataLines[0].DataSource;
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    if (obj.IsObjectDraft)
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                    else
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oPurchaseCreditNotes);

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }
                            oDocument.DocObjectCode = BoObjectTypes.oPurchaseCreditNotes;
                            if (columnheader.ColumnName.ToLower() == "BPLId".ToLower()) oDocument.BPL_IDAssignedToInvoice = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CardCode".ToLower()) oDocument.CardCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "DocDate".ToLower()) oDocument.DocDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocDueDate".ToLower()) oDocument.DocDueDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower()) oDocument.TaxDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocType".ToLower())
                            {
                                if (value.ToString() == "I")
                                    oDocument.DocType = BoDocumentTypes.dDocument_Items;
                                else
                                    oDocument.DocType = BoDocumentTypes.dDocument_Service;
                            }
                            if (columnheader.ColumnName.ToLower() == "Comments".ToLower()) oDocument.Comments = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "NumAtCard".ToLower()) oDocument.NumAtCard = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "SlpCode".ToLower()) oDocument.SalesPersonCode = (int)value;
                            if (columnheader.ColumnName.ToLower() == "OwnerCode".ToLower()) oDocument.DocumentsOwner = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CntctCode".ToLower()) oDocument.ContactPersonCode = (int)value;

                            if (columnheader.ColumnName.ToLower() == "ShipToCode".ToLower()) oDocument.ShipToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address2".ToLower()) oDocument.Address2 = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "PayToCode".ToLower()) oDocument.PayToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address".ToLower()) oDocument.Address = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrackNo".ToLower()) oDocument.TrackingNumber = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Project".ToLower()) oDocument.Project = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Indicator".ToLower()) oDocument.Indicator = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "LicTradNum".ToLower()) oDocument.FederalTaxID = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrnspCode".ToLower()) oDocument.TransportationCode = (int)value;
                            if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower()) oDocument.JournalMemo = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "ImportEnt".ToLower()) oDocument. = value.ToString();

                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                Document_UDF(oDocument, columnheader, value);
                            }
                        }
                    }

                    //FOR LINES
                    var linesByHeader = lines.Select("DocEntry=" + docEntry);
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = "Not found any items line.";
                        continue;//Tiếp tục với DocEntry khác
                    }
                    else
                    {
                        foreach (DataRow rowline in linesByHeader)
                        {
                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) oDocument.Lines.ItemCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Dscription".ToLower()) oDocument.Lines.ItemDescription = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "AcctCode".ToLower()) oDocument.Lines.AccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Quantity".ToLower()) oDocument.Lines.Quantity = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "UseBaseUn".ToLower())
                                    {
                                        if (value.ToString() == "Y")
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tYES;
                                        else
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tNO;
                                    }
                                    if (columnline.ColumnName.ToLower() == "UomEntry".ToLower())
                                    {
                                        oDocument.Lines.UoMEntry = (int)value;
                                    }
                                    if (columnline.ColumnName.ToLower() == "Price".ToLower()) oDocument.Lines.UnitPrice = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VatGroup".ToLower()) oDocument.Lines.VatGroup = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "PriceAfVAT".ToLower()) oDocument.Lines.PriceAfterVAT = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "OcrCode".ToLower()) oDocument.Lines.CostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower()) oDocument.Lines.CostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower()) oDocument.Lines.CostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower()) oDocument.Lines.CostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower()) oDocument.Lines.CostingCode5 = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "WhsCode".ToLower()) oDocument.Lines.WarehouseCode = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "SubCatNum".ToLower()) oDocument.Lines.SupplierCatNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "DiscPrcnt".ToLower()) oDocument.Lines.CostingCode5 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "VendorNum".ToLower()) oDocument.Lines.VendorNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "SerialNum".ToLower()) oDocument.Lines.SerialNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsAcct".ToLower()) oDocument.Lines.COGSAccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCod".ToLower()) oDocument.Lines.COGSCostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo2".ToLower()) oDocument.Lines.COGSCostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo3".ToLower()) oDocument.Lines.COGSCostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo4".ToLower()) oDocument.Lines.COGSCostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo5".ToLower()) oDocument.Lines.COGSCostingCode5 = value.ToString();

                                    //if (columnline.ColumnName.ToLower() == "U_ReferenceKey".ToLower()) oDocument.Lines.UserFields.Fields.Item("U_ReferenceKey").Value = (int)value;
                                    else if (columnline.ColumnName.StartsWith("U_"))
                                    {
                                        Document_Lines_UDF(oDocument.Lines, columnline, value);
                                    }
                                }
                            }
                            oDocument.Lines.Add();
                        }
                    }


                    oError = oDocument.Add();

                    if (oError == 0)
                    {
                        rowheader["SyncStatus"] = "Success";
                        rowheader["SyncMessage"] = "";
                        var referenceKey = oCompany.GetNewObjectKey();
                        //obj.TransactionNotification = "update ORPC set U_ReferenceKey={0} where DocEntry={1}";
                        daConn.ExecQuery(string.Format(obj.TransactionNotification, referenceKey, docEntry), out message);
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = message;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("CreateAPCreditMemo", ex.Message);
                return false;
            }
        }

        public bool CreateOutgoingPayment(SyncObjectCurrent obj, DataTable dataSource, List<SyncDataObjectLine> dataLines, out string message)
        {
            message = "";
            string columnName = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {
                #region OVPM
                //DocEntry    int
                //DocNum  int
                //DocType char
                //Canceled    char
                //Handwrtten  char
                //Printed char
                //DocDate datetime
                //DocDueDate  datetime
                //CardCode    nvarchar
                //CardName    nvarchar
                //Address nvarchar
                //DdctPrcnt   numeric
                //DdctSum numeric
                //DdctSumFC   numeric
                //CashAcct    nvarchar
                //CashSum numeric
                //CashSumFC   numeric
                //CreditSum   numeric
                //CredSumFC   numeric
                //CheckAcct   nvarchar
                //CheckSum    numeric
                //CheckSumFC  numeric
                //TrsfrAcct   nvarchar
                //TrsfrSum    numeric
                //TrsfrSumFC  numeric
                //TrsfrDate   datetime
                //TrsfrRef    nvarchar
                //PayNoDoc    char
                //NoDocSum    numeric
                //NoDocSumFC  numeric
                //DocCurr nvarchar
                //DiffCurr    char
                //DocRate numeric
                //SysRate numeric
                //DocTotal    numeric
                //DocTotalFC  numeric
                //Ref1    nvarchar
                //Ref2    nvarchar
                //CounterRef  nvarchar
                //Comments    nvarchar
                //JrnlMemo    nvarchar
                //TransId int
                //DocTime smallint
                //ShowAtCard  char
                //SpiltTrans  char
                //CreateTran  char
                //Flags   int
                //CntctCode   int
                //DdctSumSy   numeric
                //CashSumSy   numeric
                //CredSumSy   numeric
                //CheckSumSy  numeric
                //TrsfrSumSy  numeric
                //NoDocSumSy  numeric
                //DocTotalSy  numeric
                //ObjType nvarchar
                #endregion
                #region VPM1
                //DocNum  int
                //LineID  int
                //DueDate datetime
                //CheckNum    int
                //BankCode    nvarchar
                //Branch  nvarchar
                //AcctNum nvarchar
                //Details nvarchar
                //Trnsfrable  char
                //CheckSum    numeric
                //Currency    nvarchar
                //Flags   int
                //ObjType nvarchar
                //LogInstanc  int
                //CountryCod  nvarchar
                //CheckAct    nvarchar
                //CheckAbs    int
                //BnkActKey   int
                //ManualChk   char
                #endregion
                #region VPM2
                //DocNum  int
                //InvoiceId   int
                //DocEntry    int
                //SumApplied  numeric
                //AppliedFC   numeric
                //AppliedSys  numeric
                //InvType nvarchar
                //DocRate numeric
                //Flags   int
                //IntrsStat   char
                //DocLine int
                //vatApplied  numeric
                //vatAppldFC  numeric
                //vatAppldSy  numeric
                //selfInv char
                //ObjType nvarchar
                #endregion
                #region VPM3
                //DocNum  int
                //LineID  int
                //CreditCard  smallint
                //CreditAcct  nvarchar
                //CrCardNum   nvarchar
                //CardValid   datetime
                //VoucherNum  nvarchar
                //OwnerIdNum  nvarchar
                //OwnerPhone  nvarchar
                //CrTypeCode  smallint
                //NumOfPmnts  smallint
                //FirstDue    datetime
                //FirstSum    numeric
                //AddPmntSum  numeric
                //CreditSum   numeric
                //CreditCur   nvarchar
                //CreditRate  numeric
                //ConfNum nvarchar
                //CreditType  char
                //CredPmnts   smallint
                //PlCrdStat   nvarchar
                //MagnetStr   nvarchar
                //SpiltCred   char
                //ConsolNum   int
                //ObjType nvarchar
                //LogInstanc  int
                #endregion

                string docEntry = "";
                object value;

                oCompany = Company_SAP(out message);
                //FOR LINES
                var linesCheck = dataLines[0].DataSource;
                var linesInvoice = dataLines[1].DataSource;
                var linesCreditCard = dataLines[2].DataSource;
                var linesAccounts = dataLines[3].DataSource;
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    if (obj.IsObjectDraft)
                        oPayment = oCompany.GetBusinessObject(BoObjectTypes.oPaymentsDrafts);
                    else
                        oPayment = oCompany.GetBusinessObject(BoObjectTypes.oVendorPayments);

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        columnName = columnheader.ColumnName;
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }
                            oPayment.DocObjectCode = BoPaymentsObjectType.bopot_OutgoingPayments;
                            if (columnheader.ColumnName.ToLower() == "BPLId".ToLower()) oPayment.BPLID = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CardCode".ToLower())
                                oPayment.CardCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "DocDate".ToLower())
                                oPayment.DocDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocType".ToLower())
                            {
                                if (value.ToString() == "C")
                                    oPayment.DocType = BoRcptTypes.rCustomer;
                                else if (value.ToString() == "S")
                                    oPayment.DocType = BoRcptTypes.rSupplier;
                                else
                                    oPayment.DocType = BoRcptTypes.rAccount;
                            }
                            if (columnheader.ColumnName.ToLower() == "BpAct".ToLower()) oPayment.ControlAccount = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower()) oPayment.JournalRemarks = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Comments".ToLower()) oPayment.Remarks = value.ToString();

                            //if (columnheader.ColumnName.ToLower() == "U_ReferenceKey".ToLower()) oPayment.UserFields.Fields.Item("U_ReferenceKey").Value = (int)value;
                            //if (columnheader.ColumnName.ToLower() == "U_BillNo".ToLower()) oPayment.UserFields.Fields.Item("U_BillNo").Value = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_SoPhieu".ToLower()) oPayment.UserFields.Fields.Item("U_SoPhieu").Value = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_NhaHang".ToLower()) oPayment.UserFields.Fields.Item("U_NhaHang").Value = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_NguoiNhan".ToLower()) oPayment.UserFields.Fields.Item("U_NguoiNhan").Value = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_ContractNo".ToLower()) oPayment.UserFields.Fields.Item("U_ContractNo").Value = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_DiaChi".ToLower()) oPayment.UserFields.Fields.Item("U_DiaChi").Value = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_iSRemark".ToLower()) oPayment.UserFields.Fields.Item("U_iSRemark").Value = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "U_Cashier".ToLower()) oPayment.UserFields.Fields.Item("U_Cashier").Value = value.ToString();
                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                Payment_UDF(oPayment, columnheader, value);
                            }

                            //FOR CASH
                            if (columnheader.ColumnName.ToLower() == "CashAcct".ToLower())
                                oPayment.CashAccount = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "CashSum".ToLower())
                                oPayment.CashSum = (double)(decimal)value;

                            //FOR BANK TRANSFER
                            if (columnheader.ColumnName.ToLower() == "TrsfrAcct".ToLower()) oPayment.TransferAccount = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrsfrDate".ToLower()) oPayment.TransferDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "TrsfrSum".ToLower()) oPayment.TransferSum = (double)(decimal)value;

                            //FOR CHECK
                            if (columnheader.ColumnName.ToLower() == "CheckAcct".ToLower()) oPayment.CheckAccount = value.ToString();

                            //FOR BANK
                            if (columnheader.ColumnName.ToLower() == "BankCode".ToLower()) oPayment.BankCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "BankAcct".ToLower()) oPayment.BankAccount = value.ToString();

                            if (columnheader.ColumnName.ToLower() == "Bcgsum".ToLower()) oPayment.BankChargeAmount = (double)(decimal)value;

                        }
                    }

                    //FOR INVOICES
                    var linesInvoiceByHeader = linesInvoice.Select("docNum=" + docEntry);
                    if (linesInvoiceByHeader != null && linesInvoiceByHeader.Length > 0)
                    {
                        foreach (DataRow rowline in linesInvoiceByHeader)
                        {
                            foreach (DataColumn columnline in linesInvoice.Columns)
                            {
                                columnName = columnline.ColumnName;
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "DocEntry".ToLower())
                                        oPayment.Invoices.DocEntry = (int)value;
                                    if (columnline.ColumnName.ToLower() == "InvType".ToLower())
                                    {
                                        if (value.ToString() == "13")
                                        {
                                            oPayment.Invoices.InvoiceType = BoRcptInvTypes.it_Invoice;
                                        }
                                        else if (value.ToString() == "14")
                                        {
                                            oPayment.Invoices.InvoiceType = BoRcptInvTypes.it_CredItnote;
                                        }
                                        else if (value.ToString() == "18")
                                        {
                                            oPayment.Invoices.InvoiceType = BoRcptInvTypes.it_PurchaseInvoice;
                                        }
                                        else if (value.ToString() == "19")
                                        {
                                            oPayment.Invoices.InvoiceType = BoRcptInvTypes.it_PurchaseCreditNote;
                                        }
                                        else if (value.ToString() == "203")
                                        {
                                            oPayment.Invoices.InvoiceType = BoRcptInvTypes.it_DownPayment;
                                        }
                                        else if (value.ToString() == "204")
                                        {
                                            oPayment.Invoices.InvoiceType = BoRcptInvTypes.it_PurchaseDownPayment;
                                        }
                                    }
                                }
                            }
                            oPayment.Invoices.Add();
                        }
                    }
                    else
                    {
                        //oPayment.AccountPayments.;
                    }

                    //FOR CREDIT CARD
                    var linesCreditCardByHeader = linesCreditCard.Select("docNum=" + docEntry);
                    if (linesCreditCardByHeader != null && linesCreditCardByHeader.Length > 0)
                    {
                        foreach (DataRow rowline in linesCreditCardByHeader)
                        {
                            foreach (DataColumn columnline in linesCreditCard.Columns)
                            {
                                columnName = columnline.ColumnName;
                                value = rowline[columnline.ColumnName];

                                if (value != null && value.ToString() != "")
                                {
                                    //if (columnline.ColumnName.ToLower() == "CardValid".ToLower()) oPayment.CreditCards.CardValidUntil = (DateTime)value;
                                    if (columnline.ColumnName.ToLower() == "VoucherNum".ToLower())
                                        oPayment.CreditCards.VoucherNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CreditCard".ToLower())
                                        oPayment.CreditCards.CreditCard = (Int16)value;
                                    //if (columnline.ColumnName.ToLower() == "CrCardNum".ToLower()) oPayment.CreditCards.CreditCardNumber = "111111";
                                    if (columnline.ColumnName.ToLower() == "CreditSum".ToLower())
                                        oPayment.CreditCards.CreditSum = (double)(decimal)value;
                                }
                            }
                            oPayment.CreditCards.Add();
                        }
                    }

                    //FOR CHECK
                    var linesCheckByHeader = linesCheck.Select("docNum=" + docEntry);
                    if (linesCheckByHeader != null && linesCheckByHeader.Length > 0)
                    {
                        foreach (DataRow rowline in linesCheckByHeader)
                        {
                            foreach (DataColumn columnline in linesCheck.Columns)
                            {
                                columnName = columnline.ColumnName;
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "BankCode".ToLower()) oPayment.Checks.BankCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Branch".ToLower()) oPayment.Checks.Branch = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CountryCod".ToLower()) oPayment.Checks.CountryCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "DueDate".ToLower()) oPayment.Checks.DueDate = (DateTime)value;
                                    if (columnline.ColumnName.ToLower() == "CheckSum".ToLower()) oPayment.Checks.CheckSum = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "CheckNum".ToLower()) oPayment.Checks.CheckNumber = (Int32)value;
                                    if (columnline.ColumnName.ToLower() == "AcctNum".ToLower()) oPayment.Checks.CheckAccount = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Trnsfrable".ToLower())
                                    {
                                        if (value.ToString() == "Y")
                                            oPayment.Checks.Trnsfrable = BoYesNoEnum.tYES;
                                        else
                                            oPayment.Checks.Trnsfrable = BoYesNoEnum.tNO;
                                    }
                                }
                            }
                            oPayment.Checks.Add();
                        }
                    }

                    //FOR ACCOUNTS
                    var linesAccount = linesAccounts.Select("docNum=" + docEntry);
                    if (linesAccount != null && linesAccount.Length > 0)
                    {
                        foreach (DataRow rowline in linesAccount)
                        {
                            foreach (DataColumn columnline in linesAccounts.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "AcctCode".ToLower()) oPayment.AccountPayments.AccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "AcctName".ToLower()) oPayment.AccountPayments.AccountName = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Descrip".ToLower()) oPayment.AccountPayments.Decription = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "VatGroup".ToLower()) oPayment.AccountPayments.VatGroup = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "VatAmnt".ToLower()) oPayment.AccountPayments.VatAmount = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "Project".ToLower()) oPayment.AccountPayments.ProjectCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode".ToLower()) oPayment.AccountPayments.ProfitCenter = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower()) oPayment.AccountPayments.ProfitCenter2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower()) oPayment.AccountPayments.ProfitCenter3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower()) oPayment.AccountPayments.ProfitCenter4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower()) oPayment.AccountPayments.ProfitCenter5 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "SumApplied".ToLower()) oPayment.AccountPayments.SumPaid = (double)(decimal)value;
                                }
                            }
                            oPayment.AccountPayments.Add();
                        }
                    }

                    oError = oPayment.Add();

                    if (oError == 0)
                    {
                        rowheader["SyncStatus"] = "Success";
                        rowheader["SyncMessage"] = "";
                        var referenceKey = oCompany.GetNewObjectKey();
                        //obj.TransactionNotification = "update OVPM set U_ReferenceKey={0} where DocEntry={1}";
                        daConn.ExecQuery(string.Format(obj.TransactionNotification, referenceKey, docEntry), out message);
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = message;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("[CreateIncomingPayment][Field: " + columnName + "]", ex.Message);
                return false;
            }
        }

        public bool CreateARDownPaymentInvoice(SyncObjectCurrent obj, DataTable dataSource, List<SyncDataObjectLine> dataLines, out string message)
        {
            message = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {
                #region ODPI
                //DocEntry    int
                //DocNum  int
                //DocType char
                //DocStatus   char
                //ObjType nvarchar
                //DocDate datetime
                //DocDueDate  datetime
                //CardCode    nvarchar
                //CardName    nvarchar
                //Comments    nvarchar
                #endregion
                #region DPI1
                //DocEntry    int
                //LineNum int
                //TargetType  int
                //TrgetEntry  int
                //BaseRef nvarchar
                //BaseType    int
                //BaseEntry   int
                //BaseLine    int
                //LineStatus  char
                //ItemCode    nvarchar
                //Dscription  nvarchar
                //Quantity    numeric
                //ShipDate    datetime
                //OpenQty numeric
                //Price   numeric
                //Currency    nvarchar
                //Rate    numeric
                //DiscPrcnt   numeric
                //LineTotal   numeric
                #endregion

                string docEntry = "";
                object value;

                oCompany = Company_SAP(out message);
                //FOR LINES
                var lines = dataLines[0].DataSource;
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    if (obj.IsObjectDraft)
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                    else
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDownPayments);

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }
                            oDocument.DocObjectCode = BoObjectTypes.oDownPayments;
                            oDocument.DownPaymentType = DownPaymentTypeEnum.dptInvoice;
                            if (columnheader.ColumnName.ToLower() == "BPLId".ToLower()) oDocument.BPL_IDAssignedToInvoice = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CardCode".ToLower()) oDocument.CardCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "DocDate".ToLower()) oDocument.DocDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocDueDate".ToLower()) oDocument.DocDueDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower()) oDocument.TaxDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocType".ToLower())
                            {
                                if (value.ToString() == "I")
                                    oDocument.DocType = BoDocumentTypes.dDocument_Items;
                                else
                                    oDocument.DocType = BoDocumentTypes.dDocument_Service;
                            }
                            if (columnheader.ColumnName.ToLower() == "DocTotal".ToLower()) oDocument.DocTotal = (double)(decimal)value;
                            if (columnheader.ColumnName.ToLower() == "Comments".ToLower()) oDocument.Comments = value.ToString();

                            if (columnheader.ColumnName.ToLower() == "NumAtCard".ToLower()) oDocument.NumAtCard = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "SlpCode".ToLower()) oDocument.SalesPersonCode = (int)value;
                            if (columnheader.ColumnName.ToLower() == "OwnerCode".ToLower()) oDocument.DocumentsOwner = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CntctCode".ToLower()) oDocument.ContactPersonCode = (int)value;

                            if (columnheader.ColumnName.ToLower() == "ShipToCode".ToLower()) oDocument.ShipToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address2".ToLower()) oDocument.Address2 = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "PayToCode".ToLower()) oDocument.PayToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address".ToLower()) oDocument.Address = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrackNo".ToLower()) oDocument.TrackingNumber = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Project".ToLower()) oDocument.Project = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Indicator".ToLower()) oDocument.Indicator = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "LicTradNum".ToLower()) oDocument.FederalTaxID = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrnspCode".ToLower()) oDocument.TransportationCode = (short)value;
                            if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower()) oDocument.JournalMemo = value.ToString();

                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                Document_UDF(oDocument, columnheader, value);
                            }

                        }
                    }

                    //FOR LINES
                    var linesByHeader = lines.Select("DocEntry=" + docEntry);
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = "Not found any items line.";
                        continue;//Tiếp tục với DocEntry khác
                    }
                    else
                    {
                        foreach (DataRow rowline in linesByHeader)
                        {
                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) oDocument.Lines.ItemCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Dscription".ToLower()) oDocument.Lines.ItemDescription = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "AcctCode".ToLower()) oDocument.Lines.AccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Quantity".ToLower()) oDocument.Lines.Quantity = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "UseBaseUn".ToLower())
                                    {
                                        if (value.ToString() == "Y")
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tYES;
                                        else
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tNO;
                                    }
                                    if (columnline.ColumnName.ToLower() == "UomEntry".ToLower())
                                    {
                                        oDocument.Lines.UoMEntry = (int)value;
                                    }
                                    if (columnline.ColumnName.ToLower() == "Price".ToLower()) oDocument.Lines.UnitPrice = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VatGroup".ToLower()) oDocument.Lines.VatGroup = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "PriceAfVAT".ToLower()) oDocument.Lines.PriceAfterVAT = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "OcrCode".ToLower()) oDocument.Lines.CostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower()) oDocument.Lines.CostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower()) oDocument.Lines.CostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower()) oDocument.Lines.CostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower()) oDocument.Lines.CostingCode5 = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "WhsCode".ToLower()) oDocument.Lines.WarehouseCode = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "SubCatNum".ToLower()) oDocument.Lines.SupplierCatNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "DiscPrcnt".ToLower()) oDocument.Lines.DiscountPercent = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VendorNum".ToLower()) oDocument.Lines.VendorNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "SerialNum".ToLower()) oDocument.Lines.SerialNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsAcct".ToLower()) oDocument.Lines.COGSAccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCod".ToLower()) oDocument.Lines.COGSCostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo2".ToLower()) oDocument.Lines.COGSCostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo3".ToLower()) oDocument.Lines.COGSCostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo4".ToLower()) oDocument.Lines.COGSCostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo5".ToLower()) oDocument.Lines.COGSCostingCode5 = value.ToString();

                                    else if (columnline.ColumnName.StartsWith("U_"))
                                    {
                                        Document_Lines_UDF(oDocument.Lines, columnline, value);
                                    }
                                }
                            }
                            oDocument.Lines.Add();
                        }
                    }


                    oError = oDocument.Add();

                    if (oError == 0)
                    {
                        rowheader["SyncStatus"] = "Success";
                        rowheader["SyncMessage"] = "";

                        var referenceKey = oCompany.GetNewObjectKey();
                        //obj.TransactionNotification = "update OINV set U_ReferenceKey={0} where DocEntry={1}";
                        daConn.ExecQuery(string.Format(obj.TransactionNotification, referenceKey, docEntry), out message);
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = message;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("CreateARInvoice", ex.Message);
                return false;
            }
        }

        public bool CreateAPDownPaymentInvoice(SyncObjectCurrent obj, DataTable dataSource, List<SyncDataObjectLine> dataLines, out string message)
        {
            message = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {
                #region ODPO
                //DocEntry    int
                //DocNum  int
                //DocType char
                //DocStatus   char
                //ObjType nvarchar
                //DocDate datetime
                //DocDueDate  datetime
                //CardCode    nvarchar
                //CardName    nvarchar
                //Comments    nvarchar
                #endregion
                #region DPO1
                //DocEntry    int
                //LineNum int
                //TargetType  int
                //TrgetEntry  int
                //BaseRef nvarchar
                //BaseType    int
                //BaseEntry   int
                //BaseLine    int
                //LineStatus  char
                //ItemCode    nvarchar
                //Dscription  nvarchar
                //Quantity    numeric
                //ShipDate    datetime
                //OpenQty numeric
                //Price   numeric
                //Currency    nvarchar
                //Rate    numeric
                //DiscPrcnt   numeric
                //LineTotal   numeric
                #endregion

                string docEntry = "";
                object value;

                oCompany = Company_SAP(out message);
                //FOR LINES
                var lines = dataLines[0].DataSource;
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    if (obj.IsObjectDraft)
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                    else
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oPurchaseDownPayments);

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }
                            oDocument.DocObjectCode = BoObjectTypes.oPurchaseDownPayments;
                            oDocument.DownPaymentType = DownPaymentTypeEnum.dptInvoice;
                            if (columnheader.ColumnName.ToLower() == "BPLId".ToLower()) oDocument.BPL_IDAssignedToInvoice = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CardCode".ToLower()) oDocument.CardCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "DocDate".ToLower()) oDocument.DocDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocDueDate".ToLower()) oDocument.DocDueDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower()) oDocument.TaxDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocType".ToLower())
                            {
                                if (value.ToString() == "I")
                                    oDocument.DocType = BoDocumentTypes.dDocument_Items;
                                else
                                    oDocument.DocType = BoDocumentTypes.dDocument_Service;
                            }
                            if (columnheader.ColumnName.ToLower() == "DocTotal".ToLower()) oDocument.DocTotal = (double)(decimal)value;
                            if (columnheader.ColumnName.ToLower() == "Comments".ToLower()) oDocument.Comments = value.ToString();

                            if (columnheader.ColumnName.ToLower() == "NumAtCard".ToLower()) oDocument.NumAtCard = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "SlpCode".ToLower()) oDocument.SalesPersonCode = (int)value;
                            if (columnheader.ColumnName.ToLower() == "OwnerCode".ToLower()) oDocument.DocumentsOwner = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CntctCode".ToLower()) oDocument.ContactPersonCode = (int)value;

                            if (columnheader.ColumnName.ToLower() == "ShipToCode".ToLower()) oDocument.ShipToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address2".ToLower()) oDocument.Address2 = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "PayToCode".ToLower()) oDocument.PayToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address".ToLower()) oDocument.Address = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrackNo".ToLower()) oDocument.TrackingNumber = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Project".ToLower()) oDocument.Project = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Indicator".ToLower()) oDocument.Indicator = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "LicTradNum".ToLower()) oDocument.FederalTaxID = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrnspCode".ToLower()) oDocument.TransportationCode = (short)value;
                            if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower()) oDocument.JournalMemo = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "ImportEnt".ToLower()) oDocument. = value.ToString();

                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                Document_UDF(oDocument, columnheader, value);
                            }

                        }
                    }

                    //FOR LINES
                    var linesByHeader = lines.Select("DocEntry=" + docEntry);
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = "Not found any items line.";
                        continue;//Tiếp tục với DocEntry khác
                    }
                    else
                    {
                        foreach (DataRow rowline in linesByHeader)
                        {
                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) oDocument.Lines.ItemCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Dscription".ToLower()) oDocument.Lines.ItemDescription = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "AcctCode".ToLower()) oDocument.Lines.AccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Quantity".ToLower()) oDocument.Lines.Quantity = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "UseBaseUn".ToLower())
                                    {
                                        if (value.ToString() == "Y")
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tYES;
                                        else
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tNO;
                                    }
                                    if (columnline.ColumnName.ToLower() == "UomEntry".ToLower())
                                    {
                                        oDocument.Lines.UoMEntry = (int)value;
                                    }

                                    if (columnline.ColumnName.ToLower() == "BaseEntry".ToLower()) oDocument.Lines.BaseEntry = (int)value;
                                    if (columnline.ColumnName.ToLower() == "BaseLine".ToLower()) oDocument.Lines.BaseLine = (int)value;
                                    if (columnline.ColumnName.ToLower() == "BaseType".ToLower()) oDocument.Lines.BaseType = (int)value;


                                    if (columnline.ColumnName.ToLower() == "Price".ToLower()) oDocument.Lines.UnitPrice = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VatGroup".ToLower()) oDocument.Lines.VatGroup = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "PriceAfVAT".ToLower()) oDocument.Lines.PriceAfterVAT = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "OcrCode".ToLower()) oDocument.Lines.CostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower()) oDocument.Lines.CostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower()) oDocument.Lines.CostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower()) oDocument.Lines.CostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower()) oDocument.Lines.CostingCode5 = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "WhsCode".ToLower()) oDocument.Lines.WarehouseCode = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "SubCatNum".ToLower()) oDocument.Lines.SupplierCatNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "DiscPrcnt".ToLower()) oDocument.Lines.DiscountPercent = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VendorNum".ToLower()) oDocument.Lines.VendorNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "SerialNum".ToLower()) oDocument.Lines.SerialNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsAcct".ToLower()) oDocument.Lines.COGSAccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCod".ToLower()) oDocument.Lines.COGSCostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo2".ToLower()) oDocument.Lines.COGSCostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo3".ToLower()) oDocument.Lines.COGSCostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo4".ToLower()) oDocument.Lines.COGSCostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo5".ToLower()) oDocument.Lines.COGSCostingCode5 = value.ToString();

                                    else if (columnline.ColumnName.StartsWith("U_"))
                                    {
                                        Document_Lines_UDF(oDocument.Lines, columnline, value);
                                    }
                                }
                            }
                            oDocument.Lines.Add();
                        }
                    }


                    oError = oDocument.Add();

                    if (oError == 0)
                    {
                        rowheader["SyncStatus"] = "Success";
                        rowheader["SyncMessage"] = "";
                        var referenceKey = oCompany.GetNewObjectKey();
                        //obj.TransactionNotification = "update OINV set U_ReferenceKey={0} where DocEntry={1}";
                        daConn.ExecQuery(string.Format(obj.TransactionNotification, referenceKey, docEntry), out message);
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = message;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("CreateARInvoice", ex.Message);
                return false;
            }
        }

        public bool CreateProjects(SyncObjectCurrent obj, DataTable dataSource, out string message)
        {
            message = "";
            string columnName = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {

                #region Database OPRJ
                //PrjCode nvarchar
                //PrjName nvarchar
                //Locked  char
                //DataSource  char
                //UserSign    smallint
                //ValidFrom   datetime
                //ValidTo datetime
                //Active  char
                //LogInstanc  int
                //UserSign2   smallint
                //UpdateDate  datetime
                #endregion

                string projectCode = "";
                object value;
                bool isUpdate = false;

                oCompany = Company_SAP(out message);
                foreach (DataRow row in dataSource.Rows)
                {
                    oCmpSrv = oCompany.GetCompanyService();

                    oServiceProj = oCmpSrv.GetBusinessService(ServiceTypes.ProjectsService);
                    oProj = oServiceProj.GetDataInterface(ProjectsServiceDataInterfaces.psProject);

                    if (dataSource.Columns.Contains("PrjCode") == false)
                    {
                        message = getFunctionError("[CreateProjects]", "Not found field PrjCode.");
                        return false;
                    }
                    else
                    {
                        projectCode = row["PrjCode"].ToString();
                        isUpdate = false;
                        try
                        {
                            oProjParams = oServiceProj.GetDataInterface(SAPbobsCOM.ProjectsServiceDataInterfaces.psProjectParams);
                            oProjParams.Code = projectCode; ;
                            oProj = oServiceProj.GetProject(oProjParams);

                            isUpdate = true;
                        }
                        catch (Exception)
                        {

                        }

                    }

                    oProj.Code = projectCode;

                    foreach (DataColumn column in dataSource.Columns)
                    {
                        columnName = column.ColumnName;
                        value = row[column.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (column.ColumnName.ToLower() == "PrjName".ToLower()) oProj.Name = value.ToString();
                            else if (column.ColumnName.ToLower() == "ValidFrom".ToLower()) oProj.ValidFrom = (DateTime)value;
                            else if (column.ColumnName.ToLower() == "ValidTo".ToLower()) oProj.ValidTo = (DateTime)value;
                            else if (column.ColumnName.ToLower() == "Active".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oProj.Active = BoYesNoEnum.tYES;
                                else
                                    oProj.Active = BoYesNoEnum.tNO;
                            }

                            else if (column.ColumnName.StartsWith("U_"))
                            {
                                Projects_UDF(oProj, column, value);
                            }
                        }
                    }

                    string isSync = "N";
                    try
                    {
                        if (isUpdate)
                            oServiceProj.UpdateProject(oProj);
                        else
                            oServiceProj.AddProject(oProj);

                        isSync = "Y";
                        row["SyncStatus"] = "Success";
                        row["SyncMessage"] = "";
                        if (obj.TransactionNotification != null && obj.TransactionNotification.ToString() != "")
                        {
                            daConn.ExecQuery(string.Format(obj.TransactionNotification, projectCode, isSync, message), out message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                        row["SyncStatus"] = "Fail";
                        row["SyncMessage"] = message;

                        if (obj.TransactionNotification != null && obj.TransactionNotification.ToString() != "")
                        {
                            daConn.ExecQuery(string.Format(obj.TransactionNotification, projectCode, isSync, message), out message);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("[CreateProjects][Field: " + columnName + "]", ex.Message);
                return false;
            }
        }

        public bool SendMail(SyncObjectCurrent obj, DataTable dataSource, out string message)
        {
            message = "";
            string columnName = "";
            object value;
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);

            try
            {
                string object_key = "";
                string object_type = "";
                string mail_from = "";
                string mail_to = "";
                string mail_subject = "";
                string mail_body = "";
                string mail_host = "";//smtp.google.com
                string mail_user = "";
                string mail_password = "";
                int smtp_port = 25;
                bool smtp_enablessl = true;
                string attachFile = "";

                foreach (DataRow row in dataSource.Rows)
                {
                    foreach (DataColumn column in dataSource.Columns)
                    {
                        columnName = column.ColumnName;
                        value = row[column.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (column.ColumnName.ToLower() == "ObjectKey".ToLower()) object_key = value.ToString();
                            else if (column.ColumnName.ToLower() == "ObjectType".ToLower()) object_type = value.ToString();
                            else if (column.ColumnName.ToLower() == "Username".ToLower()) mail_user = value.ToString();
                            else if (column.ColumnName.ToLower() == "Password".ToLower()) mail_password = value.ToString();
                            else if (column.ColumnName.ToLower() == "Host".ToLower()) mail_host = value.ToString();
                            else if (column.ColumnName.ToLower() == "SmtpPort".ToLower()) smtp_port = (int)value;
                            else if (column.ColumnName.ToLower() == "EnableSsl".ToLower()) smtp_enablessl = (bool)value;
                            else if (column.ColumnName.ToLower() == "MailFrom".ToLower()) mail_from = value.ToString();
                            else if (column.ColumnName.ToLower() == "MailTo".ToLower()) mail_to = value.ToString();
                            else if (column.ColumnName.ToLower() == "Subject".ToLower()) mail_subject = value.ToString();
                            else if (column.ColumnName.ToLower() == "Body".ToLower()) mail_body = value.ToString();
                            else if (column.ColumnName.ToLower() == "AttachFile".ToLower()) attachFile = value.ToString();
                        }
                    }
                    string isSent = "N";
                    try
                    {
                        SmtpClient client = new SmtpClient();
                        client.EnableSsl = smtp_enablessl;
                        client.Port = smtp_port;
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;

                        client.Credentials = new System.Net.NetworkCredential(mail_user, mail_password);
                        client.Host = mail_host;

                        MailMessage mail = new MailMessage();
                        mail.From = new MailAddress(mail_from);
                        mail.To.Add(new MailAddress(mail_to));

                        mail.BodyEncoding = UTF8Encoding.UTF8;
                        mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                        mail.Subject = mail_subject;
                        mail.Body = mail_body;
                        if (string.IsNullOrEmpty(attachFile) == false)
                            mail.Attachments.Add(new System.Net.Mail.Attachment(attachFile));
                        client.Send(mail);

                        isSent = "Y";
                        row["SyncStatus"] = "Success";
                        row["SyncMessage"] = "";
                        if (obj.TransactionNotification != null && obj.TransactionNotification.ToString() != "")
                        {
                            daConn.ExecQuery(string.Format(obj.TransactionNotification, object_key, object_type, isSent, message), out message);
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                        row["SyncStatus"] = "Fail";
                        row["SyncMessage"] = message;

                        if (obj.TransactionNotification != null && obj.TransactionNotification.ToString() != "")
                        {
                            daConn.ExecQuery(string.Format(obj.TransactionNotification, object_key, object_type, isSent, message), out message);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            return false;
        }

        public bool CreateGoodsIssue(SyncObjectCurrent obj, DataTable dataSource, List<SyncDataObjectLine> dataLines, out string message)
        {
            message = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {
                #region OIGE
                //DocEntry    int
                //DocNum  int
                //DocType char
                //DocStatus   char
                //ObjType nvarchar
                //DocDate datetime
                //DocDueDate  datetime
                //CardCode    nvarchar
                //CardName    nvarchar
                //Comments    nvarchar
                #endregion
                #region IGE1
                //DocEntry    int
                //LineNum int
                //TargetType  int
                //TrgetEntry  int
                //BaseRef nvarchar
                //BaseType    int
                //BaseEntry   int
                //BaseLine    int
                //LineStatus  char
                //ItemCode    nvarchar
                //Dscription  nvarchar
                //Quantity    numeric
                //ShipDate    datetime
                //OpenQty numeric
                //Price   numeric
                //Currency    nvarchar
                //Rate    numeric
                //DiscPrcnt   numeric
                //LineTotal   numeric
                #endregion

                string docEntry = "";
                object value;

                oCompany = Company_SAP(out message);
                //FOR LINES
                var lines = dataLines[0].DataSource;
                var lineDPM = dataLines[1].DataSource;
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    if (obj.IsObjectDraft)
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                    else
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oInventoryGenExit);

                    //FOR HEADER
                    oDocument.DocObjectCode = BoObjectTypes.oInventoryGenExit;
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }
                            if (columnheader.ColumnName.ToLower() == "BPLId".ToLower()) oDocument.BPL_IDAssignedToInvoice = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CardCode".ToLower()) oDocument.CardCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "DocDate".ToLower()) oDocument.DocDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocDueDate".ToLower()) oDocument.DocDueDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower()) oDocument.TaxDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocType".ToLower())
                            {
                                if (value.ToString() == "I")
                                    oDocument.DocType = BoDocumentTypes.dDocument_Items;
                                else
                                    oDocument.DocType = BoDocumentTypes.dDocument_Service;
                            }
                            if (columnheader.ColumnName.ToLower() == "DocTotal".ToLower()) oDocument.DocTotal = (double)(decimal)value;
                            if (columnheader.ColumnName.ToLower() == "Comments".ToLower()) oDocument.Comments = value.ToString();

                            if (columnheader.ColumnName.ToLower() == "NumAtCard".ToLower()) oDocument.NumAtCard = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "SlpCode".ToLower()) oDocument.SalesPersonCode = (int)value;
                            if (columnheader.ColumnName.ToLower() == "OwnerCode".ToLower()) oDocument.DocumentsOwner = (int)value;
                            if (columnheader.ColumnName.ToLower() == "CntctCode".ToLower()) oDocument.ContactPersonCode = (int)value;

                            if (columnheader.ColumnName.ToLower() == "ShipToCode".ToLower()) oDocument.ShipToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address2".ToLower()) oDocument.Address2 = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "PayToCode".ToLower()) oDocument.PayToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address".ToLower()) oDocument.Address = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrackNo".ToLower()) oDocument.TrackingNumber = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Project".ToLower()) oDocument.Project = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Indicator".ToLower()) oDocument.Indicator = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "LicTradNum".ToLower()) oDocument.FederalTaxID = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrnspCode".ToLower()) oDocument.TransportationCode = (short)value;
                            if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower()) oDocument.JournalMemo = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "ImportEnt".ToLower()) oDocument. = value.ToString();

                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                Document_UDF(oDocument, columnheader, value);
                            }

                        }
                    }

                    //FOR LINES
                    var linesByHeader = lines.Select("DocEntry=" + docEntry);
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = "Not found any items line.";
                        continue;//Tiếp tục với DocEntry khác
                    }
                    else
                    {
                        foreach (DataRow rowline in linesByHeader)
                        {
                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) oDocument.Lines.ItemCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Dscription".ToLower()) oDocument.Lines.ItemDescription = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "AcctCode".ToLower()) oDocument.Lines.AccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Quantity".ToLower()) oDocument.Lines.Quantity = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "UseBaseUn".ToLower())
                                    {
                                        if (value.ToString() == "Y")
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tYES;
                                        else
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tNO;
                                    }
                                    if (columnline.ColumnName.ToLower() == "UomEntry".ToLower())
                                    {
                                        oDocument.Lines.UoMEntry = (int)value;
                                    }
                                    if (columnline.ColumnName.ToLower() == "Price".ToLower()) oDocument.Lines.UnitPrice = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VatGroup".ToLower()) oDocument.Lines.VatGroup = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "PriceAfVAT".ToLower()) oDocument.Lines.PriceAfterVAT = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "OcrCode".ToLower()) oDocument.Lines.CostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower()) oDocument.Lines.CostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower()) oDocument.Lines.CostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower()) oDocument.Lines.CostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower()) oDocument.Lines.CostingCode5 = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "WhsCode".ToLower()) oDocument.Lines.WarehouseCode = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "SubCatNum".ToLower()) oDocument.Lines.SupplierCatNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "DiscPrcnt".ToLower()) oDocument.Lines.DiscountPercent = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VendorNum".ToLower()) oDocument.Lines.VendorNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "SerialNum".ToLower()) oDocument.Lines.SerialNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsAcct".ToLower()) oDocument.Lines.COGSAccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCod".ToLower()) oDocument.Lines.COGSCostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo2".ToLower()) oDocument.Lines.COGSCostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo3".ToLower()) oDocument.Lines.COGSCostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo4".ToLower()) oDocument.Lines.COGSCostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo5".ToLower()) oDocument.Lines.COGSCostingCode5 = value.ToString();

                                    else if (columnline.ColumnName.StartsWith("U_"))
                                    {
                                        Document_Lines_UDF(oDocument.Lines, columnline, value);
                                    }
                                }
                            }
                            oDocument.Lines.Add();
                        }
                    }


                    oError = oDocument.Add();

                    if (oError == 0)
                    {
                        rowheader["SyncStatus"] = "Success";
                        rowheader["SyncMessage"] = "";
                        var referenceKey = oCompany.GetNewObjectKey();
                        //var referenceKey = "";
                        oCompany.GetNewObjectCode(out referenceKey);
                        //obj.TransactionNotification = "update OINV set U_ReferenceKey={0} where DocEntry={1}";
                        daConn.ExecQuery(string.Format(obj.TransactionNotification, referenceKey, docEntry), out message);
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        rowheader["SyncStatus"] = "Fail";
                        rowheader["SyncMessage"] = message;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("CreateGoodsIssue", ex.Message);
                return false;
            }
        }



        #region UDF VALUE
        private void Item_UDF(Items oItem, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void Projects_UDF(Project oProject, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oProject.UserFields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oProject.UserFields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oProject.UserFields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oProject.UserFields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oProject.UserFields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oProject.UserFields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oProject.UserFields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oProject.UserFields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void BP_UDF(BusinessPartners oBP, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void COA_UDF(ChartOfAccounts oChartOfAccount, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void Document_UDF(Documents oDocument, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void Document_Lines_UDF(Document_Lines oDocumentLine, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void Payment_UDF(Payments oPayment, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void JE_UDF(JournalEntries oJE, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void JV_UDF(IJournalVouchers oJV, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void JV_JE_LINES_UDF(JournalEntries_Lines oJVLine, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }
        #endregion
    }
}
