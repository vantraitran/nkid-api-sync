﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.Mail;

using DataTools.Models;
using DataTools.DataAccess;
using DataTools.DataAccess.Sync;

using SAPbobsCOM;
using DataTools.Models.Entity;

using System.Configuration;
using System.IO;
using DataTools.Utility;

namespace DataTools.SAPAccess
{
    public class CreateObject : SyncBase
    {

        public int oError = 0;
        public string oErrorMessage = "";
        public int lineNum = 0;
        public bool isAddNew = false;

        static SAPbobsCOM.Company oStaticCompany;
        static SAPbobsCOM.Company oStaticCompany_HCM;
        static SAPbobsCOM.Company oStaticCompany_HN;
        public SAPbobsCOM.Company oCompany;
        public SAPbobsCOM.CompanyService oCmpSrv;

        private SAPbobsCOM.InventoryCounting oInvCounting = null;
        private SAPbobsCOM.IInventoryCountingsService oInvCountingService = null;

        private SAPbobsCOM.ChartOfAccounts oChartOfAccount = null;
        private SAPbobsCOM.Items oItem = null;
        private SAPbobsCOM.BusinessPartners oBP = null;

        private SAPbobsCOM.Documents oDocument = null;
        private SAPbobsCOM.StockTransfer oInvTrfs = null;
        private SAPbobsCOM.ProductionOrders oPrdctOder = null;

        private SAPbobsCOM.Payments oPayment = null;
        private SAPbobsCOM.JournalEntries oJE = null;
        private SAPbobsCOM.JournalVouchers oJV = null;
        private SAPbobsCOM.ProjectsService oServiceProj = null;
        private SAPbobsCOM.ProjectParams oProjParams = null;
        private SAPbobsCOM.Project oProj = null;

        public SAPbobsCOM.Company Company_SAP(out string message)
        {
            message = "";

            if (oStaticCompany == null || oStaticCompany.Connected == false
                || oStaticCompany.GetCompanyDate() < DateTime.Today)
            {

                oStaticCompany = new Company();

                string[] cnif = new CryptoEngine().Decrypt(System.Configuration.ConfigurationManager.AppSettings["info"]).Split('|');

                var server = cnif[0];
                var licenseServer = cnif[1];
                var serverType = cnif[2];
                var dbName = cnif[3];
                var dbUser = cnif[4];
                var dbPass = cnif[5];
                var sapUser = cnif[6];
                var sapPass = cnif[7];

                //var server = System.Configuration.ConfigurationManager.AppSettings["SERVER"];
                //var licenseServer = System.Configuration.ConfigurationManager.AppSettings["LICENSESERVER"];
                //var serverType = System.Configuration.ConfigurationManager.AppSettings["SERVERTYPE"];
                //var dbName = System.Configuration.ConfigurationManager.AppSettings["DBNAME"];
                //var dbUser = System.Configuration.ConfigurationManager.AppSettings["DBUSER"];
                //var dbPass = System.Configuration.ConfigurationManager.AppSettings["DBPASS"];
                //var sapUser = System.Configuration.ConfigurationManager.AppSettings["COMPANYUSER"];
                //var sapPass = System.Configuration.ConfigurationManager.AppSettings["COMPANYPASS"];

                oStaticCompany.Server = server;
                oStaticCompany.LicenseServer = licenseServer;
                oStaticCompany.CompanyDB = dbName;
                oStaticCompany.DbUserName = dbUser;
                oStaticCompany.DbPassword = dbPass;

                if (serverType == "MSSQL_2008")
                    oStaticCompany.DbServerType = BoDataServerTypes.dst_MSSQL2008;
                else if (serverType == "MSSQL_2012")
                    oStaticCompany.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                else if (serverType == "MSSQL_2014")
                    oStaticCompany.DbServerType = BoDataServerTypes.dst_MSSQL2014;
                else if (serverType == "MSSQL_2016")
                {
                    //oStaticCompany.DbServerType = BoDataServerTypes.dst_MSSQL2016;
                }
                else if (serverType == "HANADB")
                    oStaticCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;

                oStaticCompany.UserName = sapUser;
                oStaticCompany.Password = sapPass;


                //WriteEventLog(sapUser);
                //WriteEventLog(sapPass);
            }
            if (!oStaticCompany.Connected)
            {
                int i = oStaticCompany.Connect();
                oStaticCompany.GetLastError(out i, out message);
            }

            return oStaticCompany;
        }

        public SAPbobsCOM.Company Company_SAP_By_Location(string Location,out string message)
        {
            message = "";
            if (Location.ToUpper() == "S" || Location.ToUpper() == "HCM")//HCM
            {
                if (oStaticCompany_HCM == null || oStaticCompany_HCM.Connected == false
    || oStaticCompany_HCM.GetCompanyDate() < DateTime.Today)
                {

                    oStaticCompany_HCM = new Company();

                    var server = System.Configuration.ConfigurationManager.AppSettings["SERVER"];
                    var licenseServer = System.Configuration.ConfigurationManager.AppSettings["LICENSESERVER"];
                    var serverType = System.Configuration.ConfigurationManager.AppSettings["SERVERTYPE"];
                    var dbName = System.Configuration.ConfigurationManager.AppSettings["DBNAME"];
                    var dbUser = System.Configuration.ConfigurationManager.AppSettings["DBUSER"];
                    var dbPass = System.Configuration.ConfigurationManager.AppSettings["DBPASS"];
                    var sapUser = System.Configuration.ConfigurationManager.AppSettings["COMPANYUSER"];
                    var sapPass = System.Configuration.ConfigurationManager.AppSettings["COMPANYPASS"];

                    oStaticCompany_HCM.Server = server;
                    oStaticCompany_HCM.LicenseServer = licenseServer;
                    oStaticCompany_HCM.CompanyDB = dbName;
                    oStaticCompany_HCM.DbUserName = dbUser;
                    oStaticCompany_HCM.DbPassword = dbPass;

                    if (serverType == "MSSQL_2008")
                        oStaticCompany_HCM.DbServerType = BoDataServerTypes.dst_MSSQL2008;
                    else if (serverType == "MSSQL_2012")
                        oStaticCompany_HCM.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                    else if (serverType == "MSSQL_2014")
                        oStaticCompany_HCM.DbServerType = BoDataServerTypes.dst_MSSQL2014;
                    else if (serverType == "MSSQL_2016")
                    {
                        //oStaticCompany_HCM.DbServerType = BoDataServerTypes.dst_MSSQL2016;
                    }
                    else if (serverType == "HANADB")
                        oStaticCompany_HCM.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;

                    oStaticCompany_HCM.UserName = sapUser;
                    oStaticCompany_HCM.Password = sapPass;


                    //WriteEventLog(sapUser);
                    //WriteEventLog(sapPass);
                }
                if (!oStaticCompany_HCM.Connected)
                {
                    int i = oStaticCompany_HCM.Connect();
                    oStaticCompany_HCM.GetLastError(out i, out message);
                }
                return oStaticCompany_HCM;
            }
            else if (Location.ToUpper() == "H" || Location.ToUpper() == "HN")// HANOI
            {
                if (oStaticCompany_HN == null || oStaticCompany_HN.Connected == false
                    || oStaticCompany_HN.GetCompanyDate() < DateTime.Today)
                {

                    oStaticCompany_HN = new Company();

                    var server = System.Configuration.ConfigurationManager.AppSettings["SERVER"];
                    var licenseServer = System.Configuration.ConfigurationManager.AppSettings["LICENSESERVER"];
                    var serverType = System.Configuration.ConfigurationManager.AppSettings["SERVERTYPE"];
                    var dbName = System.Configuration.ConfigurationManager.AppSettings["DBNAME"];
                    var dbUser = System.Configuration.ConfigurationManager.AppSettings["DBUSER"];
                    var dbPass = System.Configuration.ConfigurationManager.AppSettings["DBPASS"];
                    var sapUser = System.Configuration.ConfigurationManager.AppSettings["COMPANYUSER_H"];
                    var sapPass = System.Configuration.ConfigurationManager.AppSettings["COMPANYPASS_H"];

                    oStaticCompany_HN.Server = server;
                    oStaticCompany_HN.LicenseServer = licenseServer;
                    oStaticCompany_HN.CompanyDB = dbName;
                    oStaticCompany_HN.DbUserName = dbUser;
                    oStaticCompany_HN.DbPassword = dbPass;

                    if (serverType == "MSSQL_2008")
                        oStaticCompany_HN.DbServerType = BoDataServerTypes.dst_MSSQL2008;
                    else if (serverType == "MSSQL_2012")
                        oStaticCompany_HN.DbServerType = BoDataServerTypes.dst_MSSQL2012;
                    else if (serverType == "MSSQL_2014")
                        oStaticCompany_HN.DbServerType = BoDataServerTypes.dst_MSSQL2014;
                    else if (serverType == "MSSQL_2016")
                    {
                        //oStaticCompany_HN.DbServerType = BoDataServerTypes.dst_MSSQL2016;
                    }
                    else if (serverType == "HANADB")
                        oStaticCompany_HN.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;

                    oStaticCompany_HN.UserName = sapUser;
                    oStaticCompany_HN.Password = sapPass;


                    //WriteEventLog(sapUser);
                    //WriteEventLog(sapPass);
                }
                if (!oStaticCompany_HN.Connected)
                {
                    int i = oStaticCompany_HN.Connect();
                    oStaticCompany_HN.GetLastError(out i, out message);
                }
                return oStaticCompany_HN;
            }
            return null;
        }

        public void StartSapTransaction()
        {
            string message = "";
            oCompany = Company_SAP(out message);
            oCompany.StartTransaction();
        }

        public bool CommitSapTransaction(out string message)
        {
            message = "";
            try
            {
                if (oCompany.InTransaction)
                    oCompany.EndTransaction(BoWfTransOpt.wf_Commit);

                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }

        public void RollbackSapTransaction()
        {
            if (oCompany.InTransaction)
                oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
        }

        public void AddStatusColumn(DataTable dt)
        {
            if (dt != null)
            {
                if (dt.Columns.Contains("SyncStatus") == false)
                {
                    dt.Columns.Add("SyncStatus");
                }

                if (dt.Columns.Contains("SyncMessage") == false)
                {
                    dt.Columns.Add("SyncMessage");
                }
            }
        }

        public bool CreateChartofAccounts(SyncObjectCurrent obj, DataTable dataSource, out string message)
        {
            message = "";
            string columnName = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {

                #region Database
                //AcctCode nvarchar    no
                //AcctName    nvarchar no
                //CurrTotal numeric no
                //EndTotal    numeric no
                //Finanse char no
                //Groups nvarchar    no
                //Budget  char no
                //Frozen  char no
                //Free_2  char no
                //Postable    char no
                //Fixed   char no
                //Levels smallint    no
                //ExportCode  nvarchar no
                //GrpLine int no
                //FatherNum nvarchar    no
                //AccntntCod  nvarchar no
                //CashBox char no
                //GroupMask smallint    no
                //RateTrans   char no
                //TaxIncome   char no
                //ExmIncome   char no
                //ExtrMatch   int no
                //IntrMatch   int no
                //ActType char no
                //Transfered  char no
                //BlncTrnsfr  char no
                //OverType    char no
                //OverCode nvarchar    no
                //SysMatch    int no
                //PrevYear    char no
                //ActCurr nvarchar    no
                //RateDifAct  nvarchar no
                //SysTotal numeric no
                //FcTotal numeric no
                //Protected   char no
                //RealAcct    char no
                //Advance char no
                //CreateDate datetime    no
                //UpdateDate  datetime no
                //FrgnName nvarchar    no
                //Details nvarchar no
                //ExtraSum numeric no
                //Project nvarchar no
                //RevalMatch  char no
                //DataSource  char no
                //LocMth  char no
                //MTHCounter  int no
                //BNKCounter  int no
                //UserSign smallint    no
                //LocManTran  char no
                //LogInstanc  int no
                //ObjType nvarchar    no
                //ValidFor    char no
                //ValidFrom datetime    no
                //ValidTo datetime no
                //ValidComm nvarchar    no
                //FrozenFor   char no
                //FrozenFrom datetime    no
                //FrozenTo    datetime no
                //FrozenComm nvarchar    no
                //Counter int no
                #endregion

                string acctCode = "";
                object value;
                bool isUpdate = false;

                oCompany = Company_SAP(out message);
                foreach (DataRow row in dataSource.Rows)
                {
                    oChartOfAccount = oCompany.GetBusinessObject(BoObjectTypes.oChartOfAccounts);

                    if (dataSource.Columns.Contains("acctCode") == false)
                    {
                        message = getFunctionError("[CreateChartofAccounts]", "Not found field AcctCode.");
                        return false;
                    }
                    else
                    {
                        acctCode = row["acctCode"].ToString();
                        if (oChartOfAccount.GetByKey(acctCode))
                            isUpdate = true;
                        else
                            isUpdate = false;
                    }

                    oChartOfAccount.Code = acctCode;

                    foreach (DataColumn column in dataSource.Columns)
                    {
                        value = row[column.ColumnName];
                        columnName = column.ColumnName;
                        if (value != null && value.ToString() != "")
                        {
                            if (column.ColumnName.ToLower() == "Postable".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.ActiveAccount = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.ActiveAccount = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "FormatCode".ToLower())
                            {
                                oChartOfAccount.FormatCode = value.ToString();
                            }
                            if (column.ColumnName.ToLower() == "AcctName".ToLower())
                                oChartOfAccount.Name = value.ToString();
                            if (column.ColumnName.ToLower() == "FrgnName".ToLower())
                                oChartOfAccount.ForeignName = value.ToString();
                            if (column.ColumnName.ToLower() == "FatherNum".ToLower())
                                oChartOfAccount.FatherAccountKey = value.ToString();
                            if (column.ColumnName.ToLower() == "ActType".ToLower())
                            {
                                if (value.ToString() == "N")
                                    oChartOfAccount.AccountType = BoAccountTypes.at_Other;
                                else if (value.ToString() == "I")
                                    oChartOfAccount.AccountType = BoAccountTypes.at_Revenues;
                                else if (value.ToString() == "E")
                                    oChartOfAccount.AccountType = BoAccountTypes.at_Expenses;
                            }
                            //if (column.ColumnName.ToLower() == "Groups".ToLower()) oChartOfAccount = value.ToString();
                            if (column.ColumnName.ToLower() == "DfltVat".ToLower())
                                oChartOfAccount.DefaultVatGroup = value.ToString();
                            if (column.ColumnName.ToLower() == "AccntntCod".ToLower())
                                oChartOfAccount.ExternalCode = value.ToString();
                            if (column.ColumnName.ToLower() == "ActCurr".ToLower())
                                oChartOfAccount.AcctCurrency = value.ToString();
                            if (column.ColumnName.ToLower() == "ValidFor".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.ValidFor = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.ValidFor = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "ValidFrom".ToLower() && value.ToString() != "") oChartOfAccount.ValidFrom = (DateTime)value;
                            if (column.ColumnName.ToLower() == "ValidTo".ToLower() && value.ToString() != "") oChartOfAccount.ValidTo = (DateTime)value;
                            if (column.ColumnName.ToLower() == "ValidComm".ToLower()) oChartOfAccount.ValidRemarks = value.ToString();
                            if (column.ColumnName.ToLower() == "FrozenFor".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.FrozenFor = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.FrozenFor = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "FrozenFrom".ToLower() && value.ToString() != "") oChartOfAccount.FrozenFrom = (DateTime)value;
                            if (column.ColumnName.ToLower() == "FrozenTo".ToLower() && value.ToString() != "") oChartOfAccount.FrozenTo = (DateTime)value;
                            if (column.ColumnName.ToLower() == "FrozenComm".ToLower()) oChartOfAccount.FrozenRemarks = value.ToString();
                            if (column.ColumnName.ToLower() == "Details".ToLower())
                                oChartOfAccount.Details = value.ToString();
                            //if (column.ColumnName.ToLower() == "Category".ToLower() && value.ToString() != "")
                            //    oChartOfAccount.Category = (int)value;
                            if (column.ColumnName.ToLower() == "Protected".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.Protected = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.Protected = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "Finanse".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.CashAccount = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.CashAccount = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "RevalMatch".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.RevaluationCoordinated = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.RevaluationCoordinated = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "RealAcct".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.ReconciledAccount = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.ReconciledAccount = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "LocManTran".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.LockManualTransaction = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.LockManualTransaction = BoYesNoEnum.tNO;
                            }
                            //if (column.ColumnName.ToLower() == "CfwRlvnt".ToLower())
                            //{
                            //    if (value.ToString() == "Y")
                            //        oChartOfAccount. = BoYesNoEnum.tYES;
                            //    else
                            //        oChartOfAccount.TaxLiableAccount = BoYesNoEnum.tNO;
                            //}
                            if (column.ColumnName.ToLower() == "Budget".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.BudgetAccount = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.BudgetAccount = BoYesNoEnum.tNO;
                            }
                            if (column.ColumnName.ToLower() == "MultiLink".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oChartOfAccount.AllowMultipleLinking = BoYesNoEnum.tYES;
                                else
                                    oChartOfAccount.AllowMultipleLinking = BoYesNoEnum.tNO;
                            }

                            else if (column.ColumnName.StartsWith("U_"))
                            {
                                COA_UDF(oChartOfAccount, column, value);
                            }
                        }
                    }

                    if (isUpdate)
                        oError = oChartOfAccount.Update();
                    else
                        oError = oChartOfAccount.Add();

                    if (oError == 0)
                    {
                        row["SyncStatus"] = "Success";
                        row["SyncMessage"] = "";
                        if (obj.TransactionNotification != null && obj.TransactionNotification.ToString() != "")
                        {
                            //obj.TransactionNotification = "update OACT set U_isSync='Y' where acctCode='{0}'";
                            daConn.ExecQuery(string.Format(obj.TransactionNotification, acctCode), out message);
                        }
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        row["SyncStatus"] = "Fail";
                        row["SyncMessage"] = string.Format("{0}", message);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("[CreateChartofAccounts][Field: " + columnName + "]", ex.Message);
                return false;
            }
        }

        public bool CreateItemMasterData(SyncObjectCurrent obj, DataTable dataSource, out string message)
        {
            message = "";
            string columnName = "";
            AddStatusColumn(dataSource);
            var daConn = new daConnection(true);
            try
            {

                #region Database
                //ItemCode nvarchar
                //ItemName nvarchar
                //FrgnName nvarchar
                //ItmsGrpCod smallint
                //CstGrpCode smallint
                //VatGourpSa nvarchar
                //CodeBars nvarchar
                //VATLiable   char
                //PrchseItem  char
                //SellItem    char
                //InvntItem   char
                //OnHand  numeric
                //IsCommited  numeric
                //OnOrder numeric
                //IncomeAcct  nvarchar
                //ExmptIncom  nvarchar
                //MaxLevel    numeric
                //DfltWH  nvarchar
                //CardCode    nvarchar
                //SuppCatNum  nvarchar
                //BuyUnitMsr  nvarchar
                //NumInBuy    numeric
                //ReorderQty  numeric
                //MinLevel    numeric
                //LstEvlPric  numeric
                //LstEvlDate  datetime
                //CustomPer   numeric
                //Canceled    char
                //MnufctTime  int
                //WholSlsTax  char
                //RetilrTax   char
                //SpcialDisc  numeric
                //DscountCod  smallint
                //TrackSales  char
                //SalUnitMsr  nvarchar
                //NumInSale   numeric
                //Consig  numeric
                //QueryGroup  int
                //Counted numeric
                //OpenBlnc    numeric
                //EvalSystem  char
                //UserSign    smallint
                //FREE    char
                //PicturName  nvarchar
                //Transfered  char
                //BlncTrnsfr  char
                //UserText    ntext
                //SerialNum   nvarchar
                //CommisPcnt  numeric
                //CommisSum   numeric
                //CommisGrp   smallint
                //TreeType    char
                //TreeQty numeric
                //LastPurPrc  numeric
                //LastPurCur  nvarchar
                //LastPurDat  datetime
                //ExitCur nvarchar
                //ExitPrice   numeric
                //ExitWH  nvarchar
                //AssetItem   char
                //WasCounted  char
                //ManSerNum   char
                //SHeight1    numeric
                //SHght1Unit  smallint
                //SHeight2    numeric
                //SHght2Unit  smallint
                //SWidth1 numeric
                //SWdth1Unit  smallint
                //SWidth2 numeric
                //SWdth2Unit  smallint
                //SLength1    numeric
                //SLen1Unit   smallint
                //Slength2    numeric
                //SLen2Unit   smallint
                //SVolume numeric
                //SVolUnit    smallint
                //SWeight1    numeric
                //SWght1Unit  smallint
                //SWeight2    numeric
                //SWght2Unit  smallint
                //BHeight1    numeric
                //BHght1Unit  smallint
                //BHeight2    numeric
                //BHght2Unit  smallint
                //BWidth1 numeric
                //BWdth1Unit  smallint
                //BWidth2 numeric
                //BWdth2Unit  smallint
                //BLength1    numeric
                //BLen1Unit   smallint
                //Blength2    numeric
                //BLen2Unit   smallint
                //BVolume numeric
                //BVolUnit    smallint
                //BWeight1    numeric
                //BWght1Unit  smallint
                //BWeight2    numeric
                //BWght2Unit  smallint
                //FixCurrCms  nvarchar
                //FirmCode    smallint
                //LstSalDate  datetime
                //QryGroup1   char
                //QryGroup2   char
                //QryGroup3   char
                //QryGroup4   char
                //QryGroup5   char
                //QryGroup6   char
                //QryGroup7   char
                //QryGroup8   char
                //QryGroup9   char
                //QryGroup10  char
                //QryGroup11  char
                //QryGroup12  char
                //QryGroup13  char
                //QryGroup14  char
                //QryGroup15  char
                //QryGroup16  char
                //QryGroup17  char
                //QryGroup18  char
                //QryGroup19  char
                //QryGroup20  char
                //QryGroup21  char
                //QryGroup22  char
                //QryGroup23  char
                //QryGroup24  char
                //QryGroup25  char
                //QryGroup26  char
                //QryGroup27  char
                //QryGroup28  char
                //QryGroup29  char
                //QryGroup30  char
                //QryGroup31  char
                //QryGroup32  char
                //QryGroup33  char
                //QryGroup34  char
                //QryGroup35  char
                //QryGroup36  char
                //QryGroup37  char
                //QryGroup38  char
                //QryGroup39  char
                //QryGroup40  char
                //QryGroup41  char
                //QryGroup42  char
                //QryGroup43  char
                //QryGroup44  char
                //QryGroup45  char
                //QryGroup46  char
                //QryGroup47  char
                //QryGroup48  char
                //QryGroup49  char
                //QryGroup50  char
                //QryGroup51  char
                //QryGroup52  char
                //QryGroup53  char
                //QryGroup54  char
                //QryGroup55  char
                //QryGroup56  char
                //QryGroup57  char
                //QryGroup58  char
                //QryGroup59  char
                //QryGroup60  char
                //QryGroup61  char
                //QryGroup62  char
                //QryGroup63  char
                //QryGroup64  char
                //CreateDate  datetime
                //UpdateDate  datetime
                //ExportCode  nvarchar
                //SalFactor1  numeric
                //SalFactor2  numeric
                //SalFactor3  numeric
                //SalFactor4  numeric
                //PurFactor1  numeric
                //PurFactor2  numeric
                //PurFactor3  numeric
                //PurFactor4  numeric
                //SalFormula  nvarchar
                //PurFormula  nvarchar
                //VatGroupPu  nvarchar
                //AvgPrice    numeric
                //PurPackMsr  nvarchar
                //PurPackUn   numeric
                //SalPackMsr  nvarchar
                //SalPackUn   numeric
                //SCNCounter  smallint
                //ManBtchNum  char
                //ManOutOnly  char
                //DataSource  char
                //validFor    char
                //validFrom   datetime
                //validTo datetime
                //frozenFor   char
                //frozenFrom  datetime
                //frozenTo    datetime
                //BlockOut    char
                //ValidComm   nvarchar
                //FrozenComm  nvarchar
                //LogInstanc  int
                //ObjType nvarchar
                //SWW nvarchar
                //Deleted char
                //DocEntry    int
                //ExpensAcct  nvarchar
                //FrgnInAcct  nvarchar
                //ShipType    smallint
                //GLMethod    char
                //ECInAcct    nvarchar
                //FrgnExpAcc  nvarchar
                //ECExpAcc    nvarchar
                //TaxType char
                //ByWh    char
                //WTLiable    char
                //ItemType    char
                //WarrntTmpl  nvarchar
                //BaseUnit    nvarchar
                //CountryOrg  nvarchar
                //StockValue  numeric
                //Phantom char
                //IssueMthd   char
                //FREE1   char
                //PricingPrc  numeric
                //MngMethod   char
                //ReorderPnt  numeric
                //InvntryUom  nvarchar
                //PlaningSys  char
                //PrcrmntMtd  char
                //OrdrIntrvl  smallint
                //OrdrMulti   numeric
                //MinOrdrQty  numeric
                //LeadTime    int
                //IndirctTax  char
                //TaxCodeAR   nvarchar
                //TaxCodeAP   nvarchar
                //OSvcCode    int
                //ISvcCode    int
                //ServiceGrp  int
                //NCMCode int
                //MatType nvarchar
                //MatGrp  int
                //ProductSrc  nvarchar
                //ServiceCtg  int
                //ItemClass   char
                //Excisable   char
                //ChapterID   int
                //NotifyASN   nvarchar
                //ProAssNum   nvarchar
                //AssblValue  numeric
                //DNFEntry    int
                //UserSign2   smallint
                //Spec    nvarchar
                //TaxCtg  nvarchar
                //Series  smallint
                //Number  int
                //FuelCode    int
                //BeverTblC   nvarchar
                //BeverGrpC   nvarchar
                //BeverTM int
                //Attachment  ntext
                //AtcEntry    int
                //ToleranDay  int
                //UgpEntry    int
                //PUoMEntry   int
                //SUoMEntry   int
                //IUoMEntry   int
                //IssuePriBy  smallint
                //AssetClass  nvarchar
                //AssetGroup  nvarchar
                //InventryNo  nvarchar
                //Technician  int
                //Employee    int
                //Location    int
                //StatAsset   char
                //Cession char
                //DeacAftUL   char
                //AsstStatus  char
                //CapDate datetime
                //AcqDate datetime
                //RetDate datetime
                //GLPickMeth  char
                //NoDiscount  char
                //MgrByQty    char
                //AssetRmk1   nvarchar
                //AssetRmk2   nvarchar
                //AssetAmnt1  numeric
                //AssetAmnt2  numeric
                //DeprGroup   nvarchar
                //AssetSerNo  nvarchar
                //CntUnitMsr  nvarchar
                //NumInCnt    numeric
                //INUoMEntry  int
                //OneBOneRec  char
                //RuleCode    nvarchar
                //ScsCode nvarchar
                //SpProdType  nvarchar
                //IWeight1    numeric
                //IWght1Unit  smallint
                //IWeight2    numeric
                //IWght2Unit  smallint
                //CompoWH char
                #endregion

                string itemCode = "";
                object value;
                bool isUpdate = false;

                oCompany = Company_SAP(out message);
                foreach (DataRow row in dataSource.Rows)
                {
                    oItem = oCompany.GetBusinessObject(BoObjectTypes.oItems);
                    if (dataSource.Columns.Contains("ItemCode") == false)
                    {
                        message = getFunctionError("[CreateItemMasterData]", "Not found field ItemCode.");
                        return false;
                    }
                    else
                    {
                        itemCode = row["ItemCode"].ToString();
                        if (oItem.GetByKey(itemCode))
                            isUpdate = true;
                        else
                            isUpdate = false;
                    }

                    oItem.ItemCode = itemCode;

                    foreach (DataColumn column in dataSource.Columns)
                    {
                        columnName = column.ColumnName;
                        value = row[column.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (column.ColumnName.ToLower() == "ItemName".ToLower()) oItem.ItemName = value.ToString();
                            else if (column.ColumnName.ToLower() == "frgnName".ToLower()) oItem.ForeignName = value.ToString();
                            else if (column.ColumnName.ToLower() == "itmsGrpCod".ToLower()) oItem.ItemsGroupCode = (Int16)value;
                            else if (column.ColumnName.ToLower() == "Frozen".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.Frozen = BoYesNoEnum.tYES;
                                else
                                    oItem.Frozen = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "FrozenFrom".ToLower()) oItem.FrozenFrom = (DateTime)value;
                            else if (column.ColumnName.ToLower() == "FrozenComm".ToLower()) oItem.FrozenRemarks = value.ToString();
                            else if (column.ColumnName.ToLower() == "FrozenTo".ToLower()) oItem.FrozenTo = (DateTime)value;
                            else if (column.ColumnName.ToLower() == "GLMethod".ToLower())
                            {
                                if (value.ToString() == "C")
                                    oItem.GLMethod = BoGLMethods.glm_ItemClass;
                                else if (value.ToString() == "W")
                                    oItem.GLMethod = BoGLMethods.glm_WH;
                                else
                                    oItem.GLMethod = BoGLMethods.glm_ItemLevel;
                            }
                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.InventoryNumber = value.ToString();
                            else if (column.ColumnName.ToLower() == "InvntryUom".ToLower()) oItem.InventoryUOM = value.ToString();
                            else if (column.ColumnName.ToLower() == "IUoMEntry".ToLower()) oItem.InventoryUoMEntry = (int)value;
                            else if (column.ColumnName.ToLower() == "UgpEntry".ToLower()) oItem.UoMGroupEntry = (int)value;
                            else if (column.ColumnName.ToLower() == "Valid".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.Valid = BoYesNoEnum.tYES;
                                else
                                    oItem.Valid = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "ValidFrom".ToLower()) oItem.ValidFrom = (DateTime)value;
                            else if (column.ColumnName.ToLower() == "ValidComm".ToLower()) oItem.ValidRemarks = value.ToString();
                            else if (column.ColumnName.ToLower() == "ValidTo".ToLower()) oItem.ValidTo = (DateTime)value;
                            else if (column.ColumnName.ToLower() == "PrchseItem".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.PurchaseItem = BoYesNoEnum.tYES;
                                else
                                    oItem.PurchaseItem = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "VATGroupPu".ToLower()) oItem.PurchaseVATGroup = value.ToString();
                            else if (column.ColumnName.ToLower() == "Sellitem".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.SalesItem = BoYesNoEnum.tYES;
                                else
                                    oItem.SalesItem = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "InvntItem".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.InventoryItem = BoYesNoEnum.tYES;
                                else
                                    oItem.InventoryItem = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "VATGourpSa".ToLower()) oItem.SalesVATGroup = value.ToString();
                            else if (column.ColumnName.ToLower() == "EvalSystem".ToLower())
                            {
                                if (value.ToString() == "S")
                                    oItem.CostAccountingMethod = BoInventorySystem.bis_Standard;
                                else if (value.ToString() == "F")
                                    oItem.CostAccountingMethod = BoInventorySystem.bis_FIFO;
                                else if (value.ToString() == "A")
                                    oItem.CostAccountingMethod = BoInventorySystem.bis_MovingAverage;
                                else
                                    oItem.CostAccountingMethod = BoInventorySystem.bis_SNB;
                            }






                            else if (column.ColumnName.ToLower() == "AssetClass".ToLower()) oItem.AssetClass = value.ToString();
                            else if (column.ColumnName.ToLower() == "AssetGroup".ToLower()) oItem.AssetGroup = value.ToString();
                            else if (column.ColumnName.ToLower() == "AssetItem".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.AssetItem = BoYesNoEnum.tYES;
                                else
                                    oItem.AssetItem = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "AssetSerNo".ToLower()) oItem.AssetSerialNumber = value.ToString();
                            else if (column.ColumnName.ToLower() == "AvgPrice".ToLower()) oItem.AvgStdPrice = (double)(decimal)value;
                            else if (column.ColumnName.ToLower() == "CodeBars".ToLower()) oItem.BarCode = value.ToString();
                            else if (column.ColumnName.ToLower() == "BaseUnit".ToLower()) oItem.BaseUnitName = value.ToString();
                            else if (column.ColumnName.ToLower() == "CapDate".ToLower()) oItem.CapitalizationDate = (DateTime)value;
                            else if (column.ColumnName.ToLower() == "Cession".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.Cession = BoYesNoEnum.tYES;
                                else
                                    oItem.Cession = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "CommisGrp".ToLower()) oItem.CommissionGroup = (Int16)value;
                            else if (column.ColumnName.ToLower() == "CommisPcnt".ToLower()) oItem.CommissionPercent = (double)(decimal)value;
                            else if (column.ColumnName.ToLower() == "CommisSum".ToLower()) oItem.CommissionSum = (double)(decimal)value;

                            else if (column.ColumnName.ToLower() == "CstGrpCode".ToLower()) oItem.CustomsGroupCode = (Int16)value;
                            else if (column.ColumnName.ToLower() == "ExportCode".ToLower()) oItem.DataExportCode = value.ToString();
                            else if (column.ColumnName.ToLower() == "DeacAftUL".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.DeactivateAfterUsefulLife = BoYesNoEnum.tYES;
                                else
                                    oItem.DeactivateAfterUsefulLife = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "INUoMEntry".ToLower()) oItem.DefaultCountingUoMEntry = (int)value;

                            else if (column.ColumnName.ToLower() == "PUoMEntry".ToLower()) oItem.DefaultPurchasingUoMEntry = (int)value;
                            else if (column.ColumnName.ToLower() == "BuyUnitMsr".ToLower()) oItem.PurchaseUnit = value.ToString();
                            else if (column.ColumnName.ToLower() == "NumInBuy".ToLower()) oItem.PurchaseItemsPerUnit = (double)(decimal)value;

                            else if (column.ColumnName.ToLower() == "SUoMEntry".ToLower()) oItem.DefaultSalesUoMEntry = (int)value;
                            else if (column.ColumnName.ToLower() == "SalUnitMsr".ToLower()) oItem.SalesUnit = value.ToString();
                            else if (column.ColumnName.ToLower() == "NumInSale".ToLower()) oItem.SalesItemsPerUnit = (double)(decimal)value;

                            else if (column.ColumnName.ToLower() == "DfltWH".ToLower()) oItem.DefaultWarehouse = value.ToString();
                            else if (column.ColumnName.ToLower() == "Technician".ToLower()) oItem.Technician = (int)value;


                            else if (column.ColumnName.ToLower() == "IssueMthd".ToLower())
                            {
                                if (value.ToString() == "B")
                                    oItem.IssueMethod = BoIssueMethod.im_Backflush;
                                else
                                    oItem.IssueMethod = BoIssueMethod.im_Manual;
                            }
                            else if (column.ColumnName.ToLower() == "IssuePriBy".ToLower())
                            {
                                if (value.ToString() == "")
                                    oItem.IssuePrimarilyBy = IssuePrimarilyByEnum.ipbBinLocations;
                                else
                                    oItem.IssuePrimarilyBy = IssuePrimarilyByEnum.ipbSerialAndBatchNumbers;
                            }
                            else if (column.ColumnName.ToLower() == "ItemClass".ToLower())
                            {
                                if (value.ToString() == "2")
                                    oItem.ItemClass = ItemClassEnum.itcMaterial;
                                else
                                    oItem.ItemClass = ItemClassEnum.itcService;
                            }
                            else if (column.ColumnName.ToLower() == "CountryOrg".ToLower()) oItem.ItemCountryOrg = value.ToString();
                            else if (column.ColumnName.ToLower() == "ItemType".ToLower())
                            {
                                if (value.ToString() == "T")
                                    oItem.ItemType = ItemTypeEnum.itTravel;
                                else if (value.ToString() == "I")
                                    oItem.ItemType = ItemTypeEnum.itItems;
                                else if (value.ToString() == "L")
                                    oItem.ItemType = ItemTypeEnum.itLabor;
                                else
                                    oItem.ItemType = ItemTypeEnum.itFixedAssets;
                            }
                            else if (column.ColumnName.ToLower() == "Location".ToLower()) oItem.Location = (int)value;
                            else if (column.ColumnName.ToLower() == "ManBtchNum".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.ManageBatchNumbers = BoYesNoEnum.tYES;
                                else
                                    oItem.ManageBatchNumbers = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "ManSerNum".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.ManageSerialNumbers = BoYesNoEnum.tYES;
                                else
                                    oItem.ManageSerialNumbers = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "ManOutOnly".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.ManageSerialNumbersOnReleaseOnly = BoYesNoEnum.tYES;
                                else
                                    oItem.ManageSerialNumbersOnReleaseOnly = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "ByWh".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.ManageStockByWarehouse = BoYesNoEnum.tYES;
                                else
                                    oItem.ManageStockByWarehouse = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "FirmCode".ToLower()) oItem.Manufacturer = (Int16)value;
                            //else if (column.ColumnName.ToLower() == "MatType".ToLower())
                            //{
                            //    if (value.ToString() == "")
                            //        oItem.MaterialType = BoMaterialTypes.mt_RawMaterial;
                            //    else
                            //        oItem.MaterialType = BoMaterialTypes.mt_FinishedGoods;
                            //}
                            else if (column.ColumnName.ToLower() == "MinLevel".ToLower()) oItem.MaxInventory = (double)(decimal)value;
                            else if (column.ColumnName.ToLower() == "MaxLevel".ToLower()) oItem.MinInventory = (double)(decimal)value;
                            else if (column.ColumnName.ToLower() == "MinOrdrQty".ToLower()) oItem.MinOrderQuantity = (double)(decimal)value;
                            else if (column.ColumnName.ToLower() == "NoDiscount".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.NoDiscounts = BoYesNoEnum.tYES;
                                else
                                    oItem.NoDiscounts = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "PicturName".ToLower()) oItem.Picture = value.ToString();
                            else if (column.ColumnName.ToLower() == "PlaningSys".ToLower())
                            {
                                if (value.ToString() == "M")
                                    oItem.PlanningSystem = BoPlanningSystem.bop_MRP;
                                else
                                    oItem.PlanningSystem = BoPlanningSystem.bop_None;
                            }
                            //else if (column.ColumnName.ToLower() == "CardCode".ToLower())
                            //{
                            //    oItem.PreferredVendors.BPCode = value.ToString();
                            //    oItem.PreferredVendors.Add();
                            //}
                            else if (column.ColumnName.ToLower() == "PrcrmntMtd".ToLower())
                            {
                                if (value.ToString() == "B")
                                    oItem.ProcurementMethod = BoProcurementMethod.bom_Buy;
                                else
                                    oItem.ProcurementMethod = BoProcurementMethod.bom_Make;
                            }
                            //else if (column.ColumnName.ToLower() == "ProductSrc".ToLower()) oItem.ProductSource = (int)value;
                            //else if (column.ColumnName.ToLower() == "".ToLower())
                            //{
                            //    oItem.Projects.Project = value.ToString();
                            //    oItem.Projects.Add();
                            //}

                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.PurchaseItemsPerUnit = (double)value;
                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.PurchaseUnit = value.ToString();

                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.SalesItemsPerUnit = (int)value;
                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.SalesUnit = value.ToString();
                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.SerialNum = value.ToString();
                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.Series = (int)value;
                            //else if (column.ColumnName.ToLower() == "".ToLower()) oItem.ServiceGroup = (int)value;
                            else if (column.ColumnName.ToLower() == "ShipType".ToLower()) oItem.ShipType = (int)value;
                            else if (column.ColumnName.ToLower() == "StatAsset".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oItem.StatisticalAsset = BoYesNoEnum.tYES;
                                else
                                    oItem.StatisticalAsset = BoYesNoEnum.tNO;
                            }
                            else if (column.ColumnName.ToLower() == "SWW".ToLower()) oItem.SWW = value.ToString();
                            //else if (column.ColumnName.ToLower() == "".ToLower())
                            //{
                            //    if (value.ToString() == "Y")
                            //        oItem.TaxType = BoTaxTypes.tt_Yes;
                            //    else
                            //        oItem.TaxType = BoTaxTypes.tt_No;
                            //}
                            //else if (column.ColumnName.ToLower() == "".ToLower())
                            //{
                            //    oItem.UnitOfMeasurements.UoMEntry = 1;
                            //    oItem.UnitOfMeasurements.Add();
                            //}
                            //else if (column.ColumnName.ToLower() == "Valid".ToLower())
                            //{
                            //    oItem.WhsInfo.CostAccount = "";
                            //    oItem.WhsInfo.Add();
                            //}

                            //UDF   
                            //SAPbobsCOM.BoFieldTypes
                            //SAPbobsCOM.BoFldSubTypes
                            //else if (column.ColumnName == "U_Menucode") oItem.UserFields.Fields.Item("U_Menucode").Value = value.ToString();
                            //else if (column.ColumnName == "U_Menuname") oItem.UserFields.Fields.Item("U_Menuname").Value = value.ToString();

                            else if (column.ColumnName.StartsWith("U_"))
                            {
                                Item_UDF(oItem, column, value);
                            }
                        }
                    }

                    if (isUpdate)
                        oError = oItem.Update();
                    else
                        oError = oItem.Add();

                    if (oError == 0)
                    {
                        row["SyncStatus"] = "Success";
                        row["SyncMessage"] = "";
                        if (obj.TransactionNotification != null && obj.TransactionNotification.ToString() != "")
                        {
                            //obj.TransactionNotification = "update OITM set U_isSync='Y' where itemCode='{0}'";
                            daConn.ExecQuery(string.Format(obj.TransactionNotification, itemCode), out message);
                        }
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        row["SyncStatus"] = "Fail";
                        row["SyncMessage"] = message;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("[CreateItemMasterData][Field: " + columnName + "]", ex.Message);
                return false;
            }
        }

        public bool CreateBussinessPartners(Customer dataSource, DataTable dataSource_log, out string message)
        {
          
            message = "";
            StringWriter sw = new StringWriter();
            
            string sqllog = "";
            
            bool isRes = false;
            try
            {
                //var prefixCode = System.Configuration.ConfigurationManager.AppSettings["PrefixCardCode"];

                //string cardCode = DateTime.Now.ToString("yyMMddHHmmssfff");
                bool isUpdate = false;

                oCompany = Company_SAP_By_Location (dataSource.LocationCode.ToString(), out message);

                oBP = oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartners);
                if (string.IsNullOrEmpty(message) == false)
                {
                    return false;
                }
               
                if (dataSource == null)
                {
                    message = "Not found data in proccess.";
                    return false;
                }
                if (dataSource_log == null)
                {
                    message = "Not found data in proccess.";
                    return false;
                }


                dataSource_log.TableName = "BP";
                dataSource_log.WriteXml(sw);

                sqllog = string.Format("INSERT INTO [NKID_WMS_LOG] VALUES('{0}','{1}','{2}',N'{3}',N'{4}')"
                    , "BP", "PROCCESS", DateTime.Now.ToString(), sw.ToString(), ""
                    );
                DoQuery(sqllog, out oErrorMessage, dataSource.LocationCode.ToString());

                DataTools.SAPAccess.CreateObject sap = new DataTools.SAPAccess.CreateObject();
                var dtHeader = sap.ListToDataTable<Customer>(dataSource);


                sw = new StringWriter();
                dtHeader.TableName = "BP";
                dtHeader.WriteXml(sw);

                sqllog = string.Format("INSERT INTO [NKID_WMS_LOG] VALUES('{0}','{1}','{2}',N'{3}',N'{4}')"
                    , "BP", "PROCCESS_SYNC", DateTime.Now.ToString(), sw.ToString(), ""
                    );
                DoQuery(sqllog, out oErrorMessage, dataSource.LocationCode.ToString());


                if (oBP.GetByKey(dataSource.CardCode))
                {
                    isUpdate = true;
                    
                }
                oBP.CardCode = dataSource.CardCode;
                oBP.CardName = dataSource.CardName;
                oBP.CardForeignName = dataSource.CardTaxName;
                oBP.GroupCode = Convert.ToInt32(dataSource.GroupCode.ToString());
                oBP.SalesPersonCode = Convert.ToInt32( dataSource.EmpCode.ToString());
                oBP.UserFields.Fields.Item("U_SalesforceID").Value = dataSource.SalesforceID;
                oBP.UserFields.Fields.Item("U_Mavung").Value = dataSource.LocationCode;
                oBP.FederalTaxID = dataSource.TaxCode;
                oBP.UserFields.Fields.Item("U_HDaddress").Value = dataSource.TaxAdress;
                oBP.Territory = dataSource.Territory;
                oBP.Cellular = dataSource.Phone;
                oBP.OwnerCode = Convert.ToInt32(dataSource.OwnerCode.ToString());
                if (dataSource.LocationCode=="S")
                    oBP.UserFields.Fields.Item("U_province").Value = "HCM";
                else
                    oBP.UserFields.Fields.Item("U_province").Value = "HNO";

                int iError = 0;
                if (isUpdate)
                    iError = oBP.Update();
                else
                    iError = oBP.Add();
                if (iError != 0)
                {
                    message = oCompany.GetLastErrorDescription();

                    isRes = false;
                    goto WriteLog;
                }
                isRes = true;
            }
            catch (Exception ex)
            {
               
                message = getFunctionError("[CreateBussinessPartners]", ex.Message);
                isRes = false;
            }

            WriteLog:

            try
            {
                sqllog = string.Format("INSERT INTO [NKID_WMS_LOG] VALUES('{0}','{1}','{2}',N'{3}',N'{4}')"
                   , "BP", isRes.ToString().ToUpper(), DateTime.Now.ToString(), sw.ToString(), message.Replace("'","")
                   );
                DoQuery(sqllog, out oErrorMessage, dataSource.LocationCode.ToString());
            }
            catch 
            {
                //Continue...
            }

            return isRes;
        }

        public bool CreateContactPersion(Person dataSource, out string message)
        {
            message = "";
            try
            {

                string id = dataSource.sapId;

                oCompany = Company_SAP(out message);
                oBP = oCompany.GetBusinessObject(BoObjectTypes.oBusinessPartners);

                if (oBP.GetByKey(dataSource.customerId))
                {

                }
                else
                {
                    message = "Please create customer information before create contact persons.";
                    return false;
                }
                int line = 0;
                IRecordset oRecordSet = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
                string sql = string.Format("select 1 from OCPR where CntctCode='{0}'", id);
                oRecordSet.DoQuery(sql);
                var isUpdate = false;
                if (oRecordSet.RecordCount > 0)
                {
                    isUpdate = true;
                }

                sql = string.Format("select CntctCode from OCPR where cardcode='{0}' order by CntctCode", dataSource.customerId);
                oRecordSet.DoQuery(sql);
                if (oRecordSet.RecordCount > 0)
                {
                    if (isUpdate)
                    {
                        while (oRecordSet.EoF == false)
                        {
                            if (oRecordSet.Fields.Item(0).Value.ToString() == id)
                            {
                                break;
                            }
                            line = line + 1;
                            oRecordSet.MoveNext();
                        }
                        if (line == oRecordSet.RecordCount)
                        {
                            message = "[Id not correct int Create Object ContactEmployee]";
                            return false;
                        }
                    }
                    else
                    {
                        line = oRecordSet.RecordCount;
                    }
                }


                if (isUpdate)
                {
                    oBP.ContactEmployees.SetCurrentLine(line);

                }
                else
                {
                    oBP.ContactEmployees.Add();
                }

                oBP.ContactEmployees.Name = dataSource.fullName;

                oBP.ContactEmployees.E_Mail = dataSource.email;
                oBP.ContactEmployees.MobilePhone = dataSource.phone;
                oBP.ContactEmployees.Address = dataSource.address;

                oBP.ContactEmployees.Gender = (dataSource.gender == "male" ? BoGenderTypes.gt_Male : dataSource.gender == "female" ? BoGenderTypes.gt_Female : BoGenderTypes.gt_Undefined);
                if (dataSource.birthday.HasValue)
                    oBP.ContactEmployees.DateOfBirth = dataSource.birthday.Value;



                int iError = 0;
                iError = oBP.Update();

                if (iError != 0)
                {
                    message = oCompany.GetLastErrorDescription();
                    return false;
                }

                //dataSource.sapId = cardCode;
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("[CreateContactPersion]", ex.Message);
                return false;
            }
        }

        public bool CreateSalesQuotation(EventBooking dataSource, out string message)
        {
            message = "";
            string columnName = "";
            try
            {
                int lineNum = 0;
                bool isUpdate = false;

                oCompany = Company_SAP(out message);

                oCompany.StartTransaction();

                Documents oSalesQuotations = oCompany.GetBusinessObject(BoObjectTypes.oQuotations);

                if (oSalesQuotations.GetByKey(Convert.ToInt32(dataSource.sapId)))
                {
                    isUpdate = true;
                }

                oSalesQuotations.CardCode = dataSource.customerId;
                oSalesQuotations.ContactPersonCode = Convert.ToInt32(dataSource.contactPersonId);
                oSalesQuotations.DocDate = DateTime.Now;
                oSalesQuotations.DiscountPercent = dataSource.discount;
                //oSalesQuotations.DocTotal = dataSource.paymentTotal;

                for (int i = oSalesQuotations.Lines.Count - 1; i >= 0; i--)
                {
                    oSalesQuotations.Lines.SetCurrentLine(i);
                    oSalesQuotations.Lines.Delete();
                }

                //Add Lines for themeDecor, performanceActivities, foodAndBeverage
                AddLines(oSalesQuotations, dataSource);

                int iError = 0;
                if (isUpdate)
                    iError = oSalesQuotations.Update();
                else
                    iError = oSalesQuotations.Add();
                if (iError != 0)
                {
                    message = oCompany.GetLastErrorDescription();
                    if (oCompany.InTransaction)
                        oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                    return false;
                }

                dataSource.sapId = oCompany.GetNewObjectKey();
                if (isUpdate == false)
                {
                    if (CreateSaleOpportunities(dataSource, out message) == false)
                    {
                        if (oCompany.InTransaction)
                            oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                        return false;

                    }
                }

                if (oCompany.InTransaction)
                    oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("[SAP Message]", ex.Message);

                if (oCompany.InTransaction)
                    oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
                return false;
            }
        }

        public bool CreateSaleOpportunities(EventBooking dataSource, out string message)
        {
            message = "";
            string columnName = "";
            try
            {

                oCompany = Company_SAP(out message);

                SalesOpportunities oSalesOpportunities = oCompany.GetBusinessObject(BoObjectTypes.oSalesOpportunities);

                oSalesOpportunities.CardCode = dataSource.customerId;
                oSalesOpportunities.StartDate = DateTime.Now;
                oSalesOpportunities.OpportunityName = "AAA";

                oSalesOpportunities.Lines.StartDate = DateTime.Now;
                oSalesOpportunities.Lines.StageKey = 1;
                oSalesOpportunities.Lines.DocumentType = BoAPARDocumentTypes.bodt_Quotation;
                oSalesOpportunities.Lines.DocumentNumber = Convert.ToInt32(dataSource.sapId);
                oSalesOpportunities.Lines.MaxLocalTotal = Convert.ToDouble(dataSource.paymentTotal);

                int iError = 0;

                iError = oSalesOpportunities.Add();
                if (iError != 0)
                {
                    message = oCompany.GetLastErrorDescription();
                    return false;
                }


                return true;
            }
            catch (Exception ex)
            {
                message = getFunctionError("[SAP Message]", ex.Message);
                return false;
            }
        }

        private void AddLines(Documents oSalesQuotations, EventBooking eventBooking)
        {
            //themeDecor
            if (eventBooking.themeDecor != null)
            {
                //themeDecor >> packages
                if (eventBooking.themeDecor.packages != null)
                {
                    foreach (var item in eventBooking.themeDecor.packages)
                    {
                        oSalesQuotations.Lines.ItemCode = item.sapId;
                        oSalesQuotations.Lines.Quantity = Convert.ToDouble(item.quantity);
                        oSalesQuotations.Lines.Price = Convert.ToDouble(item.price);
                        if (item.name != null && item.name.vi != null)
                            oSalesQuotations.Lines.ItemDescription = item.name.vi;
                        if (item.desc != null && item.desc.vi != null)
                            oSalesQuotations.Lines.FreeText = item.desc.vi;

                        oSalesQuotations.Lines.Add();

                        if (item.items != null)
                        {
                            var item100 = item.items;

                            oSalesQuotations.Lines.ItemCode = item100.sapId;
                            oSalesQuotations.Lines.Quantity = Convert.ToDouble(item100.quantity);
                            oSalesQuotations.Lines.Price = Convert.ToDouble(item100.price);
                            if (item100.name != null && item100.name.vi != null)
                                oSalesQuotations.Lines.ItemDescription = item.name.vi;
                            if (item100.desc != null && item100.desc.vi != null)
                                oSalesQuotations.Lines.FreeText = item.desc.vi;

                            oSalesQuotations.Lines.Add();
                        }
                    }
                }

                //themeDecor >> items
                if (eventBooking.themeDecor.items != null)
                {
                    foreach (var item in eventBooking.themeDecor.items)
                    {
                        oSalesQuotations.Lines.ItemCode = item.sapId;    // Open Item
                        oSalesQuotations.Lines.Quantity = Convert.ToDouble(item.quantity);
                        oSalesQuotations.Lines.Price = Convert.ToDouble(item.price);
                        if (item.name != null && item.name.vi != null)
                            oSalesQuotations.Lines.ItemDescription = item.name.vi;
                        if (item.desc != null && item.desc.vi != null)
                            oSalesQuotations.Lines.FreeText = item.desc.vi;

                        oSalesQuotations.Lines.Add();
                    }
                }

                //themeDecor >> addMoreList
                if (eventBooking.themeDecor.addMoreList != null)
                {
                    foreach (var item in eventBooking.themeDecor.addMoreList)
                    {
                        oSalesQuotations.Lines.ItemCode = item.sapId;   // Open Item
                        oSalesQuotations.Lines.Quantity = Convert.ToDouble(item.quantity);
                        oSalesQuotations.Lines.Price = Convert.ToDouble(item.price);
                        if (item.name != null && item.name.vi != null)
                            oSalesQuotations.Lines.ItemDescription = item.name.vi;
                        if (item.desc != null && item.desc.vi != null)
                            oSalesQuotations.Lines.FreeText = item.desc.vi;

                        oSalesQuotations.Lines.Add();
                    }
                }
            }

            //performanceActivities
            if (eventBooking.performanceActivities != null)
            {
                //performanceActivities >> packages
                if (eventBooking.performanceActivities.packages != null)
                {
                    foreach (var item in eventBooking.performanceActivities.packages)
                    {
                        oSalesQuotations.Lines.ItemCode = item.sapId;
                        oSalesQuotations.Lines.Quantity = Convert.ToDouble(item.quantity);
                        oSalesQuotations.Lines.Price = Convert.ToDouble(item.price);
                        if (item.name != null && item.name.vi != null)
                            oSalesQuotations.Lines.ItemDescription = item.name.vi;
                        if (item.desc != null && item.desc.vi != null)
                            oSalesQuotations.Lines.FreeText = item.desc.vi;

                        oSalesQuotations.Lines.Add();

                        if (item.items != null)
                        {
                            var item100 = item.items;

                            oSalesQuotations.Lines.ItemCode = item100.sapId;
                            oSalesQuotations.Lines.Quantity = Convert.ToDouble(item100.quantity);
                            oSalesQuotations.Lines.Price = Convert.ToDouble(item100.price);
                            if (item100.name != null && item100.name.vi != null)
                                oSalesQuotations.Lines.ItemDescription = item.name.vi;
                            if (item100.desc != null && item100.desc.vi != null)
                                oSalesQuotations.Lines.FreeText = item.desc.vi;

                            oSalesQuotations.Lines.Add();
                        }
                    }
                }

                //performanceActivities >> items
                if (eventBooking.performanceActivities.items != null)
                {
                    foreach (var item in eventBooking.performanceActivities.items)
                    {
                        oSalesQuotations.Lines.ItemCode = item.sapId;    // Open Item
                        oSalesQuotations.Lines.Quantity = Convert.ToDouble(item.quantity);
                        oSalesQuotations.Lines.Price = Convert.ToDouble(item.price);
                        if (item.name != null && item.name.vi != null)
                            oSalesQuotations.Lines.ItemDescription = item.name.vi;
                        if (item.desc != null && item.desc.vi != null)
                            oSalesQuotations.Lines.FreeText = item.desc.vi;

                        oSalesQuotations.Lines.Add();
                    }
                }

                //performanceActivities >> addMoreList
                if (eventBooking.performanceActivities.addMoreList != null)
                {
                    foreach (var item in eventBooking.performanceActivities.addMoreList)
                    {
                        oSalesQuotations.Lines.ItemCode = item.sapId;   // Open Item
                        oSalesQuotations.Lines.Quantity = Convert.ToDouble(item.quantity);
                        oSalesQuotations.Lines.Price = Convert.ToDouble(item.price);
                        if (item.name != null && item.name.vi != null)
                            oSalesQuotations.Lines.ItemDescription = item.name.vi;
                        if (item.desc != null && item.desc.vi != null)
                            oSalesQuotations.Lines.FreeText = item.desc.vi;

                        oSalesQuotations.Lines.Add();
                    }
                }
            }

            //foodAndBeverage
            if (eventBooking.foodAndBeverage != null)
            {
                //performanceActivities >> packages
                if (eventBooking.foodAndBeverage.packages != null)
                {
                    foreach (var item in eventBooking.foodAndBeverage.packages)
                    {
                        oSalesQuotations.Lines.ItemCode = item.sapId;
                        oSalesQuotations.Lines.Quantity = Convert.ToDouble(item.quantity);
                        oSalesQuotations.Lines.Price = Convert.ToDouble(item.price);
                        if (item.name != null && item.name.vi != null)
                            oSalesQuotations.Lines.ItemDescription = item.name.vi;
                        if (item.desc != null && item.desc.vi != null)
                            oSalesQuotations.Lines.FreeText = item.desc.vi;

                        oSalesQuotations.Lines.Add();

                        if (item.items != null)
                        {
                            var item100 = item.items;

                            oSalesQuotations.Lines.ItemCode = item100.sapId;
                            oSalesQuotations.Lines.Quantity = Convert.ToDouble(item100.quantity);
                            oSalesQuotations.Lines.Price = Convert.ToDouble(item100.price);
                            if (item100.name != null && item100.name.vi != null)
                                oSalesQuotations.Lines.ItemDescription = item.name.vi;
                            if (item100.desc != null && item100.desc.vi != null)
                                oSalesQuotations.Lines.FreeText = item.desc.vi;

                            oSalesQuotations.Lines.Add();
                        }
                    }
                }

                //foodAndBeverage >> items
                if (eventBooking.foodAndBeverage.items != null)
                {
                    foreach (var item in eventBooking.foodAndBeverage.items)
                    {
                        oSalesQuotations.Lines.ItemCode = item.sapId;    // Open Item
                        oSalesQuotations.Lines.Quantity = Convert.ToDouble(item.quantity);
                        oSalesQuotations.Lines.Price = Convert.ToDouble(item.price);
                        if (item.name != null && item.name.vi != null)
                            oSalesQuotations.Lines.ItemDescription = item.name.vi;
                        if (item.desc != null && item.desc.vi != null)
                            oSalesQuotations.Lines.FreeText = item.desc.vi;

                        oSalesQuotations.Lines.Add();
                    }
                }

                //foodAndBeverage >> addMoreList
                if (eventBooking.foodAndBeverage.addMoreList != null)
                {
                    foreach (var item in eventBooking.foodAndBeverage.addMoreList)
                    {
                        oSalesQuotations.Lines.ItemCode = item.sapId;   // Open Item
                        oSalesQuotations.Lines.Quantity = Convert.ToDouble(item.quantity);
                        oSalesQuotations.Lines.Price = Convert.ToDouble(item.price);
                        if (item.name != null && item.name.vi != null)
                            oSalesQuotations.Lines.ItemDescription = item.name.vi;
                        if (item.desc != null && item.desc.vi != null)
                            oSalesQuotations.Lines.FreeText = item.desc.vi;

                        oSalesQuotations.Lines.Add();
                    }
                }
            }

            //Venues
            if (eventBooking.venues != null)
            {
                foreach (var item in eventBooking.venues)
                {
                    foreach (var time in item.timeFrames)
                    {
                        //if (isUpdate)
                        //{
                        //    oSalesQuotations.Lines.SetCurrentLine(lineNum);
                        //    lineNum = lineNum + 1;
                        //}

                        oSalesQuotations.Lines.ItemCode = item.venueId;
                        oSalesQuotations.Lines.Quantity = 1;
                        oSalesQuotations.Lines.UserFields.Fields.Item("U_FromDate").Value = item.startPrepareDate;
                        oSalesQuotations.Lines.UserFields.Fields.Item("U_ToDate").Value = item.endPrepareDate;
                        oSalesQuotations.Lines.UserFields.Fields.Item("U_FromTime").Value = parseStarttime(time);
                        oSalesQuotations.Lines.UserFields.Fields.Item("U_ToTime").Value = parsEndtime(time);


                        //if (!isUpdate)
                        //    oSalesQuotations.Lines.Add();
                        oSalesQuotations.Lines.Add();
                    }

                }
            }
        }

        #region Create Object with Datatable
        public bool CreateDocument_Multiple(string TransType, DataTable dataSource, StringWriter dataSource_log, List<DataTable> dataLines, BoObjectTypes boObjectType, bool IsObjectDraft, out string newCode, out string docnum, out int oError, out string message, int baseType = 0)
        {
            message = "";
            newCode = "";
            docnum = "";
            oError = 0;
            InventoryCounting a;
            StringWriter sw = new StringWriter();
            string sqllog = "";
            bool isRes = false;
            try
            {

                string docEntry = "";
                object value;

                if (dataSource == null || dataSource.Rows.Count == 0)
                {
                    message = "Not found data in proccess.";
                    return false;
                }
                if (dataSource_log == null)
                {
                    message = "Not found data in proccess.";
                    return false;
                }

               
                oCompany = Company_SAP(out message);

                if (string.IsNullOrEmpty(message) == false)
                {
                    return false;
                }

               
                
                //sqllog = string.Format("INSERT INTO [NKID_WMS_LOG] VALUES('{0}','{1}','{2}',N'{3}',N'{4}')"
                //    , TransType.ToString().ToUpper(), "PROCCESS", DateTime.Now.ToString(), dataSource_log.ToString(), ""
                //    );
                //DoQuery(sqllog, out oErrorMessage,dataSource.Rows[0]["Location"].ToString());

                //sw = new StringWriter();
                //dataSource.TableName = TransType.ToUpper().Trim();
                //dataSource.WriteXml(sw);

                //sqllog = string.Format("INSERT INTO [NKID_WMS_LOG] VALUES('{0}','{1}','{2}',N'{3}',N'{4}')"
                //    , TransType.ToString().ToUpper(), "PROCCESS_SYNC",  DateTime.Now.ToString(), sw.ToString(), ""
                //    );
                //DoQuery(sqllog, out oErrorMessage, dataSource.Rows[0]["Location"].ToString());

                ////FOR LINES
                var lines = dataLines[0];
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    if (IsObjectDraft)
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                    else
                        oDocument = oCompany.GetBusinessObject(boObjectType);

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }


                            oDocument.DocObjectCode = boObjectType;
                            if (columnheader.ColumnName.ToLower() == "BPLId".ToLower())
                                oDocument.BPL_IDAssignedToInvoice = Convert.ToInt32(value);
                            if (columnheader.ColumnName.ToLower() == "CardCode".ToLower())
                                oDocument.CardCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "DocDate".ToLower())
                                oDocument.DocDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocDueDate".ToLower())
                                oDocument.DocDueDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower())
                                oDocument.TaxDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocType".ToLower())
                            {
                                if (value.ToString() == "I")
                                    oDocument.DocType = BoDocumentTypes.dDocument_Items;
                                else
                                    oDocument.DocType = BoDocumentTypes.dDocument_Service;
                            }
                            if (columnheader.ColumnName.ToLower() == "DocTotal".ToLower())
                                oDocument.DocTotal = (double)(decimal)value;
                            if (columnheader.ColumnName.ToLower() == "Comments".ToLower())
                                oDocument.Comments = value.ToString();
                            
                            if (columnheader.ColumnName.ToLower() == "NumAtCard".ToLower())
                                oDocument.NumAtCard = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "SlpCode".ToLower())
                                oDocument.SalesPersonCode = Convert.ToInt32(value);
                            if (columnheader.ColumnName.ToLower() == "OwnerCode".ToLower())
                                oDocument.DocumentsOwner = Convert.ToInt32(value);
                            if (columnheader.ColumnName.ToLower() == "CntctCode".ToLower())
                                oDocument.ContactPersonCode = Convert.ToInt32(value);

                            if (columnheader.ColumnName.ToLower() == "Series".ToLower())
                                oDocument.Series = Convert.ToInt32(value);

                            if (columnheader.ColumnName.ToLower() == "ShipToCode".ToLower())
                                oDocument.ShipToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address2".ToLower())
                                oDocument.Address2 = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "PayToCode".ToLower())
                                oDocument.PayToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address".ToLower())
                                oDocument.Address = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrackNo".ToLower())
                                oDocument.TrackingNumber = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Project".ToLower())
                                oDocument.Project = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Indicator".ToLower())
                                oDocument.Indicator = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "LicTradNum".ToLower())
                                oDocument.FederalTaxID = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrnspCode".ToLower())
                                oDocument.TransportationCode = Convert.ToInt32(value);
                            if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower())
                                oDocument.JournalMemo = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Ref1".ToLower())
                                oDocument.Reference1 = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Ref2".ToLower())
                                oDocument.Reference2 = value.ToString();
                            if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                Document_UDF(oDocument, columnheader, value);
                            }
                        }
                    }

                    //FOR LINES
                    var linesByHeader = lines.Select("DocEntry=" + docEntry);   //FIX TEMP
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        message = "Not found any items line.";
                        isRes = false;
                        goto WriteLog;
                    }
                    else
                    {
                        foreach (DataRow rowline in linesByHeader)
                        {
                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "ItemCode".ToLower())
                                        oDocument.Lines.ItemCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Dscription".ToLower())
                                        oDocument.Lines.ItemDescription = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "AcctCode".ToLower())
                                        oDocument.Lines.AccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "GLAccount".ToLower())
                                        oDocument.Lines.AccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Quantity".ToLower())
                                        oDocument.Lines.Quantity = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "UseBaseUn".ToLower())
                                    {
                                        if (value.ToString() == "Y")
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tYES;
                                        else
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tNO;
                                    }
                                    if (columnline.ColumnName.ToLower() == "UomEntry".ToLower())
                                    {
                                        oDocument.Lines.UoMEntry = Convert.ToInt32(value);
                                    }

                                    if (columnline.ColumnName.ToLower() == "Price".ToLower() || columnline.ColumnName.ToLower() == "PriceBefDi".ToLower())
                                        oDocument.Lines.UnitPrice = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VatGroup".ToLower())
                                        oDocument.Lines.VatGroup = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "PriceAfVAT".ToLower())
                                        oDocument.Lines.PriceAfterVAT = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "OcrCode".ToLower())
                                        oDocument.Lines.CostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower())
                                        oDocument.Lines.CostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower())
                                        oDocument.Lines.CostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower())
                                        oDocument.Lines.CostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Segment".ToLower())
                                        oDocument.Lines.CostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower())
                                        oDocument.Lines.CostingCode5 = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "WhsCode".ToLower())
                                        oDocument.Lines.WarehouseCode = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "SubCatNum".ToLower())
                                        oDocument.Lines.SupplierCatNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "DiscPrcnt".ToLower())
                                        oDocument.Lines.DiscountPercent = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VendorNum".ToLower())
                                        oDocument.Lines.VendorNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "SerialNum".ToLower())
                                        oDocument.Lines.SerialNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsAcct".ToLower())
                                        oDocument.Lines.COGSAccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsAccount".ToLower())
                                        oDocument.Lines.COGSAccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCod".ToLower())
                                        oDocument.Lines.COGSCostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo2".ToLower())
                                        oDocument.Lines.COGSCostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo3".ToLower())
                                        oDocument.Lines.COGSCostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo4".ToLower())
                                        oDocument.Lines.COGSCostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo5".ToLower())
                                        oDocument.Lines.COGSCostingCode5 = value.ToString();
                                    
                                    if (baseType > 0)
                                    {
                                        var baseEntry = Convert.ToInt32(rowline["BaseEntry"]);
                                        var baseLine = Convert.ToInt32(rowline["BaseLine"]);
                                        if (baseEntry > 0)
                                        {
                                            oDocument.Lines.BaseEntry = baseEntry;
                                            oDocument.Lines.BaseLine = baseLine;
                                            oDocument.Lines.BaseType = baseType;
                                        }
                                    }
                                    if (columnline.ColumnName.StartsWith("U_"))
                                    {
                                        Document_Lines_UDF(oDocument.Lines, columnline, value);
                                    }
                                }
                            }
                            oDocument.Lines.Add();
                        }
                    }


                    oError = oDocument.Add();

                    if (oError == 0)
                    {
                        newCode = oCompany.GetNewObjectKey();
                        if (oDocument.GetByKey(Convert.ToInt32(newCode)))
                            docnum = oDocument.DocNum.ToString();
                        isRes = true;
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        isRes = false;
                    }
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("Create Document", ex.Message);
                isRes = false;
            }
            WriteLog:

            try
            {
                sqllog = string.Format("INSERT INTO [NKID_WMS_LOG] VALUES('{0}','{1}','{2}',N'{3}',N'{4}')"
                   , TransType.ToString().ToUpper(), isRes.ToString().ToUpper(), DateTime.Now.ToString(), sw.ToString(), message.Replace("'", "")
                   );
                DoQuery(sqllog, out oErrorMessage, dataSource.Rows[0]["Location"].ToString());
            }
            catch (Exception)
            {
                //Continue...
            }
            finally
            {
                oCompany = null;
            }
            return isRes;

        }

        public bool UpdateDocument_Multiple(string TransType,DataTable dataSource, List<DataTable> dataLines, BoObjectTypes boObjectType, bool IsObjectDraft, out string newCode, out string docnum, out int oError, out string message, int baseType = 0)
        {
            message = "";
            newCode = "";
            docnum = "";
            oError = 0;
            InventoryCounting a;
            StringWriter sw = new StringWriter();
            string sqllog = "";
            bool isRes = false;
            try
            {

                string docEntry = "";
                object value;

                oCompany = Company_SAP(out message);

                if (string.IsNullOrEmpty(message) == false)
                {
                    return false;
                }
                if (dataSource == null || dataSource.Rows.Count == 0)
                {
                    message = "Not found data in proccess.";
                    return false;
                }


                dataSource.TableName = TransType.ToUpper().Trim();
                dataSource.WriteXml(sw);

                sqllog = string.Format("INSERT INTO [NKID_WMS_LOG] VALUES('{0}','{1}','{2}','{3}','{4}')"
                    , TransType.ToString().ToUpper(), "PROCCESS", DateTime.Now.ToString(), sw.ToString(), ""
                    );
                DoQuery(sqllog, out oErrorMessage);
                //FOR LINES
                var lines = dataLines[0];
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    if (IsObjectDraft)
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                    else
                        oDocument = oCompany.GetBusinessObject(boObjectType);

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }

                            oDocument.GetByKey( Convert.ToInt32( docEntry));
                            if(oDocument.Cancelled== BoYesNoEnum.tYES)
                            {
                                newCode = oDocument.DocEntry.ToString();
                                docnum = oDocument.DocNum.ToString();
                                message = "Sales Order [" + docnum + "] cannot be updated";
                                isRes = false;
                                goto WriteLog;
                            }
                            
                            oDocument.DocObjectCode = boObjectType;
                            if (columnheader.ColumnName.ToLower() == "BPLId".ToLower())
                                oDocument.BPL_IDAssignedToInvoice = Convert.ToInt32(value);
                            if (columnheader.ColumnName.ToLower() == "CardCode".ToLower())
                                oDocument.CardCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "DocDate".ToLower())
                                oDocument.DocDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocDueDate".ToLower())
                                oDocument.DocDueDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower())
                                oDocument.TaxDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "DocType".ToLower())
                            {
                                if (value.ToString() == "I")
                                    oDocument.DocType = BoDocumentTypes.dDocument_Items;
                                else
                                    oDocument.DocType = BoDocumentTypes.dDocument_Service;
                            }
                            if (columnheader.ColumnName.ToLower() == "DocTotal".ToLower())
                                oDocument.DocTotal = (double)(decimal)value;
                            if (columnheader.ColumnName.ToLower() == "Comments".ToLower())
                                oDocument.Comments = value.ToString();

                            if (columnheader.ColumnName.ToLower() == "NumAtCard".ToLower())
                                oDocument.NumAtCard = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "SlpCode".ToLower())
                                oDocument.SalesPersonCode = Convert.ToInt32(value);
                            if (columnheader.ColumnName.ToLower() == "OwnerCode".ToLower())
                                oDocument.DocumentsOwner = Convert.ToInt32(value);
                            if (columnheader.ColumnName.ToLower() == "CntctCode".ToLower())
                                oDocument.ContactPersonCode = Convert.ToInt32(value);

                            if (columnheader.ColumnName.ToLower() == "Series".ToLower())
                                oDocument.Series = Convert.ToInt32(value);

                            if (columnheader.ColumnName.ToLower() == "ShipToCode".ToLower())
                                oDocument.ShipToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address2".ToLower())
                                oDocument.Address2 = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "PayToCode".ToLower())
                                oDocument.PayToCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Address".ToLower())
                                oDocument.Address = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrackNo".ToLower())
                                oDocument.TrackingNumber = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Project".ToLower())
                                oDocument.Project = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Indicator".ToLower())
                                oDocument.Indicator = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "LicTradNum".ToLower())
                                oDocument.FederalTaxID = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "TrnspCode".ToLower())
                                oDocument.TransportationCode = Convert.ToInt32(value);
                            if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower())
                                oDocument.JournalMemo = value.ToString();
                            //if (columnheader.ColumnName.ToLower() == "WMSNum".ToLower()) oDocument.UserFields.Fields.Item("U_WMSNum").Value = value.ToString();

                            if (columnheader.ColumnName.ToLower() == "BPInvoiceTaxCode".ToLower())
                                oDocument.UserFields.Fields.Item("U_MST").Value = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "BPInvoiceName".ToLower())
                                oDocument.UserFields.Fields.Item("U_BPName").Value = value.ToString();

                            if (columnheader.ColumnName.ToLower() == "BPInvoiceCode".ToLower())
                                oDocument.UserFields.Fields.Item("U_BPCode").Value = value.ToString();

                            if (columnheader.ColumnName.ToLower() == "BPInvoiceAddress".ToLower())
                                oDocument.UserFields.Fields.Item("U_Diachi").Value = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "PaymentType".ToLower())
                                oDocument.UserFields.Fields.Item("U_HTTT").Value = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "IssueInvoice".ToLower())
                                oDocument.UserFields.Fields.Item("U_HDgap").Value = value.ToString();

                            if (columnheader.ColumnName.ToLower() == "Remarks".ToLower())
                                oDocument.UserFields.Fields.Item("U_VNDesc").Value = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "NhomHang".ToLower())
                                oDocument.UserFields.Fields.Item("U_Nhomhang").Value = value.ToString();

                            if (columnheader.ColumnName.ToLower() == "MucDichDonHang".ToLower())
                                oDocument.UserFields.Fields.Item("U_MucDichDH").Value = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "OnlineOrder".ToLower())
                                oDocument.UserFields.Fields.Item("U_OnlineOrder").Value = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "SalesforceOrderId".ToLower())
                                oDocument.UserFields.Fields.Item("U_Sf_OrderId").Value = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "Location".ToLower())
                                oDocument.UserFields.Fields.Item("U_Mavung").Value = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "EmpCode".ToLower())
                                oDocument.SalesPersonCode = Convert.ToInt32(value);

                            if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                Document_UDF(oDocument, columnheader, value);
                            }
                        }
                    }

                    //FOR LINES
                    var linesByHeader = lines.Select("DocEntry=" + docEntry);   //FIX TEMP
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        message = "Not found any items line.";
                        isRes = false;
                        goto WriteLog;
                    }
                    else
                    {
                        int row = 0;
                        foreach (DataRow rowline in linesByHeader)
                        {
                            oDocument.Lines.SetCurrentLine(row);
                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "ItemCode".ToLower())
                                        oDocument.Lines.ItemCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Dscription".ToLower())
                                        oDocument.Lines.ItemDescription = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "AcctCode".ToLower())
                                        oDocument.Lines.AccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Quantity".ToLower())
                                        oDocument.Lines.Quantity = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "UseBaseUn".ToLower())
                                    {
                                        if (value.ToString() == "Y")
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tYES;
                                        else
                                            oDocument.Lines.UseBaseUnits = BoYesNoEnum.tNO;
                                    }
                                    if (columnline.ColumnName.ToLower() == "UomEntry".ToLower())
                                    {
                                        oDocument.Lines.UoMEntry = Convert.ToInt32(value);
                                    }

                                    if (columnline.ColumnName.ToLower() == "Price".ToLower() || columnline.ColumnName.ToLower() == "PriceBefDi".ToLower())
                                        oDocument.Lines.UnitPrice = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VatGroup".ToLower())
                                        oDocument.Lines.VatGroup = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "PriceAfVAT".ToLower())
                                        oDocument.Lines.PriceAfterVAT = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "OcrCode".ToLower())
                                        oDocument.Lines.CostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower())
                                        oDocument.Lines.CostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower())
                                        oDocument.Lines.CostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower())
                                        oDocument.Lines.CostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Segment".ToLower())
                                        oDocument.Lines.CostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower())
                                        oDocument.Lines.CostingCode5 = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "WhsCode".ToLower())
                                        oDocument.Lines.WarehouseCode = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "SubCatNum".ToLower())
                                        oDocument.Lines.SupplierCatNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "DiscPrcnt".ToLower())
                                        oDocument.Lines.DiscountPercent = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "VendorNum".ToLower())
                                        oDocument.Lines.VendorNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "SerialNum".ToLower())
                                        oDocument.Lines.SerialNum = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsAcct".ToLower())
                                        oDocument.Lines.COGSAccountCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCod".ToLower())
                                        oDocument.Lines.COGSCostingCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo2".ToLower())
                                        oDocument.Lines.COGSCostingCode2 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo3".ToLower())
                                        oDocument.Lines.COGSCostingCode3 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo4".ToLower())
                                        oDocument.Lines.COGSCostingCode4 = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CogsOcrCo5".ToLower())
                                        oDocument.Lines.COGSCostingCode5 = value.ToString();

                                    if (baseType > 0)
                                    {
                                        var baseEntry = Convert.ToInt32(rowline["BaseEntry"]);
                                        var baseLine = Convert.ToInt32(rowline["BaseLine"]);
                                        if (baseEntry > 0)
                                        {
                                            oDocument.Lines.BaseEntry = baseEntry;
                                            oDocument.Lines.BaseLine = baseLine;
                                            oDocument.Lines.BaseType = baseType;
                                        }
                                    }

                                    if (columnline.ColumnName.ToLower() == "SalesforceOrderLineId".ToLower())
                                        oDocument.Lines.UserFields.Fields.Item("U_Sf_OrderLineId").Value = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "PriceAfDisc".ToLower())
                                        oDocument.Lines.UserFields.Fields.Item("U_PublicPrice").Value = (double)(decimal)value;
                                    if (columnline.ColumnName.StartsWith("U_"))
                                    {
                                        Document_Lines_UDF(oDocument.Lines, columnline, value);
                                    }
                                }
                            }
                            row += 1;
                            //oDocument.Lines.Add();
                        }
                    }

                    newCode= oDocument.DocEntry.ToString();
                    docnum = oDocument.DocNum.ToString();
                    oError = oDocument.Update();

                    if (oError == 0)
                    {
                        //newCode = oCompany.GetNewObjectKey();
                        //if (oDocument.GetByKey(Convert.ToInt32(newCode)))
                        //    docnum = oDocument.DocNum.ToString();
                        isRes = true;
                        
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                        isRes = false;
                    }
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("Create Document", ex.Message);
                isRes = false;
            }

            WriteLog:

            try
            {
                sqllog = string.Format("INSERT INTO [NKID_WMS_LOG] VALUES('{0}','{1}','{2}','{3}','{4}')"
                   , TransType.ToString().ToUpper(), isRes.ToString().ToUpper(), DateTime.Now.ToString(), sw.ToString(), message.Replace("'", "")
                   );
                DoQuery(sqllog, out oErrorMessage);
            }
            catch (Exception)
            {
                //Continue...
            }
            return isRes;

        }

        public bool CancelDocument(DataTable dataSource, BoObjectTypes boObjectType, bool IsObjectDraft, out string newCode, out string message)
        {
            message = "";
            newCode = "";
           
            try
            {
                int docEntry = 0;
                object value;

                oCompany = Company_SAP(out message);
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    if (IsObjectDraft)
                        oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                    else
                        oDocument = oCompany.GetBusinessObject(boObjectType);

                    value = rowheader["docEntry"];
                    docEntry = (int)value;

                    if (oDocument.GetByKey(docEntry))
                    {
                        var cancelDoc = oDocument.CreateCancellationDocument();

                        if (cancelDoc == null)
                        {
                            message = "Không tìm thấy chứng từ gốc hoặc chứng từ gốc đã hủy. Vui lòng kiểm tra lại.";
                        }
                        else if (rowheader["WMSNum"].ToString() != cancelDoc.UserFields.Fields.Item("U_WMSNum").Value)
                        {
                            message = "Thông tin không hợp lệ. Vui lòng kiểm tra lại.";
                        }
                        else
                        {
                            if (rowheader["Comments"] != null && rowheader["Comments"].ToString() != "")
                                cancelDoc.Comments = rowheader["Comments"].ToString();


                            oError = cancelDoc.Add();
                            if (oError == 0)
                            {
                                newCode = oCompany.GetNewObjectKey();
                                return true;
                            }
                            else
                            {
                                oCompany.GetLastError(out oError, out message);
                                message = "[SAPMessage]" + message;
                            }
                        }
                    }
                    else
                    {
                        message = "Không tìm thấy chứng từ gôc. Vui lòng kiểm tra lại.";
                    }
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("CancelDocument", ex.Message);
            }

            return false;

        }
       
        public bool CreateStockTransfer_Multiple(string TransType, DataTable dataSource, StringWriter dataSource_log, List<DataTable> dataLines, bool IsObjectDraft, out string newCode, out string docnum, out int oError, out string message)
        {
            message = "";
            newCode = "";
            docnum = "";
            oError = 0;
            StringWriter sw = new StringWriter();
            string sqllog = "";
            bool isRes = false;
            try
            {
                string docEntry = "";
                object value;

                oCompany = Company_SAP(out message);
                //FOR LINES
                var lines = dataLines[0];
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    if (IsObjectDraft)
                        oInvTrfs = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);//Phải Document không phải StockTransfer
                    else
                        oInvTrfs = oCompany.GetBusinessObject(BoObjectTypes.oStockTransfer);

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }


                            oInvTrfs.DocObjectCode = BoObjectTypes.oStockTransfer;
                            if (columnheader.ColumnName.ToLower() == "CardCode".ToLower()) oInvTrfs.CardCode = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "DocDate".ToLower()) oInvTrfs.DocDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower()) oInvTrfs.TaxDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "Comments".ToLower()) oInvTrfs.Comments = value.ToString();
                            if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower()) oInvTrfs.JournalMemo = value.ToString();

                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                StockTransfer_UDF(oInvTrfs, columnheader, value);
                            }
                        }
                    }

                    //FOR LINES
                    var linesByHeader = lines.Select("DocEntry=" + docEntry);
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        message = "Not found any items line.";
                        continue;//Tiếp tục với DocEntry khác
                    }
                    else
                    {
                        foreach (DataRow rowline in linesByHeader)
                        {
                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) oInvTrfs.Lines.ItemCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Dscription".ToLower()) oInvTrfs.Lines.ItemDescription = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "Quantity".ToLower()) oInvTrfs.Lines.Quantity = (double)(decimal)value;

                                    if (columnline.ColumnName.ToLower() == "FromWhsCode".ToLower()) oInvTrfs.Lines.FromWarehouseCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "ToWhsCode".ToLower()) oInvTrfs.Lines.WarehouseCode = value.ToString();

                                    else if (columnline.ColumnName.StartsWith("U_"))
                                    {
                                        StockTransfer_Lines_UDF(oInvTrfs.Lines, columnline, value);
                                    }
                                }
                            }
                            oInvTrfs.Lines.Add();
                        }
                    }


                    oError = oInvTrfs.Add();

                    if (oError == 0)
                    {
                        newCode = oCompany.GetNewObjectKey();
                        if (oDocument.GetByKey(Convert.ToInt32(newCode)))
                            docnum = oDocument.DocNum.ToString();
                        isRes = true;
                       
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                    }
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("Create Document", ex.Message);
                isRes = false;
            }
        WriteLog:

            try
            {
                sqllog = string.Format("INSERT INTO [NKID_WMS_LOG] VALUES('{0}','{1}','{2}',N'{3}',N'{4}')"
                   , TransType.ToString().ToUpper(), isRes.ToString().ToUpper(), DateTime.Now.ToString(), sw.ToString(), message.Replace("'", "")
                   );
                DoQuery(sqllog, out oErrorMessage, dataSource.Rows[0]["Location"].ToString());
            }
            catch (Exception)
            {
                //Continue...
            }
            finally
            {
                oCompany = null;
            }
            return isRes;

        }

        public bool CancelStockTransfer(DataTable dataSource, out string newCode, out string message)
        {
            message = "";
            newCode = "";

            try
            {
                int docEntry = 0;
                object value;

                oCompany = Company_SAP(out message);
                foreach (DataRow rowheader in dataSource.Rows)
                {

                    oInvTrfs = oCompany.GetBusinessObject(BoObjectTypes.oStockTransfer);

                    value = rowheader["docEntry"];
                    docEntry = (int)value;

                    if (oInvTrfs.GetByKey(docEntry))
                    {
                        if (rowheader["WMSNum"].ToString() != oInvTrfs.UserFields.Fields.Item("U_WMSNum").Value)
                        {
                            message = "Thông tin không hợp lệ. Vui lòng kiểm tra lại.";
                            return false;
                        }

                        oError = oInvTrfs.Cancel();

                        if (oError == 0)
                        {
                            var or = DoQuery(string.Format("select docentry from OWTR where Comments = 'Inventory transfer no. {0} has been canceled'", docEntry), out message);
                            if (or.RecordCount > 0)
                                newCode = or.Fields.Item(0).Value.ToString();
                            return true;
                        }
                        else
                        {
                            oCompany.GetLastError(out oError, out message);
                        }
                    }
                    else
                    {
                        message = "Không tìm thấy chứng từ gôc. Vui lòng kiểm tra lại.";
                    }
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("CancelDocument", ex.Message);
            }

            return false;

        }

        public bool CreateInventoryCounting_Multiple(DataTable dataSource, List<DataTable> dataLines, bool IsObjectDraft, out string newCode, out string message)
        {
            message = "";
            newCode = "";

            StringWriter sw = new StringWriter();
            dataSource.WriteXml(sw);
            string sqllog = "";

            bool isRes = false;

            try
            {
                sqllog = string.Format("INSERT INTO [@ICC_WMS_LOG] VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}')"
                    , "INV_COUNT", "PROCCESS", "", "", DateTime.Now.ToString(), sw.ToString(), ""
                    );
                DoQuery(sqllog, out oErrorMessage);

                string docEntry = "";
                object value;

                oCompany = Company_SAP(out message);
                //oCompany.StartTransaction();

                oCmpSrv = oCompany.GetCompanyService();

                oInvCountingService = oCmpSrv.GetBusinessService(ServiceTypes.InventoryCountingsService);
                oInvCounting = oInvCountingService.GetDataInterface(InventoryCountingsServiceDataInterfaces.icsInventoryCounting);
                var oInvCountingParams = (InventoryCountingParams)oInvCountingService.GetDataInterface(InventoryCountingsServiceDataInterfaces.icsInventoryCountingParams);

                //FOR LINES
                var lines = dataLines[0];
                foreach (DataRow rowheader in dataSource.Rows)
                {

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }

                            if (columnheader.ColumnName.ToLower() == "CountDate".ToLower()) oInvCounting.CountDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "Remarks".ToLower()) oInvCounting.Remarks = value.ToString();


                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                InventoryCounting_UDF(oInvCounting, columnheader, value);
                            }
                        }
                    }

                    //FOR LINES
                    var linesByHeader = lines.Select("DocEntry=" + docEntry);
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        message = "Not found any items line.";
                        continue;//Tiếp tục với DocEntry khác
                    }
                    else
                    {
                        foreach (DataRow rowline in linesByHeader)
                        {
                            var countLine = oInvCounting.InventoryCountingLines.Add();

                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) countLine.ItemCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CountQty".ToLower())
                                    {
                                        countLine.UoMCode = rowline["UoMCode"].ToString();
                                        countLine.UoMCountedQuantity = (double)(decimal)value;
                                    }
                                    if (columnline.ColumnName.ToLower() == "WhsCode".ToLower()) countLine.WarehouseCode = value.ToString();

                                    if (columnline.ColumnName.ToLower() == "Location".ToLower()) countLine.CostingCode5 = value.ToString();
                                    if (columnline.ColumnName.StartsWith("U_"))
                                    {
                                        InventoryCounting_Lines_UDF(countLine, columnline, value);
                                    }
                                    countLine.Counted = BoYesNoEnum.tYES;
                                }
                            }
                        }
                    }

                    try
                    {
                        oInvCountingParams = oInvCountingService.Add(oInvCounting);
                        newCode = oInvCountingParams.DocumentEntry.ToString();
                        isRes = true;

                        //option1
                        //lines.Columns.Add(new DataColumn() { ColumnName = "BaseEntry", DataType = typeof(int) });
                        //lines.Columns.Add(new DataColumn() { ColumnName = "BaseLine", DataType = typeof(int) });
                        //int baseLine = 1;
                        //foreach (DataRow item in lines.Rows)
                        //{
                        //    item["BaseEntry"] = newCode;
                        //    item["BaseLine"] = baseLine;
                        //    baseLine++;
                        //}

                        //if (CreateInventoryPosting(dataSource, dataLines, false, out newCode, out message))
                        //{
                        //    oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                        //    return true;
                        //}

                        //option2
                        //InventoryPostingsService oInvPostingService = oCmpSrv.GetBusinessService(ServiceTypes.InventoryPostingsService);
                        //InventoryPostingCopyOption oInvPostingCopy = oInvPostingService.GetDataInterface(InventoryPostingsServiceDataInterfaces.ipsInventoryPostingCopyOption);

                        //oInvPostingCopy.BaseEntry = oInvCountingParams.DocumentEntry;
                        //oInvPostingCopy.CopyOption = InventoryPostingCopyOptionEnum.ipcoTeamCountedQuantity;
                        //oInvPostingService.SetCopyOption(oInvPostingCopy);

                        //InventoryPosting oInvPosting = oInvPostingService.GetDataInterface(InventoryPostingsServiceDataInterfaces.ipsInventoryPosting);
                        //InventoryPostingParams oInvPostingParams = oInvPostingService.GetDataInterface(InventoryPostingsServiceDataInterfaces.ipsInventoryPostingParams);

                        //try
                        //{

                        //    oInvPostingParams = oInvPostingService.Add(oInvPosting);
                        //    newCode = oInvPostingParams.DocumentEntry.ToString();
                        //    oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                        //    return true;
                        //}
                        //catch (Exception ex)
                        //{
                        //    message = ex.Message;
                        //}

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }

                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("CreateInventoryCounting", ex.Message);
            }

            //if (oCompany.InTransaction)
            //{
            //    oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
            //}

            try
            {
                sqllog = string.Format("INSERT INTO [@ICC_WMS_LOG] VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}')"
                   , "INV_COUNT", isRes.ToString().ToUpper(), "", "", DateTime.Now.ToString(), sw.ToString()
                   , message
                   );
                DoQuery(sqllog, out oErrorMessage);
            }
            catch (Exception)
            {
                //Continue...

            }

            return isRes;

        }

        public bool CreateInventoryPosting_Multiple(DataTable dataSource, List<DataTable> dataLines, bool IsObjectDraft, out string newCode, out string message)
        {
            message = "";
            newCode = "";

            try
            {
                string docEntry = "";
                object value;

                oCompany = Company_SAP(out message);
                oCmpSrv = oCompany.GetCompanyService();

                InventoryPostingsService oInvPostingService = oCmpSrv.GetBusinessService(ServiceTypes.InventoryPostingsService);
                InventoryPosting oInvPosting = oInvPostingService.GetDataInterface(InventoryPostingsServiceDataInterfaces.ipsInventoryPosting);
                var oInvPostingParams = (InventoryPostingParams)oInvPostingService.GetDataInterface(InventoryPostingsServiceDataInterfaces.ipsInventoryPostingParams);

                //FOR LINES
                var lines = dataLines[0];
                foreach (DataRow rowheader in dataSource.Rows)
                {

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataSource.Columns)
                    {
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }

                            if (columnheader.ColumnName.ToLower() == "CountDate".ToLower())
                            {
                                oInvPosting.PostingDate = (DateTime)value;
                                oInvPosting.CountDate = (DateTime)value;
                            }
                            if (columnheader.ColumnName.ToLower() == "Remarks".ToLower()) oInvPosting.Remarks = value.ToString();


                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                InventoryPosting_UDF(oInvPosting, columnheader, value);
                            }
                        }
                    }

                    //FOR LINES
                    var linesByHeader = lines.Select("DocEntry=" + docEntry);
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        message = "Not found any items line.";
                        continue;//Tiếp tục với DocEntry khác
                    }
                    else
                    {
                        foreach (DataRow rowline in linesByHeader)
                        {
                            var countLine = oInvPosting.InventoryPostingLines.Add();

                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) countLine.ItemCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CountQty".ToLower()) countLine.CountedQuantity = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "WhsCode".ToLower()) countLine.WarehouseCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "BaseEntry".ToLower()) countLine.BaseEntry = (int)value;
                                    if (columnline.ColumnName.ToLower() == "BaseLine".ToLower()) countLine.BaseLine = (int)value;
                                    if (columnline.ColumnName.ToLower() == "Location".ToLower()) countLine.CostingCode5 = value.ToString();

                                    if (columnline.ColumnName.StartsWith("U_"))
                                    {
                                        InventoryPosting_Lines_UDF(countLine, columnline, value);
                                    }

                                    countLine.InventoryOffsetDecreaseAccount = "62110000";  //Dummy
                                    countLine.InventoryOffsetIncreaseAccount = "62110000";  //Dummy
                                    countLine.CountDate = (DateTime)rowheader["CountDate"];  //Dummy
                                    countLine.Price = 100;  //Dummy
                                }
                            }
                        }
                    }

                    try
                    {
                        oInvPostingParams = oInvPostingService.Add(oInvPosting);
                        newCode = oInvPostingParams.DocumentEntry.ToString();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }


                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("CreateInventoryPosting", ex.Message);
            }

            return false;

        }

        public bool CreateStockTaking(DataTable dataSource, List<DataTable> dataLines, bool IsObjectDraft, out string newCode, out string message)
        {
            message = "";
            newCode = "";

            try
            {
                string docEntry = "";
                object value;

                oCompany = Company_SAP(out message);
                StockTaking oStockTaking = null;

                //FOR LINES
                var lines = dataLines[0];
                foreach (DataRow rowheader in dataSource.Rows)
                {
                    if (IsObjectDraft)
                        oStockTaking = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);//Phải Document không phải StockTransfer
                    else
                        oStockTaking = oCompany.GetBusinessObject(BoObjectTypes.oStockTakings);

                    //FOR LINES
                    var linesByHeader = lines.Select("DocEntry=" + docEntry);
                    if (linesByHeader == null || linesByHeader.Length == 0)
                    {
                        message = "Not found any items line.";
                        continue;//Tiếp tục với DocEntry khác
                    }
                    else
                    {
                        foreach (DataRow rowline in linesByHeader)
                        {
                            foreach (DataColumn columnline in lines.Columns)
                            {
                                value = rowline[columnline.ColumnName];
                                if (value != null && value.ToString() != "")
                                {
                                    if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) oStockTaking.ItemCode = value.ToString();
                                    if (columnline.ColumnName.ToLower() == "CountQty".ToLower()) oStockTaking.Counted = (double)(decimal)value;
                                    if (columnline.ColumnName.ToLower() == "WhsCode".ToLower()) oStockTaking.WarehouseCode = value.ToString();
                                }
                            }
                            oStockTaking.Add();
                        }
                    }


                    oError = oInvTrfs.Add();

                    if (oError == 0)
                    {
                        newCode = oCompany.GetNewObjectKey();
                        return true;
                    }
                    else
                    {
                        oCompany.GetLastError(out oError, out message);
                    }
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("CreateGRPO", ex.Message);
            }

            return false;

        }

        public bool CreateDocument_OnlyOne(DataTable dataHeader, DataTable dataLines, BoObjectTypes boObjectType, bool IsObjectDraft, out string newCode, out string message, int baseType = 0)
        {
            message = "";
            newCode = "";

            try
            {

                string docEntry = "";
                object value;

                if (dataHeader == null || dataHeader.Rows.Count == 0)
                {
                    message = "Not found data in proccess.";
                    return false;
                }
                if (dataLines == null || dataLines.Rows.Count == 0)
                {
                    message = "Not found any items line.";
                    return false;
                }

                DataRow rowheader = dataHeader.Rows[0];
                DataRow[] linesByHeader = dataLines.Select();

                oCompany = Company_SAP(out message);

                if (string.IsNullOrEmpty(message) == false)
                {
                    return false;
                }

                if (IsObjectDraft)
                    oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                else
                    oDocument = oCompany.GetBusinessObject(boObjectType);

                //FOR HEADER
                foreach (DataColumn columnheader in dataHeader.Columns)
                {
                    value = rowheader[columnheader.ColumnName];
                    if (value != null && value.ToString() != "")
                    {
                        if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                        {
                            docEntry = value.ToString();
                        }


                        oDocument.DocObjectCode = boObjectType;
                        if (columnheader.ColumnName.ToLower() == "BPLId".ToLower()) oDocument.BPL_IDAssignedToInvoice = (int)value;
                        if (columnheader.ColumnName.ToLower() == "CardCode".ToLower()) oDocument.CardCode = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "DocDate".ToLower()) oDocument.DocDate = (DateTime)value;
                        if (columnheader.ColumnName.ToLower() == "DocDueDate".ToLower()) oDocument.DocDueDate = (DateTime)value;
                        if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower()) oDocument.TaxDate = (DateTime)value;
                        if (columnheader.ColumnName.ToLower() == "DocType".ToLower())
                        {
                            if (value.ToString() == "I")
                                oDocument.DocType = BoDocumentTypes.dDocument_Items;
                            else
                                oDocument.DocType = BoDocumentTypes.dDocument_Service;
                        }
                        if (columnheader.ColumnName.ToLower() == "DocTotal".ToLower()) oDocument.DocTotal = (double)(decimal)value;
                        if (columnheader.ColumnName.ToLower() == "Comments".ToLower()) oDocument.Comments = value.ToString();

                        if (columnheader.ColumnName.ToLower() == "NumAtCard".ToLower()) oDocument.NumAtCard = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "SlpCode".ToLower()) oDocument.SalesPersonCode = (int)value;
                        if (columnheader.ColumnName.ToLower() == "OwnerCode".ToLower()) oDocument.DocumentsOwner = (int)value;
                        if (columnheader.ColumnName.ToLower() == "CntctCode".ToLower()) oDocument.ContactPersonCode = (int)value;

                        if (columnheader.ColumnName.ToLower() == "ShipToCode".ToLower()) oDocument.ShipToCode = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "Address2".ToLower()) oDocument.Address2 = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "PayToCode".ToLower()) oDocument.PayToCode = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "Address".ToLower()) oDocument.Address = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "TrackNo".ToLower()) oDocument.TrackingNumber = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "Project".ToLower()) oDocument.Project = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "Indicator".ToLower()) oDocument.Indicator = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "LicTradNum".ToLower()) oDocument.FederalTaxID = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "TrnspCode".ToLower()) oDocument.TransportationCode = (int)value;
                        if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower()) oDocument.JournalMemo = value.ToString();

                        if (columnheader.ColumnName.StartsWith("U_"))
                        {
                            Document_UDF(oDocument, columnheader, value);
                        }
                    }
                }


                foreach (DataRow rowline in linesByHeader)
                {
                    foreach (DataColumn columnline in dataLines.Columns)
                    {
                        value = rowline[columnline.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) oDocument.Lines.ItemCode = value.ToString();
                            if (columnline.ColumnName.ToLower() == "Dscription".ToLower()) oDocument.Lines.ItemDescription = value.ToString();
                            if (columnline.ColumnName.ToLower() == "AcctCode".ToLower()) oDocument.Lines.AccountCode = value.ToString();
                            if (columnline.ColumnName.ToLower() == "Quantity".ToLower()) oDocument.Lines.Quantity = (double)(decimal)value;
                            if (columnline.ColumnName.ToLower() == "UseBaseUn".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oDocument.Lines.UseBaseUnits = BoYesNoEnum.tYES;
                                else
                                    oDocument.Lines.UseBaseUnits = BoYesNoEnum.tNO;
                            }
                            if (columnline.ColumnName.ToLower() == "UomEntry".ToLower())
                            {
                                oDocument.Lines.UoMEntry = (int)value;
                            }

                            if (columnline.ColumnName.ToLower() == "Price".ToLower() || columnline.ColumnName.ToLower() == "PriceBefDi".ToLower())
                                oDocument.Lines.UnitPrice = (double)(decimal)value;
                            if (columnline.ColumnName.ToLower() == "VatGroup".ToLower()) oDocument.Lines.VatGroup = value.ToString();
                            if (columnline.ColumnName.ToLower() == "PriceAfVAT".ToLower()) oDocument.Lines.PriceAfterVAT = (double)(decimal)value;
                            if (columnline.ColumnName.ToLower() == "OcrCode".ToLower()) oDocument.Lines.CostingCode = value.ToString();
                            if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower()) oDocument.Lines.CostingCode2 = value.ToString();
                            if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower()) oDocument.Lines.CostingCode3 = value.ToString();
                            if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower()) oDocument.Lines.CostingCode4 = value.ToString();
                            if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower()) oDocument.Lines.CostingCode5 = value.ToString();

                            if (columnline.ColumnName.ToLower() == "WhsCode".ToLower()) oDocument.Lines.WarehouseCode = value.ToString();

                            if (columnline.ColumnName.ToLower() == "SubCatNum".ToLower()) oDocument.Lines.SupplierCatNum = value.ToString();
                            if (columnline.ColumnName.ToLower() == "DiscPrcnt".ToLower()) oDocument.Lines.DiscountPercent = (double)(decimal)value;
                            if (columnline.ColumnName.ToLower() == "VendorNum".ToLower()) oDocument.Lines.VendorNum = value.ToString();
                            if (columnline.ColumnName.ToLower() == "SerialNum".ToLower()) oDocument.Lines.SerialNum = value.ToString();
                            if (columnline.ColumnName.ToLower() == "CogsAcct".ToLower()) oDocument.Lines.COGSAccountCode = value.ToString();
                            if (columnline.ColumnName.ToLower() == "CogsOcrCod".ToLower()) oDocument.Lines.COGSCostingCode = value.ToString();
                            if (columnline.ColumnName.ToLower() == "CogsOcrCo2".ToLower()) oDocument.Lines.COGSCostingCode2 = value.ToString();
                            if (columnline.ColumnName.ToLower() == "CogsOcrCo3".ToLower()) oDocument.Lines.COGSCostingCode3 = value.ToString();
                            if (columnline.ColumnName.ToLower() == "CogsOcrCo4".ToLower()) oDocument.Lines.COGSCostingCode4 = value.ToString();
                            if (columnline.ColumnName.ToLower() == "CogsOcrCo5".ToLower()) oDocument.Lines.COGSCostingCode5 = value.ToString();

                            if (baseType > 0)
                            {
                                var baseEntry = Convert.ToInt32(rowline["BaseEntry"]);
                                var baseLine = Convert.ToInt32(rowline["BaseLine"]);
                                if (baseEntry > 0)
                                {
                                    oDocument.Lines.BaseEntry = baseEntry;
                                    oDocument.Lines.BaseLine = baseLine;
                                    oDocument.Lines.BaseType = baseType;
                                }
                            }

                            //if (columnline.ColumnName.ToLower() == "WMSNum".ToLower()) oDocument.Lines.UserFields.Fields.Item("U_WMSNum").Value = value.ToString();

                            if (columnline.ColumnName.StartsWith("U_"))
                            {
                                Document_Lines_UDF(oDocument.Lines, columnline, value);
                            }
                        }
                    }
                    oDocument.Lines.Add();
                }


                oError = oDocument.Add();

                if (oError == 0)
                {
                    newCode = oCompany.GetNewObjectKey();
                    return true;
                }
                else
                {
                    oCompany.GetLastError(out oError, out message);
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("CreateGRPO", ex.Message);
            }

            return false;

        }

        public bool CreateStockTransfer_OnlyOne(DataTable dataHeader, DataTable dataLines, bool IsObjectDraft, out string newCode, out string message)
        {
            message = "";
            newCode = "";

            try
            {
                string docEntry = "";
                object value;

                if (dataHeader == null || dataHeader.Rows.Count == 0)
                {
                    message = "Not found data in proccess.";
                    return false;
                }
                if (dataLines == null || dataLines.Rows.Count == 0)
                {
                    message = "Not found any items line.";
                    return false;
                }

                DataRow rowheader = dataHeader.Rows[0];
                DataRow[] linesByHeader = dataLines.Select();

                oCompany = Company_SAP(out message);

                if (IsObjectDraft)
                    oInvTrfs = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);//Phải Document không phải StockTransfer
                else
                    oInvTrfs = oCompany.GetBusinessObject(BoObjectTypes.oStockTransfer);

                //FOR HEADER
                foreach (DataColumn columnheader in dataHeader.Columns)
                {
                    value = rowheader[columnheader.ColumnName];
                    if (value != null && value.ToString() != "")
                    {
                        if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                        {
                            docEntry = value.ToString();
                        }


                        oInvTrfs.DocObjectCode = BoObjectTypes.oStockTransfer;
                        //if (columnheader.ColumnName.ToLower() == "CardCode".ToLower()) oInvTrfs.CardCode = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "DocDate".ToLower()) oInvTrfs.DocDate = (DateTime)value;
                        if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower()) oInvTrfs.TaxDate = (DateTime)value;
                        if (columnheader.ColumnName.ToLower() == "Comments".ToLower()) oInvTrfs.Comments = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower()) oInvTrfs.JournalMemo = value.ToString();

                        else if (columnheader.ColumnName.StartsWith("U_"))
                        {
                            StockTransfer_UDF(oInvTrfs, columnheader, value);
                        }
                    }
                }


                foreach (DataRow rowline in linesByHeader)
                {
                    foreach (DataColumn columnline in dataLines.Columns)
                    {
                        value = rowline[columnline.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) oInvTrfs.Lines.ItemCode = value.ToString();
                            if (columnline.ColumnName.ToLower() == "Dscription".ToLower()) oInvTrfs.Lines.ItemDescription = value.ToString();
                            if (columnline.ColumnName.ToLower() == "Quantity".ToLower()) oInvTrfs.Lines.Quantity = (double)(decimal)value;

                            if (columnline.ColumnName.ToLower() == "FromWhsCode".ToLower()) oInvTrfs.Lines.FromWarehouseCode = value.ToString();
                            if (columnline.ColumnName.ToLower() == "ToWhsCode".ToLower()) oInvTrfs.Lines.WarehouseCode = value.ToString();
                            if (columnline.ColumnName.ToLower() == "UseBaseUn".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oInvTrfs.Lines.UseBaseUnits = BoYesNoEnum.tYES;
                                else
                                    oInvTrfs.Lines.UseBaseUnits = BoYesNoEnum.tNO;
                            }
                            else if (columnline.ColumnName.StartsWith("U_"))
                            {
                                StockTransfer_Lines_UDF(oInvTrfs.Lines, columnline, value);
                            }
                        }
                    }
                    oInvTrfs.Lines.Add();
                }

                oError = oInvTrfs.Add();

                if (oError == 0)
                {
                    newCode = oCompany.GetNewObjectKey();
                    return true;
                }
                else
                {
                    oCompany.GetLastError(out oError, out message);
                }

            }
            catch (Exception ex)
            {
                message = getFunctionError("Create Object", ex.Message);
            }

            return false;

        }

        public bool CreateInventoryCounting_OnlyOne(DataTable dataHeader, DataTable dataLines, bool IsObjectDraft, out string newCode, out string message)
        {
            message = "";
            newCode = "";

            bool isRes = false;

            StringWriter sw = new StringWriter();
            string sqllog = "";

            try
            {
                if (dataHeader == null || dataHeader.Rows.Count == 0)
                {
                    message = "Not found data in proccess.";
                    isRes = false;
                }
                else if (dataLines == null || dataLines.Rows.Count == 0)
                {
                    message = "Not found any items line.";
                    isRes = false;
                }
                else
                {
                    dataHeader.WriteXml(sw);

                    sqllog = string.Format("INSERT INTO [@ICC_WMS_LOG] VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}')"
                        , "INV_COUNT", "PROCCESS", "", "", DateTime.Now.ToString(), sw.ToString(), ""
                        );
                    DoQuery(sqllog, out oErrorMessage);

                    string docEntry = "";
                    object value;

                    oCompany = Company_SAP(out message);
                    //oCompany.StartTransaction();

                    oCmpSrv = oCompany.GetCompanyService();

                    oInvCountingService = oCmpSrv.GetBusinessService(ServiceTypes.InventoryCountingsService);
                    oInvCounting = oInvCountingService.GetDataInterface(InventoryCountingsServiceDataInterfaces.icsInventoryCounting);
                    var oInvCountingParams = (InventoryCountingParams)oInvCountingService.GetDataInterface(InventoryCountingsServiceDataInterfaces.icsInventoryCountingParams);

                    //FOR LINES
                    DataRow rowheader = dataHeader.Rows[0];
                    DataRow[] linesByHeader = dataLines.Select();

                    //FOR HEADER
                    foreach (DataColumn columnheader in dataHeader.Columns)
                    {
                        value = rowheader[columnheader.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                            {
                                docEntry = value.ToString();
                            }

                            if (columnheader.ColumnName.ToLower() == "CountDate".ToLower()) oInvCounting.CountDate = (DateTime)value;
                            if (columnheader.ColumnName.ToLower() == "Remarks".ToLower()) oInvCounting.Remarks = value.ToString();

                            else if (columnheader.ColumnName.StartsWith("U_"))
                            {
                                InventoryCounting_UDF(oInvCounting, columnheader, value);
                            }
                        }
                    }

                    //FOR LINES
                    foreach (DataRow rowline in linesByHeader)
                    {
                        var countLine = oInvCounting.InventoryCountingLines.Add();

                        foreach (DataColumn columnline in dataLines.Columns)
                        {
                            value = rowline[columnline.ColumnName];
                            if (value != null && value.ToString() != "")
                            {
                                if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) countLine.ItemCode = value.ToString();
                                if (columnline.ColumnName.ToLower() == "CountQty".ToLower())
                                {
                                    countLine.UoMCode = rowline["UoMCode"].ToString();
                                    countLine.UoMCountedQuantity = (double)(decimal)value;
                                }
                                if (columnline.ColumnName.ToLower() == "WhsCode".ToLower()) countLine.WarehouseCode = value.ToString();

                                if (columnline.ColumnName.ToLower() == "Location".ToLower()) countLine.CostingCode5 = value.ToString();
                                if (columnline.ColumnName.StartsWith("U_"))
                                {
                                    InventoryCounting_Lines_UDF(countLine, columnline, value);
                                }
                                countLine.Counted = BoYesNoEnum.tYES;
                            }
                        }
                    }

                    try
                    {
                        oInvCountingParams = oInvCountingService.Add(oInvCounting);
                        newCode = oInvCountingParams.DocumentEntry.ToString();
                        isRes = true;

                        //option1
                        //lines.Columns.Add(new DataColumn() { ColumnName = "BaseEntry", DataType = typeof(int) });
                        //lines.Columns.Add(new DataColumn() { ColumnName = "BaseLine", DataType = typeof(int) });
                        //int baseLine = 1;
                        //foreach (DataRow item in lines.Rows)
                        //{
                        //    item["BaseEntry"] = newCode;
                        //    item["BaseLine"] = baseLine;
                        //    baseLine++;
                        //}

                        //if (CreateInventoryPosting(dataSource, dataLines, false, out newCode, out message))
                        //{
                        //    oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                        //    return true;
                        //}

                        //option2
                        //InventoryPostingsService oInvPostingService = oCmpSrv.GetBusinessService(ServiceTypes.InventoryPostingsService);
                        //InventoryPostingCopyOption oInvPostingCopy = oInvPostingService.GetDataInterface(InventoryPostingsServiceDataInterfaces.ipsInventoryPostingCopyOption);

                        //oInvPostingCopy.BaseEntry = oInvCountingParams.DocumentEntry;
                        //oInvPostingCopy.CopyOption = InventoryPostingCopyOptionEnum.ipcoTeamCountedQuantity;
                        //oInvPostingService.SetCopyOption(oInvPostingCopy);

                        //InventoryPosting oInvPosting = oInvPostingService.GetDataInterface(InventoryPostingsServiceDataInterfaces.ipsInventoryPosting);
                        //InventoryPostingParams oInvPostingParams = oInvPostingService.GetDataInterface(InventoryPostingsServiceDataInterfaces.ipsInventoryPostingParams);

                        //try
                        //{

                        //    oInvPostingParams = oInvPostingService.Add(oInvPosting);
                        //    newCode = oInvPostingParams.DocumentEntry.ToString();
                        //    oCompany.EndTransaction(BoWfTransOpt.wf_Commit);
                        //    return true;
                        //}
                        //catch (Exception ex)
                        //{
                        //    message = ex.Message;
                        //}

                    }
                    catch (Exception ex)
                    {
                        message = ex.Message;
                    }
                }

            }
            catch (Exception ex)
            {
                message = getFunctionError("CreateInventoryCounting", ex.Message);
            }

            //if (oCompany.InTransaction)
            //{
            //    oCompany.EndTransaction(BoWfTransOpt.wf_RollBack);
            //}

            try
            {
                sqllog = string.Format("INSERT INTO [@ICC_WMS_LOG] VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}')"
                   , "INV_COUNT", isRes.ToString().ToUpper(), "", "", DateTime.Now.ToString(), sw.ToString()
                   , message
                   );
                DoQuery(sqllog, out oErrorMessage);
            }
            catch (Exception)
            {
                //Continue...

            }

            return isRes;

        }

        public bool UpdateDocument_OnlyOne(DataTable dataHeader, DataTable dataLines, BoObjectTypes boObjectType, bool IsObjectDraft, out string newCode, out string message, int baseType = 0)
        {
            message = "";
            newCode = "";

            try
            {

                string docEntry = "";
                object value;

                if (dataHeader == null || dataHeader.Rows.Count == 0)
                {
                    message = "Not found data in proccess.";
                    return false;
                }
                if (dataLines == null || dataLines.Rows.Count == 0)
                {
                    message = "Not found any items line.";
                    return false;
                }

                DataRow rowheader = dataHeader.Rows[0];
                DataRow[] linesByHeader = dataLines.Select();

                oCompany = Company_SAP(out message);

                if (string.IsNullOrEmpty(message) == false)
                {
                    return false;
                }

                if (IsObjectDraft)
                    oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                else
                    oDocument = oCompany.GetBusinessObject(boObjectType);

                if (dataHeader.Columns.Contains("DocEntry") == false)
                {
                    message = "Dữ liệu không hợp lê. Không tìm thấy thông tin sapb1 number.";
                    return false;
                }

                docEntry = rowheader["DocEntry"].ToString();

                if (oDocument.GetByKey(Convert.ToInt32(docEntry)) == false)
                {
                    message = "Không tìm thấy chứng từ trên sapb1. Vui lòng kiểm tra lại thông tin.";
                    return false;
                }

                //FOR HEADER
                foreach (DataColumn columnheader in dataHeader.Columns)
                {
                    value = rowheader[columnheader.ColumnName];
                    if (value != null && value.ToString() != "")
                    {
                        if (columnheader.ColumnName.ToLower() == "docEntry".ToLower())
                        {
                            docEntry = value.ToString();
                        }


                        oDocument.DocObjectCode = boObjectType;
                        if (columnheader.ColumnName.ToLower() == "BPLId".ToLower()) oDocument.BPL_IDAssignedToInvoice = (int)value;
                        if (columnheader.ColumnName.ToLower() == "CardCode".ToLower()) oDocument.CardCode = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "DocDate".ToLower()) oDocument.DocDate = (DateTime)value;
                        if (columnheader.ColumnName.ToLower() == "DocDueDate".ToLower()) oDocument.DocDueDate = (DateTime)value;
                        if (columnheader.ColumnName.ToLower() == "TaxDate".ToLower()) oDocument.TaxDate = (DateTime)value;
                        if (columnheader.ColumnName.ToLower() == "DocType".ToLower())
                        {
                            if (value.ToString() == "I")
                                oDocument.DocType = BoDocumentTypes.dDocument_Items;
                            else
                                oDocument.DocType = BoDocumentTypes.dDocument_Service;
                        }
                        if (columnheader.ColumnName.ToLower() == "DocTotal".ToLower()) oDocument.DocTotal = (double)(decimal)value;
                        if (columnheader.ColumnName.ToLower() == "Comments".ToLower()) oDocument.Comments = value.ToString();

                        if (columnheader.ColumnName.ToLower() == "NumAtCard".ToLower()) oDocument.NumAtCard = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "SlpCode".ToLower()) oDocument.SalesPersonCode = (int)value;
                        if (columnheader.ColumnName.ToLower() == "OwnerCode".ToLower()) oDocument.DocumentsOwner = (int)value;
                        if (columnheader.ColumnName.ToLower() == "CntctCode".ToLower()) oDocument.ContactPersonCode = (int)value;

                        if (columnheader.ColumnName.ToLower() == "ShipToCode".ToLower()) oDocument.ShipToCode = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "Address2".ToLower()) oDocument.Address2 = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "PayToCode".ToLower()) oDocument.PayToCode = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "Address".ToLower()) oDocument.Address = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "TrackNo".ToLower()) oDocument.TrackingNumber = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "Project".ToLower()) oDocument.Project = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "Indicator".ToLower()) oDocument.Indicator = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "LicTradNum".ToLower()) oDocument.FederalTaxID = value.ToString();
                        if (columnheader.ColumnName.ToLower() == "TrnspCode".ToLower()) oDocument.TransportationCode = (int)value;
                        if (columnheader.ColumnName.ToLower() == "JrnlMemo".ToLower()) oDocument.JournalMemo = value.ToString();
                        //if (columnheader.ColumnName.ToLower() == "WMSNum".ToLower()) oDocument.UserFields.Fields.Item("U_WMSNum").Value = value.ToString();

                        if (columnheader.ColumnName.StartsWith("U_"))
                        {
                            Document_UDF(oDocument, columnheader, value);
                        }
                    }
                }

                //Clear row
                int maxrow = oDocument.Lines.Count - 1;
                for (int ii = maxrow; ii >= 0; ii--)
                {
                    oDocument.Lines.SetCurrentLine(ii);
                    oDocument.Lines.Delete();
                }

                foreach (DataRow rowline in linesByHeader)
                {
                    foreach (DataColumn columnline in dataLines.Columns)
                    {
                        value = rowline[columnline.ColumnName];
                        if (value != null && value.ToString() != "")
                        {
                            if (columnline.ColumnName.ToLower() == "ItemCode".ToLower()) oDocument.Lines.ItemCode = value.ToString();
                            if (columnline.ColumnName.ToLower() == "Dscription".ToLower()) oDocument.Lines.ItemDescription = value.ToString();
                            if (columnline.ColumnName.ToLower() == "AcctCode".ToLower()) oDocument.Lines.AccountCode = value.ToString();
                            if (columnline.ColumnName.ToLower() == "Quantity".ToLower()) oDocument.Lines.Quantity = (double)(decimal)value;
                            if (columnline.ColumnName.ToLower() == "UseBaseUn".ToLower())
                            {
                                if (value.ToString() == "Y")
                                    oDocument.Lines.UseBaseUnits = BoYesNoEnum.tYES;
                                else
                                    oDocument.Lines.UseBaseUnits = BoYesNoEnum.tNO;
                            }
                            if (columnline.ColumnName.ToLower() == "UomEntry".ToLower())
                            {
                                oDocument.Lines.UoMEntry = (int)value;
                            }

                            if (columnline.ColumnName.ToLower() == "Price".ToLower() || columnline.ColumnName.ToLower() == "PriceBefDi".ToLower())
                                oDocument.Lines.UnitPrice = (double)(decimal)value;
                            if (columnline.ColumnName.ToLower() == "VatGroup".ToLower()) oDocument.Lines.VatGroup = value.ToString();
                            if (columnline.ColumnName.ToLower() == "PriceAfVAT".ToLower()) oDocument.Lines.PriceAfterVAT = (double)(decimal)value;
                            if (columnline.ColumnName.ToLower() == "OcrCode".ToLower()) oDocument.Lines.CostingCode = value.ToString();
                            if (columnline.ColumnName.ToLower() == "OcrCode2".ToLower()) oDocument.Lines.CostingCode2 = value.ToString();
                            if (columnline.ColumnName.ToLower() == "OcrCode3".ToLower()) oDocument.Lines.CostingCode3 = value.ToString();
                            if (columnline.ColumnName.ToLower() == "OcrCode4".ToLower()) oDocument.Lines.CostingCode4 = value.ToString();
                            if (columnline.ColumnName.ToLower() == "OcrCode5".ToLower()) oDocument.Lines.CostingCode5 = value.ToString();

                            if (columnline.ColumnName.ToLower() == "WhsCode".ToLower()) oDocument.Lines.WarehouseCode = value.ToString();

                            if (columnline.ColumnName.ToLower() == "SubCatNum".ToLower()) oDocument.Lines.SupplierCatNum = value.ToString();
                            if (columnline.ColumnName.ToLower() == "DiscPrcnt".ToLower()) oDocument.Lines.DiscountPercent = (double)(decimal)value;
                            if (columnline.ColumnName.ToLower() == "VendorNum".ToLower()) oDocument.Lines.VendorNum = value.ToString();
                            if (columnline.ColumnName.ToLower() == "SerialNum".ToLower()) oDocument.Lines.SerialNum = value.ToString();
                            if (columnline.ColumnName.ToLower() == "CogsAcct".ToLower()) oDocument.Lines.COGSAccountCode = value.ToString();
                            if (columnline.ColumnName.ToLower() == "CogsOcrCod".ToLower()) oDocument.Lines.COGSCostingCode = value.ToString();
                            if (columnline.ColumnName.ToLower() == "CogsOcrCo2".ToLower()) oDocument.Lines.COGSCostingCode2 = value.ToString();
                            if (columnline.ColumnName.ToLower() == "CogsOcrCo3".ToLower()) oDocument.Lines.COGSCostingCode3 = value.ToString();
                            if (columnline.ColumnName.ToLower() == "CogsOcrCo4".ToLower()) oDocument.Lines.COGSCostingCode4 = value.ToString();
                            if (columnline.ColumnName.ToLower() == "CogsOcrCo5".ToLower()) oDocument.Lines.COGSCostingCode5 = value.ToString();

                            if (baseType > 0)
                            {
                                var baseEntry = Convert.ToInt32(rowline["BaseEntry"]);
                                var baseLine = Convert.ToInt32(rowline["BaseLine"]);
                                if (baseEntry > 0)
                                {
                                    oDocument.Lines.BaseEntry = baseEntry;
                                    oDocument.Lines.BaseLine = baseLine;
                                    oDocument.Lines.BaseType = baseType;
                                }
                            }

                            if (columnline.ColumnName.StartsWith("U_"))
                            {
                                Document_Lines_UDF(oDocument.Lines, columnline, value);
                            }
                        }
                    }
                    oDocument.Lines.Add();
                }


                oError = oDocument.Update();

                if (oError == 0)
                {
                    newCode = docEntry;
                    return true;
                }
                else
                {
                    oCompany.GetLastError(out oError, out message);
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("Update Document", ex.Message);
            }

            return false;

        }

        public bool CopyDocument_OnlyOne(DataTable dataHeader, int baseEntry, BoObjectTypes boBaseType, BoObjectTypes boObjectType, bool IsObjectDraft, out string newCode, out string message, object WMSNum = null)
        {
            message = "";
            newCode = "";
            //string xmlstring = "";
            try
            {

                oCompany = Company_SAP(out message);

                if (string.IsNullOrEmpty(message) == false)
                {
                    return false;
                }

                SAPbobsCOM.Documents baseDocument;
                SAPbobsCOM.Documents document;

                baseDocument = oCompany.GetBusinessObject(boBaseType);
                if (IsObjectDraft)
                    document = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                else
                    document = oCompany.GetBusinessObject(boObjectType);


                //oCompany.XmlExportType = SAPbobsCOM.BoXmlExportTypes.xet_ExportImportMode;
                //oCompany.XMLAsString = true;

                if (baseDocument.GetByKey(baseEntry) == false)
                {
                    message = "Không tìm thấy chứng từ trên sapb1. Vui lòng kiểm tra lại thông tin.";
                    return false;
                }

                //xmlstring = baseDocument.GetAsXML();


                ////Replase ObjectType
                //xmlstring = xmlstring.Replace(string.Format("<Object>{0}</Object>", (int)boBaseType), string.Format("<Object>{0}</Object>", (int)boObjectType));
                ////Replase DocObjectCode
                //xmlstring = xmlstring.Replace(string.Format("<DocObjectCode>{0}</DocObjectCode>", (int)boBaseType), string.Format("<DocObjectCode>{0}</DocObjectCode>", (int)boObjectType));
                ////Replace DocDate
                //xmlstring = xmlstring.Replace(string.Format("<DocDate>{0}</DocDate>", baseDocument.DocDate.ToString("yyyyMMdd"))
                //    , string.Format("<DocDate>{0}</DocDate>", Convert.ToDateTime(dataHeader.Rows[0]["DocDate"]).ToString("yyyyMMdd")));

                //document.Browser.ReadXml(xmlstring, 0);


                document.CardCode = baseDocument.CardCode;
                document.DocDate = Convert.ToDateTime(dataHeader.Rows[0]["DocDate"]);
                document.DocObjectCode = boObjectType;
                if (WMSNum != null)
                {
                    document.UserFields.Fields.Item("U_WMSNum").Value = WMSNum.ToString();
                }
                for (int i = 0; i < baseDocument.Lines.Count; i++)
                {
                    baseDocument.Lines.SetCurrentLine(i);
                    document.Lines.ItemCode = baseDocument.Lines.ItemCode;
                    document.Lines.Quantity = baseDocument.Lines.Quantity;
                    document.Lines.WarehouseCode = baseDocument.Lines.WarehouseCode;

                    document.Lines.BaseType = (int)boBaseType;
                    document.Lines.BaseEntry = baseDocument.Lines.DocEntry;
                    document.Lines.BaseLine = baseDocument.Lines.LineNum;

                    if (WMSNum != null)
                        document.Lines.UserFields.Fields.Item("U_WMSNum").Value = WMSNum.ToString();

                    document.Lines.Add();
                }

                oError = document.Add();

                if (oError == 0)
                {
                    newCode = oCompany.GetNewObjectKey();
                    return true;
                }
                else
                {
                    oCompany.GetLastError(out oError, out message);
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("Copy Document", ex.Message);
            }

            return false;

        }

        public bool CloseDocument_OnlyOne(DataTable dataHeader, BoObjectTypes boObjectType, bool IsObjectDraft, out string newCode, out string message)
        {
            message = "";
            newCode = "";

            try
            {

                string docEntry = "";
                object value;

                if (dataHeader == null || dataHeader.Rows.Count == 0)
                {
                    message = "Not found data in proccess.";
                    return false;
                }

                DataRow rowheader = dataHeader.Rows[0];

                oCompany = Company_SAP(out message);

                if (string.IsNullOrEmpty(message) == false)
                {
                    return false;
                }

                if (IsObjectDraft)
                    oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                else
                    oDocument = oCompany.GetBusinessObject(boObjectType);

                if (dataHeader.Columns.Contains("DocEntry") == false)
                {
                    message = "Dữ liệu không hợp lê. Không tìm thấy thông tin sapb1 number.";
                    return false;
                }

                docEntry = rowheader["DocEntry"].ToString();

                if (oDocument.GetByKey(Convert.ToInt32(docEntry)) == false)
                {
                    message = "Không tìm thấy chứng từ trên sapb1. Vui lòng kiểm tra lại thông tin.";
                    return false;
                }

                oError = oDocument.Close();

                if (oError == 0)
                {
                    newCode = docEntry;
                    return true;
                }
                else
                {
                    oCompany.GetLastError(out oError, out message);
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("Close Document", ex.Message);
            }

            return false;

        }

        public bool RemoveDocument_OnlyOne(DataTable dataHeader, BoObjectTypes boObjectType, bool IsObjectDraft, out string newCode, out string message)
        {
            message = "";
            newCode = "";

            try
            {

                string docEntry = "";
                object value;

                if (dataHeader == null || dataHeader.Rows.Count == 0)
                {
                    message = "Not found data in proccess.";
                    return false;
                }

                DataRow rowheader = dataHeader.Rows[0];

                oCompany = Company_SAP(out message);

                if (string.IsNullOrEmpty(message) == false)
                {
                    return false;
                }

                if (IsObjectDraft)
                    oDocument = oCompany.GetBusinessObject(BoObjectTypes.oDrafts);
                else
                    oDocument = oCompany.GetBusinessObject(boObjectType);

                if (dataHeader.Columns.Contains("DocEntry") == false)
                {
                    message = "Dữ liệu không hợp lê. Không tìm thấy thông tin sapb1 number.";
                    return false;
                }

                docEntry = rowheader["DocEntry"].ToString();

                if (oDocument.GetByKey(Convert.ToInt32(docEntry)) == false)
                {
                    message = "Không tìm thấy chứng từ trên sapb1. Vui lòng kiểm tra lại thông tin.";
                    return false;
                }


                oError = oDocument.Remove();

                if (oError == 0)
                {
                    newCode = docEntry;
                    return true;
                }
                else
                {
                    oCompany.GetLastError(out oError, out message);
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("Close Document", ex.Message);
            }

            return false;

        }

        #endregion

        string parseStarttime(string timeFrame)
        {
            if (timeFrame == "08-12")
            {
                return "0800";
            }
            else if (timeFrame == "12-14")
            {
                return "1200";
            }
            else if (timeFrame == "13-17")
            {
                return "1300";
            }
            else if (timeFrame == "'18-22")
            {
                return "1800";
            }
            else if (timeFrame == "23-07")
            {
                return "2300";
            }

            return "0000";
        }

        string parsEndtime(string timeFrame)
        {
            if (timeFrame == "08-12")
            {
                return "1200";
            }
            else if (timeFrame == "12-14")
            {
                return "1400";
            }
            else if (timeFrame == "13-17")
            {
                return "1700";
            }
            else if (timeFrame == "'18-22")
            {
                return "2200";
            }
            else if (timeFrame == "23-07")
            {
                return "0700";
            }

            return "0000";
        }

        #region UDF VALUE
        private void Item_UDF(Items oItem, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oItem.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void Projects_UDF(Project oProject, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oProject.UserFields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oProject.UserFields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oProject.UserFields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oProject.UserFields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oProject.UserFields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oProject.UserFields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oProject.UserFields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oProject.UserFields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void BP_UDF(BusinessPartners oBP, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oBP.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void COA_UDF(ChartOfAccounts oChartOfAccount, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oChartOfAccount.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void Document_UDF(Documents oDocument, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void Document_Lines_UDF(Document_Lines oDocumentLine, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void StockTransfer_UDF(StockTransfer oDocument, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oDocument.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void StockTransfer_Lines_UDF(StockTransfer_Lines oDocumentLine, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oDocumentLine.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void InventoryCounting_UDF(InventoryCounting oDocument, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oDocument.UserFields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oDocument.UserFields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oDocument.UserFields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oDocument.UserFields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oDocument.UserFields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oDocument.UserFields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oDocument.UserFields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oDocument.UserFields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void InventoryCounting_Lines_UDF(InventoryCountingLine oDocumentLine, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void InventoryPosting_UDF(InventoryPosting oDocument, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oDocument.UserFields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oDocument.UserFields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oDocument.UserFields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oDocument.UserFields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oDocument.UserFields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oDocument.UserFields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oDocument.UserFields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oDocument.UserFields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void InventoryPosting_Lines_UDF(InventoryPostingLine oDocumentLine, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oDocumentLine.UserFields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void Payment_UDF(Payments oPayment, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oPayment.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void JE_UDF(JournalEntries oJE, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oJE.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void JV_UDF(IJournalVouchers oJV, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oJV.JournalEntries.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }

        private void JV_JE_LINES_UDF(JournalEntries_Lines oJVLine, DataColumn column, object value)
        {
            if (column.ColumnName.StartsWith("U_") && value != null)
            {
                if (column.DataType == typeof(DateTime))
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = (DateTime)value;
                else if (column.DataType == typeof(int))
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = (int)value;
                else if (column.DataType == typeof(short))
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = (short)value;
                else if (column.DataType == typeof(double))
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = (double)value;
                else if (column.DataType == typeof(float))
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = (double)(float)value;
                else if (column.DataType == typeof(decimal))
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = (double)(decimal)value;
                else if (value.ToString().Length == 5 && value.ToString().Substring(2, 1) == ":")    //HOUR 00:06
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = Convert.ToDateTime(value);
                else
                    oJVLine.UserFields.Fields.Item(column.ColumnName).Value = value.ToString();
            }
        }
        #endregion

        #region TGNH

        public void UpdateArtical(DataTools.Models.TGNH.Artical data, out string message)
        {
            message = "";
            try
            {
                oCompany = Company_SAP(out message);
                oItem = oCompany.GetBusinessObject(BoObjectTypes.oItems);
                DataColumn column = new DataColumn();
                column.ColumnName = "U_Notes_Group";
                oItem.GetByKey("I00000002");
                Item_UDF(oItem, column, "test1");

                oItem.Update();
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
        }

        #endregion

        #region WMS
        public enum TransactionTpye
        {
            /// <summary>
            /// Nhập mua từ NCC
            /// Cho phép Hủy
            /// </summary>
            GR_PO,
            /// <summary>
            /// Nhập mua nội bộ (HM->HY)
            /// Cho phép Hủy
            /// </summary>
            GR_TR,
            /// <summary>
            /// Nhập NVL từ kho SX
            /// Cho phép Hủy
            /// </summary>
            GR_PR1,
            /// <summary>
            /// Nhập TP từ kho SX
            /// Cho phép Hủy
            /// </summary>
            GR_PR,
            /// <summary>
            /// Nhập trả từ Market
            /// </summary>
            GR_RT_SO,
            /// <summary>
            /// Nhập trả từ NPP
            /// </summary>
            GR_RT_SOA,
            /// <summary>
            /// Nhập khác
            /// </summary>
            GR_OTH,
            /// <summary>
            /// Xuất bán cho NPP, Martket
            /// Xuất cho biếu tặng
            /// Cho phép Hủy
            /// </summary>
            GI_SO,
            /// <summary>
            /// Xuất NVL cho sản xuất
            /// Cho phép Hủy
            /// </summary>
            GI_NVL,
            /// <summary>
            /// Xuất khác
            /// </summary>
            GI_OTH,
            /// <summary>
            /// Xuất trả NCC, Chưa có hóa đơn
            /// </summary>
            GI_RT,
            /// <summary>
            /// Xuất trả NCC, Đã có hóa đơn
            /// </summary>
            GI_RT_CR,
            /// <summary>
            /// Xuất bán nội bộ (HY->HM)
            /// Cho phép Hủy
            /// </summary>
            GI_TR,
            /// <summary>
            /// Kiểm kê
            /// </summary>
            INV_COUNT,
            /// <summary>
            /// Chuyển kho giữa các SLOC
            /// Cho phép Hủy
            /// </summary>
            INV_TRFS,
        }

        public bool CreateDocument_WMS(string TransType, DataTable dataHeader, DataTable dataLines, bool IsObjectDraft, out string newCode, out string message)
        {
            bool isRes = false;

            message = "";
            newCode = "";

            StringWriter sw = new StringWriter();
            string sqllog = "";

            try
            {
                if (string.IsNullOrEmpty(TransType))
                {
                    message = "Not found information transaction type . Please check again.";
                    return false;
                }
                if (dataHeader == null || dataHeader.Rows.Count == 0)
                {
                    message = "Not found data in proccess.";
                    return false;
                }
                if (dataLines == null || dataLines.Rows.Count == 0)
                {
                    message = "Not found any items line.";
                    return false;
                }

                dataHeader.TableName = "Document";
                dataHeader.WriteXml(sw);

                sqllog = string.Format("INSERT INTO [@ICC_WMS_LOG] VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}')"
                    , TransType.ToUpper(), "PROCCESS", "", "", DateTime.Now.ToString(), sw.ToString(), ""
                    );
                DoQuery(sqllog, out oErrorMessage);

                if (TransType.ToUpper() == TransactionTpye.GR_PO.ToString()
                    || TransType.ToUpper() == TransactionTpye.GR_TR.ToString()
                    || TransType.ToUpper() == TransactionTpye.GI_RT.ToString()
                    || TransType.ToUpper() == TransactionTpye.GI_RT_CR.ToString()
                    )
                {
                    if (dataLines != null && dataLines.Rows.Count > 0)
                    {
                        foreach (DataRow item in dataLines.Rows)
                        {
                            var rs = DoQuery(string.Format("select CASE WHEN ISNULL(U_Cuon2Met,0)>0 THEN U_Cuon2Met ELSE NumInBuy END NumInBuy from oitm with(nolock) where itemcode='{0}'", item["ItemCode"]), out message);
                            if (rs.RecordCount > 0)
                            {
                                item["Quantity"] = Convert.ToDouble(item["Quantity"]) / rs.Fields.Item("NumInBuy").Value;
                            }
                            else
                            {
                                message = message + " Không tìm thấy mã hàng hóa.";
                                isRes = false;
                                goto WriteLog;
                            }
                        }
                    }
                }
                else if (TransType.ToUpper() == TransactionTpye.GI_SO.ToString()
                    || TransType.ToUpper() == TransactionTpye.GI_TR.ToString()
                    || TransType.ToUpper() == TransactionTpye.GR_RT_SO.ToString()
                    || TransType.ToUpper() == TransactionTpye.GR_RT_SOA.ToString()
                    )
                {
                    if (dataLines != null && dataLines.Rows.Count > 0)
                    {
                        foreach (DataRow item in dataLines.Rows)
                        {
                            var rs = DoQuery(string.Format("select CASE WHEN ISNULL(U_Cuon2Met,0)>0 THEN U_Cuon2Met ELSE NumInSale END NumInSale from oitm with(nolock) where itemcode='{0}'", item["ItemCode"]), out message);
                            if (rs.RecordCount > 0)
                            {
                                item["Quantity"] = Convert.ToDouble(item["Quantity"]) / rs.Fields.Item("NumInSale").Value;
                            }
                            else
                            {
                                message = message + " Không tìm thấy mã hàng hóa.";
                                isRes = false;
                                goto WriteLog;
                            }
                        }
                    }
                }
                else
                {
                    if (dataLines != null && dataLines.Rows.Count > 0)
                    {
                        foreach (DataRow item in dataLines.Rows)
                        {
                            var rs = DoQuery(string.Format("select CASE WHEN ISNULL(U_Cuon2Met,0)>0 THEN U_Cuon2Met ELSE 1 END U_Cuon2Met from oitm with(nolock) where itemcode='{0}'", item["ItemCode"]), out message);
                            if (rs.RecordCount > 0)
                            {
                                item["Quantity"] = Convert.ToDouble(item["Quantity"]) / rs.Fields.Item("U_Cuon2Met").Value;
                            }
                            else
                            {
                                message = message + " Không tìm thấy mã hàng hóa.";
                                isRes = false;
                                goto WriteLog;
                            }
                        }
                    }
                }

                //U_FromWMS
                dataHeader.Columns.Add(new DataColumn() { ColumnName = "U_FromWMS", DataType = typeof(string) });
                dataHeader.Rows[0]["U_FromWMS"] = "Y";
                dataLines.Columns.Add(new DataColumn() { ColumnName = "U_FromWMS", DataType = typeof(string) });
                foreach (DataRow item in dataLines.Rows)
                {
                    item["U_FromWMS"] = "Y";
                }

                //WMS Number
                if (dataHeader.Columns.Contains("WMSNum"))
                {
                    dataHeader.Columns.Add(new DataColumn() { ColumnName = "U_WMSNum", DataType = typeof(string) });
                    dataHeader.Rows[0]["U_WMSNum"] = dataHeader.Rows[0]["WMSNum"];

                    dataLines.Columns.Add(new DataColumn() { ColumnName = "U_WMSNum", DataType = typeof(string) });
                    foreach (DataRow item in dataLines.Rows)
                    {
                        item["U_WMSNum"] = dataHeader.Rows[0]["WMSNum"];
                    }
                }

                //Chuyển kho
                if (TransType.ToUpper() == TransactionTpye.INV_TRFS.ToString())
                {
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "UseBaseUn", DataType = typeof(string) });
                    foreach (DataRow item in dataLines.Rows)
                    {
                        item["UseBaseUn"] = "Y";
                    }

                    //Chuyển kho
                    isRes = CreateStockTransfer_OnlyOne(dataHeader, dataLines, false, out newCode, out message);
                }
                //Nhập trả từ NPP, Market-->trả 1 phần
                else if (TransType.ToUpper() == TransactionTpye.GR_RT_SO.ToString())
                {
                    //Nhập trả từ NPP, Market
                    // Return_Market
                    // Tạo AR Credit Memo - Draft dựa vào số hóa đơn
                    // Không cho sửa số lượng
                    // Return_NPP
                    // Kiểm tra số hóa đơn xem đã có tạo AR Credit Memo(hoặc Draft) chưa
                    // - Nếu chưa báo lỗi
                    // - Nếu có thì cập nhật tình trạng trên SAPB1 để nhận diện Kho đã confirm

                    string cardCode = dataHeader.Rows[0]["CardCode"].ToString();
                    string sql = string.Format("select 1 from OCRD where CardCode='{0}' and GroupCode=100", cardCode);
                    var record = DoQuery(sql, out message);
                    if (string.IsNullOrEmpty(message) == false)
                    {
                        isRes = false;
                    }
                    else if (record.RecordCount > 0)
                    {
                        //Tra tu NPP
                        string invoiceNo = dataHeader.Rows[0]["InvoiceNo"].ToString();
                        sql = string.Format("select DocEntry,U_PPONum from ODRF where CardCode='{0}' and NumAtCard='{1}' and ObjType='14'", cardCode, invoiceNo);
                        record = DoQuery(sql, out message);
                        if (record.RecordCount == 0)
                        {
                            message = "Không tìm thấy đơn trả từ nhà phân phối. Vui lòng kiểm tra lại.";
                            isRes = false;
                        }
                        else
                        {
                            int docEntry = record.Fields.Item("DocEntry").Value;

                            string pponum = record.Fields.Item("U_PPONum").Value;

                            dataHeader.Columns.Add(new DataColumn() { ColumnName = "U_PPONum", DataType = typeof(string) });
                            dataHeader.Columns.Add(new DataColumn() { ColumnName = "NumAtCard", DataType = typeof(string) });

                            dataHeader.Rows[0]["DocEntry"] = docEntry;
                            dataHeader.Rows[0]["NumAtCard"] = invoiceNo;
                            dataHeader.Rows[0]["U_PPONum"] = pponum;

                            dataLines.Columns.Add(new DataColumn() { ColumnName = "U_PPONum", DataType = typeof(string) });

                            foreach (DataRow item in dataLines.Rows)
                            {
                                item["U_PPONum"] = pponum;
                            }

                            isRes = (UpdateDocument_OnlyOne(dataHeader, dataLines, BoObjectTypes.oCreditNotes, true, out newCode, out message));
                        }

                    }
                    else
                    {
                        //Tra tu Market
                        string invoiceNo = dataHeader.Rows[0]["InvoiceNo"].ToString();
                        sql = string.Format("select DocEntry from OINV where CardCode='{0}' and NumAtCard='{1}'", cardCode, invoiceNo);
                        record = DoQuery(sql, out message);
                        if (record.RecordCount == 0)
                        {
                            message = "Không tìm thấy hóa đơn gốc. Vui lòng kiểm tra lại.";
                            isRes = false;
                        }
                        else
                        {
                            isRes = CreateDocument_OnlyOne(dataHeader, dataLines, BoObjectTypes.oCreditNotes, true, out newCode, out message);
                        }
                    }
                }
                //Nhập trả từ NPP, Market-->Nguyên hóa đơn
                else if (TransType.ToUpper() == TransactionTpye.GR_RT_SOA.ToString())
                {
                    //Nhập trả từ NPP, Market
                    // Return_Market
                    // Tạo AR Credit Memo - Draft dựa vào số hóa đơn
                    // Không cho sửa số lượng
                    // Return_NPP
                    // Kiểm tra số hóa đơn xem đã có tạo AR Credit Memo(hoặc Draft) chưa
                    // - Nếu chưa báo lỗi
                    // - Nếu có thì cập nhật tình trạng trên SAPB1 để nhận diện Kho đã confirm

                    string cardCode = dataHeader.Rows[0]["CardCode"].ToString();
                    string invoiceNo = dataHeader.Rows[0]["InvoiceNo"].ToString();
                    var sql = string.Format("select DocEntry from OINV where CardCode='{0}' and NumAtCard='{1}'", cardCode, invoiceNo);
                    var record = DoQuery(sql, out message);
                    if (record.RecordCount == 0)
                    {
                        message = "Không tìm thấy hóa đơn gốc. Vui lòng kiểm tra lại.";
                        isRes = false;
                    }
                    else
                    {
                        var docEntry = record.Fields.Item("DocEntry").Value;
                        var wmsNum = dataHeader.Rows[0]["WMSNum"];

                        //Check Quantity
                        var dtLines1 = dataLines.DefaultView.ToTable(true, new string[] { "ItemCode" });
                        foreach (DataRow row in dtLines1.Rows)
                        {
                            var qty1 = dataLines.Compute("Sum(Quantity)", string.Format("ItemCode='{0}'", row["ItemCode"]));
                            var sql1 = string.Format("select SUM(Quantity) from INV1 where docEntry={0} and ItemCode='{1}'", docEntry, row["ItemCode"]);
                            var record1 = DoQuery(sql1, out message);
                            if (record1.RecordCount == 0 || record1.Fields.Item(0).Value != Convert.ToDouble(qty1))
                            {
                                message = string.Format("Số lượng trả không đúng so với hóa đơn gốc. Vui lòng kiểm tra lại. {0},{1},{2},{3}", docEntry, row["ItemCode"], qty1, record1.Fields.Item(0).Value);
                                isRes = false;
                                goto WriteLog;
                            }
                        }
                        //End Check Quantity

                        isRes = CopyDocument_OnlyOne(dataHeader, docEntry, BoObjectTypes.oInvoices, BoObjectTypes.oCreditNotes, true, out newCode, out message, wmsNum);
                    }
                }
                else if (TransType.ToUpper() == TransactionTpye.GR_TR.ToString())
                {
                    //AP-Nhập mua nội bộ

                    //So Phieu
                    var sophieu = dataHeader.Rows[0]["InvoiceSN"].ToString();
                    if (string.IsNullOrEmpty(sophieu))
                    {
                        isRes = false;
                        message = "Vui lòng nhập số phiếu kiêm VCNB.";
                    }
                    else
                    {
                        dataHeader.Columns.Add(new DataColumn() { ColumnName = "U_SoPhieu", DataType = typeof(string) });
                        dataHeader.Columns.Add(new DataColumn() { ColumnName = "NumAtCard", DataType = typeof(string) });
                        dataHeader.Rows[0]["NumAtCard"] = sophieu;
                        dataHeader.Rows[0]["U_SoPhieu"] = dataHeader.Rows[0]["InvoiceSN"];

                        //Tính Giá
                        dataLines.Columns.Add(new DataColumn() { ColumnName = "Price", DataType = typeof(decimal) });

                        foreach (DataRow item in dataLines.Rows)
                        {
                            string sql = string.Format("ICC_US_API_GET_PRICE_NHAPNOIBO @NumAtCard='{0}',@ItemCode='{1}'"
                            , sophieu
                            , item["ItemCode"]
                            );
                            var rs = DoQuery(sql, out message);
                            if (string.IsNullOrEmpty(message))
                            {
                                if (rs.RecordCount > 0)
                                {
                                    item["Price"] = rs.Fields.Item("PriceBefDi").Value;
                                }
                                else
                                {
                                    message = "Không tìm thấy mã hàng trong chứng từ xuất VCNB.";
                                    isRes = false;
                                    goto WriteLog;
                                }
                            }
                            else
                            {
                                isRes = false;
                                goto WriteLog;
                            }

                        }

                        isRes = CreateDocument_OnlyOne(dataHeader, dataLines, BoObjectTypes.oPurchaseInvoices, false, out newCode, out message);
                    }
                }
                else if (TransType.ToUpper() == TransactionTpye.GR_PR1.ToString())
                {
                    //From Warehouse
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "FromWhsCode", DataType = typeof(string) });
                    //To Warehouse
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "ToWhsCode", DataType = typeof(string) });
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "UseBaseUn", DataType = typeof(string) });
                    foreach (DataRow item in dataLines.Rows)
                    {
                        item["FromWhsCode"] = item["Location"];
                        item["ToWhsCode"] = item["WhsCode"];
                        item["UseBaseUn"] = "Y";
                    }

                    //Chuyển kho NVL từ SX về kho NVL
                    isRes = CreateStockTransfer_OnlyOne(dataHeader, dataLines, false, out newCode, out message);
                }
                else if (TransType.ToUpper() == TransactionTpye.GR_PO.ToString())
                {
                    //Số xe, Tên tài xế
                    dataHeader.Columns.Add(new DataColumn() { ColumnName = "U_SoXe", DataType = typeof(string) });
                    dataHeader.Columns.Add(new DataColumn() { ColumnName = "U_TenTaiXe", DataType = typeof(string) });
                    dataHeader.Rows[0]["U_SoXe"] = dataHeader.Rows[0]["CarNo"];
                    dataHeader.Rows[0]["U_TenTaiXe"] = dataHeader.Rows[0]["DriverName"];

                    //GRPO
                    isRes = CreateDocument_OnlyOne(dataHeader, dataLines, BoObjectTypes.oPurchaseDeliveryNotes, IsObjectDraft, out newCode, out message, 22);
                }
                else if (TransType.ToUpper() == TransactionTpye.GR_PR.ToString())
                {
                    //From Warehouse
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "FromWhsCode", DataType = typeof(string) });
                    //To Warehouse
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "ToWhsCode", DataType = typeof(string) });
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "UseBaseUn", DataType = typeof(string) });
                    foreach (DataRow item in dataLines.Rows)
                    {
                        item["FromWhsCode"] = item["Location"];
                        item["ToWhsCode"] = item["WhsCode"];
                        item["UseBaseUn"] = "Y";
                    }

                    //Chuyển kho TP từ SX về kho TP
                    isRes = CreateStockTransfer_OnlyOne(dataHeader, dataLines, false, out newCode, out message);
                }
                else if (TransType.ToUpper() == TransactionTpye.GR_OTH.ToString())
                {
                    if (string.IsNullOrEmpty(dataHeader.Rows[0]["Operation"] as string))
                    {
                        message = "Require information of operation. Please check again.";
                    }
                    else
                    {
                        var reason = dataHeader.Rows[0]["Operation"];

                        //Ly do nhap khac
                        dataLines.Columns.Add(new DataColumn() { ColumnName = "U_Reason", DataType = typeof(string) });
                        dataLines.Columns.Add(new DataColumn() { ColumnName = "UseBaseUn", DataType = typeof(string) });
                        foreach (DataRow item in dataLines.Rows)
                        {
                            item["U_Reason"] = reason; //"20303"
                            item["UseBaseUn"] = "Y";
                        }

                        dataLines.Columns.Add(new DataColumn() { ColumnName = "AcctCode", DataType = typeof(string) });
                        foreach (DataRow item in dataLines.Rows)
                        {
                            string sqlReason = string.Format("SELECT U_AcctCode FROM[@ICC_REASON] WHERE Code = N'{0}'", reason);
                            var rs = DoQuery(sqlReason, out message);
                            if (string.IsNullOrEmpty(message))
                            {
                                if (rs.RecordCount > 0)
                                {
                                    item["AcctCode"] = rs.Fields.Item(0).Value; ;
                                }
                                else
                                {
                                    message = "Không tìm thấy tài khoản theo lý do.";
                                    isRes = false;
                                    goto WriteLog;

                                }
                            }
                            else
                            {
                                isRes = false;
                                goto WriteLog;

                            }
                        }

                        //Goods Receipt
                        isRes = CreateDocument_OnlyOne(dataHeader, dataLines, BoObjectTypes.oInventoryGenEntry, false, out newCode, out message);
                    }
                }
                else if (TransType.ToUpper() == TransactionTpye.GI_SO.ToString())
                {
                    //OcrCode3
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "OcrCode3", DataType = typeof(string) });
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "Price", DataType = typeof(decimal) });
                    //Dscription
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "Dscription", DataType = typeof(string) });
                    foreach (DataRow item in dataLines.Rows)
                    {
                        string programCode = item["ProgramCode"] == null ? "" : item["ProgramCode"].ToString();
                        item["OcrCode3"] = programCode;
                        if (string.IsNullOrEmpty(programCode) == false)
                        {
                            item["Price"] = 0;
                        }
                    }

                    //NCCVanChuyen
                    dataHeader.Columns.Add(new DataColumn() { ColumnName = "U_NCCVanChuyen", DataType = typeof(string) });
                    dataHeader.Columns.Add(new DataColumn() { ColumnName = "U_SoXe", DataType = typeof(string) });
                    dataHeader.Columns.Add(new DataColumn() { ColumnName = "U_TenTaiXe", DataType = typeof(string) });
                    dataHeader.Columns.Add(new DataColumn() { ColumnName = "U_DienThoaiTaiXe", DataType = typeof(string) });
                    dataHeader.Rows[0]["U_NCCVanChuyen"] = dataHeader.Rows[0]["BPAddress"];
                    dataHeader.Rows[0]["U_SoXe"] = dataHeader.Rows[0]["CarNo"];
                    dataHeader.Rows[0]["U_TenTaiXe"] = dataHeader.Rows[0]["DriverName"];
                    dataHeader.Rows[0]["U_DienThoaiTaiXe"] = dataHeader.Rows[0]["DriverPhone"];

                    //Add Item 99xxx from Draft
                    string sql = string.Format("select x2.ItemCode, x2.Dscription, x2.Quantity, x2.Price, x2.WhsCode, x2.BaseEntry, x2.BaseLine, x2.VisOrder from odrf x1 with(nolock) join drf1 x2 with(nolock) on x1.DocEntry=x2.DocEntry where x1.ObjType=15 and x2.ItemCode like '9%' and x1.DocEntry={0} and x1.CardCode='{1}'"
                           , dataHeader.Rows[0]["DocEntry"]
                           , dataHeader.Rows[0]["CardCode"]
                           );
                    var rs = DoQuery(sql, out message);
                    if (string.IsNullOrEmpty(message))
                    {
                        if (rs.RecordCount > 0)
                        {
                            while (rs.EoF == false)
                            {
                                var row9 = dataLines.NewRow();
                                row9["ItemCode"] = rs.Fields.Item("ItemCode").Value;
                                row9["Dscription"] = rs.Fields.Item("Dscription").Value;
                                row9["Quantity"] = rs.Fields.Item("Quantity").Value;
                                row9["Price"] = rs.Fields.Item("Price").Value;
                                row9["WhsCode"] = rs.Fields.Item("WhsCode").Value;
                                row9["BaseEntry"] = rs.Fields.Item("BaseEntry").Value;
                                row9["BaseLine"] = rs.Fields.Item("BaseLine").Value;

                                dataLines.Rows.InsertAt(row9, rs.Fields.Item("VisOrder").Value);


                                rs.MoveNext();
                            }
                        }


                        //Create Delievy
                        isRes = CreateDocument_OnlyOne(dataHeader, dataLines, BoObjectTypes.oDeliveryNotes, false, out newCode, out message, 17);

                        if (isRes)
                        {
                            try
                            {
                                //Close Draft --> trường hợp close lỗi user tự close manual.
                                string sql2 = string.Format("update odrf set DocStatus='C' where ObjType=15 and DocEntry={0} and CardCode='{1}'"
                                    , dataHeader.Rows[0]["DocEntry"]
                                    , dataHeader.Rows[0]["CardCode"]
                                    );
                                string message1;
                                var rs2 = DoQuery(sql2, out message1);
                            }
                            catch { }
                        }

                        ////Delivery
                        //StartSapTransaction();
                        //try
                        //{
                        //    //Close Draft
                        //    string sql2 = string.Format("update odrf set DocStatus='C' where ObjType=15 and DocEntry={0} and CardCode='{1}'"
                        //        , dataHeader.Rows[0]["DocEntry"]
                        //        , dataHeader.Rows[0]["CardCode"]
                        //        );
                        //    var rs2 = DoQuery(sql2, out message);

                        //    //Create Delievy
                        //    isRes = CreateDocument_OnlyOne(dataHeader, dataLines, BoObjectTypes.oDeliveryNotes, false, out newCode, out message, 17);

                        //    if (isRes)
                        //    {
                        //        isRes = CommitSapTransaction(out message);
                        //    }
                        //    else
                        //    {
                        //        isRes = false;
                        //        RollbackSapTransaction();
                        //    }

                        //}
                        //catch (Exception ex)
                        //{
                        //    isRes = false;
                        //    message = ex.Message;
                        //    RollbackSapTransaction();
                        //}

                    }
                    else
                    {
                        isRes = false;
                    }

                }
                else if (TransType.ToUpper() == TransactionTpye.GI_RT.ToString())
                {
                    //Goods Return base on GRPO
                    //DocEntry Header là số của GRPO
                    //BaseLine và BaseEntry của PO

                    var docEntry = dataHeader.Rows[0]["DocEntry"];
                    foreach (DataRow item in dataLines.Rows)
                    {
                        var baseEntry = item["BaseEntry"];
                        var baseLine = item["BaseLine"];
                        string sqlBase = string.Format("SELECT LineNum FROM PDN1 with(nolock) WHERE DocEntry = {0} AND BaseEntry={1} AND BaseLine={2}", docEntry, baseEntry, baseLine);
                        var rs = DoQuery(sqlBase, out message);
                        if (string.IsNullOrEmpty(message))
                        {
                            if (rs.RecordCount > 0)
                            {
                                item["BaseEntry"] = docEntry;
                                item["BaseLine"] = rs.Fields.Item(0).Value;
                            }
                            else
                            {
                                message = "Không tìm thấy chứng từ gốc.";
                                isRes = false;
                                goto WriteLog;

                            }
                        }
                        else
                        {
                            isRes = false;
                            goto WriteLog;

                        }
                    }

                    isRes = CreateDocument_OnlyOne(dataHeader, dataLines, BoObjectTypes.oPurchaseReturns, false, out newCode, out message, 20);
                }
                else if (TransType.ToUpper() == TransactionTpye.GI_RT_CR.ToString())
                {
                    //AP Credit Memo - Draft
                    isRes = CreateDocument_OnlyOne(dataHeader, dataLines, BoObjectTypes.oPurchaseCreditNotes, true, out newCode, out message);
                }
                else if (TransType.ToUpper() == TransactionTpye.GI_TR.ToString())
                {
                    //AR-Xuất bán nội bộ

                    //So Phieu
                    dataHeader.Columns.Add(new DataColumn() { ColumnName = "U_SoPhieu", DataType = typeof(string) });
                    dataHeader.Columns.Add(new DataColumn() { ColumnName = "NumAtCard", DataType = typeof(string) });
                    dataHeader.Rows[0]["U_SoPhieu"] = dataHeader.Rows[0]["InvoiceSN"];
                    dataHeader.Rows[0]["NumAtCard"] = dataHeader.Rows[0]["InvoiceSN"];

                    //Tính Giá
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "Price", DataType = typeof(decimal) });
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "AcctCode", DataType = typeof(string) });
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "CogsAcct", DataType = typeof(string) });
                    foreach (DataRow item in dataLines.Rows)
                    {
                        item["AcctCode"] = "51180400";  //DT bán hàng nội bộ
                        item["CogsAcct"] = "63280500";  //Giá vốn bán hàng nội bộ

                        string itemCode = item["ItemCode"].ToString();
                        string sql = string.Format("ICC_US_VCNB_PRICE_BY_WHS @itemcode='{0}',@cardcode='{1}',@whscode='{2}'"
                            , item["ItemCode"].ToString()
                            , dataHeader.Rows[0]["CardCode"].ToString()
                            , item["WhsCode"].ToString()
                            );
                        var rs = DoQuery(sql, out message);
                        if (string.IsNullOrEmpty(message))
                        {
                            if (rs.RecordCount > 0)
                            {
                                item["Price"] = rs.Fields.Item(0).Value;
                            }
                            else
                            {
                                message = "Không tìm thấy giá VCNB cho mã hàng " + itemCode;
                                isRes = false;
                                goto WriteLog;

                            }
                        }
                        else
                        {
                            isRes = false;
                            goto WriteLog;

                        }
                    }

                    isRes = CreateDocument_OnlyOne(dataHeader, dataLines, BoObjectTypes.oInvoices, false, out newCode, out message);

                }
                else if (TransType.ToUpper() == TransactionTpye.GI_NVL.ToString())
                {
                    //From Warehouse
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "FromWhsCode", DataType = typeof(string) });
                    //To Warehouse
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "ToWhsCode", DataType = typeof(string) });
                    dataLines.Columns.Add(new DataColumn() { ColumnName = "UseBaseUn", DataType = typeof(string) });
                    foreach (DataRow item in dataLines.Rows)
                    {
                        item["FromWhsCode"] = item["WhsCode"];
                        item["ToWhsCode"] = item["Location"];
                        item["UseBaseUn"] = "Y";
                    }

                    //Chuyển kho NVL từ kho NVL đến kho SX
                    isRes = CreateStockTransfer_OnlyOne(dataHeader, dataLines, false, out newCode, out message);
                }
                else if (TransType.ToUpper() == TransactionTpye.GI_OTH.ToString())
                {
                    if (string.IsNullOrEmpty(dataHeader.Rows[0]["Operation"] as string))
                    {
                        message = "Require information of operation. Please check again.";
                        isRes = false;
                    }
                    else
                    {
                        var reason = dataHeader.Rows[0]["Operation"];

                        //Ly do xuat khac
                        dataLines.Columns.Add(new DataColumn() { ColumnName = "U_Reason", DataType = typeof(string) });
                        dataLines.Columns.Add(new DataColumn() { ColumnName = "UseBaseUn", DataType = typeof(string) });
                        foreach (DataRow item in dataLines.Rows)
                        {
                            item["U_Reason"] = reason;
                            item["UseBaseUn"] = "Y";
                        }

                        dataLines.Columns.Add(new DataColumn() { ColumnName = "AcctCode", DataType = typeof(string) });
                        foreach (DataRow item in dataLines.Rows)
                        {
                            string sqlReason = string.Format("SELECT U_AcctCode FROM[@ICC_REASON] WHERE Code = N'{0}'", reason);
                            var rs = DoQuery(sqlReason, out message);
                            if (string.IsNullOrEmpty(message))
                            {
                                if (rs.RecordCount > 0)
                                {
                                    item["AcctCode"] = rs.Fields.Item(0).Value; ;
                                }
                                else
                                {
                                    message = "Không tìm thấy tài khoản theo lý do.";
                                    isRes = false;
                                    goto WriteLog;

                                }
                            }
                            else
                            {
                                isRes = false;
                                goto WriteLog;

                            }

                        }

                        //Goods Issue
                        isRes = CreateDocument_OnlyOne(dataHeader, dataLines, BoObjectTypes.oInventoryGenExit, false, out newCode, out message);

                    }
                }
                else
                {
                    message = string.Format(@"Invalid transaction type ""{0}"". Please check again.", TransType);
                }

            }
            catch (Exception ex)
            {
                message = getFunctionError("CreateDocument", ex.Message);
            }


            WriteLog:

            try
            {
                sqllog = string.Format("INSERT INTO [@ICC_WMS_LOG] VALUES('{0}','{1}','{2}','{3}','{4}','{5}',N'{6}')"
                , TransType.ToUpper(), isRes.ToString().ToUpper(), "", "", DateTime.Now.ToString(), sw.ToString()
                , message
                 );

                DoQuery(sqllog, out oErrorMessage);
            }
            catch (Exception)
            {
                //Continue...
            }


            return isRes;

        }

        public bool CancelDocument_WMS(string TransType, DataTable dataHeader, bool IsObjectDraft, out string newCode, out string message)
        {
            bool isRes = false;
            message = "";
            newCode = "";

            StringWriter sw = new StringWriter();

            string sqllog = "";

            try
            {

                if (string.IsNullOrEmpty(TransType))
                {
                    message = "Not found information transaction type . Please check again.";
                    return false;
                }
                if (dataHeader == null || dataHeader.Rows.Count == 0)
                {
                    message = "Not found data in proccess.";
                    return false;
                }


                dataHeader.TableName = "CancelDocument";
                dataHeader.WriteXml(sw);

                sqllog = string.Format("INSERT INTO [@ICC_WMS_LOG] VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}')"
                   , "CANCEL " + TransType.ToUpper(), "PROCCESS", "", "", DateTime.Now.ToString(), (sw == null ? "" : sw.ToString())
                   , ""
                    );
                DoQuery(sqllog, out oErrorMessage);

                if (TransType.ToUpper() == TransactionTpye.GI_SO.ToString())
                {
                    //Hủy xuất bán
                    isRes = CancelDocument(dataHeader, BoObjectTypes.oDeliveryNotes, false, out newCode, out message);
                }
                else if (TransType.ToUpper() == TransactionTpye.GI_NVL.ToString())
                {
                    //Hủy xuất NVL đi sản xuất
                    isRes = CancelStockTransfer(dataHeader, out newCode, out message);
                }
                else if (TransType.ToUpper() == TransactionTpye.GR_PO.ToString())
                {
                    //Hủy nhập mua
                    isRes = CancelDocument(dataHeader, BoObjectTypes.oPurchaseDeliveryNotes, false, out newCode, out message);
                }
                else if (TransType.ToUpper() == TransactionTpye.GI_TR.ToString())
                {
                    //Hủy xuat ban Noi bo
                    isRes = CancelDocument(dataHeader, BoObjectTypes.oInvoices, false, out newCode, out message);
                }
                else if (TransType.ToUpper() == TransactionTpye.GR_TR.ToString())
                {
                    //Hủy nhap mua Noi bo
                    isRes = CancelDocument(dataHeader, BoObjectTypes.oPurchaseInvoices, false, out newCode, out message);
                }
                //else if (TransType.ToUpper() == TransactionTpye.GI_BT.ToString())
                //{
                //    //Hủy Xuat Cho Bieu Tang
                //    isRes = CancelDocument(dataHeader, BoObjectTypes.oInvoices, false, out newCode, out message);
                //}
                else if (TransType.ToUpper() == TransactionTpye.INV_TRFS.ToString())
                {
                    //Hủy Chuyen Kho
                    isRes = CancelStockTransfer(dataHeader, out newCode, out message);
                }
                else if (TransType.ToUpper() == TransactionTpye.GR_PR1.ToString())
                {
                    //Hủy nhập NVL từ SX trả về
                    isRes = CancelStockTransfer(dataHeader, out newCode, out message);
                }
                else if (TransType.ToUpper() == TransactionTpye.GR_PR.ToString())
                {
                    //Hủy nhập TP từ SX
                    isRes = CancelStockTransfer(dataHeader, out newCode, out message);
                }
                //else if (TransType.ToUpper() == TransactionTpye.GI_RT.ToString())
                //{
                //    //Hủy Xuat tra NPP (Chua co hoa don)
                //    isRes = CancelDocument(dataHeader, BoObjectTypes.oPurchaseReturns, false, out newCode, out message);
                //}
                //else if (TransType.ToUpper() == TransactionTpye.GI_RT_CR.ToString())
                //{
                //    //Hủy Xuat tra NPP (Da co hoa don)
                //    isRes = CancelDocument(dataHeader, BoObjectTypes.oPurchaseCreditNotes, false, out newCode, out message);
                //}
                else
                {
                    message = string.Format(@"Invalid transaction type ""{0}"". Please check again.", TransType);
                }
            }
            catch (Exception ex)
            {
                message = getFunctionError("[Exception] ", ex.Message);
            }

            try
            {
                sqllog = string.Format("INSERT INTO [@ICC_WMS_LOG] VALUES('{0}','{1}','{2}','{3}','{4}','{5}',N'{6}')"
                , "CANCEL " + TransType.ToUpper(), isRes.ToString().ToUpper(), "", "", DateTime.Now.ToString(), sw.ToString()
                , message
                 );

                DoQuery(sqllog, out oErrorMessage);
            }
            catch (Exception)
            {
                //Continue...
            }

            return isRes;
        }

        public Recordset DoQuery(string sql, out string message,string location="")
        {
            message = "";

            if(location!="")
                oCompany = Company_SAP_By_Location(location,out message);
            else
                oCompany = Company_SAP(out message);

            Recordset oRecordSet = oCompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            oRecordSet.DoQuery(sql);
            return oRecordSet;
        }

        public void SaveLog_WMS(string TransType, string xmlData, string status, string message)
        {
            try
            {
                var sqllog = string.Format("INSERT INTO [@ICC_WMS_LOG] VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}')"
                    , TransType.ToUpper(), status, "", "", DateTime.Now.ToString(), xmlData
                    , message
                     );


                DoQuery(sqllog, out oErrorMessage);

            }
            catch (Exception)
            {

            }
        }

        #endregion
    }
}
