﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Data;

using SAPbouiCOM;

namespace DataTools.SAPAccess
{
    public class BaseSAPUI : BaseSAPDI
    {
        protected string message = "";

        protected SAPbobsCOM.Company oCompany = null;
        protected SAPbobsCOM.CompanyService oCmpSrv = null;
        protected SAPbobsCOM.ProfitCentersService oProfitCentersService = null;
        protected SAPbobsCOM.ProfitCenter oProfitCenter = null;
        protected SAPbobsCOM.Documents oDocument = null;
        protected SAPbobsCOM.Payments oPayment = null;
        protected SAPbobsCOM.JournalEntries oJE = null;
        protected SAPbobsCOM.StockTransfer oStockTransfer = null;

        protected SAPbouiCOM.Application SBO_Application;
        protected SAPbouiCOM.Form oForm;
        protected SAPbouiCOM.Form oForm_UDF;
        protected SAPbouiCOM.Item oItem;
        protected SAPbouiCOM.Item oItemRef;
        protected SAPbouiCOM.EditText oEdit;
        protected SAPbouiCOM.Button oButton;
        protected SAPbouiCOM.Matrix oMatrix;
        protected SAPbouiCOM.Folder oFolder;
        protected SAPbouiCOM.Columns oColumns;
        protected SAPbouiCOM.Column oColumn;
        protected SAPbouiCOM.StaticText oStaticText;
        protected SAPbouiCOM.EditText oEditText;
        protected SAPbouiCOM.ComboBox oComboBox;

        protected SAPbobsCOM.Recordset oRecordset;

        //close PO
        protected SAPbobsCOM.ProductionOrders oPO = null;

        private void SetApplication()
        {
            //*******************************************************************
            //// Use an SboGuiApi object to establish connection
            //// with the SAP Business One application and return an
            //// initialized appliction object
            //*******************************************************************

            SAPbouiCOM.SboGuiApi SboGuiApi = default(SAPbouiCOM.SboGuiApi);
            string sConnectionString = null;

            SboGuiApi = new SAPbouiCOM.SboGuiApi();

            //// by following the steps specified above, the following
            //// statment should be suficient for either development or run mode

            sConnectionString = "0030002C0030002C00530041005000420044005F00440061007400650076002C0050004C006F006D0056004900490056";
            if (Environment.GetCommandLineArgs().Length > 1)
                sConnectionString = System.Convert.ToString(Environment.GetCommandLineArgs().GetValue(1));

            //// connect to a running SBO Application

            SboGuiApi.Connect(sConnectionString);

            //// get an initialized application object

            SBO_Application = SboGuiApi.GetApplication(-1);
            //SboGuiApi.GetAppIdFromProcessId(aProcess.Id)
            
        }

        private int SetConnectionContext()
        {

            string sCookie = null;
            string sConnectionContext = null;

            //// First initialize the Company object

            oCompany = new SAPbobsCOM.Company();

            //// Acquire the connection context cookie from the DI API.
            sCookie = oCompany.GetContextCookie();

            //// Retrieve the connection context string from the UI API using the
            //// acquired cookie.
            sConnectionContext = SBO_Application.Company.GetConnectionContext(sCookie);

            //// before setting the SBO Login Context make sure the company is not
            //// connected

            if (oCompany.Connected == true)
            {
                oCompany.Disconnect();
            }

            //// Set the connection context information to the DI API.
            return oCompany.SetSboLoginContext(sConnectionContext);

        }

        private int ConnectToCompany()
        {

            //// Establish the connection to the company database.

            //oCompany.LicenseServer = ((SAPbobsCOM.Company)SBO_Application.Company.GetDICompany()).LicenseServer;

            //oCompany = new SAPbobsCOM.Company();
            //oCompany = (SAPbobsCOM.Company)SBO_Application.Company.GetDICompany();
            //if (oCompany.Connected)
            //    oCompany.Disconnect();
            //oCompany.Server = SBO_Application.Company.ServerName;

            //int i = 0;
            //i = oCompany.Connect();

            int i = 0;
            oCompany = new SAPbobsCOM.Company();
            oCompany = (SAPbobsCOM.Company)SBO_Application.Company.GetDICompany();
            if (oCompany.Connected == false)
                i = oCompany.Connect();

            return i;

        }

        private void TerminateAddon()
        {
            System.Windows.Forms.Application.Exit();
            System.Environment.Exit(0);
        }

        //UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
        private void Class_Init()
        {
            ////*************************************************************
            //// set SBO_Application with an initialized application object
            ////*************************************************************

            SetApplication();

            ////*************************************************************
            //// Set The Connection Context
            ////*************************************************************

            //if (!(SetConnectionContext() == 0))
            //{
            //    SBO_Application.MessageBox("[Addon]Failed setting a connection to DI API", 0, "", "", "");
            //    TerminateAddon();
            //    // Terminating the Add-On Application
            //}


            ////*************************************************************
            //// Connect To The Company Data Base
            ////*************************************************************

            if (!(ConnectToCompany() == 0))
            {
                SBO_Application.MessageBox("[Addon]Failed connecting to the company's db", 0, "", "", "");
                int lErr = 0;
                string sErr = null;
                oCompany.GetLastError(out lErr, out sErr);
                SBO_Application.MessageBox(sErr, 0, "", "", "");

                TerminateAddon();

                // Terminating the Add-On Application
            }
            else
            {
                oRecordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                SBO_Application.SetStatusBarMessage("[Addon]Connect successfully to the company's db", SAPbouiCOM.BoMessageTime.bmt_Short, false);
            }

            Loading();
            // events handled by SBO_Application_AppEvent
            SBO_Application.ProgressBarEvent += new SAPbouiCOM._IApplicationEvents_ProgressBarEventEventHandler(SBO_Application_ProgressBarEvent);
            //// events handled by SBO_Application_ItemEvent
            SBO_Application.FormDataEvent += new SAPbouiCOM._IApplicationEvents_FormDataEventEventHandler(SBO_Application_FormDataEvent);
            // events handled by SBO_Application_MenuEvent
            SBO_Application.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SBO_Application_MenuEvent);
            //// events handled by SBO_Application_ItemEvent
            SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Application_ItemEvent);
            // events handled by SBO_Application_AppEvent
            SBO_Application.AppEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler(SBO_Application_AppEvent);
        }

        public BaseSAPUI()
            : base()
        {
            Class_Init();
        }

        #region Event
        public virtual void Loading()
        {

        }

        public void SBO_Application_AppEvent(SAPbouiCOM.BoAppEventTypes EventType)
        {
            switch (EventType)
            {
                case SAPbouiCOM.BoAppEventTypes.aet_ShutDown:
                case BoAppEventTypes.aet_ServerTerminition:
                case BoAppEventTypes.aet_CompanyChanged:
                    TerminateAddon();
                    break;
            }
        }

        public virtual void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {

            }
            catch (Exception ex)
            {
                SBO_Application.SetStatusBarMessage("[Menu Event]" + ex.Message);
            }
        }

        public virtual void SBO_Application_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {

            }
            catch (Exception ex)
            {
                SBO_Application.SetStatusBarMessage("[Item Event]" + ex.Message);
            }
        }

        public virtual void SBO_Application_FormDataEvent(ref SAPbouiCOM.BusinessObjectInfo pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.FormTypeEx == "149"
                    || pVal.FormTypeEx == "139")
                {
                    //Next, Previous,Last,Firt Record
                    if (pVal.EventType == BoEventTypes.et_FORM_DATA_LOAD && pVal.BeforeAction == false && pVal.ActionSuccess == true)
                    {


                    }
                    if (pVal.ActionSuccess == true && pVal.BeforeAction == false &&
                        (pVal.EventType == BoEventTypes.et_FORM_DATA_ADD || pVal.EventType == BoEventTypes.et_FORM_DATA_UPDATE))
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                SBO_Application.MessageBox("[Form Data Event]" + ex.Message);
            }

        }

        public virtual void SBO_Application_ProgressBarEvent(ref SAPbouiCOM.ProgressBarEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {

            }
            catch (Exception ex)
            {
                SBO_Application.SetStatusBarMessage("[ProgressBar Event]" + ex.Message);
            }

        }
        #endregion

        public string ConvertSapDate(object value)
        {
            try
            {
                var date = Convert.ToDateTime(value);
                return date.ToString("yyyyMMdd");
            }
            catch (Exception)
            {

            }
            return "";
        }

        public string ConvertSapTime(object value)
        {
            try
            {
                var date = Convert.ToDateTime(value);
                return date.ToString("HHmm");
            }
            catch (Exception)
            {

            }
            return "";
        }

        #region Import Excel File
        public string FindFile()
        {
            System.Threading.Thread ShowFolderBrowserThread = null;
            try
            {
                ShowFolderBrowserThread = new System.Threading.Thread(ShowFolderBrowser);
                if (ShowFolderBrowserThread.ThreadState == System.Threading.ThreadState.Unstarted)
                {
                    ShowFolderBrowserThread.SetApartmentState(System.Threading.ApartmentState.STA);
                    ShowFolderBrowserThread.Start();
                }
                else if (ShowFolderBrowserThread.ThreadState == System.Threading.ThreadState.Stopped)
                {
                    ShowFolderBrowserThread.Start();
                    ShowFolderBrowserThread.Join();

                }
                while (ShowFolderBrowserThread.ThreadState == System.Threading.ThreadState.Running)
                {
                    System.Windows.Forms.Application.DoEvents();
                }

                if (!string.IsNullOrEmpty(excelFileName))
                {
                    return excelFileName;
                }
            }
            catch (Exception ex)
            {
                SBO_Application.MessageBox("FileFile" + ex.Message);
            }

            return "";

        }

        public string excelFileName = "";
        public void ShowFolderBrowser()
        {
            System.Security.Permissions.FileIOPermission f = new System.Security.Permissions.FileIOPermission(System.Security.Permissions.PermissionState.None);
            f.AllLocalFiles = System.Security.Permissions.FileIOPermissionAccess.AllAccess;
            System.Diagnostics.Process[] MyProcs = null;
            excelFileName = "";
            System.Windows.Forms.OpenFileDialog OpenFile = new System.Windows.Forms.OpenFileDialog();

            try
            {
                OpenFile.Multiselect = false;
                OpenFile.Filter = "Excel|*.xls;*.xlsx";
                int filterindex = 0;
                try
                {
                    filterindex = 0;
                }
                catch (Exception ex)
                {
                }

                OpenFile.FilterIndex = filterindex;

                OpenFile.RestoreDirectory = true;
                MyProcs = System.Diagnostics.Process.GetProcessesByName("SAP Business One");

                if (MyProcs.Length > 0)
                {
                    System.Windows.Forms.NativeWindow nw = new System.Windows.Forms.NativeWindow();
                    // Lay Handle cua SAP
                    nw.AssignHandle(System.Diagnostics.Process.GetProcessesByName("SAP Business One")[0].MainWindowHandle);


                    System.Windows.Forms.DialogResult ret = OpenFile.ShowDialog(nw);

                    if (ret == System.Windows.Forms.DialogResult.OK)
                    {
                        excelFileName = OpenFile.FileName;
                        OpenFile.Dispose();
                    }
                    else
                    {
                        System.Windows.Forms.Application.ExitThread();
                    }
                }
            }
            catch (Exception ex)
            {
                SBO_Application.StatusBar.SetText(ex.Message);
                excelFileName = "";
            }
            finally
            {
                OpenFile.Dispose();
            }

        }

        System.Data.OleDb.OleDbConnection con;
        public System.Data.DataTable GetDataSQL(string ExcelFileName)
        {
            try
            {
                var ExcelConnectionStr = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';", ExcelFileName);

                con = new System.Data.OleDb.OleDbConnection(ExcelConnectionStr);
                string sheet = "";
                var sheets = GetSheets();
                var os = sheets.Select("table_name not like '%#_FilterDatabase%'");

                if (os != null && os.Length > 0)
                {
                    sheet = os[0]["table_name"].ToString();
                }

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                var sSQL = string.Format("select * from [{0}]", sheet);

                System.Data.OleDb.OleDbCommand ExcelCommand = new System.Data.OleDb.OleDbCommand(sSQL, con);
                System.Data.OleDb.OleDbDataReader Reader = null;
                Reader = ExcelCommand.ExecuteReader();

                System.Data.DataTable dt = new System.Data.DataTable();

                //Dim sSQL = String.Format("select * from [{0}]", sheet)
                //Dim da As System.Data.OleDb.OleDbDataAdapter = New System.Data.OleDb.OleDbDataAdapter(sSQL, con)
                //da.Fill(dt)

                dt.Load(Reader);

                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                con.Close();
                return null;
            }
        }

        public System.Data.DataTable GetSheets()
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                System.Data.DataTable dt = null;
                dt = con.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, null);
                con.Close();
                return dt;
            }
            catch (Exception ex)
            {
                SBO_Application.SetStatusBarMessage(ex.Message, SAPbouiCOM.BoMessageTime.bmt_Short, true);
                return null;
            }
        }

        private bool IsDate(object text)
        {
            try
            {
                var t = text.ToString().Trim();

                if (t.ToString().Length == 8)
                {
                    t = t.Substring(0, 4) + "-" + t.Substring(4, 2) + "-" + t.Substring(6, 2);
                }
                else if (t.ToString().Length > 10)
                {
                    t = t.Substring(0, 10);
                }

                var d = Convert.ToDateTime(t);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}
